import os
import sys
from datetime import datetime
import warnings
from sqlalchemy import and_
import pandas as pd
import numpy as np

warnings.simplefilter(action="ignore")
sys.path.append(os.getcwd())

from pluto import api
from pluto.orm import *
from pluto.wind import *
from pluto.core import FactorBase

from config import engine_wind


class LCAP(FactorBase):
    """
    Log of total market capitalization
    """
    __FACTOR_NAME__ = 'LCAP'
    __DESCRIPTION__ = 'Log of total market capitalization'

    def __init__(self):
        super().__init__()

    def _calculate(self, sid: str, date: str) -> FactorsValue:
        """

        Parameters
        ----------
        sid
        date

        Returns
        -------

        """
        # 总市值
        cap = api.session.query(ASHAREEODDERIVATIVEINDICATOR.S_VAL_MV).\
            filter(and_(ASHAREEODDERIVATIVEINDICATOR.S_INFO_WINDCODE == sid,
                        ASHAREEODDERIVATIVEINDICATOR.TRADE_DT == date)).first()
        if cap is not None:
            cap = cap[0]
            if cap is not None and cap > 0:
                fv = FactorsValue()
                fv.gen_time = datetime.now()
                fv.update_time = datetime.now()

                fv.sid = sid
                fv.date = datetime.strptime(date, '%Y%m%d')
                fv.fid = self._fid
                fv.value = cap.ln()
                return fv

    def _fast_cal(self, sid: str):
        # 不能包含今天
        sql = """
            select S_INFO_WINDCODE as sid, TRADE_DT as date, S_VAL_MV as value, now() as gen_time, now() as update_time
            from ASHAREEODDERIVATIVEINDICATOR
            where S_INFO_WINDCODE = '{0}' and TRADE_DT < '{1}'
            order by TRADE_DT
        """.format(sid, datetime.now().strftime("%Y%m%d"))

        df = pd.read_sql(sql, engine_wind)

        df['value'] = np.log(df['value'])
        df['date'] = pd.to_datetime(df['date'])
        df['fid'] = self._fid

        return df


if __name__ == '__main__':
    from logger import Logger

    logger = Logger(
        logger_name='pluto',
        file_path='./log/factor_{0}.log'.format(LCAP.__FACTOR_NAME__)
    )

    logger.info(
        "------------------------------------ START ------------------------------------")
    lcap = LCAP()
    lcap.update()

    logger.info(
        "------------------------------------  END  ------------------------------------")
