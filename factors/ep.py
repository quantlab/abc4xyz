from datetime import datetime
from sqlalchemy import and_
import pandas as pd

from pluto import api
from pluto.orm import *
from pluto.wind import *
from pluto.core import FactorBase

from config import engine_wind


class EP(FactorBase):
    __FACTOR_NAME__ = 'EP'
    __DESCRIPTION__ = 'earning to cap'

    def __init__(self):
        super().__init__()

    def _calculate(self, sid: str, date: str) -> FactorsValue:
        pe = api.session.query(ASHAREEODDERIVATIVEINDICATOR.S_VAL_PE).\
            filter(and_(ASHAREEODDERIVATIVEINDICATOR.S_INFO_WINDCODE == sid,
                        ASHAREEODDERIVATIVEINDICATOR.TRADE_DT == date)).first()
        if pe is not None:
            pe = pe[0]
            if pe is not None and pe != 0:
                fv = FactorsValue()
                fv.gen_time = datetime.now()
                fv.update_time = datetime.now()

                fv.sid = sid
                fv.date = datetime.strptime(date, '%Y%m%d')
                fv.fid = self._fid
                fv.value = float(1 / pe)
                return fv

    def _fast_cal(self, sid: str) -> pd.DataFrame:
        # 不能包含今天
        sql = """
            select S_INFO_WINDCODE as sid, TRADE_DT as date, 1/S_VAL_PE as value, now() as gen_time, now() as update_time
            from ASHAREEODDERIVATIVEINDICATOR
            where S_INFO_WINDCODE = '{0}' and TRADE_DT < '{1}'
            order by TRADE_DT
        """.format(sid, datetime.now().strftime("%Y%m%d"))

        df = pd.read_sql(sql, engine_wind)
        df['date'] = pd.to_datetime(df['date'])
        df['fid'] = self._fid

        return df


if __name__ == '__main__':
    from logger import Logger

    logger = Logger(
        logger_name='pluto',
        file_path='./log/factor_{0}.log'.format(EP.__FACTOR_NAME__)
    )

    logger.info(
        "------------------------------------ START ------------------------------------")
    ep = EP()
    ep.update()

    logger.info(
        "------------------------------------  END  ------------------------------------")

