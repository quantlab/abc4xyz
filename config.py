"""
配置文件
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


# wind 数据库
engine_wind = create_engine(
    "mysql+pymysql://hsquant:hs123456@218.1.122.196:3306/wind?charset=gbk", echo=False
)
SessionWind = sessionmaker(bind=engine_wind)


# sqlite 因字库
engine_factors = create_engine(
    "sqlite+pysqlite:///../data/factors.db"
)
SessionFactors = sessionmaker(bind=engine_factors)
