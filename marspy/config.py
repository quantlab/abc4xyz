# -*- coding: utf-8 -*-

import os
import configparser
import platform
from .data.loader import HDFDataLoader
from .data.basis import MysqlSecInfo

""""""""""""""""""""""""""""""""
"        读取配置文件内容         "
""""""""""""""""""""""""""""""""

# config file
if platform.system() == "Windows":
    config_path = "C:/Users/Public/"
elif platform.system() == "Linux":
    config_path = "/home/.marspy/"
else:
    raise Exception("unknown platform")

config_file = config_path + "default.ini"

if not os.path.isfile(config_file):
    raise Exception("config file does not exist")

# 读取常数
cfg = configparser.ConfigParser()
cfg.read(config_file, encoding="utf-8")

# 证券信息
SEC_INFO = MysqlSecInfo(cfg["Mysql"]["host"], cfg["Mysql"]["user"], cfg["Mysql"]["password"], cfg["Mysql"]["db"],
                        cfg["Security Info"]["SECURITY_INFO_PATH"]).load()

# 数据加载器
DATA_LOADER = HDFDataLoader(cfg["Quote Data"]["QUOTE_PATH"])
