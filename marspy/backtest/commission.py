# -*- coding: utf-8 -*-

import abc

from .enums import OrderSide, OrderStatus, FuturesCostUnitType, FuturesCostTradeType
from .order import Order
from .params import TradingParams


class Commission(metaclass=abc.ABCMeta):
    """
    对每笔交易计算交易佣金（包括税收）
    """

    @abc.abstractmethod
    def get_commission(self, order: Order, **kwargs):
        """
        返回 **成交** 订单所需的实际手续费
        
        Parameters
        ----------
        order: Order
            成交订单
        """
        raise NotImplementedError("method not defined")

    @abc.abstractmethod
    def get_cash_occupied(self, order: Order) -> float:
        """
        返回订单理论最大现金占用

        Notes
        -----
        **不包含手续费**

        Parameters
        ----------
        order: Order
            订单

        Returns
        -------
        float
            最大现金占用
        """
        raise NotImplementedError("method not defined")


class AStockCommission(Commission):
    """
    股票佣金模型

    Notes
    -----
    所有的股票都假设一样的佣金费率
    """

    def __init__(self):
        self._commission_rate = TradingParams.stock_commission_rate
        self._min_commission = TradingParams.stock_min_commission
        self._tax_rate = TradingParams.stock_tax_rate

        # 费率上浮比例
        self._trading_cost_up_pct = TradingParams.trading_cost_up_pct

    def get_commission(self, order: Order, **kwargs):
        """

        Parameters
        ----------
        order : Order
            订单，只应该是成交订单

        Returns
        -------
        float
            返回交易费用
        """
        assert order.status == OrderStatus.FILLED, "只有成交订单需要计算佣金"

        cost_money = order.filled_price * order.filled_qty
        v = cost_money * self._commission_rate * (1 + self._trading_cost_up_pct)
        v = max(self._min_commission, v)

        # 如果是卖出，还要扣除印花税
        if order.side == OrderSide.SELL:
            v += cost_money * self._tax_rate

        return v

    def get_cash_occupied(self, order: Order) -> float:
        """
        计算买入时需要占用的资金，卖出时不需要占用资金

        Notes
        -----
        **不包含手续费**

        Parameters
        ----------
        order: Order
            订单

        Returns
        -------
        float
            根据限价计算的最大资金占用
        """
        assert order.status == OrderStatus.NEW, "只有新订单才需要计算资金占用"

        if order.side == OrderSide.BUY:
            return order.limit_price * order.quantity
        elif order.side == OrderSide.SELL:
            return 0


class FuturesCommission(Commission):
    """
    期货佣金模型

    Notes
    -----
    每个标的一个佣金模型，根据自身的费率来设置相关参数

    Parameters
    ----------
    security : namedtuple or class
        期货，需要带有相应的字段
    """

    def __init__(self, security: tuple):
        assert TradingParams.futures_init_margin_up_pct >= 0, "初始保证金上浮比例不能为负"

        # 最小保证金和初始保证金比例
        self.__min_margin_rate = security.margin_rate
        self.__init_margin_factor = self.__min_margin_rate * (1 + TradingParams.futures_init_margin_up_pct)

        # 期货其他参数
        self.__multiplier = security.multiplier
        self.__cost_unit_type = security.cost_unit_type
        self.__close_today_cost = security.close_today_cost
        self.__open_cost = security.open_cost
        self.__close_yesterday_cost = security.close_yesterday_cost

        # 费率上浮比例
        self._trading_cost_up_pct = TradingParams.trading_cost_up_pct

    def get_commission(self, order: Order, **kwargs):
        """
        计算期货交易的实际佣金

        Parameters
        ----------
        order: Order
            订单
        kwargs: k-v 参数对
            'cost_type': FuturesCostTradeType
                期货交易成本类型：开仓，平今，平昨
        """
        assert order.status == OrderStatus.FILLED, "只有成交订单需要计算佣金"

        # 交易费率类型
        cost_trade_type = kwargs['cost_type']

        # 费率单位类型
        cost_unit_type = self.__cost_unit_type
        if cost_unit_type == FuturesCostUnitType.BY_AMOUNT:
            total = order.filled_price * order.filled_qty * self.__multiplier
        elif cost_unit_type == FuturesCostUnitType.BY_VOL:
            total = order.filled_qty
        else:
            raise Exception("未知的期货费率单位类型")

        # 根据交易费率类型计算费用
        if cost_trade_type == FuturesCostTradeType.OPEN:
            cost = total * self.__open_cost
        elif cost_trade_type == FuturesCostTradeType.CLOSE_TODAY:
            cost = total * self.__close_today_cost
        elif cost_trade_type == FuturesCostTradeType.CLOSE_YESTERDAY:
            cost = total * self.__close_yesterday_cost
        else:
            raise Exception("未知的交易费率类型")

        return cost * (1 + self._trading_cost_up_pct)

    def get_cash_occupied(self, order: Order):
        """
        计算 **开仓** 需要占用的最大资金数，按限价和初始保证金比率计算

        Notes
        -----
        **不包含手续费**

        Parameters
        ----------
        order: Order
            订单
        """
        assert order.status == OrderStatus.NEW, "只有新订单才需要计算资金占用"

        # 使用最高限价，按初始保证金比率计算
        margin_need = order.limit_price * order.quantity * self.__multiplier * self.__init_margin_factor

        return margin_need

    def get_margin(self, market_value: float, is_init=False) -> float:
        """
        计算 **净头寸** 需要的保证金占用，不考虑方向，主要用于盯市保证金调整

        Parameters
        ----------
        market_value : float
            当前标的合约的价值
        is_init : bool
            是否是初始保证金，默认为 False，即返回最低保证金

        Returns
        -------
        float
            返回需要的保证金数量
        """

        if is_init:
            return abs(market_value) * self.__init_margin_factor
        else:
            return abs(market_value) * self.__min_margin_rate
