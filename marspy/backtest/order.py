# -*- coding: utf-8 -*-

import itertools
import logging

import numpy as np
import pandas as pd

from .enums import *

logger = logging.getLogger(__name__)


class Order(object):
    """
    订单类

    Notes
    -----
    ** 暂不支持部分成交和部分撤单 **

    Notes
    -----
    * 订单 id 自增
    * limit_price 默认为 0，表示市价单，系统会自动将其替换为 当时价格 * 一定比例（比例由 TradingParams 设置）
    * 期货订单必须手动指定 offset (开平仓类型)
    * cash_needed 由 portfolio 验资时计算
    * FillStrategy 或撤单 决定 filled_qty, filled_price, end_time, status

    Parameters
    ----------
    sid: str
        证券代码：形如 600000.SH，交易代码+交易所形成的该证券的 **唯一** 标识
    quantity: int
        数量
    side: OrderSide
        交易方向
    created_time: pd.Timestamp
        创建时间
    limit_price: float
        价格， 默认为 0 表示市价单

    Attributes
    ----------
    ID_BASIS: itertools.count
        静态迭代器，用于生成表示订单的唯一 id
    id: int
        自动生成的自增的唯一 id
    sid: str
        证券代码
    limit_price: float
        价格，0 表示市价单，系统会自动将其替换为 当时价格 * 一定比例（比例由 TradingParams 设置）
    quantity: int
        数量
    side: OrderSide
        交易方向
        BUY = 1
        SELL = -1
    created_time: pd.Timestamp
        订单创建时间
    end_time: pd.Timestamp
        订单完成或撤销时间
    status: OrderStatus
        订单状态
    filled_price: float
        成交价格
    filled_qty: int
        成交数量
    cash_needed: float
        该订单理论上的最大资金占用
    offset: OffsetFlag, 仅仅对期货有效
        OPEN = 开仓
        CLOSE = 平仓

    Methods
    -------
    to_dict(self) -> dict
        获取字典形式的订单信息
    """

    __slots__ = ('id', 'sid', 'side', 'limit_price', 'quantity', 'created_time', 'end_time', 'filled_price',
                 'filled_qty', 'status', 'cash_needed', 'offset')

    # 全局订单引用迭代器
    ID_BASIS = itertools.count(1)

    def __init__(self, sid: str, quantity: int, side: OrderSide, created_time: pd.Timestamp, limit_price=0.0):
        assert quantity > 0, "order quantity must > 0, current value is {0}".format(quantity)
        assert limit_price >= 0, "order limit price must >= 0, current value is {0}".format(limit_price)

        self.id = next(self.ID_BASIS)
        self.sid = sid
        self.side = side
        self.limit_price = limit_price
        self.quantity = quantity
        self.created_time = created_time
        self.end_time = None
        self.filled_price = np.nan
        self.filled_qty = np.nan
        self.status = OrderStatus.NEW   # 初始化为 NEW

        # 资金占用，由 portfolio 进行验资时才会计算
        # 对于股票，该值等于不含手续费的按限价计算的市值
        # 对于期货，改值等于不含手续费的按限价计算的初始保证金
        self.cash_needed = 0.0

        # 期货专用字段，开仓或者平仓
        self.offset = None

    def __eq__(self, other):
        return self.id == other.id

    def __ne__(self, other):
        return self.id != other.id

    def __str__(self):
        return """sid: {0}, side: {1}, quantity: {2}, offset: {3}, price: {4}, created_time: {5}, end_time: {6},
                filled_price: {7}, filled_qty: {8}, status: {9}""".format(self.sid, self.side, self.quantity,
                self.offset, self.limit_price, self.created_time, self.end_time, self.filled_price, self.filled_qty,
                self.status)

    def to_dict(self) -> dict:
        """
        获取字典形式的订单信息

        Returns
        -------
        dict
            字典形式的订单信息
        """
        return {'id': self.id, 'sid': self.sid, 'side': self.side.value, 'limit_price': self.limit_price,
                'quantity': self.quantity, 'created_time': self.created_time,
                'end_time': self.end_time, 'filled_price': self.filled_price,
                'filled_qty': self.filled_qty, 'status': self.status.value}
