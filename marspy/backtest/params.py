

class TradingParams:
    """通用策略参数

    交易参数如下，**可根据需要自行修改**

    * 股票手续费，默认为 万八
        stock_commission_rate = 0.0008
    * 股票最低手续费，默认为 5 元
        stock_min_commission = 5
    * 印花税，默认为 千一
        stock_tax_rate = 0.001
    * 期货保证金相对交易所保证金的上浮比例，默认为上浮 30%
        futures_init_margin_up_pct = 0.3
    * 24小时制时，新的一个交易日开始的小时，默认为晚上7点
        new_day_adjust = 5
    * 计算风险所使用的一年的天数
        trading_days_a_year = 252

    """

    # 股票成本参数
    stock_commission_rate = 0.0008
    stock_min_commission = 5
    stock_tax_rate = 0.001

    # 期货保证金相对交易所保证金的上浮比例
    futures_init_margin_up_pct = 0.3

    # 交易手续费上浮比例
    trading_cost_up_pct = 0.5

    # 市价单价格上浮比例
    market_order_price_up_pct = 0.1

    # 新交易日开始时间，用来判断交易日的结束
    # 晚上 7 点以后的作为新交易的开始
    new_day_adjust = 5

    # 指定计算风险指标计算所用的全年交易日
    trading_days_a_year = 252
