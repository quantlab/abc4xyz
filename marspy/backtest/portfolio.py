# -*- coding: utf-8 -*-

import abc
import copy
import logging
from collections import namedtuple

from .enums import OrderStatus
from .order import Order
from .position import StockPosition, FuturesPosition
from ..utils.exception import NotEnoughMoneyException, InvalidOrderException
from .context import Context

# 用于记录当期损益
PnL = namedtuple("PnL", ["time", "cash", "float_profit", "total_commission", "total_value", "market_value"])

logger = logging.getLogger(__name__)


class Portfolio(metaclass=abc.ABCMeta):
    """
    多个单一证券的 position 组成一个 Portfolio, 此外还包括现金的管理

    Parameters
    ----------
    init_positions: dict of Position
        初始头寸信息
    init_cash: float
        初始资金

    Attributes
    ----------
    cash: float
        已经扣除手续费和交易税后投资组合中剩余的自由现金
    _positions: dictionary of Position
        以sid作为键，Position 对象作为值，包括 StockPosition 和 FuturePosition

    """

    def __init__(self, init_positions: dict, init_cash: float):
        # 当前剩余资金
        self.cash = init_cash

        # 初始头寸：可以认为是融券，浮动损益变化不计入组合收益计算
        # 复制头寸，保持 TradingEnvironment 里面的初始头寸不变
        self._positions = copy.deepcopy(init_positions)

    @abc.abstractmethod
    def __getitem__(self, sid):
        """
        返回证券 sid 的当前最新 position，如果 sid 不存在，则创建默认的 Position
        """
        pass

    def update_by_price(self):
        """
        依次调用每个 position的 update_by_price 方法更新持仓，再汇总计算资产组合的总价值

        Notes
        -----
        该函数调用一次即可更新全部头寸

        """
        # 重置累计值
        cash_change = 0.

        # 逐个证券更新，quotes必须包含每一个持仓证券
        for sid in Context.cur_quotes.keys():
            if sid != 'time':
                cash_change += self[sid].update_by_price()

        # 计算portfolio的cash余额以及总变现价值
        self.cash += cash_change

        if self.cash < 0:
            logger.warning("[{0}] 现金余额不足: {1}".format(Context.cur_time, self.cash))
            raise NotEnoughMoneyException("现金余额不足: {0}".format(self.cash))

    def update_by_order(self, order: Order) -> bool:
        """
        根据订单调用相应 position 的 update_by_order 方法更新持仓，再汇总计算资产组合的总价值
        该函数调用一次只能更新一个头寸，因此需要对每个订单调用一次该函数

        根据订单更新组合

        Notes
        -----
        会进行验资验券操作，资金不够或者余券不足，更新失败。

        **验资不考虑手续费，可能出现现金余额为小额负数的可能性**

        对于股票买单而言，因为股票价值不会为 0，所以手续费可以在后期卖出时的金额上补回来；对于期货而言，因为最低保证金
        比例不会为 0 且明显高于手续费，所以肯定足以支付手续费。

        Parameters
        ----------
        order: Order
            需要更新的订单

        Returns
        -------
        bool
            订单通过验资验券进入组合，返回 True，否则返回 False
        """
        sid = order.sid

        # 调用对应证券的佣金模型计算订单的资金占用

        # 只有 **新订单** 才会验资
        if order.status == OrderStatus.NEW:
            # 计算资金占用
            order.cash_needed = self[sid].commission.get_cash_occupied(order)
            if order.cash_needed > self.cash:
                # 验资失败
                logger.warning("[{0}] 订单 {1} 提交失败，因为现金余额不足，需要 {2} 实余 {3}".format(Context.cur_time,
                                                                                 order.id, order.cash_needed,
                                                                                 self.cash))
                order.status = OrderStatus.REJECTED  # 订单被拒绝
                # 该异常会终止回测
                raise NotEnoughMoneyException("现金余额不足: {0}".format(self.cash))

        cash_change = self[sid].update_by_order(order)

        # 如果 position 更新订单失败：只有校验数量时才会返回 None
        if cash_change is None:
            # 验券失败
            return False

        self.cash += cash_change

        return True

    @abc.abstractmethod
    def post_daily_trading(self):
        """
        更新所有头寸的日间操作
        """
        pass

    def __repr__(self):
        return "Portfolio({0})".format({
                                           k: v
                                           for k, v in self.__dict__.items()
                                           if not k.startswith("_")
                                           })

    def get_pnl(self) -> tuple:
        """
        获取当前投资组合的浮动损益、总佣金和组合总价值

        Returns
        -------
        PnL
            反映组合损益情况的 namedtuple
                time: 当前时间
                cash: 当前可用现金
                float_profit: 当前投资组合的浮动损益
                total_commission: 总佣金
                total_value: 总价值（包括现金）
        """
        float_profit = 0.
        total_commission = 0.
        total_value = self.cash
        market_value = 0.

        for sid, p in self._positions.items():
            float_profit += p.float_profit
            total_commission += p.commission_cost
            total_value += p.realizable_value
            market_value += abs(p.market_value)

        return PnL(Context.cur_time, self.cash, float_profit, total_commission, total_value, market_value)


class StockPortfolio(Portfolio):
    """
    Parameters
    ----------
    init_positions: dict
        初始头寸信息
    init_cash: float
        初始资金
    """

    def __init__(self, init_positions: dict, init_cash: float):
        super().__init__(init_positions, init_cash)

    def post_daily_trading(self):
        """
        更新所有头寸的日间操作，重置 qty_yesterday
        """
        for sid, p in self._positions.items():
            p.post_daily_trading()

    def __getitem__(self, sid):
        """
        返回证券 sid 的当前最新 position，如果 sid 不存在，则创建默认的 Position
        """
        if sid not in self._positions:
            self._positions[sid] = StockPosition(sid, 0)

        return self._positions[sid]


class FuturesPortfolio(Portfolio):
    """
    期货不需要初始头寸，只需要初始资金

    Parameters
    ----------
    init_cash: float
        初始资金
    """

    def __init__(self, init_cash: float):
        super().__init__({}, init_cash)

    def post_daily_trading(self):
        """
        更新所有头寸的日间操作，提取多余保证金
        """
        for sid, p in self._positions.items():
            self.cash += p.post_daily_trading()

    def __getitem__(self, sid):
        """
        返回证券 sid 的当前最新 position，如果 sid 不存在，则创建默认的 Position
        """
        if sid not in self._positions:
            self._positions[sid] = FuturesPosition(sid)

        return self._positions[sid]
