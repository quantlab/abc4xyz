

class Context:
    """
    全局运行时对象，用于记录当前时间和当前行情

    使用 Context.cur_time 访问属性，**不要创建 Context 对象来访问**
    """
    # 当前时间
    cur_time = None
    # 当前行情
    cur_quotes = None
    # 前一个行情
    pre_quotes = None