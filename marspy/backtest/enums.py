# -*- coding: utf-8 -*-

from enum import Enum


# 数据频率
class Frequency(Enum):
    """
    数据频率：
        支持 日，分钟，秒钟频率的数据
    """
    DAILY = 0   # 日频率
    MINUTE = 1  # 分钟频率
    SECOND = 2  # 秒钟频率


class SecurityType(Enum):
    """
    证券类型，目前仅支持股票和期货交易：
        STOCK - 股票，T+1，不可卖空
        FUTURES - 期货，T+0，可卖空
        INDEX - 指数，不可交易
    """
    STOCK = 0    # 股票
    FUTURES = 1  # 期货
    INDEX = 2    # 指数
    UNKNOWN = 3  # 未知


# 交易状态
class TradeStatus(Enum):
    """
    证券当前的交易状态
    """
    NORMAL = 0          # 正常交易
    HALT = 1            # 停牌
    AT_UPPER_LIMIT = 2  # 涨停
    AT_LOWER_LIMIT = 3  # 跌停


# 买、卖两个交易方向
class OrderSide(Enum):
    """
    订单交易方向，必须 BUY=1，SELL=-1
    """
    BUY = 1
    SELL = -1


# 新建、全部成交、撤销三个订单状态
class OrderStatus(Enum):
    """
    订单状态
    """
    NEW = 0
    FILLED = 1
    CANCELLED = 2
    REJECTED = 3


# 开仓、平仓两个类型：与CTP保持一致
class OffsetFlag(Enum):
    """
    只能开、平
    """
    OPEN = 0
    CLOSE = 1


class FuturesCostUnitType(Enum):
    """
    期货手续费费率单位类型
    """
    BY_AMOUNT = 0  # 按金额比率计算
    BY_VOL = 1     # 按手数计算


class FuturesCostTradeType(Enum):
    """
    期货手续费类型
    """
    OPEN = 0
    CLOSE_YESTERDAY = 1
    CLOSE_TODAY = 2
