import abc
import logging

from .commission import AStockCommission, FuturesCommission
from .enums import *
from .order import Order
from .context import Context
from .. import config

logger = logging.getLogger(__name__)


class Position(metaclass=abc.ABCMeta):
    """
    抽象头寸类，用于描述 **单一** 证券的头寸及交易损益情况，支持股票和期货。

    Notes
    -----
    支持股票卖空（通过底仓形式实现）

    Parameters
    ----------
    sid: str
        证券代码：形如 600000.SH，交易代码+交易所形成的该证券的 **唯一** 标识

    Attributes
    ----------
    sid : str
        证券代码
    commission : Commission
        佣金模型，用来计算资金占用以及费用
    quantity : int
        当前持仓净数量（包括占用头寸，但不包括股票的底仓），对股票而言表示股数，对期货而言表示合约张数
            _quantity > 0 ：多头
            _quantity < 0 : 空头
    market_value : float
        持仓总人民币市值，quantity * _price，如果是期货，还需要乘以 _multiplier
    cost_value : float
        获得该持仓时，持仓的总市值（计算方式同 _market_value），正数表示多头，负数表示空头
    average_cost : float
        当前持仓的平均价格，计算方法为数量加权平均，不同于订单成交价，任意时刻持仓都只有一个平均成本都相等，
    taken_profit : float
        交易已经实现的 **累计** 损益，不扣除交易成本
    commission_cost : float
        单个证券头寸的 **累计** 手续费及税费

    Methods
    -------
    float_profit() -> float
        属性。返回当前时刻的浮动损益
    realizable_value() -> float
        属性。返回当前时刻的可变现价值
    update_by_price(price: float) -> float
        方法。根据行情更新仓位变化
    update_by_order(order: Order) -> float
        方法。根据订单更新仓位变化，现金的变化应当 **已经包含** 手续费和税费
    to_dict() -> dict
        方法。将 position 以字典的方式返回
    post_daily_trading() -> float
        方法。收盘时的操作，返回现金变化（主要是期货保证金结算变化）
    """

    def __init__(self, sid: str):
        self.sid = sid
        self.commission = None
        self.quantity = 0
        self.market_value = 0.
        self.cost_value = 0.
        self.average_cost = 0.
        self.taken_profit = 0.
        self.commission_cost = 0.

        # ** 以下为私有字段 **

        # 证券，包含了证券相关信息，譬如类型，如果是期货，还包括保证金、交易费率等信息
        self._security = config.SEC_INFO[sid]

        # 未成交（股票买入或者期货开仓）订单的现金占用
        # 对于股票而言，_cash_occupied 永远为正
        # 对于期货而言，买入开仓，该值为正，卖出开仓，该值为负，但在计算可变现价值时应取绝对值
        self._cash_occupied = 0.

        # 未成交订单的证券占用：对股票而言，是在途未成交卖出订单的持仓占用，非负
        # 对期货而言，是在途未成交平仓订单的持仓占用，正数代表卖出平买仓，负数代表买入平卖仓
        self._quantity_occupied = 0

        # 可售昨数量，涉及到股票的今日可卖出数量和期货的可平昨数量
        self._saleable_from_yesterday = 0

    @property
    def float_profit(self) -> float:
        """
        持有头寸的浮动损益

        Returns
        -------
        float
            浮动损益 = 当前头寸的市值（多头为正，空头为负） - 当前头寸的持有成本（同前）
        """
        return self.market_value - self.cost_value

    @abc.abstractproperty
    def realizable_value(self) -> float:
        """
        该持仓的可变现价值，股票和期货的计算方法不同，具体见属性的实现

        Returns
        -------
        float
            持仓的可变现价值
        """

    @abc.abstractmethod
    def update_by_price(self) -> float:
        """
        根据行情更新仓位变化

        Returns
        -------
        float
            自由现金变化:
            对期货而言，由于可能追加保证金，因此现金会发生变化。现金管理由 Portfolio 控制，所以position需要返回现金的变化值
            对股票而言，每日浮动盈亏不影响现金，返回值只是为了保持接口统一
        """

    @abc.abstractmethod
    def update_by_order(self, order: Order) -> float:
        """
        根据订单更新仓位变化，现金的变化应当**已经包含**手续费和税费

        Parameters
        ----------
        order : Order
            订单，可以是不同的订单状态，根据状态的不同而进入不同的处理逻辑

        Returns
        -------
        float
            更新成功返回自由资金的变化信息，否则返回 None
            释放现金： float > 0
            占用现金： float < 0

        """

    @abc.abstractmethod
    def to_dict(self) -> dict:
        """
        获取头寸的字典形式

        Returns
        -------
        dict
            字典形式的 Position
        """

    @abc.abstractmethod
    def post_daily_trading(self) -> float:
        """
        日收盘处理

        Returns
        -------
        float
            日收盘处理导致的现金变化
        """


class StockPosition(Position):
    """
    股票头寸

    Parameters
    ----------
    sid: str
        证券代码：形如 600000.SH，交易代码+交易所形成的该证券的 **唯一** 标识
    init_quantity: int
        底仓数量（可以认为是融券余额），该变量用于控制 _saleable_from_yesterday，其浮动损益不参与持仓损益计算，
        不包括在持仓净数量 quantity 中

    See Also
    --------
    Position : 单一证券头寸基类
    """

    def __init__(self, sid, init_quantity):
        assert init_quantity >= 0, "初始持仓数必须非负，不能是 {0}".format(init_quantity)

        # 调用基类构造
        super().__init__(sid)

        assert self._security.type == SecurityType.STOCK, "{0} 不是股票类型，而是 {1}".format(sid, self._security.type)

        # 股票佣金模型
        self.commission = AStockCommission()

        # 底仓数量
        self._init_quantity = init_quantity
        # 当日可卖股票数量
        self._saleable_from_yesterday = init_quantity + self.quantity

    @property
    def realizable_value(self) -> float:
        """
        股票的可变现价值 = 持有市值 + 现金占用
        """
        return self.market_value + self._cash_occupied

    def update_by_price(self) -> float:
        """
        根据行情更新仓位变化

        Returns
        -------
        float
            常量 0，因为对股票头寸而言，由于没有保证金，因此浮动盈亏涉及保证金调整，因此不影响现金，返回值只是为了保持接口统一
        """

        # 由于 np.isnan 相对较慢，而 np.nan > 0 永远为 False，因此使用 _price > 0 直接检验价格合理性
        price = Context.cur_quotes[self.sid].price
        if price > 0:
            # 更新市值
            self.market_value = self.quantity * price

        return 0

    def update_by_order(self, order: Order) -> float:
        """利用订单更新头寸：外层调用应该保证 order.sid 与 self.sid 的一致性

        Notes
        -----
        *** Position 不校验订单的重复性 ***

        在A股市场中，股票是T+1且很难卖空的，所以卖空操作由底仓实现，因此这里假定底仓已经存在，且数目由 _saleable_from_yesterday 控制。

        Parameters
        ----------
        order: Order
            订单，可以是不同的订单状态，根据状态的不同而进入不同的处理逻辑

        Returns
        -------
        float
            更新成功返回自由资金信息，否则返回 None

                释放现金： float > 0
                占用现金： float < 0
        """
        if order.side == OrderSide.BUY:
            assert order.cash_needed > 0, "买入订单需要的资金必须大于 0"

        # 新单
        if order.status == OrderStatus.NEW:
            return self.__update_by_new_order(order)

        # 成交单, 假设一旦成交，则全部成交
        elif order.status == OrderStatus.FILLED:
            return self.__update_by_filled_order(order)

        # 撤单
        elif order.status == OrderStatus.CANCELLED:
            return self.__update_by_cancelled_order(order)

        # 无法识别的订单状态
        else:
            logger.error("[{0}] 无法识别的订单状态 %s", Context.cur_time, order.status)
            raise Exception("无法识别的订单状态")

    def __update_by_new_order(self, order: Order) -> float:
        """利用新单更新头寸

        新订单有两个方向：买 和 卖

        1、当是买单的时候，需要增加现金占用（数量为 order 的 cash_needed），同时返回一个负数（等于现金占用的负数）
        2、当是卖单的时候，需要先验券，余额不足时返回 None，通过则增加证券占用，并减少当日可卖数量（数量为 order 的 quantity）

        Parameters
        ----------
        order : Order
            状态为 NEW 的订单

        Returns
        -------
        float or None
            返回资金占用。
                对于卖单，该数字为 0，
                对于买单，该数字为计算过的 cash_needed * -1
                验券失败时，返回 None
        """

        # 买单的验资和现金冻结在 portfolio 中完成
        # 买单
        if order.side == OrderSide.BUY:
            # 计算资金占用
            self._cash_occupied += order.cash_needed  # 增加资金占用

            logger.info("[{0}] 订单 {1} 已报。品种 {2} 订单方向 {3} 订单数量 {4} 订单价格 {5} 当前持仓 {6}".format(
                Context.cur_time, order.id, order.sid, order.side, order.quantity, order.limit_price, self.quantity))

            # 返回资金占用
            return -order.cash_needed
        # 卖单则冻结股票
        # 验券在此时判断
        elif order.side == OrderSide.SELL:
            # 证券余额不足
            if self._saleable_from_yesterday < order.quantity:
                logger.error("[{0}] 证券 {1} 余额不足：当日剩余可卖出数量为 {2}, 订单卖出数量为 {3}".format(Context.cur_time,
                                                                                    order.sid,
                                                                                    self._saleable_from_yesterday,
                                                                                    order.quantity))
                return None

            # 昨持仓减少，占用增加
            # 昨持仓 ----> 证券占用
            self._saleable_from_yesterday -= order.quantity
            self._quantity_occupied += order.quantity

            logger.info("[{0}] 订单 {1} 已报。品种 {2} 订单方向 {3} 订单数量 {4} 订单价格 {5} 当前持仓 {6}".format(
                Context.cur_time, order.id, order.sid, order.side, order.quantity, order.limit_price, self.quantity))

            return 0.0

        else:
            logger.error("[{0}] 无法识别的交易方向 {1}".format(Context.cur_time, order.side))
            raise Exception("无法识别的交易方向")

    def __update_by_filled_order(self, order: Order) -> float:
        """利用成交单更新头寸

        根据成交单的方向和数量，需要分别判断 买卖 和 开平 情况。

        因为 self.quantity 会发生变化，所以必须更新如下成员：

        1、self.quantity - 当前净头寸
        2、self.market_value - 当前市值
        3、self.commission_cost - 当前手续费
        4、self._cash_occupied - 如果是买单成交，现金占用要减少
        5、self._quantity_occupied - 如果是卖单成交，证券占用要减少
        6、self.cost_value - 持仓成本也要变化
        7、self.average_cost - 如果是加仓，或者先平再开，平均成本都要变化
        8、self.taken_profit - 如果是平仓，实现损益要更新

        Notes
        -----
        *** Position 不校验订单的重复性 ***

        Parameters
        ----------
        order : Order
            状态为 FILLED 的订单

        Returns
        -------
        float
            返回资金变化
        """
        assert order.quantity == order.filled_qty, "成交不完全，目前暂不支持部分成交"

        # 带方向的成交量：买入为正，卖出为负
        signed_filled_qty = order.side.value * order.filled_qty
        # 记录此订单之前的净持仓数量
        previous_qty = self.quantity
        # 更新净持仓数量
        self.quantity += signed_filled_qty
        # 更新手续费：通过实际成交价计算
        cost = self.commission.get_commission(order)
        self.commission_cost += cost

        # 如果是买入成交单
        if order.side == OrderSide.BUY:
            # 资金占用要扣减当前成交的买入订单原先冻结的资金
            self._cash_occupied -= order.cash_needed
            # 实际需要的资金应该是 成交金额 + 手续费
            cash_need_actual = order.filled_qty * order.filled_price + cost
            # 返还给外层 portfolio 的资金数量：原先冻结的资金 - 实际需要的资金
            # 资金多退少补
            cash_change = order.cash_needed - cash_need_actual

        # 如果是卖出成交单
        elif order.side == OrderSide.SELL:
            # 证券占用要减少（可卖出数量已经在 NEW 时扣减）
            self._quantity_occupied -= order.filled_qty
            # 返还给外层 portfolio 的资金数量：卖出成交金额 - 手续费
            cash_change = order.filled_price * order.filled_qty - cost

        # 无法识别的订单类型
        else:
            logger.error("[{0}] 无法识别的订单类型 {1}".format(Context.cur_time, order.side))
            raise Exception("无法识别的订单类型")

        # 如果 原持仓 与 订单多空方向 一致，判定为 加仓，按数量加权计算成本
        if previous_qty * order.side.value >= 0:
            self.cost_value += order.filled_price * signed_filled_qty
            self.average_cost = self.cost_value / self.quantity
        # 如果 原持仓 与 订单多空方向 相反，判定为 平仓
        else:
            # 新持仓 与 原持仓 方向相同：纯平仓
            if self.quantity * previous_qty >= 0:
                # 平仓时按比例调整成本并计算实现损益（不扣除交易成本）
                self.cost_value = self.cost_value * self.quantity / previous_qty
                # 实现损益增加
                self.taken_profit += (order.filled_price - self.average_cost) * (-signed_filled_qty)

                # 持仓为 0 时，重置平均成本
                if self.quantity == 0:
                    self.average_cost = 0.0

            # 新持仓 与 原持仓 方向相反：先平再反向开仓
            else:
                self.taken_profit += (order.filled_price - self.average_cost) * previous_qty
                self.average_cost = order.filled_price
                self.cost_value = self.quantity * self.average_cost

        logger.info("[{0}] 订单 {1} 已成。品种 {2} 买卖方向 {3} 成交数量 {4} 成交价格 {5} 当前持仓 {6}".format(
            Context.cur_time, order.id, order.sid, order.side, order.filled_qty, order.filled_price, self.quantity))

        return cash_change

    def __update_by_cancelled_order(self, order: Order) -> float:
        """利用已撤单更新头寸

        撤销买入订单时：减少资金占用，并将其返还给 portfolio；撤销卖出订单时，减少证券占用，增加可卖数量。

        Notes
        -----
        *** Position 不校验订单的重复性 ***

        Parameters
        ----------
        order : Order
            状态为 CANCELLED 的订单

        Returns
        -------
        float
            返回资金变化：只有是买入订单的撤单才会返回非零资金

        """
        # 买单则解冻被占用的资金
        logger.info("[{0}] 订单 {1} 已撤单。品种 {2} 买卖方向 {3} 交易数量 {4}, 当前持仓 {5}".format(
            Context.cur_time, order.id, order.sid, order.side, order.quantity, self.quantity))

        if order.side == OrderSide.BUY:
            self._cash_occupied -= order.cash_needed
            return order.cash_needed
        # 卖单则解冻被占用的股票，增加昨持仓数量
        elif order.side == OrderSide.SELL:
            self._quantity_occupied -= order.quantity
            self._saleable_from_yesterday += order.quantity
            return 0.0
        else:
            logger.error("[{0}] 无法识别的交易方向 {1}".format(Context.cur_time, order.side))
            raise Exception("无法识别的交易方向")

    def post_daily_trading(self) -> float:
        """
        重设 _saleable_from_yesterday = 当前持仓 + 底仓

        Returns
        -------
        float
            返回 0，不起实际作用，保持接口一致性
        """
        self._saleable_from_yesterday = self.quantity + self._init_quantity

        return 0.0

    def to_dict(self):
        """
        获取头寸的字典形式

        Returns
        -------
        dict
            头寸的字典形式
        """
        return {'quantity': self.quantity,
                'price': Context.cur_quotes[self.sid].price,
                'market_value': self.market_value,
                'cost_value': self.cost_value,
                'average_cost': self.average_cost,
                'float_profit': self.float_profit,
                'taken_profit': self.taken_profit,
                'realizable_value': self.realizable_value,
                'cash_occupied': self._cash_occupied,
                'quantity_occupied': self._quantity_occupied,
                'commission_cost': self.commission_cost,
                'total_profit': self.float_profit + self.taken_profit - self.commission_cost}


class FuturesPosition(Position):
    """期货头寸

    Parameters
    ----------
    sid: str
        证券代码：形如 cu.SHF，交易代码+交易所形成的该证券的 **唯一** 标识

    See Also
    --------
    Position : 单一证券头寸基类
    """

    def __init__(self, sid: str):
        # 调用基类构造
        super().__init__(sid)

        assert self._security.type == SecurityType.FUTURES, "{0} 不是期货类型，而是 {1}".format(sid, self._security.type)

        # 合约乘数，每张合约一个点位代表的人民币金额
        self._multiplier = self._security.multiplier

        # 期货佣金模型
        self.commission = FuturesCommission(self._security)

        # 逐日盯市盈亏，用来表示保证金的盈余变化
        # 每个交易日结束时重置为 0
        self.mtm_profit = 0.

        # 当前时刻的保证金余额，只有在开平仓成交、追加保证金、收盘时会改变
        # 在每日收盘的时候，按市值和初始保证金水平计算
        self._margin_balance = 0.

        # 盯市成本，每日收盘时，重置为收盘价
        # 第二天如果如果有新的开仓，跟新持仓做加权
        self.__cost_for_mtm = 0.

    @property
    def realizable_value(self):
        """
        重载了基类的属性

        self._cash_occupied 可正可负，必须取绝对值

        """
        return self._margin_balance + abs(self._cash_occupied) + self.mtm_profit

    def update_by_price(self) -> float:
        """
        根据行情更新仓位变化

        Returns
        -------
        float
            现金的变动：只有在保证金余额不足时，才会返回一个负数，表示需要追加的保证金
        """
        cash_change = 0.0

        price = Context.cur_quotes[self.sid].price

        # 如果价格为nan或者负数，则不会执行以下分支
        if price > 0:
            # 如果持仓量为0，返回 0
            if self.quantity == 0:
                return cash_change

            # 更新市值
            self.market_value = self.quantity * price * self._multiplier

            # 盯市盈亏
            self.mtm_profit = (price - self.__cost_for_mtm) * self.quantity * self._multiplier

            # 可用保证金
            margin_avalible = self._margin_balance + self.mtm_profit

            # 计算最低需要的保证金
            min_margin = self.commission.get_margin(self.market_value)

            # 如果当前保证金低于最低保证金，追加至初始保证金
            if margin_avalible < min_margin:
                init_margin = self.commission.get_margin(self.market_value, is_init=True)

                # 追加保证金
                required_margin = init_margin - margin_avalible
                logger.info("[{0}] {1} 的保证金可用余额不够，需要追加保证金 {2}".format(Context.cur_time, self.sid, required_margin))

                cash_change = -required_margin  # 需要增加现金占用

                # 保证金余额升至 初始保证金
                self._margin_balance += required_margin


            #TODO 高出初始保证金的部分退给 portfolio 作为可用现金

        return cash_change

    def update_by_order(self, order: Order) -> float:
        """
        根据订单类型调整头寸

        Parameters
        ----------
        order: Order
            订单，可以是不同的订单状态，根据状态的不同而进入不同的处理逻辑

        Returns
        -------
        float:
            更新成功返回自由资金信息，否则返回 None
                释放现金： cash_change > 0
                占用现金： cash_change < 0
        """
        offset = order.offset

        assert offset is not None, "期货订单必须指定开平仓类型 offset"

        # 新单
        if order.status == OrderStatus.NEW:
            return self.__update_by_new_order(order)

        # 成交单, 假设一旦成交，则全部成交
        elif order.status == OrderStatus.FILLED:
            return self.__update_by_filled_order(order)

        # 撤单
        elif order.status == OrderStatus.CANCELLED:
            return self.__update_by_cancelled_order(order)

        else:
            logger.error("[{0}] 无法识别的订单状态 {1}".format(Context.cur_time, order.status))
            return None

    def __update_by_new_order(self, order: Order) -> float:
        """
        利用新单更新头寸，要分别从开平和买卖两个角度来看

        Parameters
        ----------
        order : Order
            状态为 NEW 的订单

        Returns
        -------
        float or None
            下单成功返回保证金占用，否则返回 None
        """

        assert self.quantity * self._quantity_occupied >= 0, "当前净头寸必须与证券占用同向"

        # 可用数量：当前净持仓扣减证券占用之后的剩余
        qty_available = self.quantity - self._quantity_occupied
        assert qty_available * self.quantity >= 0, "可用平仓数量必须与净头寸同向"

        # 带符号的量
        signed_quantity = order.quantity * order.side.value

        # 开平仓标识符
        offset = order.offset

        cash_change = 0.0

        # 如果是开仓
        if offset == OffsetFlag.OPEN:
            # 何时不能开仓？

            # 1、不能在有可用多头的时候开空仓，或者在有可用空头的时候开多仓
            # **注意**：qty_available 等于 0 便允许发出开仓订单，但如果当时有在途平仓订单，当前订单不能早于在途平仓单成交，
            #          若 fill_strategy 判定先判定当前订单为成交，要在 FILLED 分支中抛出异常。
            if qty_available * signed_quantity < 0:
                logger.error("[{0}] 品种 {1} 当前可用净头寸：{2} 开仓方向：{3} 开仓数量：{4}，"
                             "只有可用净头寸小于等于0（大于等于0）时才能卖出（买入）开仓".format(Context.cur_time, order.sid,
                                                                     qty_available, order.side, order.quantity))
                # 标记为被拒绝订单
                order.status = OrderStatus.REJECTED
                return None

            # 2、不允许在卖出开空仓（买入开多仓）但尚未成交的同时又买入开多仓（卖出开空仓）
            if self._cash_occupied * signed_quantity < 0:
                logger.error("[{0}] 品种 {1} 不允许在卖出开空仓（买入开多仓）但尚未成交的同时又买入开多仓（卖出开空仓）："
                             "现金占用 {2} 但交易方向为 {3}".format(Context.cur_time, order.sid, self._cash_occupied,
                                                          order.side))

                # 标记为被拒绝订单
                order.status = OrderStatus.REJECTED
                return None

            # 3、禁止先平但未成交，接着又反向再开
            if self._quantity_occupied * signed_quantity > 0:
                logger.error("[{0}] 品种 {1} 不允许先平但未成交，接着又反向再开："
                             "证券占用 {2} 但交易方向为 {3}".format(Context.cur_time, order.sid, self._quantity_occupied,
                                                          order.side))

                # 标记为被拒绝订单
                order.status = OrderStatus.REJECTED
                return None

            # 订单需要的资金直接作为保证金：实际上就是以限价计算的初始保证金
            # order.cash_needed 一定为正，返回给 portfolio 的必须为负
            cash_change = -order.cash_needed

            # 但记录的 _cash_occupied 应该带符号
            # 买入开仓的为正，卖出开仓的为负
            self._cash_occupied += order.cash_needed * order.side.value  # 资金被冻结

            logger.info("[{0}] 平仓订单 {1} 已报。品种 {2} 订单方向 {3} 订单数量 {4} 当前持仓 {5} 资金占用 {6} 证券占用 {7}".format(
                Context.cur_time, order.id, order.sid, order.side, order.quantity, self.quantity, self._cash_occupied,
                self._quantity_occupied))

        # 如果是平仓
        elif offset == OffsetFlag.CLOSE:
            # 何时能够平仓？

            # 1、当有尚未成交的开仓单时，不允许平仓
            if self._cash_occupied != 0:
                logger.error("品种 {0} 当前尚有未成交的开仓单，不允许平仓".format(self.sid))

                # 标记为被拒绝订单
                order.status = OrderStatus.REJECTED
                return None

            # 2、不允许在有可用多头的时候买平（只能卖平），不允许在有可用空头的时候卖平（只能买平）
            if qty_available * signed_quantity > 0:
                logger.error("[{0}] 品种 {1} 当前可用净头寸 {2} 平仓方向 {3} 平仓数量 {4}："
                             "只有净头寸为小于0（大于0）时才能买入（卖出）平仓".format(Context.cur_time, order.sid, self.quantity,
                                                                order.side, order.quantity))
                return None

            # 3、不能超过可用平仓数量
            if abs(qty_available) < order.quantity:
                logger.error("[{0}] 品种 {1} 可用数量不足。当前持仓数量为 {2} 可用平仓数量为 {3} 订单平仓数量为 {4} 方向为 {5}".format(
                    Context.cur_time,
                    order.sid,
                    self.quantity,
                    qty_available,
                    order.quantity,
                    order.side))
                return None

            # 计算冻结数目
            # 买入平空头时原有 _quantity_occupied 必须 <= 0，
            # 卖出平多头时原有 _quantity_occupied 必须 >= 0，

            assert self._quantity_occupied * signed_quantity <= 0, "平仓只是同时发生在一个方向上"
            self._quantity_occupied -= signed_quantity

            # !!平仓开仓是不需要保证金的

            logger.info("[{0}] 平仓订单 {1} 已报。品种 {2} 订单方向 {3} 订单数量 {4} 当前持仓 {5} 资金占用 {6} 证券占用 {7}".format(
                Context.cur_time, order.id, order.sid, order.side, order.quantity, self.quantity, self._cash_occupied,
                self._quantity_occupied))

        return cash_change

    def __update_by_filled_order(self, order: Order) -> float:
        """
        根据期货成交单更新期货头寸

        Parameters
        ----------
        order : Order
            状态为 FILLED 的订单

        Returns
        -------
        float

        """
        # 记录之前的持仓
        previous_qty = self.quantity

        # 带方向的成交量
        signed_filled_quantity = order.side.value * order.filled_qty

        # 开平仓标识符
        offset = order.offset

        cash_change = 0.0

        # 如果是开仓，则按数量加权计算成本
        # 不需要返回保证金变化
        if offset == OffsetFlag.OPEN:

            # 如果开仓成交方向和现有持仓方向不一致，直接抛出异常
            if self.quantity * signed_filled_quantity < 0:
                logger.error("[{0}] 订单 {1} 异常。品种 {1} 当前持仓 {2} 开仓方向 {3} 开仓数量 {4}："
                             "未了结反向头寸，无法开仓".format(Context.cur_time, order.id, order.sid, self.quantity,
                                                   order.side, order.quantity))

                raise Exception("开仓单交易方向和现有持仓方向不一致")

            self.quantity += signed_filled_quantity

            assert self.quantity != 0, "开仓成交之后数量不能等于 0"

            # 更新盯市成本
            order_value = signed_filled_quantity * order.filled_price
            self.__cost_for_mtm = (previous_qty * self.__cost_for_mtm + order_value) / self.quantity

            # 计算开仓手续费
            cost = self.commission.get_commission(order, cost_type=FuturesCostTradeType.OPEN)
            self.commission_cost += cost

            # 重置资金占用，并按实际成交金额调整资金变化
            self._cash_occupied -= order.cash_needed * order.side.value

            # margin_balance 加上 现金占用 扣减 手续费
            self._margin_balance += (order.cash_needed - cost)

            # 持仓合约的持仓总成本和平均成本
            self.cost_value += order.filled_price * signed_filled_quantity * self._multiplier
            self.average_cost = self.cost_value / self.quantity / self._multiplier

            logger.info("[{0}] 开仓订单 {1} 已成。品种 {2} 订单方向 {3} 成交数量 {4} 成交价格 {5} "
                        "当前持仓 {6} 资金占用 {7} 证券占用 {8}".format(Context.cur_time, order.id, order.sid,
                                                            order.side, order.filled_qty,
                                                            order.filled_price, self.quantity,
                                                            self._cash_occupied, self._quantity_occupied))

            # !! 开仓成交不需要返回现金变化

        # 平仓
        # 需要返回释放的保证金（并扣除交易手续费）
        elif offset == OffsetFlag.CLOSE:
            if self.quantity * signed_filled_quantity >= 0:
                logger.error("[{0}] 订单 {1} 异常。品种 {2} 数量 {3} 订单方向 {4} 订单数量 {5}："
                             "该品种已被其他订单处理，不允许重复平仓".format(Context.cur_time, order.id, order.sid,
                                                          self.quantity, order.side, order.quantity))
                raise Exception("平仓单交易方向和现有持仓方向相同")

            assert previous_qty != 0, "平仓成交时，原有持仓数量不能为 0"

            # 更新持仓和市值
            self.quantity += signed_filled_quantity

            # 交易手续费：区分开仓、平今与平昨，优先平昨
            # 当昨持仓有剩余但不够时，全部当做平昨
            if self._saleable_from_yesterday != 0:
                cost = self.commission.get_commission(order, cost_type=FuturesCostTradeType.CLOSE_YESTERDAY)
                if self._saleable_from_yesterday > 0:
                    # 优先平昨日仓位，直到昨日仓位为0
                    self._saleable_from_yesterday += signed_filled_quantity
                    self._saleable_from_yesterday = max(0, self._saleable_from_yesterday)

                elif self._saleable_from_yesterday < 0:
                    # 优先平昨日仓位，直到昨日仓位为0
                    self._saleable_from_yesterday += signed_filled_quantity
                    self._saleable_from_yesterday = min(0, self._saleable_from_yesterday)
            else:
                cost = self.commission.get_commission(order, cost_type=FuturesCostTradeType.CLOSE_TODAY)

            # 增加手续费
            self.commission_cost += cost

            # 减少期货手数占用
            self._quantity_occupied += order.filled_qty * order.side.value

            # 平仓时按比例调整成本并结算浮动损益（不扣除交易成本）
            self.cost_value = self.cost_value * self.quantity / previous_qty
            # 市值也按比例调整
            self.market_value = self.market_value * self.quantity / previous_qty

            # 计算已实现损益：相对于盯市成本而言
            # 要扣减保证金
            chg = (order.filled_price - self.__cost_for_mtm) * (-signed_filled_quantity) * self._multiplier

            # 已平仓部分占用的保证金 + 已平仓部分的盯市损益 - 成本 = 应该归还给 portfolio 的可用现金
            margin_balance_released = self._margin_balance * (-signed_filled_quantity) / previous_qty
            self._margin_balance = self._margin_balance * self.quantity / previous_qty
            cash_change = margin_balance_released + chg - cost

            self.taken_profit += chg    # 实现损益要增加

            # 如果持仓变为0，重置盯市成本、盯市浮盈和平均成本
            if self.quantity == 0:
                self.__cost_for_mtm = 0.
                self.mtm_profit = 0.
                self.average_cost = 0.

            logger.info("[{0}] 平仓订单 {1} 已成。品种 {2} 订单方向 {3} 成交数量 {4} 成交价格 {5} "
                        "当前持仓 {6} 资金占用 {7} 证券占用 {8}".format(Context.cur_time, order.id, order.sid,
                                                            order.side, order.filled_qty,
                                                            order.filled_price, self.quantity,
                                                            self._cash_occupied, self._quantity_occupied))

        return cash_change

    def __update_by_cancelled_order(self, order: Order) -> float:
        """
        利用撤销订单更新头寸

        Parameters
        ----------
        order : Order
            状态为 CANCELLED 的订单

        Returns
        -------
        float
            只有开仓的撤单才需要返回保证金
        """
        # 开平仓标识符
        offset = order.offset

        cash_change = 0.0

        # 开仓单则解冻被占用的资金
        if offset == OffsetFlag.OPEN:
            cash_change = order.cash_needed
            self._cash_occupied -= order.cash_needed * order.side.value
        # 平仓单则解冻被占用的手数
        elif offset == OffsetFlag.CLOSE:
            self._quantity_occupied += order.quantity * order.side.value

        logger.info("[{0}] 订单 {1} 已撤单。品种：{2} 买卖方向：{3} 开平仓标识 {4} 交易数量 {5} 当前持仓 {6} 资金占用 {7} 证券占用 {8}".format(
            Context.cur_time, order.id, order.sid, order.side, order.offset, order.quantity, self.quantity,
            self._cash_occupied, self._quantity_occupied))
        return cash_change

    def post_daily_trading(self) -> float:
        """
        取出多余的保证金，多余初始保证金的部分可以作为自由现金供 portfolio 继续使用

        如果当日发生展期，则需要调整合约价差

        Returns
        -------
        float
            现金的变动
        """
        cash_change = 0.0
        quote = Context.cur_quotes[self.sid]

        # 如果今天是展期日，则调整损益
        if quote.rollover:
            if self._saleable_from_yesterday != 0:
                #如果昨日有持仓，则需要对不同合约的价差进行调整
                if quote.open > 0 and quote.rollover_open > 0:
                    rollover_adjust = self._saleable_from_yesterday * (quote.rollover_open - quote.open) * self._multiplier
                    cash_change += rollover_adjust
                    # 价差调整计入实现损益
                    self.taken_profit += rollover_adjust
                else:
                    logger.error('[{0}] 品种 {1} 展期调整数据缺失'.format(Context.cur_time, self.sid))

                # TODO 对展期手续费进行调整

        # 设置昨持仓为当前持仓
        self._saleable_from_yesterday = self.quantity

        # 盯市结算
        if self.quantity != 0:
            if quote.price > 0 and quote.settle > 0:
                # 重置用于计算保证金的成本为当前结算价
                self.__cost_for_mtm = quote.settle
                self.mtm_profit += (quote.settle - quote.price) * self.quantity * self._multiplier
            else:
                logger.error('[{0}] 品种 {1} 盯市数据缺失'.format(Context.cur_time, self.sid))

            # 逐日盯市盈亏要反映到 portfolio 中，并重置保证金到初始水平：以收盘价计算
            init_margin = self.commission.get_margin(self.market_value, is_init=True)
            cash_change += (self._margin_balance - init_margin + self.mtm_profit)
            self._margin_balance = init_margin

            # 盯市盈亏每日结算计入实现损益
            self.taken_profit += self.mtm_profit

            # 重置盯市盈亏
            self.mtm_profit = 0.


        return cash_change

    def to_dict(self):
        """
        获取头寸的字典形式

        期货的总损益 = 盯市损益 + 已实现损益 - 佣金
        """
        return {'quantity': self.quantity,
                'price': Context.cur_quotes[self.sid].price,
                'market_value': self.market_value,
                'cost_value': self.cost_value,
                'average_cost': self.average_cost,
                'float_profit': self.float_profit,
                'taken_profit': self.taken_profit,
                'margin_balance': self._margin_balance,
                'mtm_profit': self.mtm_profit,
                'realizable_value': self.realizable_value,
                'cash_occupied': self._cash_occupied,
                'quantity_occupied': self._quantity_occupied,
                'commission_cost': self.commission_cost,
                'quantity_yesterday': self._saleable_from_yesterday,
                'total_profit': self.mtm_profit + self.taken_profit - self.commission_cost}
