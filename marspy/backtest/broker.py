# -*- coding: utf-8 -*-

import logging

import numpy as np

from .enums import OrderStatus, SecurityType
from .fill import FillStrategy
from .order import Order
from .portfolio import Portfolio, StockPortfolio, FuturesPortfolio
from .context import Context

logger = logging.getLogger(__name__)


class Broker:
    """
    Broker类，接收原始订单，并根据不同的成交策略处理订单

    Parameters
    ----------
    init_cash: float
        初始资金
    init_positions: dict of Position
        初始持仓信息：持仓收益不计入损益计算
    fill_strategy: FillStrategy
        成交策略
    security_type: SecurityType
        证券类型，不支持混合证券类型的组合

    Attributes
    ----------
    portfolio: Portfolio
    active_limit_orders: list of Order
        接受策略发出的限价订单，在随后的每个时间点逐个判断能否成交
    cancelled_orders: list of Order
        记录撤销的订单
    filled_orders: list of Order
        记录成交的订单
    infos: list of Portfolio
        记录Portfolio在每一个日期的明细
    position_hist: dict of List
        记录每一个头寸的详细持仓变化，每日更新
    """

    def __init__(self, init_cash: float, init_positions: dict, fill_strategy: FillStrategy,
                 security_type: SecurityType):
        # 组合信息
        if security_type == SecurityType.STOCK:
            self.portfolio = StockPortfolio(init_positions, init_cash)
        elif security_type == SecurityType.FUTURES:
            self.portfolio = FuturesPortfolio(init_cash)
        else:
            raise Exception("暂不支持 STOCK 和 FUTURES 之外的 Portfolio 类型")

        # 活跃订单列表
        self.active_limit_orders = []
        # 已撤单订单列表
        self.cancelled_orders = []
        # 已拒绝订单列表：只有新单才会被拒绝
        self.rejected_orders = []
        # 已成交订单列表
        self.filled_orders = []
        # 当前组合基本信息列表
        self.infos = []
        # 详细的历史头寸
        self.position_hist = {}

        # 成交模型
        self.__fill_strategy = fill_strategy

    def place_order(self, order: Order) -> bool:
        """
        下单

        Parameters
        ----------
        order: Order
            需要提交的订单

        Returns
        -------
        bool
            提交成功返回 True，否则返回 False
        """

        # 检查价格
        # 如果当前行情是缺失的，则不能进行下单
        if np.isnan(order.limit_price):
            logger.warning("[{1}] 订单 [{0}] 因为 [{1}] 行情非法无法下达".format(order.id, order.created_time))
            return False
        # NEW 订单更新持仓
        if self.portfolio.update_by_order(order):
            self.active_limit_orders.append(order)  # 只有提交成功才添加
            return True
        else:
            self.rejected_orders.append(order)  # 提交失败的计入拒绝单列表
            return False

    def on_quotes(self):
        """
        对每个新行情，处理流程如下：
        1、撮合上一阶段的订单
        2、更新 portfolio 的价格
        """

        # 如果订单队列不是空的，则判断能否成交
        if self.active_limit_orders:
            for order in self.active_limit_orders:
                sid = order.sid

                # 如果当前行情是缺失的，则不能进行交易
                if sid not in Context.cur_quotes:
                    continue

                quote = Context.cur_quotes[sid]

                # 更新价格需要price字段
                if quote.price > 0:
                    # filled?
                    filled = self.__fill_strategy.fill_order(order, quote)

                    if filled:
                        # 更新持仓
                        self.portfolio.update_by_order(order)
                        # 记录成交订单
                        self.filled_orders.append(order)

            self.active_limit_orders = [order for order in self.active_limit_orders if
                                        order.status == OrderStatus.NEW]

        # 利用价格更新组合状态
        self.portfolio.update_by_price()

    def cancel_order(self, order: Order):
        """
        撤销指定订单

        Parameters
        ----------
        order: Order
            需要撤销的订单
        """
        if order in self.active_limit_orders:
            if order.status == OrderStatus.NEW:
                order.status = OrderStatus.CANCELLED
                order.end_time = Context.cur_time

                self.portfolio.update_by_order(order)

                # 移除
                self.active_limit_orders.remove(order)

                # 记录撤销的订单
                self.cancelled_orders.append(order)

    def cancel_orders(self):
        """
        撤销所有未成交订单
        """

        # 撤销所有未成交订单
        for order in self.active_limit_orders:
            if order.status == OrderStatus.NEW:
                order.status = OrderStatus.CANCELLED
                order.end_time = Context.cur_time

                self.portfolio.update_by_order(order)

                # 记录撤销的订单
                self.cancelled_orders.append(order)

        self.active_limit_orders = [order for order in self.active_limit_orders if
                                    order.status == OrderStatus.NEW]

    def post_daily_trading(self, need_cancel=True):
        """
        每日交易结束之后的处理
            1. 所有的未完成订单都会被撤销
            2. 调用每个 position 的 post_daily_trading 方法

        Parameters
        ----------
        need_cancel : bool
            是否需要撤销跨日订单，对于不需要撤销跨日订单的日策略，需要设置为 False
        """
        if need_cancel:
            self.cancel_orders()

        self.portfolio.post_daily_trading()

        # 记录每日头寸详情
        for sid, p in self.portfolio._positions.items():
            record = p.to_dict()
            record['time'] = Context.cur_time
            if sid in self.position_hist:
                self.position_hist[sid].append(record)
            else:
                self.position_hist[sid] = [record]

    def add_one_record(self):
        """
        每个时间点交易完成后记录下组合损益信息
        """
        # 获取组合损益情况，包括：时间，可用现金，浮动损益、总佣金、总组合价值
        pnl = self.portfolio.get_pnl()
        self.infos.append(pnl)
