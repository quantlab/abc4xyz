# -*- coding: utf-8 -*-

import abc

import pandas as pd

from . import analyser


class Risk(metaclass=abc.ABCMeta):
    """
    抽象Risk类

    Attributes
    ----------

    """

    def __init__(self, net_value: pd.Series):
        """
        初始化

        Parameters
        ----------
        net_value
            策略净值的时间序列，支持原始账户总资产的时间序列
        """
        self._total_return = .0
        self._annal_return = .0
        self._annal_volatility = .0
        self._max_drawdown_info = None
        self._nv = net_value

    @abc.abstractmethod
    def calculate(self):
        raise NotImplementedError('method not defined!')

    def __repr__(self):
        return "Risk({0})".format(self.__dict__)


class DailyRisk(Risk):
    """
    DailyRisk类，用于表征策略日频风险

    Attributes
    ----------

    """

    def __init__(self, net_value: pd.Series, bench_value=None, rf=None, true_beta=False):
        """
        初始化

        Parameters
        ----------
        net_value
            策略净值的时间序列，支持原始账户总资产的时间序列

        bench_value
            基准净值的时间序列，支持原始账户总资产的时间序列，默认为None

        rf
            无风险收益的时间序列，默认为None

        true_beta
            计算alpha时，是否用真实beta，默认False，即直接减bench的收益

        注：只有当bench_value和rf均不为None时，才开启_bench_flag，否则仅计算总收益、年化收益、年化波动率、最大回撤
        """

        super().__init__(net_value)

        self._nv = self._nv.resample('D').last().dropna()
        self._bench_nv = None
        self._rf = None

        self._bench_flag = False
        self._true_beta = true_beta

        self._beta = .0
        self._alpha = .0
        self._sharpe = .0
        self._information_ratio = .0
        self._tracking_error = .0
        self._Sortino = .0
        self._Treynor = .0
        self._calmar = .0
        self._downside_risk = .0

        self._bench_total_return = .0
        self._bench_annal_return = .0

        self._timing_indicator = None

        if bench_value is not None and rf is not None:
            self._bench_flag = True  # 是否有bench和rf的标识
            self._bench_nv = bench_value.resample('D').last().dropna()
            self._rf = rf.resample('D').last().dropna()

    def calculate(self):

        self._total_return = self._nv.values[-1] / self._nv.values[0] - 1  # 总收益
        self._annal_return = analyser.cal_annal_return(self._nv)
        self._annal_volatility = analyser.cal_annal_volatility(self._nv)

        self._max_drawdown_info = analyser.cal_max_drawdown(self._nv)

        self._calmar = analyser.cal_calmar(self._nv)
        self._sharpe = analyser.cal_sharpe(self._nv, self._rf)
        self._downside_risk = analyser.cal_downside_risk(self._nv, self._bench_nv)

        self._timing_indicator = analyser.cal_timing_indicator(self._nv, self._bench_nv, self._rf)

        if self._bench_flag:

            if analyser.check(self._nv, self._bench_nv, self._rf):  # 检验数据

                self._bench_total_return = self._bench_nv.values[-1] / self._bench_nv.values[0] - 1
                self._bench_annal_return = analyser.cal_annal_return(self._bench_nv)

                self._beta = analyser.cal_beta(self._nv, self._bench_nv, self._rf)

                if self._true_beta:  # 用真实的策略beta计算alpha
                    self._alpha = analyser.cal_alpha(self._nv, self._bench_nv, self._rf, beta=self._beta)
                else:  # 直接计算超额收益，即beta=1
                    self._alpha = analyser.cal_alpha(self._nv, self._bench_nv, self._rf)

                self._information_ratio = analyser.cal_information_ratio(self._nv, self._bench_nv)
                self._tracking_error = analyser.cal_tracking_error(self._nv, self._bench_nv)
                self._Sortino = analyser.cal_sortino(self._nv, self._rf)
                self._Treynor = analyser.cal_treynor(self._nv, self._bench_nv, self._rf)


                # 返回字典信息
                # return {
                #     'annal_return': self._annal_return,
                #     'annal_volatility': self._annal_volatility,
                #     'max_drawdown': self._max_drawdown,
                #     'downside_risk': self._downside_risk,
                #     'beta': self._beta,
                #     'alpha': self._alpha,
                #     'Sharpe': self._sharpe,
                #     'information_rate': self._information_ratio,
                #     'tracking_error': self._information_ratio,
                #     'Sortino': self._Sortino,
                #     'Treynor': self._Treynor
                # }


class IntradayRisk(Risk):
    """
    IntradayRisk类，用于表征日内策略的风险

    Attributes
    ----------

    """

    def __init__(self, net_value: pd.Series):
        """初始化"""

        super().__init__(net_value)



    def calculate(self):
        pass
