# -*- coding: utf-8 -*-

import os
import abc
import logging
from typing import Union

import numpy as np
import pandas as pd

import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from .broker import Broker
from .datasource import DataSource, HDFDataSource
from .enums import Frequency, SecurityType, OrderSide
from .fill import SimpleFillStrategy
from .order import Order
from .params import TradingParams
from .context import Context
from ..utils.exception import NotEnoughMoneyException

plt.style.use('ggplot')

logger = logging.getLogger(__name__)


def new_trading_day(index: pd.DatetimeIndex) -> list:
    """
    根据一个 时间index 生成一个等长度的 list，当对应时间是一个交易日的结束，也就是说下一个时间是一个新的交易日，
    则为 True，否则为 False

    Parameters
    ----------
    index : pd.DatetimeIndex
        时间 index

    Returns
    -------
    list of bool
        与 index 等长度的 list，对应时间如果是一个交易日的结束，则为 True，否则为 False
    """

    __df = pd.DataFrame(index, columns=['cur_time'])
    # 将当前时间向后平移，以判断一天是否结束
    # 2016-12-16 15:00 2016—12-16 21:01
    # 平移5小时后变为
    # 2016-12-16 20:00 2016—12-19 02:01
    # 比较平移滞后的date来判断一天是否结束
    __df['adj_time'] = __df['cur_time'] + pd.Timedelta(5, 'h')

    __df['next_adj_time'] = __df['adj_time'].shift(-1)

    # 调整后的当前时间
    cur_idx_adjusted = pd.DatetimeIndex(__df['adj_time'])

    # 调整后的下一个时间
    next_idx_adjusted = pd.DatetimeIndex(__df['next_adj_time'])

    # 不同日期为新的交易日
    arr = cur_idx_adjusted.day != next_idx_adjusted.day

    # 在一个交易日结束的时候就要标记为 True
    __df['out'] = arr
    __df['out'].fillna(True, inplace=True)

    # 返回 list
    return __df['out'].tolist()


class StrategyEnvironment:
    """
    策略环境变量，用于配置具体策略的运行参数

    策略环境有若干不能通过初始化函数进行调整的默认参数（通常此类参数需要调整的情况不多）

    Notes
    -----
    只支持纯股票策略或者纯期货策略

    Parameters
    ----------
    sids : list
        证券代码列表，譬如 [600000.SH, 600030.SH]，每个证券代码是由交易代码+交易所形成的该证券的 **唯一** 标识
    start : pd.Timestamp or str
        开始时间
    end: pd.Timestamp or str
        结束时间
    init_cash : float
        初始资金数量，默认为 10000000.0
    benchmark : str
        基准代码，默认为 None，即不带基准
    risk_free : str
        无风险收益代码，默认为 None，即不指定无风险利率
    security_type : SecurityType
        证券类型：只支持单一证券类型策略, SecurityType.STOCK, SecurityType.FUTURES, SecurityType.INDEX，默认为 股票
    frequency : Frequency
        数据频率：支持 Frequency.DAILY, Frequency.MINUTE, Frequency.SECOND 三种类型，默认为日频率
    end_day_cancel : bool
        是否需要撤销隔日订单，默认为 False，即不撤销，对日策略有效，但 **日内策略必须设置为 True**
    fill_strategy : FillStrategy
        成交策略，默认为简单成交策略
    align : "intersect" or "union"
        交易时间不一致的交易品种的行情对齐方式，默认为取交集

    Attributes
    ----------
    security_type : SecurityType
        证券类型：只支持单一证券类型策略
    init_positions : dict of Position
        初始头寸，默认为空
    fill_strategy : FillStrategy
        成交策略，默认为简单成交策略
    init_cash : float
        初始资金，默认为 10000000.0

    Methods
    -------
    datasource() -> DataSource
        属性。StrategyEnvironment 内部的数据源，初始化时生成，无法再度修改。

    """

    def __init__(self, sids: list,
                 start: Union[pd.Timestamp, str], end: Union[pd.Timestamp, str],
                 init_cash=10000000.0, benchmark=None, risk_free=None,
                 security_type=SecurityType.STOCK, frequency=Frequency.DAILY,
                 end_day_cancel=False, fill_strategy=SimpleFillStrategy(), align="intersect"):

        # ================
        #     公有字段
        # ================

        # 证券类型
        self.security_type = security_type

        # 初始持仓：默认全部为空
        self.init_positions = {}

        # 成交策略：默认为 SimpleFillStrategy
        self.fill_strategy = fill_strategy

        # 撤销隔夜订单
        # 默认为 False，即不撤销，对日策略有效，但 **日内策略必须设置为 True**
        self.end_day_cancel = end_day_cancel

        # 基准
        self.benchmark = benchmark
        self.risk_free = risk_free

        # 初始资金
        self.init_cash = init_cash

        # ================
        #     私有字段
        # ================

        # 数据源
        # StrategyEnvironment 的数据源只能初始化一次，更改其他参数无法再次生成 datasource
        start_ = pd.to_datetime(start)
        end_ = pd.to_datetime(end)
        self.start = start_
        self.end = end_
        self.__datasource = HDFDataSource(
            sids, start_, end_, benchmark, frequency, align=align)

        # 如果是期货日频数据，自动添加 rollover 标记
        if self.security_type == SecurityType.FUTURES and frequency == Frequency.DAILY:
            for sid in sids:
                # 根据收益率计算调整后的价格序列，.ffill()避免复权价缺失
                adj_price = np.exp(self.__datasource[sid][
                                   'log_return']).cumprod().ffill()
                # 归一化处理
                if sum(adj_price > 0) > 0:
                    adj_price /= adj_price[adj_price > 0].ix[0]
                    ratio = adj_price / self.__datasource[sid]['price']
                else:
                    ratio = np.nan
                self.__datasource[sid]['adj_open'] = self.__datasource[
                    sid]['open'] * ratio
                self.__datasource[sid]['adj_high'] = self.__datasource[
                    sid]['high'] * ratio
                self.__datasource[sid][
                    'adj_low'] = self.__datasource[sid]['low'] * ratio
                self.__datasource[sid]['adj_close'] = self.__datasource[
                    sid]['price'] * ratio

    @property
    def datasource(self) -> DataSource:
        """
        数据源

        Returns
        -------
        DataSource
            返回 StrategyEnvironment 内部的数据源
        """
        return self.__datasource

    def __repr__(self):
        return ', '.join("%s: %s" % item for item in self.__dict__.items())


class Strategy(metaclass=abc.ABCMeta):
    """
    策略基类，同时也是回测的驱动引擎

    Parameters
    ----------
    env: StrategyEnvironment
        策略环境变量，用于配置具体策略的运行参数

    Methods
    -------
    _sids(self) -> list
        属性，获取证券代码
    _pnl(self) -> float
        属性，资产组合当前损益
    _get_quantity(self, sid: str) -> int
        返回指定证券的持仓量
    _get_hist(self, length: int) -> dict
        获取当前时刻指定长度的历史行情数据
    get_portfolio_infos(self) -> pd.DataFrame
        获取资产组合的信息，包括cash, float_profit, commission, total_value
    get_position_holdings(self) -> pd.DataFrame
        获取各个证券的持仓明细
    _add_rolling_indicator(self, indicator_name: str, source_name: str, window: int, func)
        增加滚动指标
    _place_order(self, order) -> bool
        下单
    _cancel_order(self, order: Order, dt: pd.Timestamp)
        撤单
    _on_data(self)
        必须重载的抽象方法，在此实现策略逻辑
    run(self)
        调用此方法进行策略回测
    """

    def __init__(self, env: StrategyEnvironment, name='Strategy'):
        # 策略名称
        self._name = name

        # 策略的运行环境
        self._env = env

        # broker
        self._broker = Broker(env.init_cash, env.init_positions,
                              env.fill_strategy, env.security_type)

        # 游标：用于控制行情遍历的位置，用来获取历史行情
        self._cursor = 0

        # frequency，直接获取数据源的频率
        self._frequency = self._env.datasource.frequency

        # data source
        self._datasource = self._env.datasource

        self.__new_trading_day_list = new_trading_day(
            pd.DatetimeIndex(self._datasource.index))

        # 证券代码列表，直接获取数据源中的证券代码
        self._sids = self._datasource.sids

        # 策略的风险指标
        self._risk = None

    @property
    def _pnl(self):
        """
        返回当前时刻点投资组合当前损益

        Returns
        -------
        tuple
            形如 namedtuple("PnL", ["time", "cash", "float_profit", 
            "total_commission", "total_value"]) 的 tuple
        """
        return self._broker.portfolio.get_pnl()

    def _get_quantity(self, sid: str) -> int:
        """
        返回指定证券的净持仓量（对于股票而言，不包括初始底仓）

        Parameters
        ----------
        sid: str
            证券代码是由交易代码+交易所形成的该证券的 **唯一** 标识

        Returns
        -------
        int
            指定证券的持仓量
        """
        return self._broker.portfolio[sid].quantity

    def _get_hist(self, length: int) -> dict:
        """
        获取当前时刻指定长度的历史数据

        Parameters
        ----------
        length: int
            历史数据的长度

        Returns
        -------
        dict of pd.DataFrame
            历史行情字典
        """
        return self._env.datasource.get_hist(self._cursor, length)

    def get_portfolio_infos(self) -> pd.DataFrame:
        """
        获取投资组合的历史信息

        Returns
        -------
        pd.DataFrame
            各列分别为: ["time", "cash", "float_profit", "total_commission", "total_value"]
        """
        return pd.DataFrame(self._broker.infos).set_index('time')

    def get_position_holdings(self, sid: str) -> pd.DataFrame:
        """
        获取各个证券的历史持仓明细

        Returns
        -------
        pd.DataFrame
        """

        return pd.DataFrame.from_records(self._broker.position_hist[sid], index='time', coerce_float=True)

    def _add_rolling_indicator(self, indicator_name: str, source_name: str, window: int, func):
        """
        增加一个滚动指标

        Notes
        -----
        在 Strategy 的初始化操作中增加一个指标

        Parameters
        ----------
        indicator_name: str
            需要添加的指标的名字
        source_name: str
            源指标的名字
        window: int
            窗口大小，跟数据频率相关
        func: function
            通过一个数组产生一个标量的函数
        """

        if indicator_name in self._env.datasource.columns:
            raise Exception("需要创建的指标 {0} 已经存在".format(indicator_name))

        if source_name not in self._env.datasource.columns:
            raise Exception("源指标 {0} 不存在".format(source_name))

        for sid in self._env.datasource.sids:
            df = self._env.datasource[sid]
            # 直接修改，无需重新赋值
            df[indicator_name] = df[source_name].rolling(window).apply(func)

    def _place_order(self, order: Order) -> bool:
        """
        向 broker 转发订单，在下一阶段由 broker 判断能否成交

        如果订单是市价单，用当前阶段的收盘价计算现金占用（虽然不合理，但会在post_daily_trading里面调整保证金）

        Parameters
        ----------
        order : Order
            策略发出的订单

        Returns
        -------
        bool
            下单成功返回 True，否则返回 False
        """
        # TODO 增加简单校验

        # 转发订单到 broker
        if order.limit_price == 0.0:
            # 市价单的限价只是为了计算现金占用，并非实际成交价格
            order.limit_price = Context.cur_quotes[order.sid].price * 1.1

        return self._broker.place_order(order)

    def _cancel_order(self, order: Order, dt: pd.Timestamp):
        """
        撤销指定订单

        Parameters
        ----------
        order: Order
            需要撤销的订单

        dt: pd.Timestamp
            撤单时间
        """

        # 直接转发到 Broker
        self._broker.cancel_order(order)

    def _cancel_orders(self, dt: pd.Timestamp):
        """
        撤销所有未成交订单

        Parameters
        ----------
        dt: pd.Timestamp
            撤单时间
        """
        self._broker.cancel_orders()

    @abc.abstractmethod
    def _on_data(self):
        """
        抽象方法，在此实现策略的全部逻辑，必要时调用_place_order() 方法下达订单
        """
        raise NotImplementedError('method not defined')

    def _on_quotes(self, quotes: dict):
        """单次行情的响应

        Notes
        -----
        以备跟踪策略时使用

        Parameters
        ----------
        quotes : dict
            当前时刻点的行情字典
                包含一个 time key，value 是当前时间
                其他 key 为证券代码，value 如下形式的 namedtuple 行情
                Quote(Index="2016-01-01", price=15.16, quantity=50200)
        """
        # 记录当前行情
        Context.cur_quotes = quotes
        # 当前时间
        Context.cur_time = quotes['time']

        # 响应最新行情，撮合上一时刻发出的订单，更新组合价格
        # 如果现金不足，则终止模拟
        self._broker.on_quotes()

        # 策略发出信号
        self._on_data()

        # 日收盘操作
        # 当前行情是否是收盘由事先计算好的 __new_trading_day_list 来判断
        if self.__new_trading_day_list[self._cursor]:
            self._broker.post_daily_trading(self._env.end_day_cancel)

        # 增加记录
        self._broker.add_one_record()

        # 记录时间
        Context.pre_quotes = quotes

    def run(self) -> None:
        """
        回放行情进行回测
        """

        # 控制一个 strategy 对象实例只能运行一次
        assert self._cursor == 0, "策略已经运行完毕，不能再次运行"

        # 遍历所有的行情
        for quotes in self._env.datasource:
            try:
                self._on_quotes(quotes)
            except NotEnoughMoneyException:
                break

            # 游标 增加
            self._cursor += 1

    def summary(self, plot=True, path=None):
        """
        打印基本统计信息，绘制基本图形

        Parameters
        ----------
        plot : bool
            是否绘图
        path : str
            文件地址，若提供该字段，则从外部csv文件读取净值数据，必须包含market_value和total_value两列

        Returns
        -------
        Risk
            风险指标
        """
        from .risk import DailyRisk
        from .. import config

        from tabulate import tabulate

        # """基本统计信息"""
        if path is not None:
            # 从外部文件读取
            df = pd.read_csv(path, header=0, index_col=0, parse_dates=True)
            assert 'market_value' in df
            assert 'total_value' in df
        else:
            # 获取策略运行结果
            df = pd.DataFrame(self._broker.infos)
            df.set_index('time', inplace=True)

        # 读取基准行情
        bench = config.DATA_LOADER.read(self._env.benchmark, self._env.start, self._env.end,
                                        frequency=Frequency.DAILY)
        risk_free = config.DATA_LOADER.read(self._env.risk_free, self._env.start, self._env.end,
                                            frequency=Frequency.DAILY)

        # 计算daily风险指标
        if bench is None or risk_free is None:

            self._risk = DailyRisk(df['total_value'] / df['total_value'][0])
        else:
            df['bench'] = bench
            df['risk_free'] = risk_free

            self._risk = DailyRisk(
                df['total_value'] / df['total_value'][0], df['bench'] / df['bench'][0], df['risk_free'])

        self._risk.calculate()

        self._risk.Average_Leverage = (
            df['market_value'] / df['total_value']).mean()

        # 不需要绘图时直接返回 risk
        if not plot:
            return self._risk

        # 基本风险指标
        basic_risk_indicators = []
        basic_risk_indicators.append(
            ['Total Returns', "{0:.3%}".format(self._risk._total_return)])
        basic_risk_indicators.append(
            ['Annual Returns', "{0:.3%}".format(self._risk._annal_return)])
        basic_risk_indicators.append(
            ['Bench Total Returns', "{0:.3%}".format(self._risk._bench_total_return)])
        basic_risk_indicators.append(
            ['Bench Annual Returns', "{0:.3%}".format(self._risk._bench_annal_return)])
        basic_risk_indicators.append(
            ['Alpha', "{0:.4}".format(self._risk._alpha)])
        basic_risk_indicators.append(
            ['Beta', "{0:.4}".format(self._risk._beta)])
        basic_risk_indicators.append(
            ['Annual Volatility', "{0:.4}".format(self._risk._annal_volatility)])
        basic_risk_indicators.append(
            ['MaxDrawdown', "{0:.3%}".format(self._risk._max_drawdown_info[0])])
        basic_risk_indicators.append(['MaxDrawdown Period', "{0} to {1}, {2} days".format(
            self._risk._max_drawdown_info[1], self._risk._max_drawdown_info[2], self._risk._max_drawdown_info[3])]
        )
        basic_risk_indicators.append(
            ['Sharpe', "{0:.4}".format(self._risk._sharpe)])
        basic_risk_indicators.append(
            ['Information Ratio', "{0:.4}".format(self._risk._information_ratio)])

        basic_risk_indicators.append(
            ['Calmar', "{0:.4}".format(self._risk._calmar)])

        basic_risk_indicators.append(
            ['Sortino', "{0:.4}".format(self._risk._Sortino)])

        basic_risk_indicators.append(
            ['Tracking Error', "{0:.4}".format(self._risk._tracking_error)])
        basic_risk_indicators.append(
            ['Downside Risk', "{0:.4}".format(self._risk._downside_risk)])

        basic_risk_indicators.append(
            ['Average Leverage', "{0:.4}".format(self._risk.Average_Leverage)])
        basic_risk_indicators.append(
            ['Win Rate', "{0:.3%}".format(self._risk._timing_indicator[0])])
        basic_risk_indicators.append(
            ['Loss Rate', "{0:.3%}".format(self._risk._timing_indicator[1])])
        basic_risk_indicators.append(
            ['Win Odds', "{0:.5}".format(self._risk._timing_indicator[2])])
        basic_risk_indicators.append(
            ['PL Ratio', "{0:.5}".format(self._risk._timing_indicator[3])])

        # 输出基本指标
        headers = ["indicator", "value"]
        print(tabulate(basic_risk_indicators, headers))

        # """绘制策略表现图"""
        red = "#aa4643"
        fig = plt.figure(self._name, figsize=(18, 12))
        gs = gridspec.GridSpec(5, 2)

        # 1. 净值表现图
        ax_nav = fig.add_subplot(gs[0:2, :])

        # 自动调整坐标显示范围
        ax_nav.get_xaxis().set_minor_locator(matplotlib.ticker.AutoMinorLocator())
        ax_nav.get_yaxis().set_minor_locator(matplotlib.ticker.AutoMinorLocator())

        ax_nav.grid(b=True, which='minor', linewidth=.2)
        ax_nav.grid(b=True, which='major', linewidth=1)

        nav = self._risk._nv
        ax_nav.plot(nav, label=self._name, alpha=1, linewidth=2, color=red)

        leg = plt.legend(loc="upper left")
        leg.get_frame().set_alpha(0.5)
        ax_nav.set_title('Net Value')

        # 在净值上标识最大回撤区间
        md_start = pd.to_datetime(self._risk._max_drawdown_info[1])
        md_end = pd.to_datetime(self._risk._max_drawdown_info[2])

        ax_nav.axvspan(md_start, md_end, facecolor='g', alpha=0.2)

        # 2. 月度收益图
        ax_month_return = plt.subplot(gs[4:, 0])
        nv_mon = df['total_value'].resample('m').last().dropna()
        ret_first = nv_mon.ix[0] / df['total_value'].ix[0] - 1
        ret_mon = nv_mon.pct_change()
        ret_mon.ix[0] = ret_first

        # 自动调整坐标显示范围
        ax_month_return.get_xaxis().set_minor_locator(
            matplotlib.ticker.AutoMinorLocator())
        ax_month_return.get_yaxis().set_minor_locator(
            matplotlib.ticker.AutoMinorLocator())

        if len(ret_mon) <= 6:
            xn = 1
        else:
            xn = int(len(ret_mon) / 6)  # 只显示6个月份时间戳

        ax_month_return.vlines(range(len(ret_mon)), [
                               0], ret_mon.values, linewidth=5, alpha=0.75, color='blue')

        x_labels = [str(a.month) + '/' + str(a.year) for a in ret_mon.index]
        x_labels = [x_labels[i] for i in range(0, len(x_labels), xn)]

        plt.setp(ax_month_return, xticks=[y for y in range(
            0, len(ret_mon), xn)], xticklabels=x_labels)
        plt.setp(ax_month_return, yticks=ax_month_return.get_yticks(),
                 yticklabels=[str(a * 100) + '%' for a in ax_month_return.get_yticks()])
        ax_month_return.set_xlabel('Monthly Return')

        # 3. 日收益分布图
        ax_daily_hist = fig.add_subplot(gs[4:, 1])
        nv_daily = df['total_value'].resample('D').last().dropna()
        ret_first = nv_daily.ix[0] / df['total_value'].ix[0] - 1
        ret_daily = nv_daily.pct_change()
        ret_daily.ix[0] = ret_first

        mu = ret_daily.mean()
        sigma = ret_daily.std()
        num_bins = 50
        n, bins, patches = ax_daily_hist.hist(
            ret_daily.values, num_bins, normed=1, facecolor='green', alpha=0.5)
        y = mlab.normpdf(bins, mu, sigma)
        step = bins[1] - bins[0]
        yticks = ax_daily_hist.get_yticks()
        plt.setp(ax_daily_hist, yticks=yticks, yticklabels=[
                 str(a * step)[:4] for a in yticks])

        ax_daily_hist.plot(bins, y, 'r--')                   # 画拟合的正太分布图
        ax_daily_hist.plot([mu, mu], [0, yticks[-1]], 'k:')  # 画日均值的竖线
        ax_daily_hist.set_xlabel('Daily Return')
        ax_daily_hist.set_ylabel('Probability')

        # 4. 回撤图
        ax = fig.add_subplot(gs[2, :], sharex=ax_nav)
        ax.fill_between(self._risk._max_drawdown_info[
                        -1].index, self._risk._max_drawdown_info[-1].values, color='#778899', label='Daily Drawdown')
        ax.set_ylim(top=0)
        leg = plt.legend(loc="lower left", fontsize='medium')
        leg.get_frame().set_alpha(0.5)

        # 5. 杠杆率图
        ax = fig.add_subplot(gs[3, :], sharex=ax_nav)
        ax.plot((df['market_value'] / df['total_value']).resample('D').last().dropna(),
                color='DarkOrange', label='Leverage Ratio')
        ax.hlines(self._risk.Average_Leverage, nav.index[
                  0], nav.index[-1], linestyles='dotted', label='Average Level')
        ax.set_ylim(bottom=0)
        leg = plt.legend(loc="upper left", fontsize='medium')
        leg.get_frame().set_alpha(0.5)

        # 显示图形
        plt.show()
        if not os.path.exists('./backtest_results'):
            os.mkdir('./backtest_results')
        plt.savefig('./backtest_results/summary.jpeg', dpi=300)
