# -*- coding: utf-8 -*-

import abc
from random import random

from .enums import OrderSide, OrderStatus
from .order import Order


class FillStrategy(metaclass=abc.ABCMeta):
    """
    订单成交策略基类

    Methods
    -------
    fill_order(self, order, quote) -> bool:
        抽象方法。根据 行情 数据成交订单。成交时返回 True 否则返回 False
    """

    @abc.abstractmethod
    def fill_order(self, order: Order, quote: tuple) -> bool:
        """
        根据 行情 数据成交订单

        Parameters
        ----------
        order: Order
            指定的订单

        quote: tuple (namedtuple)

            包含当前时刻的证券行情 tuple

            Quote(Index="2016-01-01", price=15.16, quantity=50200)

        Returns
        -------
        bool
            成交时返回 True 否则返回 False
        """


class SimpleFillStrategy(FillStrategy):
    """
    假设订单全部以开盘价成交
    """

    def fill_order(self, order: Order, quote: tuple) -> bool:
        if quote.open > 0:
            order.status = OrderStatus.FILLED
            order.filled_price = quote.open
            order.filled_qty = order.quantity
            order.end_time = quote.Index
            return True
        else:
            return False


class IntradayProbabilityFillStrategy(FillStrategy):
    """
    日内交易概率成交策略

    根据盘口和成交价判断成交

    Attributes
    ----------
    prob: float
        报价在当前最优档位的成交概率
    """

    def __init__(self, prob=0.2):
        self._prob = prob

    def fill_order(self, order: Order, quote: tuple) -> bool:
        """
        根据 行情 数据成交订单

        Parameters
        ----------
        order: Order
            指定的订单

        quote: tuple (namedtuple)

            包含当前时刻的证券行情 tuple

            Quote(Index="2016-01-01", price=15.16, quantity=50200)

        Returns
        -------
        bool
            成交时返回 True 否则返回 False

        """

        price = quote.price
        dt = quote.Index

        if price > 0:
            # 提取其他字段
            ask_price = quote.ask_price
            bid_price = quote.bid_price
            side = quote.side

            # ** 判断成交 **
            order_price = order.price

            # 如果是 买
            if order.side == OrderSide.BUY:
                # 下一个盘口卖价小于等于限价，成交
                if order_price >= ask_price:
                    order.status = OrderStatus.FILLED
                    order.filled_price = order_price  # 假设限价成交
                    order.filled_qty = order.quantity
                    order.end_time = dt
                    return True

                # 订单位于 买一价，概率成交：只有向下卖的时候可能成交
                elif order_price == bid_price:
                    if side == -1 and random() < self._prob:
                        order.status = OrderStatus.FILLED
                        order.filled_price = order_price  # 假设限价成交
                        order.filled_qty = order.quantity
                        order.end_time = dt
                        return True
                # 其余的都不能成交
                else:
                    return False

            # 如果是卖
            elif order.side == OrderSide.SELL:
                # 下一个盘口的买价大于等于限价，成交
                if order_price <= bid_price:
                    order.status = OrderStatus.FILLED
                    order.filled_price = order_price  # 假设限价成交
                    order.filled_qty = order.quantity
                    order.end_time = dt
                    return True
                # 订单位于 卖一价，概率成交：只有向上买的时候可能成交
                elif order_price == ask_price:
                    if side == 1 and random() < self._prob:
                        order.status = OrderStatus.FILLED
                        order.filled_price = order_price  # 假设限价成交
                        order.filled_qty = order.quantity
                        order.end_time = dt
                        return True
                else:
                    return False
            else:
                raise Exception("unrecognized order side")

        # 价格为 nan 或者 小于0，返回 False
        else:
            return False


class FixedPercentSlippageFillStrategy(FillStrategy):
    """
    固定比例冲击成本成交模型
    """

    def __init__(self, rate=0.001):
        """
        初始化成交模型

        Parameters
        ----------
        rate: float
            固定的冲击成本比例，默认为 千分之一
        """
        pass

    def fill_order(self, order: Order, quote: tuple) -> bool:
        pass
