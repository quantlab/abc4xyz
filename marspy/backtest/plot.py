# -*- coding: utf-8 -*-

from typing import Union
import os

import numpy as np
import pandas as pd

import plotly
import plotly.graph_objs as go
from plotly.offline import iplot, plot
from plotly.tools import FigureFactory as FF

plotly.offline.init_notebook_mode()


class NotebookPlot:
    """
    适用于Notebook的画图类，成员方法为各个不同的画图方法

    Parameters
    ----------
    stg: Strategy instance
    is_notebook: bool
        运行环境是否为notebook，否则在html窗口画图

    Methods
    -------
    profit_line_plot(sids: Union[str, list])
        画出资产组合累计收益曲线与各头寸收益曲线，直观反映每个头寸的收益贡献
    profit_bar_plot(sids: Union[str, list])
        画出指定证券最终的损益情况
    pnl_plot():
        生成可视化表格，记录每个月份投资组合的收益情况
    holdings_plot(self, sid):
        反映指定证券的持仓情况
    """

    def __init__(self, stg, is_notebook=True):
        self._stg = stg
        self._is_notebook = is_notebook

    def profit_line_plot(self, sids: Union[str, list]):
        """画出资产组合累计收益曲线与各头寸收益曲线，直观反映每个头寸的收益贡献

        Parameters
        ----------
        sids : Union[str, list]
            需要绘制图形的证券代码列表
        """
        df = pd.DataFrame.from_records(self._stg._broker.infos,
                                       columns=['time', 'cash', 'float_profit', 'commission',
                                                'total_value', 'market_value'])
        df.set_index('time', inplace=True)
        index = df.index
        traces = []
        traces.append(go.Scatter(
            x=index,
            y=df["total_value"] - df['total_value'].ix[0],
            name='Portfolio',
            marker = dict(color="#aa4643")
        ))

        # 如果是单一证券，wrap成list
        if isinstance(sids, str):
            sids = [sids]

        for sid in sids:
            if sid not in self._stg._sids:
                continue

            h = self._stg.get_position_holdings(sid)
            s = h.total_profit
            traces.append(
                go.Scatter(
                    x=index,
                    y=s,
                    name=sid,
                )
            )
        data = traces

        layout = go.Layout(
            # barmode='group',

            title='Portfolio P&L vs Individual Asset P&L',
            yaxis=dict(
                title='P&L'
            )
        )

        fig = go.Figure(data=data, layout=layout)
        if self._is_notebook:
            iplot(fig, filename='individual_plot', show_link=False)
        else:
            if not os.path.exists('./backtest_results'):
                os.mkdir('./backtest_results')
            plot(fig, filename='backtest_results/individual_plot.html', show_link=False)

    def profit_bar_plot(self, sids: Union[str, list]):
        """
        各个品种的最终损益 Bar 图

        Parameters
        ----------
        sids : Union[str, list]
            需要绘制图形的证券代码列表
        """

        x_ = []
        y_ = []

        # 如果是单一证券，wrap成list
        if isinstance(sids, str):
            sids = [sids]

        for sid in sids:
            if sid not in self._stg._sids:
                continue

            x_.append(sid)
            y_.append(self._stg.get_position_holdings(sid).ix[-1].total_profit)

        trace = go.Bar(
            x = x_,
            y = y_
        )

        data = [trace]
        layout = go.Layout(
            title='Final Profit By Sid',
        )

        fig = go.Figure(data=data, layout=layout)
        if self._is_notebook:
            iplot(fig, filename='profit_bar', show_link=False)
        else:
            if not os.path.exists('./backtest_results'):
                os.mkdir('./backtest_results')
            plot(fig, filename='backtest_results/profit_bar.html', show_link=False)

    def pnl_plot(self, sids: Union[str, list]):
        """生成可视化表格，记录每个月份投资组合的收益情况

        Parameters
        ----------
        sids : Union[str, list]
            需要绘制图形的证券代码列表
        """
        # 获取指定列表的历史收益，df的每一列代表一只标的的累计总损益
        df = pd.DataFrame(np.full((len(self._stg._env.datasource.index), len(sids)), np.nan),
                          index=self._stg._env.datasource.index,
                          columns=sids)
        for sid in sids:
            df[sid] = self._stg.get_position_holdings(sid).total_profit
        total_value = df.sum(axis=1) + self._stg._env.init_cash

        monthly_return = np.log(total_value / total_value.shift(1)).resample('M').sum()
        start_year = monthly_return.index[0].year
        end_year = monthly_return.index[-1].year
        months = [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
            'Total'
        ]
        years = np.arange(start_year, end_year + 1)
        pnl_df = pd.DataFrame(np.full((end_year - start_year + 1, 13), np.nan), index=years, columns=months)
        for y in years:
            s = monthly_return[str(y)].map(lambda x: '{0:.2%}'.format(x))
            n = len(s)
            if y == start_year:
                pnl_df.ix[y, 12-n:-1] = s.values
            elif y == end_year:
                pnl_df.ix[y, 0:n] = s.values
            else:
                assert len(s) == 12
                pnl_df.ix[y, :-1] = s.values
            # 统计全年总收益
            pnl_df.ix[y, -1] = '{0:.2%}'.format(monthly_return[str(y)].sum())

        table = FF.create_table(pnl_df, index=True, index_title='Year')
        if self._is_notebook:
            iplot(table, filename='pnl_plot', show_link=False)
        else:
            if not os.path.exists('./backtest_results'):
                os.mkdir('./backtest_results')
            plot(table, filename='backtest_results/pnl_plot.html', show_link=False)

    def holdings_plot(self, sid: str, window=10, adjust=False):
        """反映单一证券的持仓情况

        Parameters
        ----------
        sid : str
        window : int
            指数平滑周期
        adjust : bool
            是否绘制复权价
        """
        assert isinstance(sid, str), "只支持获取单一证券的历史持仓信息"
        assert sid in self._stg._sids, "证券代码不存在"

        index = self._stg._datasource.index
        df = self._stg._datasource[sid]

        if adjust:
            open_, high, low, close = df.adj_open, df.adj_high, df.adj_low, df.adj_price
        else:
            open_, high, low, close = df.open, df.high, df.low, df.price

        # Make increasing ohlc sticks and customize their color and name
        fig_increasing = FF.create_candlestick(open_, high, low, close, dates=index,
            direction='increasing',
            line=go.Line(color='rgb(255, 0, 0)'))

        # Make decreasing ohlc sticks and customize their color and name
        fig_decreasing = FF.create_candlestick(open_, high, low, close, dates=index,
            direction='decreasing',
            line=go.Line(color='rgb(0, 128, 0)'))

        # Initialize the figure
        fig = fig_increasing

        # Add decreasing data with .extend()
        fig['data'].extend(fig_decreasing['data'])

        # Add exponential moving average
        add_line = go.Scatter(
            x=index,
            y=close.ewm(span=window).mean(),
            name= '{0}-day EMA'.format(window),
            line=go.Line(color='black')
            )
        fig['data'].extend([add_line])

        # Add closing price
        add_line = go.Scatter(
            x=index,
            y=close,
            name= 'Close',
            line=go.Line(color='blue')
            )
        fig['data'].extend([add_line])

        # Add holdings
        add_bar = go.Bar(
                x=index,
                y=self._stg.get_position_holdings(sid).quantity,
                yaxis='y2',
                marker=dict(
                color='#ff7f00'),
                name='holdings'
            )
        fig['data'].extend([add_bar])

        layout = go.Layout(
            title='Holdings of {0}'.format(sid),
            yaxis=dict(
                title='Price',
                domain=[0.3, 1]
            ),
            yaxis2=dict(
                title='Quantity',
                domain=[0., 0.25]
            )
        )

        fig = go.Figure(data=fig['data'], layout=layout)
        if self._is_notebook:
            iplot(fig, filename='holdings_plot', show_link=False)
        else:
            if not os.path.exists('./backtest_results'):
                os.mkdir('./backtest_results')
            plot(fig, filename='backtest_results/{0} holdings_plot.html'.format(sid))

    def __repr__(self):
        return "Marsplot({0})".format(self.__dict__)
