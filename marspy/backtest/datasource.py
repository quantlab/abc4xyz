# -*- coding: utf-8 -*-

import abc

import pandas as pd

from .enums import Frequency
from .. import config


class DataSource(metaclass=abc.ABCMeta):
    """
    数据源基类, DataSource 是一个由 {str:dataframe} 组成的数据集，每个 key 对应于一个证券，每个 dataframe 都具有相同的 pd.DatatimeIndex

    Notes
    -----
    所有的 DataFrame 应该具有相同的 pd.DatatimeIndex，对于交易时间不一致的标的，按照 align 进行对齐处理

    Parameters
    ----------
    sids: list
        证券代码列表，譬如 [600000.SH, 600030.SH]，每个证券代码是由交易代码+交易所形成的该证券的 **唯一** 标识
    frequency: Frequency
        数据频率：支持 Frequency.DAILY, Frequency.MINUTE, Frequency.SECOND 三种类型

    Attributes
    ----------
    _sids: str
        证券代码列表，譬如 [600000.SH, 600030.SH]，每个证券代码是由交易代码+交易所形成的该证券的 **唯一** 标识
    _frequency: Frequency
        数据频率：支持 Frequency.DAILY, Frequency.MINUTE, Frequency.SECOND 三种类型
    _data: dict of pd.DataFrame
        每个键代表一个证券，DataFrame的各列代表证券的数据信息，如 OHLC
        DataFrame的索引类型必须是 pd.DatatimeIndex 且每个证券的索引必须对齐
    _columns: list of str
        pd.DataFrame 的列名，默认假设所有 DataFrame 的列名都一样
    _iter: dict of iter
        对应于 _data，key 为 sid，value 为 DataFrame.itertuples()，用于迭代
    _index: pd.DatetimeIndex
        对齐之后共有的时间索引转换成的 list
    _size: int
        共有的时间索引的长度

    """

    def __init__(self, sids: list, frequency: Frequency):
        self._sids = list(set(sids))  # 去掉重复
        self._frequency = frequency
        self._data = {}
        self._columns = None
        self._iter = {}
        self._index = []  # 对齐之后共有的时间索引转换成的 list 初始化为空
        self._size = 0

        self._nrow = 0  # 迭代器位置标记

    def _check_valid(self):
        """
        检查数据是否符合规范，尤其是是否 DataFrame 是否具有必要的字段
        """
        assert len(self._sids) > 0, "证券代码列表不能为空"

        # 必须字段
        assert 'price' in self._columns, "数据必须包含 price 字段"

        # 日或者分钟数据需要有开\高\低字段
        if self._frequency == Frequency.DAILY or self._frequency == Frequency.MINUTE:
            assert 'open' in self._columns, "数据必须包含 'open' 字段"
            assert 'high' in self._columns, "数据必须包含 'high' 字段"
            assert 'low' in self._columns, "数据必须包含 'low' 字段"
            assert 'amount' in self._columns, "数据必须包含 'amount' 字段"
            assert 'volume' in self._columns, "数据必须包含 'volume' 字段"
            # assert 'vwap' in self._columns, "数据必须包含 'vwap' 字段"

        # 秒钟数据要有一档盘口
        else:
            assert 'sp01' in self._columns, "数据必须包含 'sp01' 卖一价 字段"
            assert 'sv01' in self._columns, "数据必须包含 'sv01' 卖一量 字段"
            assert 'bp01' in self._columns, "数据必须包含 'bp01' 买一价 字段"
            assert 'bv01' in self._columns, "数据必须包含 'bv01' 买一量 字段"

    @property
    def sids(self) -> list:
        return self._sids

    @property
    def columns(self) -> list:
        return self._columns

    @property
    def index(self) -> list:
        return self._index

    @property
    def frequency(self):
        return self._frequency

    @property
    def data(self):
        return self._data

    def __getitem__(self, sid) -> pd.DataFrame:
        """
        获取数据源底层的数据

        Parameters
        ----------
        sid: str
            证券代码：形如 600000.SH，交易代码+交易所形成的该证券的 **唯一** 标识

        Returns
        -------
        pd.DataFrame
            sid 对应的 DataFrame
        """
        return self._data[sid]

    def __len__(self):
        """
        获取 DataSource 的长度，即 共有的时间索引 的长度

        Returns
        -------
        int
            所有 DataFrame 共有的 DatetimeIndex 的长度
        """
        return self._size

    def __iter__(self):
        # 初始化迭代器位置
        self._nrow = 0
        # 生成 _iter 字典
        for sid, df in self._data.items():
            self._iter[sid] = df.itertuples(index=True, name='Quote')

        return self

    def __next__(self) -> dict:
        """
        获取下一个时刻点的行情字典

        Returns
        -------
        dict
            其中一个 key 为 time，value为当前时间
            其他 key 为证券代码，value为当前的行情(namedtuple)，如
                Quote(Index="2016-01-01", price=15.16, quantity=50200)
        """
        bars = {}

        # 对每个证券
        for sid, it in self._iter.items():
            bars[sid] = next(it)
        bars['time'] = self._index[self._nrow]
        self._nrow += 1  # 移动 + 1

        return bars

    def get_hist(self, until: int, length: int) -> dict:
        """
        获取指定段的数据

        Parameters
        ----------
        until: int
            截止的位置
        length: int
            往回取的长度

        Returns
        -------
        dict of pd.DataFrame
            所有标的指定段的历史数据
        """
        assert length > 0, "所取数据长度必须大于 0, length = {0}".format(length)

        hists = {}
        if length > until + 1:
            return hists

        for sid, df in self._data.items():
            hists[sid] = df[until - length + 1: until + 1]

        return hists


class HDFDataSource(DataSource):
    """
    从 HDFDataLoader 中读取数据生成的 DataSource

    Notes
    -----
    所有的 DataFrame 应该具有相同的 pd.DatatimeIndex，对于交易时间不一致的标的，按照 align 进行对齐处理

    Parameters
    ----------
    sids: list
        证券代码列表，譬如 [600000.SH, 600030.SH]，每个证券代码是由交易代码+交易所形成的该证券的 **唯一** 标识
    start: pd.Timestamp
        开始时间
    end: pd.Timestamp
        结束时间
    benchmark: str or None
        基准代码
    frequency: Frequency
        数据频率：支持 Frequency.DAILY, Frequency.MINUTE, Frequency.SECOND 三种类型
    align: "intersect" or "union"
        交易时间不一致的交易品种的对齐方式

    """

    def __init__(self, sids: list, start: pd.Timestamp, end: pd.Timestamp, benchmark: str, frequency: Frequency,
                 align: str):
        super().__init__(sids, frequency)
        # 对去掉重复的每一个证券，读取指定的行情
        for sid in self._sids:
            # HDFDataLoader 适用于一次读取一个证券的数据
            df = config.DATA_LOADER.read(sid, start, end, frequency)

            if not isinstance(df.index, pd.DatetimeIndex):
                raise Exception("DataFrame 的 index 必须是 pd.DatetimeIndex 类型")

            self._data[sid] = df

            if self._columns is None:
                self._columns = df.columns

            # 对齐时间索引：会排序
            if len(self._index) == 0:
                self._index = df.index
            else:
                if align == "intersect":
                    self._index = self._index.intersection(df.index)  # 交集
                elif align == "union":
                    self._index = self._index.union(df.index)  # 并集
                else:
                    raise Exception("不支持的对齐方式: ", align)

        # 基准数据
        if benchmark is not None:
            if isinstance(benchmark, str):
                bench = config.DATA_LOADER.read(benchmark, start, end, frequency)

                if not isinstance(bench.index, pd.DatetimeIndex):
                    raise Exception("DataFrame 的 index 必须是 pd.DatetimeIndex 类型")

                self._data[benchmark] = bench
            else:
                raise Exception("benchmark must be a sid")

            # 对齐时间索引：会排序
            if self._index is None:
                self._index = bench.index
            else:
                if align == "intersect":
                    self._index = self._index.intersection(bench.index)  # 交集
                elif align == "union":
                    self._index = self._index.union(bench.index)  # 并集
                else:
                    raise Exception("不支持的对齐方式: ", align)

        # 重设 index
        for sid, df in self._data.items():
            self._data[sid] = df.reindex(self._index)

        # 将 index 转换成 list
        self._index = self._index.tolist()

        # 设置长度
        self._size = len(self._index)

        # 检查数据有效性
        self._check_valid()
