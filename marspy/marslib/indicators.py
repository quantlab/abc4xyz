# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd

# 防止出现不必要的warning
np.seterr(divide='ignore', invalid='ignore')


def TrueRange(high: pd.Series, low: pd.Series, pre_close: pd.Series) -> tuple:
    """    
    Description
    -----------
    https://www.incrediblecharts.com/indicators/average_true_range.php
    
    True Range High (TRH) is the greater of:
        High [today] and Closing price [yesterday]
    
    True Range Low (TRL) is the lesser of:
        Low [today] and Closing price [yesterday]
        
    True Range = TRH - TRL
    
    Returns
    -------
    TRH, TRL: tuple of pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert isinstance(pre_close, pd.Series)
    assert high.size == low.size == pre_close.size

    df = pd.DataFrame({'high': high, 'low': low, 'pre_close': pre_close})

    return df[['high', 'pre_close']].max(axis=1), df[['low', 'pre_close']].min(axis=1)


def ATR(high: pd.Series, low: pd.Series, pre_close: pd.Series, periods=14) -> pd.Series:
    """
    Exponential moving average* of the True Range
    
    ATR = ((n-1)ATR_{t-1} + TR) / n
    
    Returns
    -------
    ATR: pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert isinstance(pre_close, pd.Series)
    assert high.size == low.size == pre_close.size

    TRH, TRL = TrueRange(high, low, pre_close)
    return (TRH - TRL).ewm(alpha=1. / periods).mean()


def ChaikinMoneyFlow(high: pd.Series, low: pd.Series, close: pd.Series, volume: pd.Series, periods=21) -> pd.Series:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/chaikin_money_flow.php
    
    The formula first positions the close in relation to the trading range:

        {(Close - Low) - (High - Close)} / {High - Low}

    and then calculates AD by multiplying the result by the day's volume:

        {(Close - Low) - (High - Close)} / {High - Low} * Volume

    The Chaikin Money Flow sums AD for 21 days and then divides the result by volume for the identical period. 
    Volume is used as the divisor so that the indicator will fluctuate between a maximum of 1 and a minimum of -1.
    
    Returns
    -------
    CMF: pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert isinstance(close, pd.Series)
    assert isinstance(volume, pd.Series)
    assert high.size == low.size == close.size == volume.size

    def AD_cal(x):
        if x[0] - x[1] != 0:
            return ((x[2] - x[1]) - (x[0] - x[2])) / (x[0] - x[1]) * x[3]
        else:
            return 0.

    AD = pd.Series(np.fromiter(map(AD_cal, zip(high, low, close, volume)), np.float), index=volume.index)

    return AD.rolling(window=periods).sum() / volume.rolling(window=periods).sum()


def TwiggsMoneyFlow(high: pd.Series, low: pd.Series, close: pd.Series, pre_close: pd.Series, volume: pd.Series,
                    periods=21) -> pd.Series:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/twiggs_money_flow.php
    
    Calculate True Range High and True Range Low:
    True Range High (TRH) is the greater of:
        High [today] and Closing price [yesterday]
    
    True Range Low (TRL) is the lesser of:
        Low [today] and Closing price [yesterday]
    
    Calculate AD using True Range High and True Range Low:

    AD = {(Close - TRL) - (TRH - Close)} / {TRH - TRL} * Volume

    Apply exponential smoothing* to AD:
    Calculate AD[21] as the sum of AD for the first 21 days
    On the next day, multiply AD[21] by 20/21 and add AD for day 22
    Repeat this process for each subsequent day**.
    
    Do the same with the divisor:
    Calculate V[21] as the sum of volume for the first 21 days
    On the next day, multiply V[21] by 20/21 and add Volume for day 22
    Repeat this process for each subsequent day**

    Divide AD[21] by V[21]:
    Twiggs Money Flow = AD[21] / V[21] expressed as a percentage.
    
    Returns
    -------
    TMF: pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert isinstance(close, pd.Series)
    assert isinstance(pre_close, pd.Series)
    assert isinstance(volume, pd.Series)
    assert high.size == low.size == close.size == volume.size

    TRH, TRL = TrueRange(high, low, pre_close)

    def AD_cal(x):
        if x[0] - x[1] != 0:
            return ((x[2] - x[1]) - (x[0] - x[2])) / (x[0] - x[1]) * x[3]
        else:
            return 0.

    AD = pd.Series(np.fromiter(map(AD_cal, zip(TRH, TRL, close, volume)), np.float), index=volume.index)

    valid_AD = AD[~AD.isnull()]
    valid_volume = volume[~volume.isnull()]
    sum_AD = valid_AD.rolling(window=periods).sum()
    sum_volume = valid_volume.rolling(window=periods).sum()

    for i in range(periods, valid_AD.size):
        sum_AD.ix[i] = (periods - 1) / periods * sum_AD.ix[i - 1] + valid_AD.ix[i]

    for i in range(periods, valid_volume.size):
        sum_volume.ix[i] = (periods - 1) / periods * sum_volume.ix[i - 1] + valid_volume.ix[i]

    return (sum_AD / sum_volume).reindex(volume.index)


def StochasticOscillator(high: pd.Series, low: pd.Series, close: pd.Series, obs_periods=9, k_periods=3,
                         d_periods=3) -> tuple:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/stochastic.php
    
    %K
    CL = Close [today] - Lowest Low [in %K Periods] 
    HL =Highest High [in %K Periods] - Lowest Low [in %K Periods] 
    RSV(raw stochastic value) = CL / HL *100
    
    %K is the simple moving average of RSV
    %D is the simple moving average of %D
    
    Returns
    -------
    K, D: tuple of pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert isinstance(close, pd.Series)
    assert high.size == low.size == close.size

    CL = close - low.rolling(window=obs_periods).min()
    HL = high.rolling(window=obs_periods).max() - low.rolling(window=obs_periods).min()
    RSV = CL / HL * 100
    K = RSV.rolling(window=k_periods).mean()
    D = K.rolling(window=d_periods).mean()

    return K, D


def Aroon(high: pd.Series, low: pd.Series, periods=14) -> tuple:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/aroon_oscillator.php
    
    Determine the time period ("n")
    Count back the number of days from the end of the period to the highest high for the period ("HH")
    Calculate Aroon Up using the formula [( n - HH )/ n ] x 100%
    
    For example, if the period is 25 days and the highest high is today (HH=0), Aroon Up = (25-0)/25 x 100% = 100%. 
    If the highest high occurred 10 days ago (HH=10), then the current value would be (25-10)/25 x 100% = 60%.
    
    Count back the number of days from the end of the period to the lowest low for the period ("LL")
    Calculate Aroon Down using the formula [( n - LL )/ n ] x 100% 
    
    Returns
    -------
    AroonUp, AroonDown: tuple of pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert high.size == low.size

    HH = high.rolling(window=periods).apply(lambda x: periods - 1 - x.argmax())
    LL = low.rolling(window=periods).apply(lambda x: periods - 1 - x.argmin())
    Aroon_up = (periods - HH) / periods * 100
    Aroon_down = (periods - LL) / periods * 100

    return Aroon_up, Aroon_down


def ChandeMomentumOscillator(close: pd.Series, pre_close: pd.Series, periods=14) -> pd.Series:
    """
    Description
    -----------
    https://www.tradingtechnologies.com/help/x-study/technical-indicator-definitions/chande-momentum-oscillator-cmo/

    Sum_up = sum of (close - pre_close) where close > pre_close
    Sum_down = sum of (pre_close - close) where close < pre_close
    
    CMO = (Sum_Up - Sum_Down) / (Sum_Up + Sum_Down) * 100
    
    Returns
    -------
    CMO: pd.Series
    """
    assert isinstance(close, pd.Series)
    assert isinstance(pre_close, pd.Series)
    assert close.size == pre_close.size

    diff = close - pre_close
    sum_up = diff.rolling(window=periods).apply(lambda x: x[x > 0].sum())
    sum_down = diff.rolling(window=periods).apply(lambda x: -x[x < 0].sum())

    return (sum_up - sum_down) / (sum_up + sum_down) * 100


def RelativeStrengthIndex(close: pd.Series, pre_close: pd.Series, periods=14) -> pd.Series:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/rsi_relative_strength_index.php
    
    Compare Closing price [today] to Closing price [yesterday].
    upward movements = Closing price [today] - Closing price [yesterday] if Closing price [today] > Closing price [yesterday]
    downward movements = Closing price [yesterday] - Closing price [today] if Closing price [today] < Closing price [yesterday]
    
    Apply exponential moving average* to the upward and downward movements:
    Average Upward Price Move = 1/14 * Upward Movements + 13/14 * Average Upward Price Move [yesterday]
    Average Downward Price Move = 1/14 * Downward Movements + 13/14 * Average Downward Price Move [yesterday]

    RS = Average Upward Price Move / Average Downward Price Move 
    
    Calculate the Relative Strength Index (RSI): 
    
    RSI = 100 - 100 / ( 1 + RS ) = avg_up / (avg_up + avg_down)
    
    Returns
    -------
    RSI: pd.Series
    """
    assert isinstance(close, pd.Series)
    assert isinstance(pre_close, pd.Series)
    assert close.size == pre_close.size

    diff = close - pre_close

    def Up(x):
        if np.isnan(x):
            return np.nan
        elif x > 0:
            return x
        elif x < 0:
            return 0.

    def Down(x):
        if np.isnan(x):
            return np.nan
        elif x > 0:
            return 0
        elif x < 0:
            return -x

    avg_up = diff.map(Up).ewm(alpha=1. / periods).mean()
    avg_down = diff.map(Down).ewm(alpha=1. / periods).mean()
    RS = avg_up / avg_down

    return 100 - 100 / (1 + RS)


def CommodityChannelIndex(high: pd.Series, low: pd.Series, close: pd.Series, periods=20) -> pd.Series:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/commodity_channel_index.php
    
    Calculate Typical Price ("TP"): 

        (High + Low + Close) / 3 
    
    Calculate TPMA: a 20-day simple MA of TP.
    
    Calculate MeanDeviation:
        Subtract today's TPMA from TP for each of the last 20 days.
        Sum the absolute values and divide by 20.
    
    CCI = (TP - TPMA) / (0.015 * MeanDeviation)
    
    Returns
    -------
    CCI: pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert isinstance(close, pd.Series)
    assert high.size == low.size == close.size

    TP = (high + low + close) / 3
    TPMA = TP.rolling(window=periods).mean()
    MD = (TP - TPMA).abs().rolling(window=periods).mean()

    return (TP - TPMA) / (0.015 * MD)


def MACD(close: pd.Series, fast_periods=12, slow_periods=26, signal_periods=9) -> tuple:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/macd.php

    MACD = 12 Day exponential moving average - 26 Day exponential moving average
    
    The signal line is calculated as a 9 day exponential moving average of MACD
    
    Returns
    -------
    macd, signal: pd.Series
    """
    assert isinstance(close, pd.Series)

    macd = close.ewm(span=fast_periods).mean() - close.ewm(span=slow_periods).mean()
    signal = macd.ewm(span=signal_periods).mean()

    return macd, signal


def ADX(high: pd.Series, low: pd.Series, pre_close: pd.Series, periods=14) -> pd.Series:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/directional_movement.php#directional_movement-formula
    
    Calculate the Directional movement for today
    +DM = Today's High - Yesterday's High (when price moves upward)
    -DM = Yesterday's Low - Today's Low (when price moves downward)
    You cannot have both +DM and -DM on the same day. If there is an outside day (where both calculations are positive),
    then the larger of the two results is taken. An inside day (where both calculations are negative) will always equal zero.
    
    Calculate the true range for the day. 
    
    +DM14 = exponential moving average* of +DM
    -DM14 = exponential moving average* of -DM
    TR14 = exponential moving average of True Range 
    
    Next, calculate the Directional Indicators:
    +DI14 = +DM14 divided by TR14
    -DI14 = -DM14 divided by TR14
    
    Then, calculate the components of the Average Directional Movement Index (ADX):
    Calculate the DI Difference:
    Record the difference between +DI14 and -DI14 as a positive number.
    
    Calculate the Directional Index (DX):
    DX = DI Difference divided by the sum of +DI14 and -DI14
    
    ADX = the exponential moving average* of DX
    
    Returns
    -------
    ADX: pd.Series
    """
    assert isinstance(high, pd.Series)
    assert isinstance(low, pd.Series)
    assert isinstance(pre_close, pd.Series)
    assert high.size == low.size == pre_close.size

    DM_plus = high - high.shift(1)
    DM_minus = low.shift(1) - low
    DM_plus.ix[(DM_plus < DM_minus) | (DM_plus < 0)] = 0
    DM_minus.ix[(DM_minus < DM_plus) | (DM_minus < 0)] = 0

    EWM_DM_plus = DM_plus.ewm(alpha=1. / periods).mean()
    EWM_DM_minus = DM_minus.ewm(alpha=1. / periods).mean()
    ATR_ = ATR(high, low, pre_close, periods)

    DI_plus = EWM_DM_plus / ATR_
    DI_minus = EWM_DM_minus / ATR_

    diff = (DI_plus - DI_minus).abs()
    DX = diff / (DI_plus + DI_minus)

    return DX.ewm(alpha=1. / periods).mean() * 100


def DetrendedPriceOscillator(close: pd.Series, periods=14) -> pd.Series:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/detrended_price_oscillator.php
    
    Decide on the time frame that you wish to analyze. Set n as half of that cycle period.
    Calculate a simple moving average for n periods.
    Calculate    (n / 2 + 1)
    Subtract the moving average, from (n / 2 + 1) days ago, from the closing price: 

    DPO = Close - Simple moving average [from (n / 2 + 1) days ago]
    
    Returns
    -------
    DPO: pd.Series
    """
    assert isinstance(close, pd.Series)

    return close - close.rolling(window=periods).mean().shift(periods // 2 + 1)


def VolatilityRatio(high: pd.Series, low: pd.Series, pre_close: pd.Series, periods=14) -> pd.Series:
    """
    Description
    -----------
    https://www.incrediblecharts.com/indicators/volatility_ratio_schwager.php
    
    Volatility Ratio = True Range / EMA of True Range for the past n periods

    EMA = exponential moving average
    
    Returns
    -------
    VR: pd.Series
    """
    TRH, TRL = TrueRange(high, low, pre_close)

    return (TRH - TRL) / (TRH - TRL).ewm(span=periods).mean()
