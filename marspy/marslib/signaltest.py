# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------------
# Name:        signalTesting
# Purpose:     
# Author:      JFYU 
# Email:       jfyu@zju.edu.cn
# Created:     2016/10/24 10:33
#
# Copyright 2016 Jianfeng Yu
#
# -------------------------------------------------------------------------------

import abc
import logging
from typing import Union

import numpy as np
import pandas as pd
from statsmodels.stats.weightstats import ztest
from tabulate import tabulate
from collections import Counter

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from marspy.backtest.strategy import StrategyEnvironment

plt.style.use('ggplot')
logger = logging.getLogger(__name__)

class SingleTest:
    """
    信号测试基类
    """

    def __init__(self, env: StrategyEnvironment, name='Signal', event_before=5, event_after=10):
        """
        单信号测试

            对单指标信号前后发生的日收益率和累计收益率进行统计，可用于单指标有效性的判断

            注：可以有两种种方法对数据源进行操作，生成信号bool
            1. 在__init__中向量化生成
            2. 在_generate_signal()里实现信号的计算

        Parameters
        ----------
        env
            测试环境: StrategyEnvironment
        name
            信号名称
        event_before
            信号发生前观察天数
        event_after
            信号发生后的观察天数
        """

        # 指标名称
        self._name = name

        # 测试环境
        self._env = env

        # 数据频率
        self._frequency = self._env.datasource.frequency

        # 数据源
        self._datasource = self._env.datasource

        # 证券代码列表
        self._sids = self._datasource.sids

        # 时间发生的时间窗口组成的df
        self._event_before = event_before
        self._event_after = event_after
        self._event_df = {}
        self._test_res = None


    @abc.abstractmethod
    def _generate_signal(self, external_data=None):
        """
        抽象方法，在此实现信号的算法，需要内部实现对所有证券行情的遍历,并生成signal，True or False。
        signal中True表示信号发生，False表示无信号
        """
        raise NotImplementedError('method not defined')

    def testing(self, plot=True) -> None:
        """
        单信号测试

            将信号发生的指定时间段内的证券行情取出，组成一个df，信号的字段必须是'signal'，
            并计算事件前后的每个阶段的收益率和累计收益率(datasource里有'log_return'字段)

        Parameters
        ----------
        plot
            是否画图

        Returns
            信号触发次数的sid分布与年份分布
        -------

        """

        self._generate_signal()

        self._test_res = {}

        # 1. 取出窗口数据
        no_signal_sid = []
        for sid in self._sids:

            # 因为采用'union'拼接，行情中可能有NAN
            df = self._datasource[sid].dropna(how='any')

            # 对每个证券检查是否有single字段
            assert 'signal' in df.columns, 'Security {0} does not have signal!'.format(sid)

            # 遍历，寻找时间的发生点
            tmp_event_single_sid = []
            year_count = []
            for i in range(self._event_before, len(df) - self._event_after):
                if df.signal[i] == True:
                    # 确定发生的年份
                    tmp_year = df.signal.index[i].year
                    # 取出时间发生窗口
                    tmp_event_window = df.iloc[i-self._event_before:i+self._event_after+1]
                    # 只记录主力合约时的收益率
                    tmp_event_window_log_return = tmp_event_window.MAINCON * tmp_event_window.log_return

                    tmp_event_single_sid.append(tmp_event_window_log_return.values)
                    year_count.append(tmp_year)

            if len(tmp_event_single_sid) !=0:
                tmp_event_single_sid = pd.DataFrame.from_records(tmp_event_single_sid, index= year_count,
                                                             columns=range(-self._event_before, self._event_after+1))
                self._event_df[sid] = tmp_event_single_sid

            else:
                no_signal_sid.append(sid)

        if len(no_signal_sid) == len(self._sids):
            raise ValueError('Signal is never True!')

        # 2. test
        if len(self._event_df.keys()) > 1:

            # 2.1 对每个票的每一天做t-test
            t_test_res = {sid: ztest(self._event_df[sid]) for sid in self._event_df.keys()}

            # 2.2 计算每个票的cumulative return序列
            c_re = {sid: self._event_df[sid].mean().cumsum() for sid in self._event_df.keys()}

            # 2.3 计算每个票的触发次数
            count = {sid: len(self._event_df[sid]) for sid in self._event_df.keys()}

            self._test_res = {
                'sid_t_test': t_test_res,
                'sid_c_re': c_re,
                'sid_count': count
            }

        # 2.4 计算对所有合约的指标
        total_df = pd.concat(self._event_df.values())
        total_t_test_res = ztest(total_df)

        total_c_re = total_df.mean().cumsum() # 计算全样本的累计收益
        total_c_re -= total_c_re[0] # 将累计收益从信号发出那天开始算起
        total_c_re[:self._event_before] = 0 # 将信号发出之前的累计收益归零

        total_count = len(total_df)
        total_return = total_df.mean()
        self._test_res['t_test'] = total_t_test_res
        self._test_res['cumulate_return'] = total_c_re
        self._test_res['count'] = total_count
        self._test_res['return'] = total_return
        self._test_res['total_df'] = total_df

        # 输出未发生信号的合约
        if len(no_signal_sid)>0:
            print('No Signal Sids: {0}'.format(', '.join(no_signal_sid)+'.\n'))

        # 格式化输出每个合约信号触发的次数
        headers = ["Sid", "Count", 'Percent']
        out = [['total', total_count, '100%']]
        if 'sid_count' in self._test_res.keys():
            for sid in self._test_res['sid_count'].keys():
                out.append([sid, self._test_res['sid_count'][sid], "{0:.3%}".format(count[sid]/total_count)])
        else:
            out.append([self._sids[0], total_count, '100%'])

        print(tabulate(out, headers), '\n')

        # 格式化输出每年的触发次数
        headers = ["Year", "Count", 'Percent']
        out = [['total', total_count, '100%']]
        year_count = Counter(self._test_res['total_df'].index)

        years = list(year_count.keys())
        years.sort()
        for year in years:
            out.append([year, year_count[year], "{0:.3%}".format(year_count[year]/total_count)])
        print(tabulate(out,headers),'\n')

        if plot:
            self.plot()

    def plot(self):
        if self._test_res is None:
            raise AttributeError('Testing should be applied first!')

        fig = plt.figure(self._name, figsize=(18, 12))
        gs = gridspec.GridSpec(4,2)

        # 1. 每日的收益bar图和累计收益图
        ax_ret = fig.add_subplot(gs[0:2,:], title='Signal Test: {0}'.format(self._name))
        ax_ret.vlines(self._test_res['return'].index, [0], self._test_res['return'].values,
                    colors='#483D8B', label='Daily Return', linewidth=4)
        ax_ret.fill_between(self._test_res['cumulate_return'].index, self._test_res['cumulate_return'].values,
                          color='DarkOrange', label='Cumulate Return', alpha=0.65)

        ax_ret.set_xlim(right=self._event_after+1, left=-self._event_before)

        leg = plt.legend(loc="upper left", fontsize='medium')
        leg.get_frame().set_alpha(0.5)

        # 2. 画每日收益显著性图
        ax_p = fig.add_subplot(gs[2,:], sharex=ax_ret)
        p_value = pd.Series(self._test_res['t_test'][1], index=self._test_res['return'].index)
        ax_p.vlines(p_value.index, [0],p_value.values, label='p-value')
        ax_p.axhline(y=0.01, label='0.01', color='r')
        ax_p.axhline(y=0.05, label='0.05', color='y')
        ax_p.axhline(y=0.10, label='0.10', color='g')
        ax_p.set_xlim(right=self._event_after + 0.5, left=-self._event_before-0.5)
        ax_p.set_ylim(top=1.0)

        leg = plt.legend(loc="upper left", fontsize='medium')
        leg.get_frame().set_alpha(0.5)

        # 3. 触发次数的合约分布图
        ax_sids_count = fig.add_subplot(gs[3,0])
        if 'sid_count' in self._test_res.keys():
            length = len(self._test_res['sid_count'])
            xlabel = [a for a in self._test_res['sid_count'].keys()]
            yvalue = [self._test_res['sid_count'][a] for a in xlabel]
            xlabel = [a.split('.')[0] for a in xlabel]

        else:
            length =1
            yvalue = [self._test_res['count']]
            xlabel = ['{0}'.format(self._sids[0])]

        ax_sids_count.vlines(range(length), [0], yvalue, label='Signal Count: Sid',
                             color='blue', alpha=0.75, linewidth=5)
        ax_sids_count.set_xticks(np.arange(length))
        ax_sids_count.set_xticklabels(xlabel)

        ax_sids_count.set_ylim(top=max(yvalue)*1.2)
        ax_sids_count.set_xlim(left=-1, right=length)
        leg = plt.legend(loc="upper center", fontsize='medium')
        leg.get_frame().set_alpha(0.5)


        # 4. 触发次数的年份分布图
        ax_year_count = fig.add_subplot(gs[3,1])
        year_count = Counter(self._test_res['total_df'].index)
        years = list(year_count.keys())
        years.sort()
        length = len(years)

        xlabel = [a for a in years]
        yvalue = [year_count[a] for a in xlabel]

        ax_year_count.vlines(range(length), [0], yvalue, color='green', alpha=0.75,
                             label='Signal Count: Year', linewidth=5)

        ax_year_count.set_xticks(np.arange(length))
        ax_year_count.set_xticklabels(xlabel)

        ax_year_count.set_ylim(top=max(yvalue)*1.2)
        ax_year_count.set_xlim(left=-1, right=length)
        leg = plt.legend(loc="upper center", fontsize='medium')
        leg.get_frame().set_alpha(0.5)


        # 显示图形
        plt.show()

    def sid_plot(self, sids: list):
        pass



