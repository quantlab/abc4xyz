# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------------
# Name:        IntradayAnalyser
# Purpose:     
# Author:      JFYU 
# Email:       jfyu@zju.edu.cn
# Created:     2016/11/3 13:34
#
# Copyright 2016 Jianfeng Yu
#
# -------------------------------------------------------------------------------

import abc
import logging
from typing import Union

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from marspy.backtest.strategy import Strategy
from marspy.backtest.commission import AStockCommission
from marspy.backtest.enums import SecurityType

logger = logging.getLogger(__name__)


def OpenCloseAnalyser(s: Strategy):

    # check是否为单标的
    if len(s._sids) != 1:
        raise ValueError('This analyser does not support more than one sid!')

    # check 策略是否运行过
    if s._cursor == 0:
        s.run()

    # check 策略是否有成交交易
    assert len(s._broker.filled_orders) != 0, 'There have to be at least two filled orders!'

    # 将成交单做分析
    df_FilledOrder = pd.DataFrame.from_records([a.to_dict() for a in s._broker.filled_orders], index='end_time')

    # 获取佣金信息
    if s._env.security_type == SecurityType.STOCK:
        commision = AStockCommission
    else:
        raise TypeError('The SecurityType is not supported!')

    df_FilledOrder['commission'] = [commision().get_commission(a) for a in s._broker.filled_orders]
    df_FilledOrder['amount'] = df_FilledOrder.filled_price * df_FilledOrder.filled_qty

    df_FilledOrder = df_FilledOrder.sort_index()

    # check是否为等交易量的open-close匹配
    assert len(set(df_FilledOrder.filled_qty)) == 1, "The trading quantity should be identity!"

    assert len(df_FilledOrder)%2 == 0, "The times of trading should be even!"

    assert df_FilledOrder.side.sum() == 0 and sum(abs(df_FilledOrder.side.cumsum())>1) == 0, "The trading signal is not open-close matched!"

    # 计算开平次数
    res_OC_count = len(df_FilledOrder) / 2

    amount = df_FilledOrder.amount
    commision_fee = df_FilledOrder['commission']

    ret_index = [amount.index[i+1] for i in range(0,len(amount), 2)]
    res_ret = pd.Series(
        [(amount[i+1] - commision_fee[i+1] - commision_fee[i] -  amount[i]) / amount[i] for i in range(0,len(amount), 2)],
        index=ret_index
    )

    # 计算胜率
    res_winRatio = sum(res_ret>0) / len(res_ret)

    # 计算盈亏比
    res_PLRatio = res_ret[res_ret>0].mean() / abs(res_ret[res_ret<0].mean())

    return {
        'Open-Close Count': res_OC_count,
        'WinRatio': res_winRatio,
        'PL_Ratio': res_PLRatio,
        'return Series': res_ret
    }



