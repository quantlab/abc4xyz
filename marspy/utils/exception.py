# -*- coding: utf-8 -*-


class NotEnoughMoneyException(Exception):
    """
    现金余额不足异常
    """
    pass


class NotEnoughShareException(Exception):
    """
    证券余额不足异常
    """

class InvalidOrderException(Exception):
    """

    """