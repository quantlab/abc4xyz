# -*- coding: utf-8 -*-

import abc
import logging
from typing import Union

import pandas as pd
import numpy as np
import rqdatac as rq

from .. import config
from ..backtest.enums import Frequency, SecurityType

logger = logging.getLogger(__name__)


class DataLoader(metaclass=abc.ABCMeta):
    """数据加载基类
    """

    @abc.abstractmethod
    def read(self, sids: (str, list),  start: Union[pd.Timestamp, str],
             end: Union[pd.Timestamp, str], frequency: Frequency) -> pd.DataFrame:
        """读取指定证券的数据

        Notes
        -----
        返回的 DataFrame 必须具有相同的列名

        Parameters
        ----------
        sids: str or list
            证券代码：形如 600000.SH，交易代码+交易所形成的该证券的 **唯一** 标识，或者证券代码的列表 [600000.SH, 600001.SH, ...]
        start: pd.Timestamp or str
            开始时间
        end: pd.Timestamp or str
            结束时间
        frequency: Frequency
            数据频率：支持 Frequency.DAILY, Frequency.MINUTE, Frequency.SECOND 三种类型

        Returns
        -------
        pd.DataFrame
            对应数据
        """


class HDFDataLoader(DataLoader):
    """从 HDF5 文件中读取数据

    只支持日频数据

    Notes
    -----
    仅支持一次读取一个证券的数据

    """

    def __init__(self, path: str):
        """HDFDataLoader 从 HDF5 文件中读取数据"""
        self._path = path

    def read(self, sid: str, start: Union[pd.Timestamp, str],
             end: Union[pd.Timestamp, str], frequency: Frequency) -> pd.DataFrame:
        try:
            sec_type = config.SEC_INFO[sid].type
        except KeyError:
            return None

        start = pd.to_datetime(start)
        end = pd.to_datetime(end)

        where = "(index>='{0}' & index<='{1}')".format(start, end)

        try:
            if sec_type == SecurityType.STOCK:
                if frequency == Frequency.DAILY:
                    with pd.HDFStore(self._path + "marspy_daily.h5", mode='r') as store:
                        df = store.select("/data/stock/daily/" + sid, where=where)
                elif frequency == Frequency.MINUTE:
                    with pd.HDFStore(self._path + "marspy_minute.h5", mode='r') as store:
                        df = store.select("/data/stock/minute/" + sid, where=where)
                elif frequency == Frequency.SECOND:
                    with pd.HDFStore(self._path + "marspy_second.h5", mode='r') as store:
                        df = store.select("/data/stock/second/" + sid, where=where)

            elif sec_type == SecurityType.INDEX:
                if frequency == Frequency.DAILY:
                    with pd.HDFStore(self._path + "marspy_daily.h5", mode='r') as store:
                        df = store.select("/data/index/daily/" + sid, where=where)
                elif frequency == Frequency.MINUTE:
                    with pd.HDFStore(self._path + "marspy_minute.h5", mode='r') as store:
                        df = store.select("/data/index/minute/" + sid, where=where)
                elif frequency == Frequency.SECOND:
                    with pd.HDFStore(self._path + "marspy_second.h5", mode='r') as store:
                        df = store.select("/data/index/second/" + sid, where=where)

            elif sec_type == SecurityType.FUTURES:
                if frequency == Frequency.DAILY:
                    with pd.HDFStore(self._path + "marspy_daily.h5", mode='r') as store:
                        df = store.select("/data/futures/daily/" + sid, where=where)
                elif frequency == Frequency.MINUTE:
                    with pd.HDFStore(self._path + "marspy_minute.h5", mode='r') as store:
                        df = store.select("/data/futures/minute/" + sid, where=where)
                elif frequency == Frequency.SECOND:
                    with pd.HDFStore(self._path + "marspy_second.h5", mode='r') as store:
                        df = store.select("/data/futures/second/" + sid, where=where)

            # 去重、排序
            df.drop_duplicates(inplace=True)
            df.sort_index(inplace=True)

            return df

        except Exception as ex:
            logging.error("读取 {0} 行情数据时发生错误: {1}".format(sid, ex))

        return None
