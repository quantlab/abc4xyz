# -*- coding: utf-8 -*-

import abc
import pymysql
import pandas as pd
import logging

from ..backtest.enums import SecurityType

logger = logging.getLogger(__name__)


class SecInfo(metaclass=abc.ABCMeta):
    """
    证券基本信息抽象基类
    """

    @abc.abstractmethod
    def load(self) -> dict:
        """
        获取所有证券的信息

        Returns
        -------
        dict of Security
            返回字典，key 为证券代码，value 为 Security
        """


class MysqlSecInfo(SecInfo):
    def __init__(self, host: str, user: str, pwd: str, db: str, extra_path: str):
        """
        初始化

        Parameters
        ----------
        user : str
            mysql 用户名
        pwd : str
            mysql 密码
        db : str
            mysql 数据库
        extra_path : str
            保存品种交易信息的csv的位置
        """
        self.__extra_path = extra_path

        self.__con = pymysql.connect(host=host, user=user, password=pwd, db=db, charset='gbk')

    def load(self) -> dict:
        """
        Returns
        -------
        out: dict of namedtuple
            key是证券代码，必须包含的字段是type
            对期货，还需要包含乘数，保证金，手续费等信息
        """
        print(">>> 准备加载全部证券信息......")
        out = {}

        # 查询A股信息
        sql = "SELECT S_INFO_WINDCODE AS sid FROM ASHAREDESCRIPTION;"

        # 证券基本信息：代码，类别
        df_stock = pd.read_sql(sql, self.__con)
        df_stock['type'] = SecurityType.STOCK

        # 查询A股指数信息
        sql = "SELECT S_INFO_WINDCODE AS sid FROM AINDEXDESCRIPTION;"

        # 证券基本信息：代码，类别
        df_index = pd.read_sql(sql, self.__con)
        df_index['type'] = SecurityType.INDEX

        # # 读取期货信息
        # df_futures = pd.read_csv(self.__extra_path)
        # df_futures['type'] = SecurityType.FUTURES
        #
        # def apply_cost_type(t: float):
        #     if t == 0:
        #         return FuturesCostUnitType.BY_AMOUNT
        #     elif t == 1:
        #         return FuturesCostUnitType.BY_VOL
        #     else:
        #         return None

        # df_futures["cost_unit_type"] = df_futures['cost_unit_type'].apply(apply_cost_type)

        # 遍历
        for info in df_stock.itertuples(index=False, name="Stock"):
            out[info.sid] = info

        for info in df_index.itertuples(index=False, name="Index"):
            out[info.sid] = info

        # for info in df_futures.itertuples(index=False, name="Futures"):
        #     out[info.sid] = info

        print(">>> 加载全部证券信息完毕")

        return out
