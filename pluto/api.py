"""
数据 API 模块
"""

from datetime import datetime
import pandas as pd

from pluto.wind import *
from config import SessionWind, engine_wind, engine_factors

session = SessionWind()


def get_trading_days(since, until=None, included_since=False, included_until=False) -> list:
    """
    获取指定区间的交易日列表，递增排列。以上交所交易日为准。

    Parameters
    ----------
    since : str
        起始交易日，必须是 yyyymmdd 格式字符串

    until : str or None
        结束交易日，非 None 时必须是 yyyymmdd 格式字符串

    included_since : bool
        是否包含起始点，默认不包含

    included_until : bool
        是否包含结束点，默认不包含

    Returns
    -------
    list
        交易日期列表
    """

    q = session.query(ASHARECALENDAR.TRADE_DAYS).\
        distinct(ASHARECALENDAR.TRADE_DAYS).\
        filter(ASHARECALENDAR.S_INFO_EXCHMARKET == "SSE")

    if included_since:
        q = q.filter(ASHARECALENDAR.TRADE_DAYS >= since)
    else:
        q = q.filter(ASHARECALENDAR.TRADE_DAYS > since)

    if until is None:
        until = datetime.now().strftime('%Y%m%d')

    if included_until:
        q = q.filter(ASHARECALENDAR.TRADE_DAYS <= until)
    else:
        q = q.filter(ASHARECALENDAR.TRADE_DAYS < until)

    dts = q.order_by(ASHARECALENDAR.TRADE_DAYS).all()

    return [dt[0] for dt in dts]


def get_n_trading_days(n: int, until=None, included_until=False) -> list:
    """获取截止日期之前的 n 个交易日，日期**降序**排列。以上交所交易日为准。

    Parameters
    ----------
    n : int
        需要的交易日数量

    until : str or None
        结束交易日，非 None 时必须是 yyyymmdd 格式字符串

    included_until : bool
        是否包含结束点，默认不包含

    Returns
    -------
    list
        需要的交易日列表
    """
    assert n > 0, "日期必须大于 0 "

    q = session.query(ASHARECALENDAR.TRADE_DAYS).\
        distinct(ASHARECALENDAR.TRADE_DAYS).\
        filter(ASHARECALENDAR.S_INFO_EXCHMARKET == "SSE")

    if until is None:
        until = datetime.now().strftime('%Y%m%d')

    if included_until:
        q = q.filter(ASHARECALENDAR.TRADE_DAYS <= until)
    else:
        q = q.filter(ASHARECALENDAR.TRADE_DAYS < until)

    dts = q.order_by(ASHARECALENDAR.TRADE_DAYS.desc()).limit(n).all()

    return [dt[0] for dt in dts]


def get_stk_prices(sid: str, since: str, until: str) -> pd.DataFrame:
    """获取指定股票的区间行情

    Parameters
    ----------
    sid : str
        指定的股票代码
    since : str
        起始日期（包含），必须是 yyyymmdd 格式的字符串
    until : str
        结束日期（包含），必须是 yyyymmdd 格式的字符串

    Returns
    -------
    DataFrame
        行情的 DataFrame
    """
    sql = """
        select * from ASHAREEODPRICES
        where S_INFO_WINDCODE = '{0}' and TRADE_DT >= '{1}' and TRADE_DT <= '{2}'
            and S_DQ_TRADESTATUS = '交易'
        order by TRADE_DT
    """.format(sid, since, until)

    df_tmp = pd.read_sql(sql, engine_wind, index_col='TRADE_DT', parse_dates=['TRADE_DT'])

    return df_tmp


trading_day_list = pd.read_sql("""select TRADE_DAYS from ASHARECALENDAR
                                  where S_INFO_EXCHMARKET = 'SSE' order by TRADE_DAYS""",
                               engine_wind, parse_dates=['TRADE_DAYS'],
                               index_col='TRADE_DAYS').index


def get_last_trading_day(dt: str):
    """
    获取最近一个交易日

    Parameters
    ----------
    dt

    Returns
    -------
    str
        yyyy-mm-dd 格式字符串日期
    """
    return trading_day_list.asof(dt).strftime('%Y-%m-%d')


def get_sid_description(sid: str):
    """
    获取证券信息

    Parameters
    ----------
    sid : str
        证券代码

    Returns
    -------
    AShareDescription
        证券信息
    """
    return session.query(ASHAREDESCRIPTION).filter(ASHAREDESCRIPTION.S_INFO_WINDCODE == sid).first()


def get_factors_value_by_sid(fname: str, sid: str):
    """
    获取指定证券的因子数据

    Parameters
    ----------
    fname : str
        因子名称
    sid : str
        证券代码

    Returns
    -------
    DataFrame
        因子时间序列值
    """
    sql = """
        select b.sid, b.date, b.value
        from factors_info a join factors_value b on a.fid = b.fid
        where a.name = '{0}' and b.sid = '{1}'
        order by b.date
        """.format(fname, sid)

    df = pd.read_sql(sql, engine_factors, index_col='date')

    return df


def get_factors_value_by_date(fname: str, dt: str):
    """
    获取指定日期的
    Parameters
    ----------
    fname : str
        因子名称
    dt : str
        日期

    Returns
    -------
    DataFrame
        因子截面值

    Notes
    -----
    sqlite 在日期查询时，需要填充后面的 0
    """

    sql = """
        select b.sid, b.date, b.value
        from factors_info a join factors_value b on a.fid = b.fid
        where a.name = '{0}' and b.date = '{1} 00:00:00.000000'
        order by b.sid
        """.format(fname, dt)

    df = pd.read_sql(sql, engine_factors, index_col='sid')

    return df


if __name__ == '__main__':
    #df = get_stk_prices('600030.SH', '20160101', '20160501').S_DQ_ADJCLOSE
    # df = get_factors_value_by_date('EP', '2017-01-24')
    # print(df.mean())
    last_trading_day = get_last_trading_day('2015-01-01')
    fvs = get_factors_value_by_date("EP", last_trading_day)
    pass
