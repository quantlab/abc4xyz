"""
数据库内部计算模块
"""


import pandas as pd
import numpy as np
from scipy import stats

"""
单因子数据的清洗
需要raw_df的column为时间戳
"""


def del_extreme(raw_s: pd.Series, n_diverge=9, min_count=50, center_median=False, dist_median=False):
    """截面去极值"""

    s = raw_s.copy()

    if s.count() < min_count:  # 一天内有效因子数小于最小值，则返回全为np.nan的pd.Series
        return pd.Series([]).reindex_like(raw_s)
    else:
        center = s.median() if center_median else s.mean()
        dist = (s - center).median() if dist_median else s.std()
        s[abs(s - center) > n_diverge * dist] = np.nan

        return s


def winsorize(raw_s: pd.Series, wp=0.01):
    """截面截尾"""

    s = raw_s.copy()

    u = s.quantile(1-wp)
    l = s.quantile(wp)
    s[s < l] = l
    s[s > u] = u

    return s


def normalize(raw_s: pd.Series, weighted=False, cap_weight=None):
    """截面中心化"""
    assert raw_s.shape == cap_weight.shape, "The shape of raw series and of weight series are not matched!"

    center = (raw_s * cap_weight).sum() if weighted else raw_s.mean()
    std = raw_s.std()

    return (raw_s - center) / std


def scale(raw_s: pd.Series, scale=1.0) -> pd.Series:
    """尺度放缩"""
    return raw_s / raw_s.sum() * scale


def weight_mean(raw_s: pd.Series, weight):
    """加权求和"""
    assert raw_s.shape == weight.shape, "The shape of raw series and of weight series are not matched!"
    return (raw_s * scale(weight)).sum()


"""
单因子分析
需要f_loading的column为时间戳
"""


def check_df_shape(*args):
    """检查几个pd.DataFrame的行列是否相等"""
    index = args[0].index
    columns = args[0].columns
    for arg in args:
        assert arg.index == index, "The indexes of pd.DateFrame should be same!"
        assert arg.columns == columns, "The columns of pd.DateFrame should be same!"


def cal_group_return(f_loading: pd.Series, s_return: pd.Series, n_bins=5, weight=None, ascending=True) -> pd.Series:
    """
    计算单个截面的因子分组收益

    Parameters
    ----------
    f_loading
        pd.Series, 因子载荷截面，即因子值。
    s_return
        pd.Series, 股票收益截面，注意为因子值对应的下一期的股票收益。
    n_bins
        int，默认为5, 股票分组个数。
    weight
        pd.Series，默认为None，即等权。股票权重截面，一般用市值加权，用于计算组内的加权收益
    ascending
        bool，默认为True，以递增排序。调用pd.cut方法。

    Returns
    -------
        pd.Series，index为分组号。

    """

    # 分组label
    bin_labels = [str(a+1) for a in range(n_bins)]

    # 对因子值排序,得到分位数
    f_loading_rank = f_loading.rank(ascending=ascending, pct=True)

    # 用分位数进行分组
    group = pd.cut(f_loading_rank, bins=n_bins, labels=bin_labels)

    if weight is None:  # 若不给权重则等权
        df = pd.DataFrame({'s_ret': s_return, 'group': group})
        df_gb = df.groupby('group')
        ret_bg = df_gb.mean()
        ret_bg.columns = ['return']
        return ret_bg['return']

    else:  # 按照权重组内加权
        df = pd.DataFrame({'s_ret': s_return, 'w_t': weight, 'group': group})
        df_gb = df.groupby('group')

        # 定义计算加均值的函数
        def df_weight_mean(df):
            return weight_mean(df.s_ret, df.w_t)

        ret_bg = df_gb.apply(df_weight_mean)
        ret_bg.name = 'return'
        return ret_bg


def cal_ic(forcast_return: pd.Series, real_return: pd.Series, method='pearson') -> tuple:
    """
    计算因子截面IC,调用scipy.stats.pearsonr和scipy.stats.spearmanr方法

    Parameters
    ----------
    forcast_return
        pd.Series, 预测收益值。
    real_return
        pd.Series, 真实收益值。
    method
        {'pearson', 'spearman'}，默认为'pearson'。

    Returns
    -------
        namedtuple, (correlation, p-vlaue)

    """

    if method == 'pearson':
        return stats.pearsonr(forcast_return, real_return)
    elif method == 'spearman':
        return stats.spearmanr(forcast_return, real_return)
    else:
        raise Exception("The method is not supported!")


if __name__ == '__main__':
    # 测试截面单因子收益计算
    f_loading = pd.Series([1, 2, 3, 4, 123, 2])
    s_return = pd.Series([0.2, 0.4, 0.5, 0.6, 0.32, 0.1])
    wt = pd.Series([4, 2, 5, 3, 2, 3])

    aaa = cal_group_return(f_loading, s_return, weight=wt, n_bins=3)
    print(aaa)
