import pandas as pd
from datetime import datetime, timedelta
from dateutil import relativedelta
from config import engine_factors
from pluto import api

# 行情数据
store = pd.HDFStore("../data/stock_quotes.h5")


# 获得月初日期
def get_first_date_of_month(dt: datetime):
    return datetime(year=dt.year, month=dt.month, day=1)


def get_month_list(since: datetime, until: datetime):
    """
    获取区间的每月第一个日期，包括 until 作为最后一个元素

    Parameters
    ----------
    since : datetime
        开始日期
    until : datetime
        结束日期

    Returns
    -------
    list
        月初日期列表

    """
    assert since < until, "开始日期必须早于结束日期"

    since = get_first_date_of_month(since)

    out = []
    while since < until:
        out.append(since)
        since = since + relativedelta.relativedelta(months=1)

    out.append(until)

    return out


def single_factor_test(fname: str):
    """
    单因子测试，计算得到分档的因子收益

    每个月月初根据因子值调仓，按因子值分为 5 档，暂时采用**等权重**加权

    Parameters
    ----------
    fname : str
        因子名称

    Returns
    -------
    None

    """
    # 查询 fid
    sql = """
        select fid from factors_info where name = '{0}'
    """.format(fname)

    df_fid = pd.read_sql(sql, engine_factors)

    if len(df_fid) == 0:
        print("因子 {0} 不存在".format(fname))
        return
    fid = df_fid.fid.ix[0]

    # 获取已有的因子收益日期

    sql = """
        select b.date, b.fid
        from factors_info a join factors_return b on a.fid = b.fid
        where a.name = '{0}'
        """.format(fname)

    df = pd.read_sql(sql, engine_factors)

    # 因子收益存在，找到取现有日期
    if len(df) > 0:
        # sqlite 提取的日期包含尾部的 0000，需要去掉
        start_date = datetime.strptime(df['date'].ix[0][0:10], '%Y-%m-%d')
    else:
        # 默认最早的日期
        start_date = datetime.strptime('2007-01-01', '%Y-%m-%d')

    end_date = datetime.now() + relativedelta.relativedelta(days=-1)

    month_list = get_month_list(start_date, end_date)

    # 因子收益
    factors_rtn = []

    # 对每个月份
    n = len(month_list) - 1
    for i in range(n):
        since = month_list[i]
        until = month_list[i+1]

        # 获取since之前的第一个有效日期的因子值
        last_trading_day = api.get_last_trading_day(since.strftime("%Y-%m-%d"))

        # 获取当天的因子值
        fvs = api.get_factors_value_by_date(fname, last_trading_day)

        # 从小到大分为 5 组
        rank = fvs.value.rank(pct=True)
        group = pd.cut(rank, bins=5)
        portfolios = pd.DataFrame({'sid': group.index, 'group': group}).groupby('group')

        i = 0
        rtns = {}
        for portfolio in portfolios:
            i += 1
            rtn = cal_portfolio_return(portfolio[1], last_trading_day, until.strftime("%Y-%m-%d"))
            rtns["ret_g{0}".format(i)] = rtn

        tmp = pd.DataFrame.from_dict(rtns)
        factors_rtn.append(tmp)

    fr = pd.concat(factors_rtn).dropna()

    fr['date'] = fr.index
    fr['gen_time'] = datetime.now()
    fr['update_time'] = datetime.now()
    fr['fid'] = fid

    fr_to_insert = fr[fr.index > start_date.strftime('%Y-%m-%d')]
    if len(fr_to_insert) > 0:
        fr_to_insert.to_sql('factors_return', engine_factors, if_exists='append', index=False)


def cal_portfolio_return(portfolio: pd.DataFrame, since: str, until: str):
    """
    计算组合的收益，默认为等权

    Parameters
    ----------
    portfolio : Series
        sid 列

    since : str
        起始日期，YYYY-MM-dd

    until : str
        结束日期，YYYY-MM-dd

    Returns
    -------
    Series
        index为日期，列为收益
    """
    sids = "'" + "','".join(portfolio.sid) + "'"

    condition = "sid=[{0}] & date>='{1}' & date<='{2}'".format(sids, since, until)

    quotes = store.select('data', where=condition)

    # 去掉新股第一天上市的收益
    quotes = quotes[quotes['status'] != 'N']

    # 转换成 panel 并取个股收益率
    rtn = quotes.to_panel()['rtn']

    # 计算净值序列
    nv = (rtn + 1).cumprod()
    nv.fillna(method='ffill', inplace=True)

    # 乘以权重并相加得到组合收益
    port_nv = nv.sum(axis=1) / len(nv.columns)

    return port_nv.pct_change()


def plot_factor_nv(fname: str):
    """
    获取因子累计净值

    Parameters
    ----------
    fname : str
        因子名
    """
    sql = """
        select ret_g1, ret_g2, ret_g3, ret_g4, ret_g5, date from factors_return a join factors_info b on a.fid=b.fid
        where b.name = '{0}'
        order by a.date
    """.format(fname)

    df = pd.read_sql(sql, engine_factors, index_col='date', parse_dates=['date'])

    nv = (1 + df).cumprod()
    nv.plot()

if __name__ == '__main__':
    #single_factor_test('EP')
    import matplotlib.pyplot as plt

    plot_factor_nv('EP')
    plt.show()
    pass
