"""
因子计算核心模块
"""

import abc
import logging
from datetime import datetime
import pandas as pd

from config import SessionFactors, SessionWind, engine_factors
from .orm import FactorsInfo, FactorsReturn, FactorsValue
from . import api
from .wind import *


# logging
logger = logging.getLogger(__name__)

# session
session_factors = SessionFactors()
session_wind = SessionWind()


class FactorBase(metaclass=abc.ABCMeta):
    """因子计算基类

    在创建任何新因子的时候，只需要继承该类，并重载 calculate 方法。
    """

    # 因子名称
    __FACTOR_NAME__ = None
    # 因子描述
    __DESCRIPTION__ = None

    def __init__(self, group=0, status=0):
        """
        初始化，当是新创建的因子时，需要先添加 FactorsInfo 记录
        """
        # 查询因子是否存在
        obj = session_factors.query(FactorsInfo).\
            filter(FactorsInfo.name == self.__FACTOR_NAME__).first()

        if obj is None:
            fi = FactorsInfo()
            fi.name = self.__FACTOR_NAME__
            fi.description = self.__DESCRIPTION__
            fi.gen_time = datetime.now()
            fi.update_time = datetime.now()
            fi.group = group
            fi.status = status

            session_factors.add(fi)
            session_factors.commit()
            logger.info("Add new factor: {0}.".format(fi.name))

        self._fid = session_factors.query(FactorsInfo).\
            filter(FactorsInfo.name == self.__FACTOR_NAME__).first().fid

    def _fast_cal(self, sid: str) -> pd.DataFrame:
        """
        快速计算因子的历史值

        Parameters
        ----------
        sid : str
            证券代码

        Returns
        -------
        pd.DataFrame
            包含历史因子值的 DataFrame

            必须包含的列
                gen_time：生成时间
                update_time：更新时间
                sid：证券代码
                date：因子值日期
                fid：因子id
                value：因子值
        """
        raise NotImplementedError("快速计算因子值的方法尚未实现")

    @abc.abstractmethod
    def _calculate(self, sid: str, date: str) -> FactorsValue:
        """计算指定日期的因子值

        Parameters
        ----------
        sid : str
            指定的证券代码

        date : str
            指定的日期，计算当天因子的值，必须是 yyyymmdd 格式字符串

        Returns
        -------
        FactorsValue
            需要返回的因子对象，也可以返回 None
        """

    def _get_last_record(self, sid: str):
        """获取最新的一条记录，无数据返回 None

        Parameters
        ----------
        sid : str
            指定的证券代码
        """
        obj = session_factors.query(FactorsValue, FactorsInfo).\
            filter(FactorsValue.fid == FactorsInfo.fid).\
            filter(FactorsValue.sid == sid).\
            filter(FactorsInfo.name == self.__FACTOR_NAME__).\
            order_by(FactorsValue.date.desc()).first()

        if obj is None:
            return None
        else:
            return obj[0]

    def update(self, base_date='20060101'):
        """更新当前因子
        """

        # 获取所有股票代码
        sids = session_wind.query(ASHAREDESCRIPTION.S_INFO_WINDCODE).all()

        # 对于每一只股票
        for sid in sids:
            sid = sid[0]    # 每一个行是一个 tuple

            print("更新 {0}".format(sid))

            # 获取证券信息
            info = api.get_sid_description(sid)

            # 判断证券是否在历史上上市，若未上市则跳过计算因子值，wind数据库中有IPO终止证券的SID
            if info.S_INFO_LISTDATE is None:
                continue

            # 获取最新的因子值
            record = self._get_last_record(sid)

            # 如果记录为空，从头开始计算，否则，从截止日之后开始计算
            if record is None:
                # 尝试快速计算历史值
                try:
                    df = self._fast_cal(sid)
                    if len(df) > 0:
                        print(" >>> 批量更新")
                        df.to_sql("factors_value", engine_factors, if_exists='append', index=False)
                    continue

                # 当无法快速计算时，采用逐日计算的方法得到历史值
                # 只捕捉 NotImplementedError 异常
                except NotImplementedError:
                    if base_date < info.S_INFO_LISTDATE:
                        start = info.S_INFO_LISTDATE
                    else:
                        start = base_date
                    dts = api.get_trading_days(
                        since=start, until=info.S_INFO_DELISTDATE)
            else:
                dts = api.get_trading_days(record.date.strftime(
                    '%Y%m%d'), until=info.S_INFO_DELISTDATE)

            if len(dts) > 0:
                print("逐日更新")

                commit_flag = False
                # 对每一个交易日
                for dt in dts:
                    # print('factor generating: {0} @ {1}'.format(sid, dt))
                    factor_value = self._calculate(sid, dt)
                    if factor_value is None:
                        continue
                    session_factors.add(factor_value)
                    commit_flag = True

                if commit_flag:
                    session_factors.commit()
                    logger.info("Update {0} days' {1} factor of {2}".format(len(dts), self.__FACTOR_NAME__, sid))
