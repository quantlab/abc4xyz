# coding: utf-8
from sqlalchemy import Column, DateTime, Index, Numeric, String
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class AEQUFROPLEINFOREPPEREND(Base):
    __tablename__ = 'AEQUFROPLEINFOREPPEREND'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(10))
    PRICE_DATE = Column(String(8), index=True)
    F_NAV_UNIT = Column(String(8))
    F_NAV_ACCUMULATED = Column(String(200))
    F_NAV_DIVACCUMULATED = Column(Numeric(20, 4))
    F_NAV_ADJFACTOR = Column(Numeric(20, 4))
    CRNCY_CODE = Column(Numeric(20, 4))
    F_NAV_ADJUSTED = Column(String(400))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXALTERNATIVEMEMBER(Base):
    __tablename__ = 'AINDEXALTERNATIVEMEMBERS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_CON_WINDCODE = Column(String(40))
    ANN_DATE = Column(String(8))
    SEQUENCE = Column(Numeric(20, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXDESCRIPTION(Base):
    __tablename__ = 'AINDEXDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_CODE = Column(String(40))
    S_INFO_NAME = Column(String(50))
    S_INFO_COMPNAME = Column(String(100))
    S_INFO_EXCHMARKET = Column(String(40))
    S_INFO_INDEX_BASEPER = Column(String(8))
    S_INFO_INDEX_BASEPT = Column(Numeric(20, 4))
    S_INFO_LISTDATE = Column(String(8))
    S_INFO_INDEX_WEIGHTSRULE = Column(String(10))
    S_INFO_PUBLISHER = Column(String(100))
    S_INFO_INDEXCODE = Column(Numeric(9, 0))
    S_INFO_INDEXSTYLE = Column(String(40))
    INDEX_INTRO = Column(String)
    WEIGHT_TYPE = Column(Numeric(9, 0))
    EXPIRE_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXEODPRICE(Base):
    __tablename__ = 'AINDEXEODPRICES'
    __table_args__ = (
        Index('SID_TD_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True, unique=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_PCTCHANGE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    SEC_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXHS300CLOSEWEIGHT(Base):
    __tablename__ = 'AINDEXHS300CLOSEWEIGHT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_CON_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(10))
    I_WEIGHT = Column(Numeric(20, 4))
    S_IN_INDEX = Column(Numeric(20, 2))
    I_WEIGHT_11 = Column(Numeric(20, 2))
    I_WEIGHT_12 = Column(String(2))
    I_WEIGHT_14 = Column(Numeric(20, 8))
    I_WEIGHT_15 = Column(Numeric(20, 4))
    I_WEIGHT_16 = Column(String(2))
    I_WEIGHT_17 = Column(Numeric(20, 2))
    I_WEIGHT_18 = Column(Numeric(20, 2))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXHS300FREEWEIGHT(Base):
    __tablename__ = 'AINDEXHS300FREEWEIGHT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_CON_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8), index=True)
    I_WEIGHT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXINDUSTRIESEODCITIC(Base):
    __tablename__ = 'AINDEXINDUSTRIESEODCITICS'
    __table_args__ = (
        Index('SID_TD_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_PCTCHANGE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXMEMBER(Base):
    __tablename__ = 'AINDEXMEMBERS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_CON_WINDCODE = Column(String(40))
    S_CON_INDATE = Column(String(8))
    S_CON_OUTDATE = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXMEMBERSCITIC(Base):
    __tablename__ = 'AINDEXMEMBERSCITICS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_CON_WINDCODE = Column(String(40))
    S_CON_INDATE = Column(String(8))
    S_CON_OUTDATE = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXMEMBERSCITICS2(Base):
    __tablename__ = 'AINDEXMEMBERSCITICS2'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_CON_WINDCODE = Column(String(40))
    S_CON_INDATE = Column(String(8))
    S_CON_OUTDATE = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXMEMBERSCITICS3(Base):
    __tablename__ = 'AINDEXMEMBERSCITICS3'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_CON_WINDCODE = Column(String(40))
    S_CON_INDATE = Column(String(8))
    S_CON_OUTDATE = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXMEMBERSWIND(Base):
    __tablename__ = 'AINDEXMEMBERSWIND'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    S_CON_WINDCODE = Column(String(40))
    S_CON_INDATE = Column(String(8))
    S_CON_OUTDATE = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class AINDEXWINDINDUSTRIESEOD(Base):
    __tablename__ = 'AINDEXWINDINDUSTRIESEOD'
    __table_args__ = (
        Index('SID_TD_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_PCTCHANGE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREAGENCY(Base):
    __tablename__ = 'ASHAREAGENCY'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_AGENCY_NAME = Column(String(200))
    S_RELATION_TYPCODE = Column(String(10))
    S_BUSINESS_TYPCODE = Column(Numeric(9, 0))
    S_AGENCY_NAMEID = Column(String(200))
    BEGINDATE = Column(String(8))
    ENDDATE = Column(String(8))
    SEQUENCE = Column(String(6))
    ANN_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREANNFINANCIALINDICATOR(Base):
    __tablename__ = 'ASHAREANNFINANCIALINDICATOR'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(10))
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    IFLISTED_DATA = Column(Numeric(1, 0))
    STATEMENT_TYPE = Column(Numeric(9, 0))
    CRNCY_CODE = Column(String(10))
    S_FA_EPS_DILUTED = Column(Numeric(20, 4))
    S_FA_EPS_BASIC = Column(Numeric(20, 4))
    S_FA_EPS_DILUTED2 = Column(Numeric(20, 6))
    S_FA_EPS_EX = Column(Numeric(20, 4))
    S_FA_EPS_EXBASIC = Column(Numeric(20, 4))
    S_FA_EPS_EXDILUTED = Column(Numeric(20, 4))
    S_FA_BPS = Column(Numeric(22, 4))
    S_FA_BPS_SH = Column(Numeric(20, 4))
    S_FA_BPS_ADJUST = Column(Numeric(20, 4))
    ROE_DILUTED = Column(Numeric(20, 4))
    ROE_WEIGHTED = Column(Numeric(20, 4))
    ROE_EX = Column(Numeric(20, 4))
    ROE_EXWEIGHTED = Column(Numeric(20, 4))
    NET_PROFIT = Column(Numeric(20, 4))
    RD_EXPENSE = Column(Numeric(20, 4))
    S_FA_EXTRAORDINARY = Column(Numeric(22, 4))
    S_FA_CURRENT = Column(Numeric(20, 4))
    S_FA_QUICK = Column(Numeric(20, 4))
    S_FA_ARTURN = Column(Numeric(20, 4))
    S_FA_INVTURN = Column(Numeric(20, 4))
    S_FT_DEBTTOASSETS = Column(Numeric(20, 4))
    S_FA_OCFPS = Column(Numeric(20, 4))
    S_FA_YOYOCFPS = Column(Numeric(22, 4))
    S_FA_DEDUCTEDPROFIT = Column(Numeric(20, 4))
    S_FA_DEDUCTEDPROFIT_YOY = Column(Numeric(22, 4))
    CONTRIBUTIONPS = Column(Numeric(20, 4))
    GROWTH_BPS_SH = Column(Numeric(22, 4))
    S_FA_YOYEQUITY = Column(Numeric(22, 4))
    YOY_ROE_DILUTED = Column(Numeric(22, 4))
    YOY_NET_CASH_FLOWS = Column(Numeric(22, 4))
    S_FA_YOYEPS_BASIC = Column(Numeric(22, 4))
    S_FA_YOYEPS_DILUTED = Column(Numeric(22, 4))
    S_FA_YOYOP = Column(Numeric(20, 4))
    S_FA_YOYEBT = Column(Numeric(20, 4))
    NET_PROFIT_YOY = Column(Numeric(20, 4))
    S_INFO_DIV = Column(String(40))
    MEMO = Column(String(100))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREAUDITOPINION(Base):
    __tablename__ = 'ASHAREAUDITOPINION'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    S_STMNOTE_AUDIT_CATEGORY = Column(Numeric(9, 0))
    S_STMNOTE_AUDIT_AGENCY = Column(String(100))
    S_STMNOTE_AUDIT_CPA = Column(String(100))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREBALANCESHEET(Base):
    __tablename__ = 'ASHAREBALANCESHEET'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_CODE = Column(String(40))
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPE = Column(String(10))
    CRNCY_CODE = Column(String(10))
    MONETARY_CAP = Column(Numeric(20, 4))
    TRADABLE_FIN_ASSETS = Column(Numeric(20, 4))
    NOTES_RCV = Column(Numeric(20, 4))
    ACCT_RCV = Column(Numeric(20, 4))
    OTH_RCV = Column(Numeric(20, 4))
    PREPAY = Column(Numeric(20, 4))
    DVD_RCV = Column(Numeric(20, 4))
    INT_RCV = Column(Numeric(20, 4))
    INVENTORIES = Column(Numeric(20, 4))
    CONSUMPTIVE_BIO_ASSETS = Column(Numeric(20, 4))
    DEFERRED_EXP = Column(Numeric(20, 4))
    NON_CUR_ASSETS_DUE_WITHIN_1Y = Column(Numeric(20, 4))
    SETTLE_RSRV = Column(Numeric(20, 4))
    LOANS_TO_OTH_BANKS = Column(Numeric(20, 4))
    PREM_RCV = Column(Numeric(20, 4))
    RCV_FROM_REINSURER = Column(Numeric(20, 4))
    RCV_FROM_CEDED_INSUR_CONT_RSRV = Column(Numeric(20, 4))
    RED_MONETARY_CAP_FOR_SALE = Column(Numeric(20, 4))
    OTH_CUR_ASSETS = Column(Numeric(20, 4))
    TOT_CUR_ASSETS = Column(Numeric(20, 4))
    FIN_ASSETS_AVAIL_FOR_SALE = Column(Numeric(20, 4))
    HELD_TO_MTY_INVEST = Column(Numeric(20, 4))
    LONG_TERM_EQY_INVEST = Column(Numeric(20, 4))
    INVEST_REAL_ESTATE = Column(Numeric(20, 4))
    TIME_DEPOSITS = Column(Numeric(20, 4))
    OTH_ASSETS = Column(Numeric(20, 4))
    LONG_TERM_REC = Column(Numeric(20, 4))
    FIX_ASSETS = Column(Numeric(20, 4))
    CONST_IN_PROG = Column(Numeric(20, 4))
    PROJ_MATL = Column(Numeric(20, 4))
    FIX_ASSETS_DISP = Column(Numeric(20, 4))
    PRODUCTIVE_BIO_ASSETS = Column(Numeric(20, 4))
    OIL_AND_NATURAL_GAS_ASSETS = Column(Numeric(20, 4))
    INTANG_ASSETS = Column(Numeric(20, 4))
    R_AND_D_COSTS = Column(Numeric(20, 4))
    GOODWILL = Column(Numeric(20, 4))
    LONG_TERM_DEFERRED_EXP = Column(Numeric(20, 4))
    DEFERRED_TAX_ASSETS = Column(Numeric(20, 4))
    LOANS_AND_ADV_GRANTED = Column(Numeric(20, 4))
    OTH_NON_CUR_ASSETS = Column(Numeric(20, 4))
    TOT_NON_CUR_ASSETS = Column(Numeric(20, 4))
    CASH_DEPOSITS_CENTRAL_BANK = Column(Numeric(20, 4))
    ASSET_DEP_OTH_BANKS_FIN_INST = Column(Numeric(20, 4))
    PRECIOUS_METALS = Column(Numeric(20, 4))
    DERIVATIVE_FIN_ASSETS = Column(Numeric(20, 4))
    AGENCY_BUS_ASSETS = Column(Numeric(20, 4))
    SUBR_REC = Column(Numeric(20, 4))
    RCV_CEDED_UNEARNED_PREM_RSRV = Column(Numeric(20, 4))
    RCV_CEDED_CLAIM_RSRV = Column(Numeric(20, 4))
    RCV_CEDED_LIFE_INSUR_RSRV = Column(Numeric(20, 4))
    RCV_CEDED_LT_HEALTH_INSUR_RSRV = Column(Numeric(20, 4))
    MRGN_PAID = Column(Numeric(20, 4))
    INSURED_PLEDGE_LOAN = Column(Numeric(20, 4))
    CAP_MRGN_PAID = Column(Numeric(20, 4))
    INDEPENDENT_ACCT_ASSETS = Column(Numeric(20, 4))
    CLIENTS_CAP_DEPOSIT = Column(Numeric(20, 4))
    CLIENTS_RSRV_SETTLE = Column(Numeric(20, 4))
    INCL_SEAT_FEES_EXCHANGE = Column(Numeric(20, 4))
    RCV_INVEST = Column(Numeric(20, 4))
    TOT_ASSETS = Column(Numeric(20, 4))
    ST_BORROW = Column(Numeric(20, 4))
    BORROW_CENTRAL_BANK = Column(Numeric(20, 4))
    DEPOSIT_RECEIVED_IB_DEPOSITS = Column(Numeric(20, 4))
    LOANS_OTH_BANKS = Column(Numeric(20, 4))
    TRADABLE_FIN_LIAB = Column(Numeric(20, 4))
    NOTES_PAYABLE = Column(Numeric(20, 4))
    ACCT_PAYABLE = Column(Numeric(20, 4))
    ADV_FROM_CUST = Column(Numeric(20, 4))
    FUND_SALES_FIN_ASSETS_RP = Column(Numeric(20, 4))
    HANDLING_CHARGES_COMM_PAYABLE = Column(Numeric(20, 4))
    EMPL_BEN_PAYABLE = Column(Numeric(20, 4))
    TAXES_SURCHARGES_PAYABLE = Column(Numeric(20, 4))
    INT_PAYABLE = Column(Numeric(20, 4))
    DVD_PAYABLE = Column(Numeric(20, 4))
    OTH_PAYABLE = Column(Numeric(20, 4))
    ACC_EXP = Column(Numeric(20, 4))
    DEFERRED_INC = Column(Numeric(20, 4))
    ST_BONDS_PAYABLE = Column(Numeric(20, 4))
    PAYABLE_TO_REINSURER = Column(Numeric(20, 4))
    RSRV_INSUR_CONT = Column(Numeric(20, 4))
    ACTING_TRADING_SEC = Column(Numeric(20, 4))
    ACTING_UW_SEC = Column(Numeric(20, 4))
    NON_CUR_LIAB_DUE_WITHIN_1Y = Column(Numeric(20, 4))
    OTH_CUR_LIAB = Column(Numeric(20, 4))
    TOT_CUR_LIAB = Column(Numeric(20, 4))
    LT_BORROW = Column(Numeric(20, 4))
    BONDS_PAYABLE = Column(Numeric(20, 4))
    LT_PAYABLE = Column(Numeric(20, 4))
    SPECIFIC_ITEM_PAYABLE = Column(Numeric(20, 4))
    PROVISIONS = Column(Numeric(20, 4))
    DEFERRED_TAX_LIAB = Column(Numeric(20, 4))
    DEFERRED_INC_NON_CUR_LIAB = Column(Numeric(20, 4))
    OTH_NON_CUR_LIAB = Column(Numeric(20, 4))
    TOT_NON_CUR_LIAB = Column(Numeric(20, 4))
    LIAB_DEP_OTH_BANKS_FIN_INST = Column(Numeric(20, 4))
    DERIVATIVE_FIN_LIAB = Column(Numeric(20, 4))
    CUST_BANK_DEP = Column(Numeric(20, 4))
    AGENCY_BUS_LIAB = Column(Numeric(20, 4))
    OTH_LIAB = Column(Numeric(20, 4))
    PREM_RECEIVED_ADV = Column(Numeric(20, 4))
    DEPOSIT_RECEIVED = Column(Numeric(20, 4))
    INSURED_DEPOSIT_INVEST = Column(Numeric(20, 4))
    UNEARNED_PREM_RSRV = Column(Numeric(20, 4))
    OUT_LOSS_RSRV = Column(Numeric(20, 4))
    LIFE_INSUR_RSRV = Column(Numeric(20, 4))
    LT_HEALTH_INSUR_V = Column(Numeric(20, 4))
    INDEPENDENT_ACCT_LIAB = Column(Numeric(20, 4))
    INCL_PLEDGE_LOAN = Column(Numeric(20, 4))
    CLAIMS_PAYABLE = Column(Numeric(20, 4))
    DVD_PAYABLE_INSURED = Column(Numeric(20, 4))
    TOT_LIAB = Column(Numeric(20, 4))
    CAP_STK = Column(Numeric(20, 4))
    CAP_RSRV = Column(Numeric(20, 4))
    SPECIAL_RSRV = Column(Numeric(20, 4))
    SURPLUS_RSRV = Column(Numeric(20, 4))
    UNDISTRIBUTED_PROFIT = Column(Numeric(20, 4))
    LESS_TSY_STK = Column(Numeric(20, 4))
    PROV_NOM_RISKS = Column(Numeric(20, 4))
    CNVD_DIFF_FOREIGN_CURR_STAT = Column(Numeric(20, 4))
    UNCONFIRMED_INVEST_LOSS = Column(Numeric(20, 4))
    MINORITY_INT = Column(Numeric(20, 4))
    TOT_SHRHLDR_EQY_EXCL_MIN_INT = Column(Numeric(20, 4))
    TOT_SHRHLDR_EQY_INCL_MIN_INT = Column(Numeric(20, 4))
    TOT_LIAB_SHRHLDR_EQY = Column(Numeric(20, 4))
    COMP_TYPE_CODE = Column(String(2))
    ACTUAL_ANN_DT = Column(String(8))
    SPE_CUR_ASSETS_DIFF = Column(Numeric(20, 4))
    TOT_CUR_ASSETS_DIFF = Column(Numeric(20, 4))
    SPE_NON_CUR_ASSETS_DIFF = Column(Numeric(20, 4))
    TOT_NON_CUR_ASSETS_DIFF = Column(Numeric(20, 4))
    SPE_BAL_ASSETS_DIFF = Column(Numeric(20, 4))
    TOT_BAL_ASSETS_DIFF = Column(Numeric(20, 4))
    SPE_CUR_LIAB_DIFF = Column(Numeric(20, 4))
    TOT_CUR_LIAB_DIFF = Column(Numeric(20, 4))
    SPE_NON_CUR_LIAB_DIFF = Column(Numeric(20, 4))
    TOT_NON_CUR_LIAB_DIFF = Column(Numeric(20, 4))
    SPE_BAL_LIAB_DIFF = Column(Numeric(20, 4))
    TOT_BAL_LIAB_DIFF = Column(Numeric(20, 4))
    SPE_BAL_SHRHLDR_EQY_DIFF = Column(Numeric(20, 4))
    TOT_BAL_SHRHLDR_EQY_DIFF = Column(Numeric(20, 4))
    SPE_BAL_LIAB_EQY_DIFF = Column(Numeric(20, 4))
    TOT_BAL_LIAB_EQY_DIFF = Column(Numeric(20, 4))
    LT_PAYROLL_PAYABLE = Column(Numeric(20, 4))
    OTHER_COMP_INCOME = Column(Numeric(20, 4))
    OTHER_EQUITY_TOOLS = Column(Numeric(20, 4))
    OTHER_EQUITY_TOOLS_P_SHR = Column(Numeric(20, 4))
    LENDING_FUNDS = Column(Numeric(20, 4))
    ACCOUNTS_RECEIVABLE = Column(Numeric(20, 4))
    ST_FINANCING_PAYABLE = Column(Numeric(20, 4))
    PAYABLES = Column(Numeric(20, 4))
    S_INFO_COMPCODE = Column(String(10))
    TOT_SHR = Column(Numeric(20, 4))
    HFS_ASSETS = Column(Numeric(20, 4))
    HFS_SALES = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREBANKINDICATOR(Base):
    __tablename__ = 'ASHAREBANKINDICATOR'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPE = Column(String(10))
    CRNCY_CODE = Column(String(10))
    CAPI_ADE_RATIO = Column(Numeric(20, 4))
    CORE_CAPI_ADE_RATIO = Column(Numeric(20, 4))
    NPL_RATIO = Column(Numeric(20, 4))
    LOAN_DEPO_RATIO = Column(Numeric(20, 4))
    LOAN_DEPO_RATIO_RMB = Column(Numeric(20, 4))
    LOAN_DEPO_RATIO_NORMB = Column(Numeric(20, 4))
    ST_ASSET_LIQ_RATIO_RMB = Column(Numeric(20, 4))
    ST_ASSET_LIQ_RATIO_NORMB = Column(Numeric(20, 4))
    LOAN_FROM_BANKS_RATIO = Column(Numeric(20, 4))
    LEND_TO_BANKS_RATIO = Column(Numeric(20, 4))
    LARGEST_CUSTOMER_LOAN = Column(Numeric(20, 4))
    TOP_TEN_CUSTOMER_LOAN = Column(Numeric(20, 4))
    TOTAL_LOAN = Column(Numeric(20, 4))
    TOTAL_DEPOSIT = Column(Numeric(20, 4))
    LOAN_LOSS_PROVISION = Column(Numeric(20, 4))
    BAD_LOAD_FIVE_CLASS = Column(Numeric(20, 4))
    NPL_PROVISION_COVERAGE = Column(Numeric(20, 4))
    COST_INCOME_RATIO = Column(Numeric(20, 4))
    NON_INTEREST_MARGIN = Column(Numeric(20, 4))
    NET_CAPITAL = Column(Numeric(20, 4))
    CORE_CAPI_NET_AMOUNT = Column(Numeric(20, 4))
    RISK_WEIGHT_ASSET = Column(Numeric(20, 4))
    INTEREST_BEARING_ASSET = Column(Numeric(20, 4))
    INTEREST_BEARING_ASSET_COMP = Column(Numeric(20, 4))
    INTEREST_BEARING_LIA = Column(Numeric(20, 4))
    INTEREST_BEARING_LIA_COMP = Column(Numeric(20, 4))
    NON_INTEREST_INCOME = Column(Numeric(20, 4))
    NONEANING_ASSET = Column(Numeric(20, 4))
    NONEANING_LIA = Column(Numeric(20, 4))
    NET_INTEREST_MARGIN = Column(Numeric(20, 4))
    NET_INTEREST_MARGIN_IS_ANN = Column(Numeric(20, 4))
    NET_INTEREST_SPREAD = Column(Numeric(20, 4))
    NET_INTEREST_SPREAD_IS_ANN = Column(Numeric(20, 4))
    OVERDUE_LOAN = Column(Numeric(20, 4))
    TOTAL_INTEREST_INCOME = Column(Numeric(20, 4))
    TOTAL_INTEREST_EXP = Column(Numeric(20, 4))
    CASH_ON_HAND = Column(Numeric(20, 4))
    LONGTERM_LOANS_RATIO_CNY = Column(Numeric(20, 4))
    LONGTERM_LOANS_RATIO_FC = Column(Numeric(20, 4))
    IBUSINESS_LOAN_RATIO = Column(Numeric(20, 4))
    INTERECT_COLLECTION_RATIO = Column(Numeric(20, 4))
    CASH_RESERVE_RATIO_CNY = Column(Numeric(20, 4))
    CASH_RESERVE_RATIO_FC = Column(Numeric(20, 4))
    OVERSEAS_FUNDS_APP_RATIO = Column(Numeric(20, 4))
    MARKET_RISK_CAPITAL = Column(Numeric(20, 4))
    INTEREST_BEARING_ASSET_IFPUB = Column(Numeric(1, 0))
    INTEREST_BEARING_LIA_IFPUB = Column(Numeric(1, 0))
    NET_INTEREST_MARGIN_IFPUB = Column(Numeric(1, 0))
    LOANRESERVESRATIO = Column(Numeric(20, 4))
    SUBORDINATED_NET_CAPI = Column(Numeric(20, 4))
    INT_BEAR_ASSET_AVG_BALANCE = Column(Numeric(20, 4))
    INT_BEAR_ASSET_AVG_RETURN = Column(Numeric(20, 4))
    INT_CCRUED_LIAB_AVG_BALANCE = Column(Numeric(20, 4))
    INT_CCRUED_LIAB_AVG_COSTRATIO = Column(Numeric(20, 4))
    RESCHEDULEDLOANS = Column(Numeric(20, 4))
    CORETIER1_NET_CAPI = Column(Numeric(20, 4))
    TIER1_NET_CAPI = Column(Numeric(20, 4))
    NET_CAPITAL_2013 = Column(Numeric(20, 4))
    CORETIER1CAPI_ADE_RATIO = Column(Numeric(20, 4))
    TIER1CAPI_ADE_RATIO = Column(Numeric(20, 4))
    CAPI_ADE_RATIO_2013 = Column(Numeric(20, 4))
    RISK_WEIGHT_NET_ASSET_2013 = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREBLOCKTRADE(Base):
    __tablename__ = 'ASHAREBLOCKTRADE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    S_BLOCK_PRICE = Column(Numeric(20, 4))
    S_BLOCK_VOLUME = Column(Numeric(20, 4))
    S_BLOCK_AMOUNT = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    S_BLOCK_BUYERNAME = Column(String(200))
    S_BLOCK_SELLERNAME = Column(String(200))
    S_BLOCK_FREQUENCY = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECALENDAR(Base):
    __tablename__ = 'ASHARECALENDAR'

    OBJECT_ID = Column(String(100), primary_key=True)
    TRADE_DAYS = Column(String(8), index=True)
    S_INFO_EXCHMARKET = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECAPITALIZATION(Base):
    __tablename__ = 'ASHARECAPITALIZATION'

    OBJECT_ID = Column(String(100), primary_key=True)
    WIND_CODE = Column(String(40))
    S_INFO_WINDCODE = Column(String(40), index=True)
    CHANGE_DT = Column(String(8), index=True)
    TOT_SHR = Column(Numeric(20, 4))
    FLOAT_SHR = Column(Numeric(20, 4))
    FLOAT_A_SHR = Column(Numeric(20, 4))
    FLOAT_B_SHR = Column(Numeric(20, 4))
    FLOAT_H_SHR = Column(Numeric(20, 4))
    FLOAT_OVERSEAS_SHR = Column(Numeric(20, 4))
    RESTRICTED_A_SHR = Column(Numeric(20, 4))
    S_SHARE_RTD_STATE = Column(Numeric(20, 4))
    S_SHARE_RTD_STATEJUR = Column(Numeric(20, 4))
    S_SHARE_RTD_SUBOTHERDOMES = Column(Numeric(20, 4))
    S_SHARE_RTD_DOMESJUR = Column(Numeric(20, 4))
    S_SHARE_RTD_INST = Column(Numeric(20, 4))
    S_SHARE_RTD_DOMESNP = Column(Numeric(20, 4))
    S_SHARE_RTD_SENMANAGER = Column(Numeric(20, 4))
    S_SHARE_RTD_SUBFRGN = Column(Numeric(20, 4))
    S_SHARE_RTD_FRGNJUR = Column(Numeric(20, 4))
    S_SHARE_RTD_FRGNNP = Column(Numeric(20, 4))
    RESTRICTED_B_SHR = Column(Numeric(20, 4))
    OTHER_RESTRICTED_SHR = Column(Numeric(20, 4))
    NON_TRADABLE_SHR = Column(Numeric(20, 4))
    S_SHARE_NTRD_STATE_PCT = Column(Numeric(20, 4))
    S_SHARE_NTRD_STATE = Column(Numeric(20, 4))
    S_SHARE_NTRD_STATJUR = Column(Numeric(20, 4))
    S_SHARE_NTRD_SUBDOMESJUR = Column(Numeric(20, 4))
    S_SHARE_NTRD_DOMESINITOR = Column(Numeric(20, 4))
    S_SHARE_NTRD_IPOJURIS = Column(Numeric(20, 4))
    S_SHARE_NTRD_GENJURIS = Column(Numeric(20, 4))
    S_SHARE_NTRD_STRTINVESTOR = Column(Numeric(20, 4))
    S_SHARE_NTRD_FUNDBAL = Column(Numeric(20, 4))
    S_SHARE_NTRD_IPOINIP = Column(Numeric(20, 4))
    S_SHARE_NTRD_TRFNSHARE = Column(Numeric(20, 4))
    S_SHARE_NTRD_SNORMNGER = Column(Numeric(20, 4))
    S_SHARE_NTRD_INSDEREMP = Column(Numeric(20, 4))
    S_SHARE_NTRD_PRFSHARE = Column(Numeric(20, 4))
    S_SHARE_NTRD_NONLSTFRGN = Column(Numeric(20, 4))
    S_SHARE_NTRD_STAQ = Column(Numeric(20, 4))
    S_SHARE_NTRD_NET = Column(Numeric(20, 4))
    S_SHARE_CHANGEREASON = Column(String(30))
    ANN_DT = Column(String(8))
    CHANGE_DT1 = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECAPITALOPERATION(Base):
    __tablename__ = 'ASHARECAPITALOPERATION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    S_CAPITALOPERATION_ENDDATE = Column(String(8), index=True)
    S_CAPITALOPERATION_SHARE = Column(Numeric(20, 4))
    S_CAPITALOPERATION_AMOUNT = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(3))
    S_CAPITALOPERATION_COMPANYNAME = Column(String(100))
    S_CAPITALOPERAT_COMPWINDCODE = Column(String(40))
    S_END_BAL = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECASHFLOW(Base):
    __tablename__ = 'ASHARECASHFLOW'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_CODE = Column(String(40))
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPE = Column(String(10))
    CRNCY_CODE = Column(String(10))
    CASH_RECP_SG_AND_RS = Column(Numeric(20, 4))
    RECP_TAX_RENDS = Column(Numeric(20, 4))
    NET_INCR_DEP_COB = Column(Numeric(20, 4))
    NET_INCR_LOANS_CENTRAL_BANK = Column(Numeric(20, 4))
    NET_INCR_FUND_BORR_OFI = Column(Numeric(20, 4))
    CASH_RECP_PREM_ORIG_INCO = Column(Numeric(20, 4))
    NET_INCR_INSURED_DEP = Column(Numeric(20, 4))
    NET_CASH_RECEIVED_REINSU_BUS = Column(Numeric(20, 4))
    NET_INCR_DISP_TFA = Column(Numeric(20, 4))
    NET_INCR_INT_HANDLING_CHRG = Column(Numeric(20, 4))
    NET_INCR_DISP_FAAS = Column(Numeric(20, 4))
    NET_INCR_LOANS_OTHER_BANK = Column(Numeric(20, 4))
    NET_INCR_REPURCH_BUS_FUND = Column(Numeric(20, 4))
    OTHER_CASH_RECP_RAL_OPER_ACT = Column(Numeric(20, 4))
    STOT_CASH_INFLOWS_OPER_ACT = Column(Numeric(20, 4))
    CASH_PAY_GOODS_PURCH_SERV_REC = Column(Numeric(20, 4))
    CASH_PAY_BEH_EMPL = Column(Numeric(20, 4))
    PAY_ALL_TYP_TAX = Column(Numeric(20, 4))
    NET_INCR_CLIENTS_LOAN_ADV = Column(Numeric(20, 4))
    NET_INCR_DEP_CBOB = Column(Numeric(20, 4))
    CASH_PAY_CLAIMS_ORIG_INCO = Column(Numeric(20, 4))
    HANDLING_CHRG_PAID = Column(Numeric(20, 4))
    COMM_INSUR_PLCY_PAID = Column(Numeric(20, 4))
    OTHER_CASH_PAY_RAL_OPER_ACT = Column(Numeric(20, 4))
    STOT_CASH_OUTFLOWS_OPER_ACT = Column(Numeric(20, 4))
    NET_CASH_FLOWS_OPER_ACT = Column(Numeric(20, 4))
    CASH_RECP_DISP_WITHDRWL_INVEST = Column(Numeric(20, 4))
    CASH_RECP_RETURN_INVEST = Column(Numeric(20, 4))
    NET_CASH_RECP_DISP_FIOLTA = Column(Numeric(20, 4))
    NET_CASH_RECP_DISP_SOBU = Column(Numeric(20, 4))
    OTHER_CASH_RECP_RAL_INV_ACT = Column(Numeric(20, 4))
    STOT_CASH_INFLOWS_INV_ACT = Column(Numeric(20, 4))
    CASH_PAY_ACQ_CONST_FIOLTA = Column(Numeric(20, 4))
    CASH_PAID_INVEST = Column(Numeric(20, 4))
    NET_CASH_PAY_AQUIS_SOBU = Column(Numeric(20, 4))
    OTHER_CASH_PAY_RAL_INV_ACT = Column(Numeric(20, 4))
    NET_INCR_PLEDGE_LOAN = Column(Numeric(20, 4))
    STOT_CASH_OUTFLOWS_INV_ACT = Column(Numeric(20, 4))
    NET_CASH_FLOWS_INV_ACT = Column(Numeric(20, 4))
    CASH_RECP_CAP_CONTRIB = Column(Numeric(20, 4))
    INCL_CASH_REC_SAIMS = Column(Numeric(20, 4))
    CASH_RECP_BORROW = Column(Numeric(20, 4))
    PROC_ISSUE_BONDS = Column(Numeric(20, 4))
    OTHER_CASH_RECP_RAL_FNC_ACT = Column(Numeric(20, 4))
    STOT_CASH_INFLOWS_FNC_ACT = Column(Numeric(20, 4))
    CASH_PREPAY_AMT_BORR = Column(Numeric(20, 4))
    CASH_PAY_DIST_DPCP_INT_EXP = Column(Numeric(20, 4))
    INCL_DVD_PROFIT_PAID_SC_MS = Column(Numeric(20, 4))
    OTHER_CASH_PAY_RAL_FNC_ACT = Column(Numeric(20, 4))
    STOT_CASH_OUTFLOWS_FNC_ACT = Column(Numeric(20, 4))
    NET_CASH_FLOWS_FNC_ACT = Column(Numeric(20, 4))
    EFF_FX_FLU_CASH = Column(Numeric(20, 4))
    NET_INCR_CASH_CASH_EQU = Column(Numeric(20, 4))
    CASH_CASH_EQU_BEG_PERIOD = Column(Numeric(20, 4))
    CASH_CASH_EQU_END_PERIOD = Column(Numeric(20, 4))
    NET_PROFIT = Column(Numeric(20, 4))
    UNCONFIRMED_INVEST_LOSS = Column(Numeric(20, 4))
    PLUS_PROV_DEPR_ASSETS = Column(Numeric(20, 4))
    DEPR_FA_COGA_DPBA = Column(Numeric(20, 4))
    AMORT_INTANG_ASSETS = Column(Numeric(20, 4))
    AMORT_LT_DEFERRED_EXP = Column(Numeric(20, 4))
    DECR_DEFERRED_EXP = Column(Numeric(20, 4))
    INCR_ACC_EXP = Column(Numeric(20, 4))
    LOSS_DISP_FIOLTA = Column(Numeric(20, 4))
    LOSS_SCR_FA = Column(Numeric(20, 4))
    LOSS_FV_CHG = Column(Numeric(20, 4))
    FIN_EXP = Column(Numeric(20, 4))
    INVEST_LOSS = Column(Numeric(20, 4))
    DECR_DEFERRED_INC_TAX_ASSETS = Column(Numeric(20, 4))
    INCR_DEFERRED_INC_TAX_LIAB = Column(Numeric(20, 4))
    DECR_INVENTORIES = Column(Numeric(20, 4))
    DECR_OPER_PAYABLE = Column(Numeric(20, 4))
    INCR_OPER_PAYABLE = Column(Numeric(20, 4))
    OTHERS = Column(Numeric(20, 4))
    IM_NET_CASH_FLOWS_OPER_ACT = Column(Numeric(20, 4))
    CONV_DEBT_INTO_CAP = Column(Numeric(20, 4))
    CONV_CORP_BONDS_DUE_WITHIN_1Y = Column(Numeric(20, 4))
    FA_FNC_LEASES = Column(Numeric(20, 4))
    END_BAL_CASH = Column(Numeric(20, 4))
    LESS_BEG_BAL_CASH = Column(Numeric(20, 4))
    PLUS_END_BAL_CASH_EQU = Column(Numeric(20, 4))
    LESS_BEG_BAL_CASH_EQU = Column(Numeric(20, 4))
    IM_NET_INCR_CASH_CASH_EQU = Column(Numeric(20, 4))
    FREE_CASH_FLOW = Column(Numeric(20, 4))
    COMP_TYPE_CODE = Column(String(2))
    ACTUAL_ANN_DT = Column(String(8))
    SPE_BAL_CASH_INFLOWS_OPER = Column(Numeric(20, 4))
    TOT_BAL_CASH_INFLOWS_OPER = Column(Numeric(20, 4))
    SPE_BAL_CASH_OUTFLOWS_OPER = Column(Numeric(20, 4))
    TOT_BAL_CASH_OUTFLOWS_OPER = Column(Numeric(20, 4))
    TOT_BAL_NETCASH_OUTFLOWS_OPER = Column(Numeric(20, 4))
    SPE_BAL_CASH_INFLOWS_INV = Column(Numeric(20, 4))
    TOT_BAL_CASH_INFLOWS_INV = Column(Numeric(20, 4))
    SPE_BAL_CASH_OUTFLOWS_INV = Column(Numeric(20, 4))
    TOT_BAL_CASH_OUTFLOWS_INV = Column(Numeric(20, 4))
    TOT_BAL_NETCASH_OUTFLOWS_INV = Column(Numeric(20, 4))
    SPE_BAL_CASH_INFLOWS_FNC = Column(Numeric(20, 4))
    TOT_BAL_CASH_INFLOWS_FNC = Column(Numeric(20, 4))
    SPE_BAL_CASH_OUTFLOWS_FNC = Column(Numeric(20, 4))
    TOT_BAL_CASH_OUTFLOWS_FNC = Column(Numeric(20, 4))
    TOT_BAL_NETCASH_OUTFLOWS_FNC = Column(Numeric(20, 4))
    SPE_BAL_NETCASH_INC = Column(Numeric(20, 4))
    TOT_BAL_NETCASH_INC = Column(Numeric(20, 4))
    SPE_BAL_NETCASH_EQU_UNDIR = Column(Numeric(20, 4))
    TOT_BAL_NETCASH_EQU_UNDIR = Column(Numeric(20, 4))
    SPE_BAL_NETCASH_INC_UNDIR = Column(Numeric(20, 4))
    TOT_BAL_NETCASH_INC_UNDIR = Column(Numeric(20, 4))
    S_INFO_COMPCODE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECOMPANYFILING(Base):
    __tablename__ = 'ASHARECOMPANYFILINGS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    S_EST_FLSTITLE_INST = Column(String(100))
    S_EST_FLSABSTRACT_INST = Column(String)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECOMPANYHOLDSHARE(Base):
    __tablename__ = 'ASHARECOMPANYHOLDSHARES'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    ENDDATE = Column(String(8), index=True)
    S_CAPITALOPERATION_COMPANYNAME = Column(String(100))
    S_CAPITALOPERATION_COMPANYID = Column(String(10))
    S_CAPITALOPERATION_COMAINBUS = Column(String(150))
    RELATIONS_CODE = Column(String(40))
    S_CAPITALOPERATION_PCT = Column(Numeric(20, 4))
    VOTING_RIGHTS = Column(Numeric(20, 4))
    S_CAPITALOPERATION_AMOUNT = Column(Numeric(20, 4))
    OPERATIONCRNCY_CODE = Column(String(10))
    S_CAPITALOPERATION_COREGCAP = Column(Numeric(20, 4))
    CAPITALCRNCY_CODE = Column(String(10))
    IS_CONSOLIDATE = Column(Numeric(5, 0))
    NOTCONSOLIDATE_REASON = Column(String(500))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECOMPRESTRICTED(Base):
    __tablename__ = 'ASHARECOMPRESTRICTED'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_LISTDATE = Column(String(8))
    S_HOLDER_NAME = Column(String(200))
    S_SHARE_LSTTYPECODE = Column(Numeric(9, 0))
    S_SHARE_LSTTYPENAME = Column(String(200))
    S_SHARE_LST = Column(Numeric(20, 4))
    S_SHARE_RATIO = Column(Numeric(20, 4))
    S_SHARE_PLACEMENT_ENDDT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECONSENSUSDATUM(Base):
    __tablename__ = 'ASHARECONSENSUSDATA'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_CODE = Column(String(40))
    EST_DT = Column(String(8))
    EST_REPORT_DT = Column(String(8), index=True)
    NUM_EST_INST = Column(Numeric(20, 0))
    EPS_AVG = Column(Numeric(20, 4))
    MAIN_BUS_INC_AVG = Column(Numeric(20, 4))
    NET_PROFIT_AVG = Column(Numeric(20, 4))
    EBIT_AVG = Column(Numeric(20, 4))
    EBITDA_AVG = Column(Numeric(20, 4))
    EPS_MEDIAN = Column(Numeric(20, 4))
    MAIN_BUS_INC_MEDIAN = Column(Numeric(20, 4))
    NET_PROFIT_MEDIAN = Column(Numeric(20, 4))
    EBIT_MEDIAN = Column(Numeric(20, 4))
    EBITDA_MEDIAN = Column(Numeric(20, 4))
    CONSEN_DATA_CYCLE_TYP = Column(String(10))
    EPS_DEV = Column(Numeric(20, 4))
    MAIN_BUS_INC_DEV = Column(Numeric(20, 4))
    NET_PROFIT_DEV = Column(Numeric(20, 4))
    EBIT_DEV = Column(Numeric(20, 4))
    EBITDA_DEV = Column(Numeric(20, 4))
    EPS_MAX = Column(Numeric(20, 4))
    EPS_MIN = Column(Numeric(20, 4))
    MAIN_BUS_INC_MAX = Column(Numeric(20, 4))
    MAIN_BUS_INC_MIN = Column(Numeric(20, 4))
    MAIN_BUS_INC_UPGRADE = Column(Numeric(20, 4))
    MAIN_BUS_INC_DOWNGRADE = Column(Numeric(20, 4))
    MAIN_BUS_INC_MAINTAIN = Column(Numeric(20, 4))
    NET_PROFIT_MAX = Column(Numeric(20, 4))
    NET_PROFIT_MIN = Column(Numeric(20, 4))
    NET_PROFIT_UPGRADE = Column(Numeric(20, 4))
    NET_PROFIT_DOWNGRADE = Column(Numeric(20, 4))
    NET_PROFIT_MAINTAIN = Column(Numeric(20, 4))
    S_EST_AVGCPS = Column(Numeric(20, 4))
    S_EST_MEDIANCPS = Column(Numeric(20, 4))
    S_EST_STDCPS = Column(Numeric(20, 4))
    S_EST_MAXCPS = Column(Numeric(20, 4))
    S_EST_MINCPS = Column(Numeric(20, 4))
    S_EST_AVGDPS = Column(Numeric(20, 4))
    S_EST_MEDIANDPS = Column(Numeric(20, 4))
    S_EST_STDDPS = Column(Numeric(20, 4))
    S_EST_MAXDPS = Column(Numeric(20, 4))
    S_EST_MINDPS = Column(Numeric(20, 4))
    EBIT_MAX = Column(Numeric(20, 4))
    EBIT_MIN = Column(Numeric(20, 4))
    EBITDA_MAX = Column(Numeric(20, 4))
    EBITDA_MIN = Column(Numeric(20, 4))
    S_EST_AVGBPS = Column(Numeric(20, 4))
    S_EST_MEDIANBPS = Column(Numeric(20, 4))
    S_EST_STDBPS = Column(Numeric(20, 4))
    S_EST_MAXBPS = Column(Numeric(20, 4))
    S_EST_MINBPS = Column(Numeric(20, 4))
    S_EST_AVGEBT = Column(Numeric(20, 4))
    S_EST_MEDIANEBT = Column(Numeric(20, 4))
    S_EST_STDEBT = Column(Numeric(20, 4))
    S_EST_MAXEBT = Column(Numeric(20, 4))
    S_EST_MINEBT = Column(Numeric(20, 4))
    S_EST_AVGROA = Column(Numeric(20, 4))
    S_EST_MEDIANROA = Column(Numeric(20, 4))
    S_EST_STDROA = Column(Numeric(20, 4))
    S_EST_MAXROA = Column(Numeric(20, 4))
    S_EST_MINROA = Column(Numeric(20, 4))
    S_EST_AVGROE = Column(Numeric(20, 4))
    S_EST_MEDIANROE = Column(Numeric(20, 4))
    S_EST_STDROE = Column(Numeric(20, 4))
    S_EST_MAXROE = Column(Numeric(20, 4))
    S_EST_MINROE = Column(Numeric(20, 4))
    S_EST_AVGOPERATINGPROFIT = Column(Numeric(20, 4))
    S_EST_MEDIANOPERATINGPROFIT = Column(Numeric(20, 4))
    S_EST_STDOPERATINGPROFIT = Column(Numeric(20, 4))
    S_EST_MAXOPERATINGPROFIT = Column(Numeric(20, 4))
    S_EST_MINOPERATINGPROFIT = Column(Numeric(20, 4))
    S_EST_EPSINSTNUM = Column(Numeric(20, 4))
    S_EST_MAINBUSINCINSTNUM = Column(Numeric(20, 4))
    S_EST_NETPROFITINSTNUM = Column(Numeric(20, 4))
    S_EST_CPSINSTNUM = Column(Numeric(20, 4))
    S_EST_DPSINSTNUM = Column(Numeric(20, 4))
    S_EST_EBITINSTNUM = Column(Numeric(20, 4))
    S_EST_EBITDAINSTNUM = Column(Numeric(20, 4))
    S_EST_BPSINSTNUM = Column(Numeric(20, 4))
    S_EST_EBTINSTNUM = Column(Numeric(20, 4))
    S_EST_ROAINSTNUM = Column(Numeric(20, 4))
    S_EST_ROEINSTNUM = Column(Numeric(20, 4))
    S_EST_OPROFITINSTNUM = Column(Numeric(20, 4))
    S_EST_AVGOC = Column(Numeric(20, 4))
    S_EST_MEDIAOC = Column(Numeric(20, 4))
    S_EST_STOC = Column(Numeric(20, 4))
    S_EST_MAXOC = Column(Numeric(20, 4))
    S_EST_MINOC = Column(Numeric(20, 4))
    S_EST_OCINSTNUM = Column(Numeric(20, 4))
    S_EST_BASESHARE = Column(Numeric(20, 4))
    S_EST_YEARTYPE = Column(String(20))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECONSEPTION(Base):
    __tablename__ = 'ASHARECONSEPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_SEC_CODE = Column(String(50), index=True)
    WIND_SEC_NAME = Column(String(100))
    ENTRY_DT = Column(String(8))
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARECSINDUSTRIESCLAS(Base):
    __tablename__ = 'ASHARECSINDUSTRIESCLASS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    CS_IND_CODE = Column(String(50), index=True)
    ENTRY_DT = Column(String(8))
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREDESCRIPTION(Base):
    __tablename__ = 'ASHAREDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_CODE = Column(String(40))
    S_INFO_NAME = Column(String(50))
    S_INFO_COMPNAME = Column(String(100))
    S_INFO_COMPNAMEENG = Column(String(100))
    S_INFO_ISINCODE = Column(String(40))
    S_INFO_EXCHMARKET = Column(String(40))
    S_INFO_LISTBOARD = Column(String(10))
    S_INFO_LISTDATE = Column(String(8))
    S_INFO_DELISTDATE = Column(String(8))
    S_INFO_SEDOLCODE = Column(String(40))
    CRNCY_CODE = Column(String(10))
    S_INFO_PINYIN = Column(String(10))
    S_INFO_LISTBOARDNAME = Column(String(10))
    IS_SHSC = Column(Numeric(5, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREDIVIDEND(Base):
    __tablename__ = 'ASHAREDIVIDEND'

    OBJECT_ID = Column(String(100), primary_key=True)
    WIND_CODE = Column(String(40))
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_DIV_PROGRESS = Column(String(10))
    STK_DVD_PER_SH = Column(Numeric(20, 8))
    CASH_DVD_PER_SH_PRE_TAX = Column(Numeric(24, 8))
    CASH_DVD_PER_SH_AFTER_TAX = Column(Numeric(24, 8))
    EQY_RECORD_DT = Column(String(8))
    EX_DT = Column(String(8))
    DVD_PAYOUT_DT = Column(String(8))
    LISTING_DT_OF_DVD_SHR = Column(String(8))
    S_DIV_PRELANDATE = Column(String(8))
    S_DIV_SMTGDATE = Column(String(8))
    DVD_ANN_DT = Column(String(8))
    S_DIV_BASEDATE = Column(String(8))
    S_DIV_BASESHARE = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    ANN_DT = Column(String(8))
    IS_CHANGED = Column(Numeric(5, 0))
    REPORT_PERIOD = Column(String(8))
    S_DIV_CHANGE = Column(String(500))
    S_DIV_BONUSRATE = Column(Numeric(20, 8))
    S_DIV_CONVERSEDRATE = Column(Numeric(20, 8))
    MEMO = Column(String(200))
    S_DIV_PREANNDT = Column(String(8))
    S_DIV_OBJECT = Column(String(100))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREEARNINGEST(Base):
    __tablename__ = 'ASHAREEARNINGEST'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_CODE = Column(String(40))
    RESEARCH_INST_NAME = Column(String(20))
    ANALYST_NAME = Column(String(20))
    EST_DT = Column(String(8))
    REPORTING_PERIOD = Column(String(8), index=True)
    EST_EPS_DILUTED = Column(Numeric(20, 4))
    EST_NET_PROFIT = Column(Numeric(20, 4))
    EST_MAIN_BUS_INC = Column(Numeric(20, 4))
    EST_EBIT = Column(Numeric(20, 4))
    EST_EBITDA = Column(Numeric(20, 4))
    EST_BASE_CAP = Column(Numeric(20, 4))
    ANN_DT = Column(String(8))
    S_EST_CPS = Column(Numeric(20, 4))
    S_EST_DPS = Column(Numeric(20, 4))
    S_EST_BPS = Column(Numeric(20, 4))
    S_EST_EBT = Column(Numeric(20, 4))
    S_EST_ROA = Column(Numeric(20, 4))
    S_EST_ROE = Column(Numeric(20, 4))
    S_EST_OPROFIT = Column(Numeric(20, 4))
    S_EST_EPSDILUTED = Column(Numeric(20, 4))
    S_EST_EPSBASIC = Column(Numeric(20, 4))
    S_EST_OC = Column(Numeric(20, 4))
    S_EST_NPCAL = Column(Numeric(20, 4))
    S_EST_EPSCAL = Column(Numeric(20, 4))
    S_EST_NPRATE = Column(Numeric(20, 4))
    S_EST_EPSRATE = Column(Numeric(20, 4))
    S_EST_PE = Column(Numeric(20, 4))
    S_EST_PB = Column(Numeric(20, 4))
    S_EST_EVEBITDA = Column(Numeric(20, 4))
    S_EST_DIVIDENDYIELD = Column(Numeric(20, 4))
    S_EST_ENDDATE = Column(String(8))
    S_EST_OPE = Column(Numeric(10, 4))
    ANALYST_ID = Column(String(200))
    COLLECT_TIME = Column(DateTime)
    FIRST_OPTIME = Column(DateTime)
    REPORT_TYPECODE = Column(Numeric(9, 0))
    REPORT_NAME = Column(String(1024))
    REPORT_SUMMARY = Column(String)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREEODDERIVATIVEINDICATOR(Base):
    __tablename__ = 'ASHAREEODDERIVATIVEINDICATOR'
    __table_args__ = (
        Index('SID_TD_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_VAL_MV = Column(Numeric(20, 4))
    S_DQ_MV = Column(Numeric(20, 4))
    S_PQ_HIGH_52W_ = Column(Numeric(20, 4))
    S_PQ_LOW_52W_ = Column(Numeric(20, 4))
    S_VAL_PE = Column(Numeric(20, 4))
    S_VAL_PB_NEW = Column(Numeric(20, 4))
    S_VAL_PE_TTM = Column(Numeric(20, 4))
    S_VAL_PCF_OCF = Column(Numeric(20, 4))
    S_VAL_PCF_OCFTTM = Column(Numeric(20, 4))
    S_VAL_PCF_NCF = Column(Numeric(20, 4))
    S_VAL_PCF_NCFTTM = Column(Numeric(20, 4))
    S_VAL_PS = Column(Numeric(20, 4))
    S_VAL_PS_TTM = Column(Numeric(20, 4))
    S_DQ_TURN = Column(Numeric(20, 4))
    S_DQ_FREETURNOVER = Column(Numeric(20, 4))
    TOT_SHR_TODAY = Column(Numeric(20, 4))
    FLOAT_A_SHR_TODAY = Column(Numeric(20, 4))
    S_DQ_CLOSE_TODAY = Column(Numeric(20, 4))
    S_PRICE_DIV_DPS = Column(Numeric(20, 4))
    S_PQ_ADJHIGH_52W = Column(Numeric(20, 4))
    S_PQ_ADJLOW_52W = Column(Numeric(20, 4))
    FREE_SHARES_TODAY = Column(Numeric(20, 4))
    NET_PROFIT_PARENT_COMP_TTM = Column(Numeric(20, 4))
    NET_PROFIT_PARENT_COMP_LYR = Column(Numeric(20, 4))
    NET_ASSETS_TODAY = Column(Numeric(20, 4))
    NET_CASH_FLOWS_OPER_ACT_TTM = Column(Numeric(20, 4))
    NET_CASH_FLOWS_OPER_ACT_LYR = Column(Numeric(20, 4))
    OPER_REV_TTM = Column(Numeric(20, 4))
    OPER_REV_LYR = Column(Numeric(20, 4))
    NET_INCR_CASH_CASH_EQU_TTM = Column(Numeric(20, 4))
    NET_INCR_CASH_CASH_EQU_LYR = Column(Numeric(20, 4))
    UP_DOWN_LIMIT_STATUS = Column(Numeric(2, 0))
    LOWEST_HIGHEST_STATUS = Column(Numeric(2, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREEODPRICES(Base):
    __tablename__ = 'ASHAREEODPRICES'
    __table_args__ = (
        Index('SID_TD_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_PCTCHANGE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_ADJPRECLOSE = Column(Numeric(20, 4))
    S_DQ_ADJOPEN = Column(Numeric(20, 4))
    S_DQ_ADJHIGH = Column(Numeric(20, 4))
    S_DQ_ADJLOW = Column(Numeric(20, 4))
    S_DQ_ADJCLOSE = Column(Numeric(20, 4))
    S_DQ_ADJFACTOR = Column(Numeric(20, 6))
    S_DQ_AVGPRICE = Column(Numeric(20, 4))
    S_DQ_TRADESTATUS = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREEQUITYPLEDGEINFO(Base):
    __tablename__ = 'ASHAREEQUITYPLEDGEINFO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    S_PLEDGE_BGDATE = Column(String(8))
    S_PLEDGE_ENDDATE = Column(String(8))
    S_HOLDER_NAME = Column(String(200))
    S_PLEDGE_SHARES = Column(Numeric(20, 4))
    S_PLEDGOR = Column(String(200))
    S_DISCHARGE_DATE = Column(String(8))
    S_REMARK = Column(String(1000))
    IS_DISCHARGE = Column(Numeric(1, 0))
    S_HOLDER_TYPE_CODE = Column(Numeric(9, 0))
    S_HOLDER_ID = Column(String(10))
    S_PLEDGOR_TYPE_CODE = Column(Numeric(9, 0))
    S_PLEDGOR_ID = Column(String(10))
    S_SHR_CATEGORY_CODE = Column(Numeric(9, 0))
    S_TOTAL_HOLDING_SHR = Column(Numeric(20, 4))
    S_TOTAL_PLEDGE_SHR = Column(Numeric(20, 4))
    S_PLEDGE_SHR_RATIO = Column(Numeric(20, 4))
    S_TOTAL_HOLDING_SHR_RATIO = Column(Numeric(20, 4))
    IS_EQUITY_PLEDGE_REPO = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREEQUITYRELATIONSHIP(Base):
    __tablename__ = 'ASHAREEQUITYRELATIONSHIPS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    ENDDATE = Column(String(8))
    S_INFO_COMPNAME = Column(String(200))
    S_INFO_COMPCODE = Column(String(10))
    RELATION_TYPE = Column(String(40))
    S_HOLDER_NAME = Column(String(200))
    S_HOLDER_CODE = Column(String(10))
    S_HOLDER_TYPE = Column(Numeric(5, 0))
    S_HOLDER_PCT = Column(Numeric(20, 4))
    IS_ACTUALCONTROLLER = Column(Numeric(5, 0))
    ACTUALCONTROLLER_TYPE = Column(String(80))
    ACTUALCONTROLLER_IS_ANN = Column(Numeric(5, 0))
    ACTUALCONTROLLER_INTRO = Column(String(1000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREEQUITYTRANSFER(Base):
    __tablename__ = 'ASHAREEQUITYTRANSFER'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(200))
    TRANSFEROR = Column(String(100))
    TRANSFEROR_DISCLOSURE = Column(String(40))
    TRANSFEREE = Column(String(100))
    TRANSFEREE_DISCLOSURE = Column(String(40))
    TARGETCOMPANY = Column(String(100))
    TARGETCOMPANY_DISCLOSURE = Column(String(40))
    PROGRESS = Column(String(10))
    TRADE_DT = Column(String(8))
    TRANSFERMETHOD = Column(String(40))
    SHARECATEGORYBEFTRANSFER = Column(String(40))
    SHARECATEGORYAFTTRANSFER = Column(String(40))
    TRANSFERREDSHARES = Column(Numeric(20, 4))
    TRANSFERPROPORATION = Column(Numeric(20, 4))
    TRANSFEEFISHAREHOLDING = Column(Numeric(20, 4))
    TRANSFERPRICEPERSHARE = Column(Numeric(20, 4))
    TRADINGAMOUNT = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    IS_TRANSFERRED = Column(Numeric(5, 0))
    IS_RELDPARTRANSACTIONS = Column(Numeric(1, 0))
    FIRST_DT = Column(String(8))
    SASACDATE = Column(String(8))
    APPROVEDDATE = Column(String(8))
    ANN_DT = Column(String(8))
    SUMMARY = Column(String(3000))
    TRADE_RESULT = Column(String(2000))
    INFLUENCE = Column(String)
    HIS_CHANGE = Column(String(1000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREEXRIGHTDIVIDENDRECORD(Base):
    __tablename__ = 'ASHAREEXRIGHTDIVIDENDRECORD'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    EX_DATE = Column(String(8))
    EX_TYPE = Column(String(40))
    EX_DESCRIPTION = Column(String(100))
    CASH_DIVIDEND_RATIO = Column(Numeric(15, 4))
    BONUS_SHARE_RATIO = Column(Numeric(15, 4))
    RIGHTSISSUE_RATIO = Column(Numeric(15, 4))
    RIGHTSISSUE_PRICE = Column(Numeric(15, 4))
    CONVERSED_RATIO = Column(Numeric(15, 4))
    SEO_PRICE = Column(Numeric(15, 4))
    SEO_RATIO = Column(Numeric(15, 4))
    CONSOLIDATE_SPLIT_RATIO = Column(Numeric(20, 6))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREFINANCIALEXPENSE(Base):
    __tablename__ = 'ASHAREFINANCIALEXPENSE'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPECODE = Column(Numeric(9, 0))
    S_STMNOTE_INTEXP = Column(Numeric(20, 4))
    S_STMNOTE_INTINC = Column(Numeric(20, 4))
    S_STMNOTE_EXCH = Column(Numeric(20, 4))
    S_STMNOTE_FEE = Column(Numeric(20, 4))
    S_STMNOTE_OTHERS = Column(Numeric(20, 4))
    S_STMNOTE_FINEXP = Column(Numeric(20, 4))
    S_STMNOTE_FINEXP_1 = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREFINANCIALINDICATOR(Base):
    __tablename__ = 'ASHAREFINANCIALINDICATOR'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_CODE = Column(String(40))
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_FA_EXTRAORDINARY = Column(Numeric(20, 4))
    S_FA_DEDUCTEDPROFIT = Column(Numeric(20, 4))
    S_FA_GROSSMARGIN = Column(Numeric(20, 4))
    S_FA_OPERATEINCOME = Column(Numeric(20, 4))
    S_FA_INVESTINCOME = Column(Numeric(20, 4))
    S_STMNOTE_FINEXP = Column(Numeric(20, 4))
    S_STM_IS = Column(Numeric(20, 4))
    S_FA_EBIT = Column(Numeric(20, 4))
    S_FA_EBITDA = Column(Numeric(20, 4))
    S_FA_FCFF = Column(Numeric(20, 4))
    S_FA_FCFE = Column(Numeric(20, 4))
    S_FA_EXINTERESTDEBT_CURRENT = Column(Numeric(20, 4))
    S_FA_EXINTERESTDEBT_NONCURRENT = Column(Numeric(20, 4))
    S_FA_INTERESTDEBT = Column(Numeric(20, 4))
    S_FA_NETDEBT = Column(Numeric(20, 4))
    S_FA_TANGIBLEASSET = Column(Numeric(20, 4))
    S_FA_WORKINGCAPITAL = Column(Numeric(20, 4))
    S_FA_NETWORKINGCAPITAL = Column(Numeric(20, 4))
    S_FA_INVESTCAPITAL = Column(Numeric(20, 4))
    S_FA_RETAINEDEARNINGS = Column(Numeric(20, 4))
    S_FA_EPS_BASIC = Column(Numeric(20, 4))
    S_FA_EPS_DILUTED = Column(Numeric(20, 4))
    S_FA_EPS_DILUTED2 = Column(Numeric(20, 4))
    S_FA_BPS = Column(Numeric(20, 4))
    S_FA_OCFPS = Column(Numeric(20, 4))
    S_FA_GRPS = Column(Numeric(20, 4))
    S_FA_ORPS = Column(Numeric(20, 4))
    S_FA_SURPLUSCAPITALPS = Column(Numeric(20, 4))
    S_FA_SURPLUSRESERVEPS = Column(Numeric(20, 4))
    S_FA_UNDISTRIBUTEDPS = Column(Numeric(20, 4))
    S_FA_RETAINEDPS = Column(Numeric(20, 4))
    S_FA_CFPS = Column(Numeric(20, 4))
    S_FA_EBITPS = Column(Numeric(20, 4))
    S_FA_FCFFPS = Column(Numeric(20, 4))
    S_FA_FCFEPS = Column(Numeric(20, 4))
    S_FA_NETPROFITMARGIN = Column(Numeric(20, 4))
    S_FA_GROSSPROFITMARGIN = Column(Numeric(20, 4))
    S_FA_COGSTOSALES = Column(Numeric(20, 4))
    S_FA_EXPENSETOSALES = Column(Numeric(20, 4))
    S_FA_PROFITTOGR = Column(Numeric(20, 4))
    S_FA_SALEEXPENSETOGR = Column(Numeric(20, 4))
    S_FA_ADMINEXPENSETOGR = Column(Numeric(20, 4))
    S_FA_FINAEXPENSETOGR = Column(Numeric(20, 4))
    S_FA_IMPAIRTOGR_TTM = Column(Numeric(20, 4))
    S_FA_GCTOGR = Column(Numeric(20, 4))
    S_FA_OPTOGR = Column(Numeric(20, 4))
    S_FA_EBITTOGR = Column(Numeric(20, 4))
    S_FA_ROE = Column(Numeric(20, 4))
    S_FA_ROE_DEDUCTED = Column(Numeric(20, 4))
    S_FA_ROA2 = Column(Numeric(20, 4))
    S_FA_ROA = Column(Numeric(20, 4))
    S_FA_ROIC = Column(Numeric(20, 4))
    S_FA_ROE_YEARLY = Column(Numeric(20, 4))
    S_FA_ROA2_YEARLY = Column(Numeric(20, 4))
    S_FA_ROE_AVG = Column(Numeric(20, 4))
    S_FA_OPERATEINCOMETOEBT = Column(Numeric(20, 4))
    S_FA_INVESTINCOMETOEBT = Column(Numeric(20, 4))
    S_FA_NONOPERATEPROFITTOEBT = Column(Numeric(20, 4))
    S_FA_TAXTOEBT = Column(Numeric(20, 4))
    S_FA_DEDUCTEDPROFITTOPROFIT = Column(Numeric(20, 4))
    S_FA_SALESCASHINTOOR = Column(Numeric(20, 4))
    S_FA_OCFTOOR = Column(Numeric(20, 4))
    S_FA_OCFTOOPERATEINCOME = Column(Numeric(20, 4))
    S_FA_CAPITALIZEDTODA = Column(Numeric(20, 4))
    S_FA_DEBTTOASSETS = Column(Numeric(20, 4))
    S_FA_ASSETSTOEQUITY = Column(Numeric(20, 4))
    S_FA_DUPONT_ASSETSTOEQUITY = Column(Numeric(20, 4))
    S_FA_CATOASSETS = Column(Numeric(20, 4))
    S_FA_NCATOASSETS = Column(Numeric(20, 4))
    S_FA_TANGIBLEASSETSTOASSETS = Column(Numeric(20, 4))
    S_FA_INTDEBTTOTOTALCAP = Column(Numeric(20, 4))
    S_FA_EQUITYTOTOTALCAPITAL = Column(Numeric(20, 4))
    S_FA_CURRENTDEBTTODEBT = Column(Numeric(20, 4))
    S_FA_LONGDEBTODEBT = Column(Numeric(20, 4))
    S_FA_CURRENT = Column(Numeric(20, 4))
    S_FA_QUICK = Column(Numeric(20, 4))
    S_FA_CASHRATIO = Column(Numeric(20, 4))
    S_FA_OCFTOSHORTDEBT = Column(Numeric(20, 4))
    S_FA_DEBTTOEQUITY = Column(Numeric(20, 4))
    S_FA_EQUITYTODEBT = Column(Numeric(20, 4))
    S_FA_EQUITYTOINTERESTDEBT = Column(Numeric(20, 4))
    S_FA_TANGIBLEASSETTODEBT = Column(Numeric(20, 4))
    S_FA_TANGASSETTOINTDEBT = Column(Numeric(20, 4))
    S_FA_TANGIBLEASSETTONETDEBT = Column(Numeric(20, 4))
    S_FA_OCFTODEBT = Column(Numeric(20, 4))
    S_FA_OCFTOINTERESTDEBT = Column(Numeric(20, 4))
    S_FA_OCFTONETDEBT = Column(Numeric(20, 4))
    S_FA_EBITTOINTEREST = Column(Numeric(20, 4))
    S_FA_LONGDEBTTOWORKINGCAPITAL = Column(Numeric(20, 4))
    S_FA_EBITDATODEBT = Column(Numeric(20, 4))
    S_FA_TURNDAYS = Column(Numeric(20, 4))
    S_FA_INVTURNDAYS = Column(Numeric(20, 4))
    S_FA_ARTURNDAYS = Column(Numeric(20, 4))
    S_FA_INVTURN = Column(Numeric(20, 4))
    S_FA_ARTURN = Column(Numeric(20, 4))
    S_FA_CATURN = Column(Numeric(20, 4))
    S_FA_FATURN = Column(Numeric(20, 4))
    S_FA_ASSETSTURN = Column(Numeric(20, 4))
    S_FA_ROA_YEARLY = Column(Numeric(20, 4))
    S_FA_DUPONT_ROA = Column(Numeric(20, 4))
    S_STM_BS = Column(Numeric(20, 4))
    S_FA_PREFINEXPENSE_OPPROFIT = Column(Numeric(20, 4))
    S_FA_NONOPPROFIT = Column(Numeric(20, 4))
    S_FA_OPTOEBT = Column(Numeric(20, 4))
    S_FA_NOPTOEBT = Column(Numeric(20, 4))
    S_FA_OCFTOPROFIT = Column(Numeric(20, 4))
    S_FA_CASHTOLIQDEBT = Column(Numeric(20, 4))
    S_FA_CASHTOLIQDEBTWITHINTEREST = Column(Numeric(20, 4))
    S_FA_OPTOLIQDEBT = Column(Numeric(20, 4))
    S_FA_OPTODEBT = Column(Numeric(20, 4))
    S_FA_ROIC_YEARLY = Column(Numeric(20, 4))
    S_FA_TOT_FATURN = Column(Numeric(20, 4))
    S_FA_PROFITTOOP = Column(Numeric(20, 4))
    S_QFA_OPERATEINCOME = Column(Numeric(20, 4))
    S_QFA_INVESTINCOME = Column(Numeric(20, 4))
    S_QFA_DEDUCTEDPROFIT = Column(Numeric(20, 4))
    S_QFA_EPS = Column(Numeric(20, 4))
    S_QFA_NETPROFITMARGIN = Column(Numeric(20, 4))
    S_QFA_GROSSPROFITMARGIN = Column(Numeric(20, 4))
    S_QFA_EXPENSETOSALES = Column(Numeric(20, 4))
    S_QFA_PROFITTOGR = Column(Numeric(20, 4))
    S_QFA_SALEEXPENSETOGR = Column(Numeric(20, 4))
    S_QFA_ADMINEXPENSETOGR = Column(Numeric(20, 4))
    S_QFA_FINAEXPENSETOGR = Column(Numeric(20, 4))
    S_QFA_IMPAIRTOGR_TTM = Column(Numeric(20, 4))
    S_QFA_GCTOGR = Column(Numeric(20, 4))
    S_QFA_OPTOGR = Column(Numeric(20, 4))
    S_QFA_ROE = Column(Numeric(20, 4))
    S_QFA_ROE_DEDUCTED = Column(Numeric(20, 4))
    S_QFA_ROA = Column(Numeric(20, 4))
    S_QFA_OPERATEINCOMETOEBT = Column(Numeric(20, 4))
    S_QFA_INVESTINCOMETOEBT = Column(Numeric(20, 4))
    S_QFA_DEDUCTEDPROFITTOPROFIT = Column(Numeric(20, 4))
    S_QFA_SALESCASHINTOOR = Column(Numeric(20, 4))
    S_QFA_OCFTOSALES = Column(Numeric(20, 4))
    S_QFA_OCFTOOR = Column(Numeric(20, 4))
    S_FA_YOYEPS_BASIC = Column(Numeric(20, 4))
    S_FA_YOYEPS_DILUTED = Column(Numeric(20, 4))
    S_FA_YOYOCFPS = Column(Numeric(20, 4))
    S_FA_YOYOP = Column(Numeric(20, 4))
    S_FA_YOYEBT = Column(Numeric(20, 4))
    S_FA_YOYNETPROFIT = Column(Numeric(20, 4))
    S_FA_YOYNETPROFIT_DEDUCTED = Column(Numeric(20, 4))
    S_FA_YOYOCF = Column(Numeric(20, 4))
    S_FA_YOYROE = Column(Numeric(20, 4))
    S_FA_YOYBPS = Column(Numeric(20, 4))
    S_FA_YOYASSETS = Column(Numeric(20, 4))
    S_FA_YOYEQUITY = Column(Numeric(20, 4))
    S_FA_YOY_TR = Column(Numeric(20, 4))
    S_FA_YOY_OR = Column(Numeric(20, 4))
    S_QFA_YOYGR = Column(Numeric(20, 4))
    S_QFA_CGRGR = Column(Numeric(20, 4))
    S_QFA_YOYSALES = Column(Numeric(20, 4))
    S_QFA_CGRSALES = Column(Numeric(20, 4))
    S_QFA_YOYOP = Column(Numeric(20, 4))
    S_QFA_CGROP = Column(Numeric(20, 4))
    S_QFA_YOYPROFIT = Column(Numeric(20, 4))
    S_QFA_CGRPROFIT = Column(Numeric(20, 4))
    S_QFA_YOYNETPROFIT = Column(Numeric(20, 4))
    S_QFA_CGRNETPROFIT = Column(Numeric(20, 4))
    S_FA_YOY_EQUITY = Column(Numeric(20, 4))
    RD_EXPENSE = Column(Numeric(20, 4))
    WAA_ROE = Column(Numeric(20, 4))
    S_INFO_COMPCODE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREFLOATHOLDER(Base):
    __tablename__ = 'ASHAREFLOATHOLDER'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    ANN_DT = Column(String(8))
    S_HOLDER_ENDDATE = Column(String(8))
    S_HOLDER_HOLDERCATEGORY = Column(String(1))
    S_HOLDER_NAME = Column(String(100))
    S_HOLDER_QUANTITY = Column(Numeric(20, 4))
    S_HOLDER_WINDNAME = Column(String(200))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREFREEFLOAT(Base):
    __tablename__ = 'ASHAREFREEFLOAT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    CHANGE_DT = Column(String(8))
    S_SHARE_FREESHARES = Column(Numeric(20, 4))
    CHANGE_DT1 = Column(String(8))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREFREEFLOATCALENDAR(Base):
    __tablename__ = 'ASHAREFREEFLOATCALENDAR'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_LISTDATE = Column(String(8))
    ANN_DT = Column(String(8))
    S_SHARE_LST = Column(Numeric(20, 4))
    S_SHARE_NONLST = Column(Numeric(20, 4))
    S_SHARE_UNRESTRICTED = Column(Numeric(20, 4))
    S_SHARE_LSTTYPECODE = Column(Numeric(9, 0))
    S_SHARE_LST_IS_ANN = Column(String(1))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREGUARANTEE(Base):
    __tablename__ = 'ASHAREGUARANTEE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(40), index=True)
    RELATION = Column(String(40))
    GUARANTOR = Column(String(100))
    RELATION2 = Column(String(40))
    SECUREDPARTY = Column(String(100))
    METHOD = Column(String(40))
    AMOUNT = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    TERM = Column(Numeric(20, 4))
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    IS_COMPLETE = Column(Numeric(1, 0))
    IS_RELATED = Column(Numeric(1, 0))
    TRADE_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8))
    IS_OVERDUE = Column(Numeric(1, 0))
    OVERDUE_AMOUNT = Column(Numeric(20, 4))
    IS_COUNTERGUARANTEE = Column(Numeric(1, 0))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREHOLDERNUMBER(Base):
    __tablename__ = 'ASHAREHOLDERNUMBER'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    ANN_DT = Column(String(8))
    S_HOLDER_ENDDATE = Column(String(8))
    S_HOLDER_NUM = Column(Numeric(20, 4))
    S_HOLDER_TOTAL_NUM = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREHOLDERSMEETING(Base):
    __tablename__ = 'ASHAREHOLDERSMEETING'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    MEETING_DT = Column(String(8))
    MEETING_TYPE = Column(String(20))
    VOTINGCODE = Column(Numeric(9, 0))
    SMTGRECDATE = Column(String(8))
    SPOTMTGSTARTDATE = Column(String(8))
    SPOTMTGENDDATE = Column(String(8))
    IS_INTNET = Column(String(1))
    INTNETCODE = Column(String(10))
    INTNETNAME = Column(String(20))
    MEETING_CONTENT = Column(String)
    CHANGE_HIS = Column(String(100))
    MEETEVENT_ID = Column(String(40))
    IS_NEW = Column(Numeric(5, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREIBROKERINDICATOR(Base):
    __tablename__ = 'ASHAREIBROKERINDICATOR'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    REPORT_PERIOD = Column(String(8), index=True)
    ANN_DT = Column(String(8))
    STATEMENT_TYPE = Column(String(40))
    IFLISTED_DATA = Column(Numeric(5, 0))
    NET_CAPITAL = Column(Numeric(20, 4))
    TRUSTED_CAPITAL = Column(Numeric(20, 4))
    NET_GEARING_RATIO = Column(Numeric(20, 4))
    PROP_EQUITY_RATIO = Column(Numeric(20, 4))
    LONGTERM_INVEST_RATIO = Column(Numeric(20, 4))
    FIXED_CAPITAL_RATIO = Column(Numeric(20, 4))
    FEE_BUSINESS_RATIO = Column(Numeric(20, 4))
    TOTAL_CAPITAL_RETURN = Column(Numeric(20, 4))
    NET_CAPITAL_YIELD = Column(Numeric(20, 4))
    CURRENT_RATIO = Column(Numeric(20, 4))
    ASSET_LIABILITY_RATIO = Column(Numeric(20, 4))
    ASSET_TURNOVER_RATIO = Column(Numeric(20, 4))
    NET_CAPITAL_RETURN = Column(Numeric(20, 4))
    CONTINGENT_LIABILITY_RATIO = Column(Numeric(20, 4))
    PROP_SECURITIES = Column(Numeric(20, 4))
    TREASURY_BOND = Column(Numeric(20, 4))
    INVESTMENT_FUNDS = Column(Numeric(20, 4))
    STOCKS = Column(Numeric(20, 4))
    CONVERTIBLE_BOND = Column(Numeric(20, 4))
    PER_CAPITA_PROFITS = Column(Numeric(20, 4))
    NET_CAP_TOTAL_RISKPROV = Column(Numeric(20, 4))
    NET_CAP_NET_ASSETS = Column(Numeric(20, 4))
    PROP_EQU_DER_NETCAP = Column(Numeric(20, 4))
    PROP_FIXEDINCOME_NETCAP = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREILLEGALITY(Base):
    __tablename__ = 'ASHAREILLEGALITY'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    ILLEG_TYPE = Column(String(100))
    SUBJECT_TYPE = Column(Numeric(9, 0))
    SUBJECT = Column(String(100))
    RELATION_TYPE = Column(Numeric(9, 0))
    BEHAVIOR = Column(String)
    DISPOSAL_DT = Column(String(8))
    DISPOSAL_TYPE = Column(String(100))
    METHOD = Column(String(2000))
    PROCESSOR = Column(String(200))
    AMOUNT = Column(Numeric(20, 4))
    BAN_YEAR = Column(Numeric(20, 4))
    REF_RULE = Column(String(300))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINCDESCRIPTION(Base):
    __tablename__ = 'ASHAREINCDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INC_SEQUENCE = Column(String(6), index=True)
    S_INC_SUBJECT = Column(Numeric(9, 0), index=True)
    S_INC_TYPE = Column(Numeric(9, 0))
    S_INC_QUANTITY = Column(Numeric(20, 4))
    S_INC_FIRSTINC = Column(String(8))
    S_INC_ENDINC = Column(String(8))
    S_INC_INITEXECPRI = Column(Numeric(20, 4))
    S_INC_EXPIRYDATE = Column(Numeric(20, 4))
    ANN_DT = Column(String(8))
    S_INC_PROGRAMDESCRIPT = Column(String(3000))
    S_INC_INCENTSHARESALEDESCRIPT = Column(String(1000))
    S_INC_INCENTCONDITION = Column(String(2000))
    S_INC_OPTEXESPECIALCONDITION = Column(String(2000))
    PROGRESS = Column(String(10))
    PRICE_DESCRIPTION = Column(String(80))
    INC_NUMBERS_RATE = Column(Numeric(20, 4))
    PREPLAN_ANN_DATE = Column(String(8))
    GM_DATE = Column(String(8))
    IMPLEMENT_DATE = Column(String(8))
    INC_FUND_DESCRIPTION = Column(String(1000))
    INTERVAL_MONTHS = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINCEXECQTYPRI(Base):
    __tablename__ = 'ASHAREINCEXECQTYPRI'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(100), index=True)
    S_INC_SEQUENCE = Column(String(10), index=True)
    S_INC_NAME = Column(String(200))
    S_INC_EXECQTY = Column(Numeric(20, 4))
    S_INC_EXECPRI = Column(Numeric(20, 4))
    S_INC_EXECDATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINCEXERCISEPCT(Base):
    __tablename__ = 'ASHAREINCEXERCISEPCT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INC_SEQUENCE = Column(String(6), index=True)
    S_INC_EXECBATCH = Column(String(6))
    S_INC_EXECPCT = Column(Numeric(20, 4))
    S_INC_INTERVALTIME = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINCOME(Base):
    __tablename__ = 'ASHAREINCOME'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_CODE = Column(String(40))
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPE = Column(String(10))
    CRNCY_CODE = Column(String(10))
    TOT_OPER_REV = Column(Numeric(20, 4))
    OPER_REV = Column(Numeric(20, 4))
    INT_INC = Column(Numeric(20, 4))
    NET_INT_INC = Column(Numeric(20, 4))
    INSUR_PREM_UNEARNED = Column(Numeric(20, 4))
    HANDLING_CHRG_COMM_INC = Column(Numeric(20, 4))
    NET_HANDLING_CHRG_COMM_INC = Column(Numeric(20, 4))
    NET_INC_OTHER_OPS = Column(Numeric(20, 4))
    PLUS_NET_INC_OTHER_BUS = Column(Numeric(20, 4))
    PREM_INC = Column(Numeric(20, 4))
    LESS_CEDED_OUT_PREM = Column(Numeric(20, 4))
    CHG_UNEARNED_PREM_RES = Column(Numeric(20, 4))
    INCL_REINSURANCE_PREM_INC = Column(Numeric(20, 4))
    NET_INC_SEC_TRADING_BROK_BUS = Column(Numeric(20, 4))
    NET_INC_SEC_UW_BUS = Column(Numeric(20, 4))
    NET_INC_EC_ASSET_MGMT_BUS = Column(Numeric(20, 4))
    OTHER_BUS_INC = Column(Numeric(20, 4))
    PLUS_NET_GAIN_CHG_FV = Column(Numeric(20, 4))
    PLUS_NET_INVEST_INC = Column(Numeric(20, 4))
    INCL_INC_INVEST_ASSOC_JV_ENTP = Column(Numeric(20, 4))
    PLUS_NET_GAIN_FX_TRANS = Column(Numeric(20, 4))
    TOT_OPER_COST = Column(Numeric(20, 4))
    LESS_OPER_COST = Column(Numeric(20, 4))
    LESS_INT_EXP = Column(Numeric(20, 4))
    LESS_HANDLING_CHRG_COMM_EXP = Column(Numeric(20, 4))
    LESS_TAXES_SURCHARGES_OPS = Column(Numeric(20, 4))
    LESS_SELLING_DIST_EXP = Column(Numeric(20, 4))
    LESS_GERL_ADMIN_EXP = Column(Numeric(20, 4))
    LESS_FIN_EXP = Column(Numeric(20, 4))
    LESS_IMPAIR_LOSS_ASSETS = Column(Numeric(20, 4))
    PREPAY_SURR = Column(Numeric(20, 4))
    TOT_CLAIM_EXP = Column(Numeric(20, 4))
    CHG_INSUR_CONT_RSRV = Column(Numeric(20, 4))
    DVD_EXP_INSURED = Column(Numeric(20, 4))
    REINSURANCE_EXP = Column(Numeric(20, 4))
    OPER_EXP = Column(Numeric(20, 4))
    LESS_CLAIM_RECB_REINSURER = Column(Numeric(20, 4))
    LESS_INS_RSRV_RECB_REINSURER = Column(Numeric(20, 4))
    LESS_EXP_RECB_REINSURER = Column(Numeric(20, 4))
    OTHER_BUS_COST = Column(Numeric(20, 4))
    OPER_PROFIT = Column(Numeric(20, 4))
    PLUS_NON_OPER_REV = Column(Numeric(20, 4))
    LESS_NON_OPER_EXP = Column(Numeric(20, 4))
    IL_NET_LOSS_DISP_NONCUR_ASSET = Column(Numeric(20, 4))
    TOT_PROFIT = Column(Numeric(20, 4))
    INC_TAX = Column(Numeric(20, 4))
    UNCONFIRMED_INVEST_LOSS = Column(Numeric(20, 4))
    NET_PROFIT_INCL_MIN_INT_INC = Column(Numeric(20, 4))
    NET_PROFIT_EXCL_MIN_INT_INC = Column(Numeric(20, 4))
    MINORITY_INT_INC = Column(Numeric(20, 4))
    OTHER_COMPREH_INC = Column(Numeric(20, 4))
    TOT_COMPREH_INC = Column(Numeric(20, 4))
    TOT_COMPREH_INC_PARENT_COMP = Column(Numeric(20, 4))
    TOT_COMPREH_INC_MIN_SHRHLDR = Column(Numeric(20, 4))
    EBIT = Column(Numeric(20, 4))
    EBITDA = Column(Numeric(20, 4))
    NET_PROFIT_AFTER_DED_NR_LP = Column(Numeric(20, 4))
    NET_PROFIT_UNDER_INTL_ACC_STA = Column(Numeric(20, 4))
    COMP_TYPE_CODE = Column(String(2))
    S_FA_EPS_BASIC = Column(Numeric(20, 4))
    S_FA_EPS_DILUTED = Column(Numeric(20, 4))
    ACTUAL_ANN_DT = Column(String(8))
    INSURANCE_EXPENSE = Column(Numeric(20, 4))
    SPE_BAL_OPER_PROFIT = Column(Numeric(20, 4))
    TOT_BAL_OPER_PROFIT = Column(Numeric(20, 4))
    SPE_BAL_TOT_PROFIT = Column(Numeric(20, 4))
    TOT_BAL_TOT_PROFIT = Column(Numeric(20, 4))
    SPE_BAL_NET_PROFIT = Column(Numeric(20, 4))
    TOT_BAL_NET_PROFIT = Column(Numeric(20, 4))
    UNDISTRIBUTED_PROFIT = Column(Numeric(20, 4))
    ADJLOSSGAIN_PREVYEAR = Column(Numeric(20, 4))
    TRANSFER_FROM_SURPLUSRESERVE = Column(Numeric(20, 4))
    TRANSFER_FROM_HOUSINGIMPREST = Column(Numeric(20, 4))
    TRANSFER_FROM_OTHERS = Column(Numeric(20, 4))
    DISTRIBUTABLE_PROFIT = Column(Numeric(20, 4))
    WITHDR_LEGALSURPLUS = Column(Numeric(20, 4))
    WITHDR_LEGALPUBWELFUNDS = Column(Numeric(20, 4))
    WORKERS_WELFARE = Column(Numeric(20, 4))
    WITHDR_BUZEXPWELFARE = Column(Numeric(20, 4))
    WITHDR_RESERVEFUND = Column(Numeric(20, 4))
    DISTRIBUTABLE_PROFIT_SHRHDER = Column(Numeric(20, 4))
    PRFSHARE_DVD_PAYABLE = Column(Numeric(20, 4))
    WITHDR_OTHERSURPRESERVE = Column(Numeric(20, 4))
    COMSHARE_DVD_PAYABLE = Column(Numeric(20, 4))
    CAPITALIZED_COMSTOCK_DIV = Column(Numeric(20, 4))
    S_INFO_COMPCODE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINCQUANTITYDETAIL(Base):
    __tablename__ = 'ASHAREINCQUANTITYDETAILS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INC_SEQUENCE = Column(String(6), index=True)
    S_INC_NAME = Column(String(80))
    S_INC_POST = Column(String(80))
    S_INC_QUANTITY = Column(Numeric(20, 4))
    S_INC_TOTALQTYPCT = Column(Numeric(20, 4))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINCQUANTITYPRICE(Base):
    __tablename__ = 'ASHAREINCQUANTITYPRICE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INC_SEQUENCE = Column(String(6), index=True)
    S_INC_TRANSFERPRIPER = Column(Numeric(20, 4))
    S_INC_QUANTITY = Column(Numeric(20, 4))
    S_INC_GETFUNDQTY = Column(Numeric(20, 4))
    S_INC_ISCOMPLETED = Column(Numeric(5, 0))
    S_INC_GRANTDATE = Column(String(8))
    S_INC_DNEXEC_QUANTITY = Column(Numeric(20, 4))
    S_INC_ENDDATE = Column(String(8))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINDUSTRIESCLAS(Base):
    __tablename__ = 'ASHAREINDUSTRIESCLASS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    WIND_CODE = Column(String(40))
    WIND_IND_CODE = Column(String(50), index=True)
    ENTRY_DT = Column(String(8))
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINDUSTRIESCLASSCITIC(Base):
    __tablename__ = 'ASHAREINDUSTRIESCLASSCITICS'

    OBJECT_ID = Column(String(100), primary_key=True)
    WIND_CODE = Column(String(40))
    S_INFO_WINDCODE = Column(String(40), index=True)
    CITICS_IND_CODE = Column(String(50), index=True)
    ENTRY_DT = Column(String(8))
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINSIDEHOLDER(Base):
    __tablename__ = 'ASHAREINSIDEHOLDER'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    ANN_DT = Column(String(8))
    S_HOLDER_ENDDATE = Column(String(8))
    S_HOLDER_HOLDERCATEGORY = Column(String(1))
    S_HOLDER_NAME = Column(String(100))
    S_HOLDER_QUANTITY = Column(Numeric(20, 4))
    S_HOLDER_PCT = Column(Numeric(20, 4))
    S_HOLDER_SHARECATEGORY = Column(String(40))
    S_HOLDER_RESTRICTEDQUANTITY = Column(Numeric(20, 4))
    S_HOLDER_ANAME = Column(String(100))
    S_HOLDER_SEQUENCE = Column(String(10))
    S_HOLDER_SHARECATEGORYNAME = Column(String(40))
    S_HOLDER_MEMO = Column(String(1500))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINSIDERTRADE(Base):
    __tablename__ = 'ASHAREINSIDERTRADE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    RELATED_MANAGER_NAME = Column(String(100))
    REPORTED_TRADER_NAME = Column(String(100))
    CHANGE_VOLUME = Column(Numeric(20, 4))
    TRADE_AVG_PRICE = Column(Numeric(20, 4))
    POSITION_AFTER_TRADE = Column(Numeric(20, 4))
    TRADE_DT = Column(String(8))
    ANN_DT = Column(String(8))
    TRADE_REASON_CODE = Column(Numeric(9, 0))
    RELATED_MANAGER_POST = Column(String(80))
    TRADER_MANAGER_RELATION = Column(String(20))
    ACTUAL_ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINSTHOLDERDERDATUM(Base):
    __tablename__ = 'ASHAREINSTHOLDERDERDATA'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    REPORT_PERIOD = Column(String(8))
    S_HOLDER_COMPCODE = Column(String(40))
    S_HOLDER_NAME = Column(String(200))
    S_HOLDER_HOLDERCATEGORY = Column(String(40))
    S_HOLDER_QUANTITY = Column(Numeric(20, 4))
    S_HOLDER_PCT = Column(Numeric(20, 4))
    ANN_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINSURANCEINDICATOR(Base):
    __tablename__ = 'ASHAREINSURANCEINDICATOR'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    REPORT_PERIOD = Column(String(8), index=True)
    ANN_DT = Column(String(8))
    STATEMENT_TYPE = Column(Numeric(9, 0))
    CAP_ADEQUACY_RATIO_LIFE = Column(Numeric(20, 4))
    CAP_ADEQUACY_RATIO_PROPERTY = Column(Numeric(20, 4))
    SURRENDER_RATE = Column(Numeric(20, 4))
    POLICY_PERSISTENCY_RATE_13M = Column(Numeric(20, 4))
    POLICY_PERSISTENCY_RATE_25M = Column(Numeric(20, 4))
    POLICY_PERSISTENCY_RATE_14M = Column(Numeric(20, 4))
    POLICY_PERSISTENCY_RATE_26M = Column(Numeric(20, 4))
    NET_INVESTMENT_YIELD = Column(Numeric(20, 4))
    TOTAL_INVESTMENT_YIELD = Column(Numeric(20, 4))
    RISK_DISCOUNT_RATE = Column(Numeric(20, 4))
    COMBINED_COST_PROPERTY = Column(Numeric(20, 4))
    LOSS_RATIO_PROPERTY = Column(Numeric(20, 4))
    FEE_RATIO_PROPERTY = Column(Numeric(20, 4))
    INTRINSIC_VALUE_LIFE = Column(Numeric(20, 4))
    VALUE_NEW_BUSINESS_LIFE = Column(Numeric(20, 4))
    VALUE_EFFECTIVE_BUSINESS_LIFE = Column(Numeric(20, 4))
    ACTUAL_CAPITAL_LIFE = Column(Numeric(20, 4))
    MINIMUN_CAPITAL_LIFE = Column(Numeric(20, 4))
    ACTUAL_CAPITAL_PROPERTY = Column(Numeric(20, 4))
    MINIMUN_CAPITAL_PROPERTY = Column(Numeric(20, 4))
    ACTUAL_CAPITAL_GROUP = Column(Numeric(20, 4))
    MINIMUN_CAPITAL_GROUP = Column(Numeric(20, 4))
    CAPITAL_ADEQUACY_RATIO_GROUP = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    REPORT_TYPE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINTRODUCTION(Base):
    __tablename__ = 'ASHAREINTRODUCTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_PROVINCE = Column(String(20))
    S_INFO_CITY = Column(String(20))
    S_INFO_CHAIRMAN = Column(String(38))
    S_INFO_PRESIDENT = Column(String(38))
    S_INFO_BDSECRETARY = Column(String(500))
    S_INFO_REGCAPITAL = Column(Numeric(20, 4))
    S_INFO_FOUNDDATE = Column(String(8))
    S_INFO_CHINESEINTRODUCTION = Column(String(2000))
    S_INFO_COMPTYPE = Column(String(20))
    S_INFO_WEBSITE = Column(String(80))
    S_INFO_EMAIL = Column(String(80))
    S_INFO_OFFICE = Column(String(80))
    ANN_DT = Column(String(8))
    S_INFO_COUNTRY = Column(String(20))
    S_INFO_BUSINESSSCOPE = Column(String(2000))
    S_INFO_COMPANY_TYPE = Column(String(10))
    S_INFO_TOTALEMPLOYEES = Column(Numeric(20, 0))
    S_INFO_MAIN_BUSINESS = Column(String(1000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREINTRODUCTIONBT(Base):
    __tablename__ = 'ASHAREINTRODUCTIONBT'

    OBJECT_ID = Column(String(100), primary_key=True)
    LATEST_OBJECT_ID = Column(String(100))
    BACKREASON_CODE = Column(String(2))
    BACKTIME = Column(DateTime)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_PROVINCE = Column(String(20))
    S_INFO_CITY = Column(String(20))
    S_INFO_CHAIRMAN = Column(String(38))
    S_INFO_PRESIDENT = Column(String(38))
    S_INFO_BDSECRETARY = Column(String(500))
    S_INFO_REGCAPITAL = Column(Numeric(20, 4))
    S_INFO_FOUNDDATE = Column(String(8))
    S_INFO_CHINESEINTRODUCTION = Column(String(2000))
    S_INFO_COMPTYPE = Column(String(20))
    S_INFO_WEBSITE = Column(String(80))
    S_INFO_EMAIL = Column(String(80))
    S_INFO_OFFICE = Column(String(80))
    ANN_DT = Column(String(8))
    S_INFO_COUNTRY = Column(String(20))
    S_INFO_BUSINESSSCOPE = Column(String(2000))
    S_INFO_COMPANY_TYPE = Column(String(10))
    S_INFO_TOTALEMPLOYEES = Column(Numeric(20, 0))
    S_INFO_MAIN_BUSINESS = Column(String(1000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREIPO(Base):
    __tablename__ = 'ASHAREIPO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    CRNCY_CODE = Column(String(10))
    S_IPO_PRICE = Column(Numeric(20, 4))
    S_IPO_PRE_DILUTEDPE = Column(Numeric(20, 4))
    S_IPO_DILUTEDPE = Column(Numeric(20, 4))
    S_IPO_AMOUNT = Column(Numeric(20, 4))
    S_IPO_AMTBYPLACING = Column(Numeric(20, 4))
    S_IPO_AMTTOJUR = Column(Numeric(20, 4))
    S_IPO_COLLECTION = Column(Numeric(20, 4))
    S_IPO_CASHRATIO = Column(Numeric(20, 8))
    S_IPO_PURCHASECODE = Column(String(10))
    S_IPO_SUBDATE = Column(String(8))
    S_IPO_JURISDATE = Column(String(8))
    S_IPO_INSTISDATE = Column(String(8))
    S_IPO_EXPECTLISTDATE = Column(String(8))
    S_IPO_FUNDVERIFICATIONDATE = Column(String(8))
    S_IPO_RATIODATE = Column(String(8))
    S_FELLOW_UNFROZEDATE = Column(String(8))
    S_IPO_LISTDATE = Column(String(8))
    S_IPO_PUBOFFRDATE = Column(String(8))
    S_IPO_ANNCEDATE = Column(String(8))
    S_IPO_ANNCELSTDATE = Column(String(8))
    S_IPO_ROADSHOWSTARTDATE = Column(String(8))
    S_IPO_ROADSHOWENDDATE = Column(String(8))
    S_IPO_PLACINGDATE = Column(String(8))
    S_IPO_APPLYSTARTDATE = Column(String(8))
    S_IPO_APPLYENDDATE = Column(String(8))
    S_IPO_PRICEANNOUNCEDATE = Column(String(8))
    S_IPO_PLACINGRESULTDATE = Column(String(8))
    S_IPO_FUNDENDDATE = Column(String(8))
    S_IPO_CAPVERIFICATIONDATE = Column(String(8))
    S_IPO_REFUNDDATE = Column(String(8))
    S_IPO_EXPECTEDCOLLECTION = Column(Numeric(20, 4))
    S_IPO_LIST_FEE = Column(Numeric(20, 4))
    S_IPO_LPURNAMEONL = Column(String(20))
    S_IPO_CASHAMTUPLIMIT = Column(Numeric(10, 0))
    S_IPO_CASHMONEYUPLIMIT = Column(Numeric(20, 4))
    S_IPO_NAMEBYPLACING = Column(String(20))
    S_IPO_SHOWPRICEDOWNLIMIT = Column(Numeric(20, 4))
    S_IPO_PAR = Column(Numeric(20, 4))
    S_IPO_PURCHASEUPLIMIT = Column(Numeric(20, 4))
    S_IPO_OP_UPLIMIT = Column(Numeric(20, 4))
    S_IPO_OP_DOWNLIMIT = Column(Numeric(20, 4))
    S_IPO_PURCHASEMV_DT = Column(String(8))
    S_IPO_PUBOSDTOTISSCOLL = Column(Numeric(20, 4))
    S_IPO_OSDEXPOFFAMOUNT = Column(Numeric(20, 4))
    S_IPO_OSDEXPOFFAMOUNTUP = Column(Numeric(20, 4))
    S_IPO_OSDACTOFFAMOUNT = Column(Numeric(20, 4))
    S_IPO_OSDACTOFFPRICE = Column(Numeric(20, 4))
    S_IPO_OSDUNDERWRITINGFEES = Column(Numeric(20, 4))
    S_IPO_PUREFFSUBRATIO = Column(Numeric(20, 4))
    S_IPO_REPORATE = Column(Numeric(20, 4))
    ANN_DT = Column(String(8))
    IS_FAILURE = Column(Numeric(5, 0))
    S_IPO_OTC_CASH_PCT = Column(Numeric(24, 8))
    MIN_APPLYUNIT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREISACTIVITY(Base):
    __tablename__ = 'ASHAREISACTIVITY'

    OBJECT_ID = Column(String(100), primary_key=True)
    EVENT_ID = Column(String(40))
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_ACTIVITIESTYPE = Column(Numeric(9, 0), index=True)
    S_SURVEYDATE = Column(String(8))
    S_SURVEYTIME = Column(String(20))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREISPARTICIPANT(Base):
    __tablename__ = 'ASHAREISPARTICIPANT'

    OBJECT_ID = Column(String(100), primary_key=True)
    EVENT_ID = Column(String(40))
    S_INSTITUTIONNAME = Column(String(100))
    S_INSTITUTIONCODE = Column(String(10), index=True)
    S_INSTITUTIONTYPE = Column(Numeric(9, 0), index=True)
    S_ANALYSTNAME = Column(String(20))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREISQA(Base):
    __tablename__ = 'ASHAREISQA'

    OBJECT_ID = Column(String(100), primary_key=True)
    EVENT_ID = Column(String(40))
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_ASKDATE = Column(String(8))
    S_QUESTIONTYPE = Column(Numeric(9, 0), index=True)
    S_QUESTIONCONTENT = Column(String(2000))
    S_ANSWERCONTENT = Column(String)
    ORIGINALWEBSITE = Column(String(2000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREISSUECOMMAUDIT(Base):
    __tablename__ = 'ASHAREISSUECOMMAUDIT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPANYNAME = Column(String(100))
    S_IC_YEAR = Column(String(4), index=True)
    S_IC_SESSIONTIMES = Column(String(3))
    S_IC_AUDITTYPE = Column(String(40))
    S_IC_TYPE = Column(Numeric(9, 0))
    S_IC_ANNOUNCEMENTDATE = Column(String(8))
    S_IC_DATE = Column(String(8))
    S_IC_AUDITOCETYPE = Column(String(1))
    S_IC_AUDITANNOCEDATE = Column(String(8))
    S_INFO_EXPECTEDISSUESHARES = Column(Numeric(20, 4))
    S_INFO_EXPECTEDCOLLECTION = Column(Numeric(20, 4))
    S_INFO_VETO_REASON = Column(String(2000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREISSUINGDATEPREDICT(Base):
    __tablename__ = 'ASHAREISSUINGDATEPREDICT'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    REPORT_PERIOD = Column(String(8), index=True)
    S_STM_PREDICT_ISSUINGDATE = Column(String(9))
    S_STM_ACTUAL_ISSUINGDATE = Column(String(8))
    S_STM_CORRECT_NUM = Column(String(20))
    S_STM_CORRECT_ISSUINGDATE = Column(String(100))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREISSUINGDATEPREDICTBT(Base):
    __tablename__ = 'ASHAREISSUINGDATEPREDICTBT'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    LATEST_OBJECT_ID = Column(String(100), index=True)
    BACKREASON_CODE = Column(String(2))
    BACKTIME = Column(DateTime)
    S_INFO_WINDCODE = Column(String(40), index=True)
    REPORT_PERIOD = Column(String(8), index=True)
    S_STM_PREDICT_ISSUINGDATE = Column(String(8))
    S_STM_ACTUAL_ISSUINGDATE = Column(String(8))
    S_STM_CORRECT_NUM = Column(String(20))
    S_STM_CORRECT_ISSUINGDATE = Column(String(100))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREL2INDICATOR(Base):
    __tablename__ = 'ASHAREL2INDICATORS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    S_LI_INITIATIVEBUYRATE = Column(Numeric(20, 4))
    S_LI_INITIATIVEBUYMONEY = Column(Numeric(20, 4))
    S_LI_INITIATIVEBUYAMOUNT = Column(Numeric(20, 4))
    S_LI_INITIATIVESELLRATE = Column(Numeric(20, 4))
    S_LI_INITIATIVESELLMONEY = Column(Numeric(20, 4))
    S_LI_INITIATIVESELLAMOUNT = Column(Numeric(20, 4))
    S_LI_LARGEBUYRATE = Column(Numeric(20, 4))
    S_LI_LARGEBUYMONEY = Column(Numeric(20, 4))
    S_LI_LARGEBUYAMOUNT = Column(Numeric(20, 4))
    S_LI_LARGESELLRATE = Column(Numeric(20, 4))
    S_LI_LARGESELLMONEY = Column(Numeric(20, 4))
    S_LI_LARGESELLAMOUNT = Column(Numeric(20, 4))
    S_LI_ENTRUSTRATE = Column(Numeric(20, 4))
    S_LI_ENTRUDIFFERAMOUNT = Column(Numeric(20, 4))
    S_LI_ENTRUDIFFERAMONEY = Column(Numeric(20, 4))
    S_LI_ENTRUSTBUYMONEY = Column(Numeric(20, 4))
    S_LI_ENTRUSTSELLMONEY = Column(Numeric(20, 4))
    S_LI_ENTRUSTBUYAMOUNT = Column(Numeric(20, 4))
    S_LI_ENTRUSTSELLAMOUNT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARELEADUNDERWRITER(Base):
    __tablename__ = 'ASHARELEADUNDERWRITER'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_LU_ANNISSUEDATE = Column(String(8))
    S_LU_ISSUEDATE = Column(String(8))
    S_LU_ISSUETYPE = Column(String(1), index=True)
    S_LU_TOTALISSUECOLLECTION = Column(Numeric(20, 4))
    S_LU_TOTALISSUEEXPENSES = Column(Numeric(20, 4))
    S_LU_TOTALUDERANDSPONEFEE = Column(Numeric(20, 4))
    S_LU_NUMBER = Column(String(1))
    S_LU_NAME = Column(String(100))
    S_LU_INSTITYPE = Column(String(40))
    S_LU_AUX_TYPE = Column(String(40))
    S_INFO_COMPCODE = Column(String(40))
    ALL_LU = Column(String(1000))
    MEETING_DT = Column(String(8))
    PASS_DT = Column(String(8))
    S_INFO_LISTDATE = Column(String(8))
    TYPE = Column(String(40))
    NETCOLLECTION = Column(Numeric(20, 4))
    AVG_TOTALCOLL = Column(Numeric(20, 4))
    AVG_NETCOLL = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMAJOREVENT(Base):
    __tablename__ = 'ASHAREMAJOREVENT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_EVENT_CATEGORYCODE = Column(Numeric(9, 0), index=True)
    S_EVENT_ANNCEDATE = Column(String(8))
    S_EVENT_HAPDATE = Column(String(8))
    S_EVENT_EXPDATE = Column(String(8))
    S_EVENT_CONTENT = Column(String)
    S_EVENT_TEMPLATEID = Column(Numeric(12, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMAJORHOLDERPLANHOLD(Base):
    __tablename__ = 'ASHAREMAJORHOLDERPLANHOLD'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_HOLDER_NAME = Column(String(200))
    S_PH_STARTDATE = Column(String(8))
    S_PH_ENDDATE = Column(String(8))
    S_PH_CONDITIONORNOT = Column(Numeric(5, 0))
    S_PH_TRIGGERPRICE = Column(Numeric(20, 4))
    S_PH_CONTINUOUSDAYS = Column(Numeric(20, 4))
    S_PH_CALCULATEDAYS = Column(Numeric(20, 4))
    S_PH_CALCULATEPRICEMODE = Column(String(80))
    S_PH_SHARENUMDOWNLIMIT = Column(Numeric(20, 4))
    S_PH_SHARENUMUPLIMIT = Column(Numeric(20, 4))
    S_PH_INTENDPUTMONEYDOWNLIMIT = Column(Numeric(20, 4))
    S_PH_INTENDPUTMONEYUPLIMIT = Column(Numeric(20, 4))
    S_PH_PRICEUPLIMIT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMANAGEMENT(Base):
    __tablename__ = 'ASHAREMANAGEMENT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8))
    S_INFO_MANAGER_NAME = Column(String(80))
    S_INFO_MANAGER_GENDER = Column(String(10))
    S_INFO_MANAGER_EDUCATION = Column(String(10))
    S_INFO_MANAGER_NATIONALITY = Column(String(40))
    S_INFO_MANAGER_BIRTHYEAR = Column(String(8))
    S_INFO_MANAGER_STARTDATE = Column(String(8))
    S_INFO_MANAGER_LEAVEDATE = Column(String(8))
    S_INFO_MANAGER_TYPE = Column(Numeric(5, 0))
    S_INFO_MANAGER_POST = Column(String(40))
    S_INFO_MANAGER_INTRODUCTION = Column(String(2000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMANAGEMENTHOLDREWARD(Base):
    __tablename__ = 'ASHAREMANAGEMENTHOLDREWARD'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8), index=True)
    END_DATE = Column(String(8), index=True)
    CRNY_CODE = Column(String(10))
    S_INFO_MANAGER_NAME = Column(String(80))
    S_INFO_MANAGER_POST = Column(String(40))
    S_MANAGER_RETURN = Column(Numeric(20, 4))
    S_MANAGER_QUANTITY = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMARGINSHORTFEERATE(Base):
    __tablename__ = 'ASHAREMARGINSHORTFEERATE'

    OBJECT_ID = Column(String(100), primary_key=True)
    PUBLISHER_ID = Column(String(40))
    ITEM_CODE = Column(Numeric(9, 0))
    EFFECTIVE_DATE = Column(String(8))
    FEE_RATE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMARGINSUBJECT(Base):
    __tablename__ = 'ASHAREMARGINSUBJECT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_MARGIN_SHARETYPE = Column(Numeric(9, 0))
    S_MARGIN_EFFECTDATE = Column(String(8))
    S_MARGIN_ELIMINDATE = Column(String(8))
    S_MARGIN_MARGINRATE = Column(Numeric(20, 4))
    S_MARGIN_CONVERSIONRATE = Column(Numeric(20, 4))
    S_MARGIN_RATEEFFECTDATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMARGINTRADE(Base):
    __tablename__ = 'ASHAREMARGINTRADE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    S_MARGIN_TRADINGBALANCE = Column(Numeric(20, 4))
    S_MARGIN_PURCHWITHBORROWMONEY = Column(Numeric(20, 4))
    S_MARGIN_REPAYMENTTOBROKER = Column(Numeric(20, 4))
    S_MARGIN_SECLENDINGBALANCE = Column(Numeric(20, 4))
    S_MARGIN_SECLENDINGBALANCEVOL = Column(Numeric(20, 4))
    S_MARGIN_SALESOFBORROWEDSEC = Column(Numeric(20, 4))
    S_MARGIN_REPAYMENTOFBORROWSEC = Column(Numeric(20, 4))
    S_MARGIN_MARGINTRADEBALANCE = Column(Numeric(20, 4))
    S_REFIN_SL_VOL_3D = Column(Numeric(20, 0))
    S_REFIN_SL_VOL_7D = Column(Numeric(20, 0))
    S_REFIN_SL_VOL_14D = Column(Numeric(20, 0))
    S_REFIN_SL_VOL_28D = Column(Numeric(20, 0))
    S_REFIN_SL_VOL_182D = Column(Numeric(20, 0))
    S_REFIN_SB_VOL_3D = Column(Numeric(20, 0))
    S_REFIN_SB_VOL_7D = Column(Numeric(20, 0))
    S_REFIN_SB_VOL_14D = Column(Numeric(20, 0))
    S_SB_VOL_28D = Column(Numeric(20, 0))
    S_SB_VOL_182D = Column(Numeric(20, 0))
    S_REFIN_SL_EOD_VOL = Column(Numeric(20, 0))
    S_REFIN_SB_EOD_VOL = Column(Numeric(20, 0))
    S_REFIN_SL_EOP_VOL = Column(Numeric(20, 0))
    S_REFIN_SL_EOP_BAL = Column(Numeric(20, 4))
    S_REFIN_REPAY_VOL = Column(Numeric(20, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMARGINTRADESUM(Base):
    __tablename__ = 'ASHAREMARGINTRADESUM'

    OBJECT_ID = Column(String(100), primary_key=True)
    TRADE_DT = Column(String(8))
    S_MARSUM_EXCHMARKET = Column(String(40))
    S_MARSUM_TRADINGBALANCE = Column(Numeric(20, 4))
    S_MARSUM_PURCHWITHBORROWMONEY = Column(Numeric(20, 4))
    S_MARSUM_REPAYMENTTOBROKER = Column(Numeric(20, 4))
    S_MARSUM_SECLENDINGBALANCE = Column(Numeric(20, 4))
    S_MARSUM_SALESOFBORROWEDSEC = Column(Numeric(20, 4))
    S_MARSUM_MARGINTRADEBALANCE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMERGERSACQUISITION(Base):
    __tablename__ = 'ASHAREMERGERSACQUISITIONS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(200))
    TARGETOFACQUISN = Column(String(100))
    TARGETOFACQUISN_DISCLOSURE = Column(String(40))
    ACQUIRINGFIRM = Column(String(100))
    ACQUIRINGFIRM_DISCLOSURE = Column(String(40))
    PROGRESS = Column(String(10))
    CRNCY_CODE = Column(String(10))
    PAYMENTMETHOD = Column(String(40))
    TRADE_DT = Column(String(8))
    TRADINGAMOUNT = Column(Numeric(20, 4))
    IS_RELDPARTRANSACTIONS = Column(Numeric(1, 0))
    FIRST_DT = Column(String(8))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMJRHOLDERTRADE(Base):
    __tablename__ = 'ASHAREMJRHOLDERTRADE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    ANN_DT = Column(String(8))
    TRANSACT_STARTDATE = Column(String(8))
    TRANSACT_ENDDATE = Column(String(8))
    HOLDER_NAME = Column(String(200))
    HOLDER_TYPE = Column(String(1))
    TRANSACT_TYPE = Column(String(4))
    TRANSACT_QUANTITY = Column(Numeric(20, 4))
    TRANSACT_QUANTITY_RATIO = Column(Numeric(20, 4))
    HOLDER_QUANTITY_NEW = Column(Numeric(20, 4))
    HOLDER_QUANTITY_NEW_RATIO = Column(Numeric(20, 4))
    AVG_PRICE = Column(Numeric(20, 4))
    WHETHER_AGREED_REPUR_TRANS = Column(Numeric(1, 0))
    BLOCKTRADE_QUANTITY = Column(Numeric(20, 4))
    IS_RESTRICTED = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMONEYFLOW(Base):
    __tablename__ = 'ASHAREMONEYFLOW'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    BUY_VALUE_EXLARGE_ORDER = Column(Numeric(20, 4))
    SELL_VALUE_EXLARGE_ORDER = Column(Numeric(20, 4))
    BUY_VALUE_LARGE_ORDER = Column(Numeric(20, 4))
    SELL_VALUE_LARGE_ORDER = Column(Numeric(20, 4))
    BUY_VALUE_MED_ORDER = Column(Numeric(20, 4))
    SELL_VALUE_MED_ORDER = Column(Numeric(20, 4))
    BUY_VALUE_SMALL_ORDER = Column(Numeric(20, 4))
    SELL_VALUE_SMALL_ORDER = Column(Numeric(20, 4))
    BUY_VOLUME_EXLARGE_ORDER = Column(Numeric(20, 4))
    SELL_VOLUME_EXLARGE_ORDER = Column(Numeric(20, 4))
    BUY_VOLUME_LARGE_ORDER = Column(Numeric(20, 4))
    SELL_VOLUME_LARGE_ORDER = Column(Numeric(20, 4))
    BUY_VOLUME_MED_ORDER = Column(Numeric(20, 4))
    SELL_VOLUME_MED_ORDER = Column(Numeric(20, 4))
    BUY_VOLUME_SMALL_ORDER = Column(Numeric(20, 4))
    SELL_VOLUME_SMALL_ORDER = Column(Numeric(20, 4))
    TRADES_COUNT = Column(Numeric(20, 4))
    BUY_TRADES_EXLARGE_ORDER = Column(Numeric(20, 4))
    SELL_TRADES_EXLARGE_ORDER = Column(Numeric(20, 4))
    BUY_TRADES_LARGE_ORDER = Column(Numeric(20, 4))
    SELL_TRADES_LARGE_ORDER = Column(Numeric(20, 4))
    BUY_TRADES_MED_ORDER = Column(Numeric(20, 4))
    SELL_TRADES_MED_ORDER = Column(Numeric(20, 4))
    BUY_TRADES_SMALL_ORDER = Column(Numeric(20, 4))
    SELL_TRADES_SMALL_ORDER = Column(Numeric(20, 4))
    VOLUME_DIFF_SMALL_TRADER = Column(Numeric(20, 4))
    VOLUME_DIFF_SMALL_TRADER_ACT = Column(Numeric(20, 4))
    VOLUME_DIFF_MED_TRADER = Column(Numeric(20, 4))
    VOLUME_DIFF_MED_TRADER_ACT = Column(Numeric(20, 4))
    VOLUME_DIFF_LARGE_TRADER = Column(Numeric(20, 4))
    VOLUME_DIFF_LARGE_TRADER_ACT = Column(Numeric(20, 4))
    VOLUME_DIFF_INSTITUTE = Column(Numeric(20, 4))
    VOLUME_DIFF_INSTITUTE_ACT = Column(Numeric(20, 4))
    VALUE_DIFF_SMALL_TRADER = Column(Numeric(20, 4))
    VALUE_DIFF_SMALL_TRADER_ACT = Column(Numeric(20, 4))
    VALUE_DIFF_MED_TRADER = Column(Numeric(20, 4))
    VALUE_DIFF_MED_TRADER_ACT = Column(Numeric(20, 4))
    VALUE_DIFF_LARGE_TRADER = Column(Numeric(20, 4))
    VALUE_DIFF_LARGE_TRADER_ACT = Column(Numeric(20, 4))
    VALUE_DIFF_INSTITUTE = Column(Numeric(20, 4))
    VALUE_DIFF_INSTITUTE_ACT = Column(Numeric(20, 4))
    S_MFD_INFLOWVOLUME = Column(Numeric(20, 4))
    NET_INFLOW_RATE_VOLUME = Column(Numeric(20, 4))
    S_MFD_INFLOW_OPENVOLUME = Column(Numeric(20, 4))
    OPEN_NET_INFLOW_RATE_VOLUME = Column(Numeric(20, 4))
    S_MFD_INFLOW_CLOSEVOLUME = Column(Numeric(20, 4))
    CLOSE_NET_INFLOW_RATE_VOLUME = Column(Numeric(20, 4))
    S_MFD_INFLOW = Column(Numeric(20, 4))
    NET_INFLOW_RATE_VALUE = Column(Numeric(20, 4))
    S_MFD_INFLOW_OPEN = Column(Numeric(20, 4))
    OPEN_NET_INFLOW_RATE_VALUE = Column(Numeric(20, 4))
    S_MFD_INFLOW_CLOSE = Column(Numeric(20, 4))
    CLOSE_NET_INFLOW_RATE_VALUE = Column(Numeric(20, 4))
    TOT_VOLUME_BID = Column(Numeric(20, 4))
    TOT_VOLUME_ASK = Column(Numeric(20, 4))
    MONEYFLOW_PCT_VOLUME = Column(Numeric(20, 4))
    OPEN_MONEYFLOW_PCT_VOLUME = Column(Numeric(20, 4))
    CLOSE_MONEYFLOW_PCT_VOLUME = Column(Numeric(20, 4))
    MONEYFLOW_PCT_VALUE = Column(Numeric(20, 4))
    OPEN_MONEYFLOW_PCT_VALUE = Column(Numeric(20, 4))
    CLOSE_MONEYFLOW_PCT_VALUE = Column(Numeric(20, 4))
    S_MFD_INFLOWVOLUME_LARGE_ORDER = Column(Numeric(20, 4))
    NET_INFLOW_RATE_VOLUME_L = Column(Numeric(20, 6))
    S_MFD_INFLOW_LARGE_ORDER = Column(Numeric(20, 4))
    NET_INFLOW_RATE_VALUE_L = Column(Numeric(20, 6))
    MONEYFLOW_PCT_VOLUME_L = Column(Numeric(20, 6))
    MONEYFLOW_PCT_VALUE_L = Column(Numeric(20, 6))
    S_MFD_INFLOW_OPENVOLUME_L = Column(Numeric(20, 4))
    OPEN_NET_INFLOW_RATE_VOLUME_L = Column(Numeric(20, 6))
    S_MFD_INFLOW_OPEN_LARGE_ORDER = Column(Numeric(20, 4))
    OPEN_NET_INFLOW_RATE_VALUE_L = Column(Numeric(20, 6))
    OPEN_MONEYFLOW_PCT_VOLUME_L = Column(Numeric(20, 6))
    OPEN_MONEYFLOW_PCT_VALUE_L = Column(Numeric(20, 6))
    S_MFD_INFLOW_CLOSEVOLUME_L = Column(Numeric(20, 4))
    CLOSE_NET_INFLOW_RATE_VOLUME_L = Column(Numeric(20, 6))
    S_MFD_INFLOW_CLOSE_LARGE_ORDER = Column(Numeric(20, 4))
    CLOSE_NET_INFLOW_RATE_VALU_L = Column(Numeric(20, 6))
    CLOSE_MONEYFLOW_PCT_VOLUME_L = Column(Numeric(20, 6))
    CLOSE_MONEYFLOW_PCT_VALUE_L = Column(Numeric(20, 6))
    BUY_VALUE_EXLARGE_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VALUE_EXLARGE_ORDER_ACT = Column(Numeric(20, 4))
    BUY_VALUE_LARGE_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VALUE_LARGE_ORDER_ACT = Column(Numeric(20, 4))
    BUY_VALUE_MED_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VALUE_MED_ORDER_ACT = Column(Numeric(20, 4))
    BUY_VALUE_SMALL_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VALUE_SMALL_ORDER_ACT = Column(Numeric(20, 4))
    BUY_VOLUME_EXLARGE_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VOLUME_EXLARGE_ORDER_ACT = Column(Numeric(20, 4))
    BUY_VOLUME_LARGE_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VOLUME_LARGE_ORDER_ACT = Column(Numeric(20, 4))
    BUY_VOLUME_MED_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VOLUME_MED_ORDER_ACT = Column(Numeric(20, 4))
    BUY_VOLUME_SMALL_ORDER_ACT = Column(Numeric(20, 4))
    SELL_VOLUME_SMALL_ORDER_ACT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMONTHLYREPORTSOFBROKER(Base):
    __tablename__ = 'ASHAREMONTHLYREPORTSOFBROKERS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPECODE = Column(Numeric(9, 0))
    ACC_STA_CODE = Column(Numeric(1, 0))
    IFLISTED_DATA = Column(Numeric(5, 0))
    OPER_REV = Column(Numeric(20, 4))
    NET_PROFIT_INCL_MIN_INT_INC = Column(Numeric(20, 4))
    NET_PROFIT_EXCL_MIN_INT_INC = Column(Numeric(20, 4))
    TOT_SHRHLDR_EQY_EXCL_MIN_INT = Column(Numeric(20, 4))
    TOT_SHRHLDR_EQY_INCL_MIN_INT = Column(Numeric(20, 4))
    S_INFO_COMPNAME = Column(String(100))
    S_INFO_COMPCODE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREMONTHLYYIELD(Base):
    __tablename__ = 'ASHAREMONTHLYYIELD'
    __table_args__ = (
        Index('SID_TD_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_MQ_PCTCHANGE = Column(Numeric(20, 4))
    S_MQ_TURN = Column(Numeric(20, 4))
    S_MQ_AVGTURN = Column(Numeric(20, 4))
    S_MQ_AMOUNT = Column(Numeric(20, 4))
    S_RISK_BETAR24 = Column(Numeric(20, 4))
    S_RISK_BETAR60 = Column(Numeric(20, 4))
    S_MQ_VARPCTCHANGE24 = Column(Numeric(20, 4))
    S_MQ_DEVPCTCHANGE24 = Column(Numeric(20, 4))
    S_MQ_AVGPCTCHANGE24 = Column(Numeric(20, 4))
    S_MQ_VARPCTCHANGE60 = Column(Numeric(20, 4))
    S_MQ_DEVPCTCHANGE60 = Column(Numeric(20, 4))
    S_MQ_AVGPCTCHANGE60 = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREOPERATIONEVENT(Base):
    __tablename__ = 'ASHAREOPERATIONEVENT'

    OBJECT_ID = Column(String(38), primary_key=True)
    COMP_ID = Column(String(10))
    SEC_ID = Column(String(10))
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8))
    EVENT_TYPE = Column(Numeric(9, 0))
    EVENT_NAME = Column(String(200))
    CATTYPE_ID = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREOWNERSHIP(Base):
    __tablename__ = 'ASHAREOWNERSHIP'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_COMPCODE = Column(String(40), index=True)
    S_INFO_COMPNAME = Column(String(200))
    WIND_SEC_CODE = Column(String(10), index=True)
    ENTRY_DT = Column(String(8))
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREPLACEMENTDETAIL(Base):
    __tablename__ = 'ASHAREPLACEMENTDETAILS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_HOLDER_NAME = Column(String(200))
    S_HOLDER_TYPECODE = Column(Numeric(9, 0))
    S_HOLDER_TYPE = Column(String(20))
    TYPEOFINVESTOR = Column(String(20))
    ORDQTY = Column(Numeric(20, 4))
    PLACEMENT = Column(Numeric(20, 4))
    TRADE_DT = Column(String(8))
    ANN_DT = Column(String(8))
    LOCKMONTH = Column(Numeric(20, 4))
    TRADABLE_DT = Column(String(8))
    ANN_ORDQTY = Column(Numeric(20, 4))
    IS_SEOORIPO = Column(Numeric(1, 0))
    LATEST_OWN_QTY = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREPREVIOUSENNAME(Base):
    __tablename__ = 'ASHAREPREVIOUSENNAME'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(10), index=True)
    CHANGE_DT = Column(String(8))
    S_INFO_ENAME = Column(String(100))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREPREVIOUSNAME(Base):
    __tablename__ = 'ASHAREPREVIOUSNAME'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    BEGINDATE = Column(String(8))
    ENDDATE = Column(String(8))
    ANN_DT = Column(String(8))
    S_INFO_NAME = Column(String(40))
    CHANGEREASON = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREPROFITEXPRES(Base):
    __tablename__ = 'ASHAREPROFITEXPRESS'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    OPER_REV = Column(Numeric(20, 4))
    OPER_PROFIT = Column(Numeric(20, 4))
    TOT_PROFIT = Column(Numeric(20, 4))
    NET_PROFIT_EXCL_MIN_INT_INC = Column(Numeric(20, 4))
    TOT_ASSETS = Column(Numeric(20, 4))
    TOT_SHRHLDR_EQY_EXCL_MIN_INT = Column(Numeric(20, 4))
    EPS_DILUTED = Column(Numeric(20, 4))
    ROE_DILUTED = Column(Numeric(20, 4))
    S_ISAUDIT = Column(Numeric(5, 0))
    YOYNET_PROFIT_EXCL_MIN_INT_INC = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREPROFITNOTICE(Base):
    __tablename__ = 'ASHAREPROFITNOTICE'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'S_PROFITNOTICE_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_PROFITNOTICE_DATE = Column(String(8))
    S_PROFITNOTICE_PERIOD = Column(String(8), index=True)
    S_PROFITNOTICE_STYLE = Column(Numeric(9, 0), index=True)
    S_PROFITNOTICE_SIGNCHANGE = Column(String(10))
    S_PROFITNOTICE_CHANGEMIN = Column(Numeric(20, 4))
    S_PROFITNOTICE_CHANGEMAX = Column(Numeric(20, 4))
    S_PROFITNOTICE_NETPROFITMIN = Column(Numeric(20, 4))
    S_PROFITNOTICE_NETPROFITMAX = Column(Numeric(20, 4))
    S_PROFITNOTICE_NUMBER = Column(Numeric(15, 4))
    S_PROFITNOTICE_FIRSTANNDATE = Column(String(8))
    S_PROFITNOTICE_ABSTRACT = Column(String(200))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREPROSECUTION(Base):
    __tablename__ = 'ASHAREPROSECUTION'
    __table_args__ = (
        Index('SID_ADT_INDEX', 'S_INFO_WINDCODE', 'ANN_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(40))
    ANN_DT = Column(String(8), index=True)
    TITLE = Column(String(40))
    INTRODUCTION = Column(String)
    ACCUSER = Column(String(3000))
    DEFENDANT = Column(String(3000))
    PRO_TYPE = Column(String(10))
    AMOUNT = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    PROSECUTE_DT = Column(String(8))
    COURT = Column(String(200))
    JUDGE_DT = Column(String(8))
    RESULT = Column(String)
    IS_APPEAL = Column(Numeric(5, 0))
    APPELLANT = Column(String(1))
    COURT2 = Column(String(200))
    JUDGE_DT2 = Column(String(8))
    RESULT2 = Column(String(2000))
    RESULTAMOUNT = Column(Numeric(20, 4))
    BRIEFRESULT = Column(String(100))
    EXECUTION = Column(String)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARERALATEDTRADE(Base):
    __tablename__ = 'ASHARERALATEDTRADE'
    __table_args__ = (
        Index('SID_ADT_INDEX', 'S_INFO_WINDCODE', 'ANN_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8), index=True)
    S_RELATEDTRADE_NAME = Column(String(80))
    S_RELATEDTRADE_RELATIONCODE = Column(String(300))
    S_RELATEDTRADE_CONTROL = Column(Numeric(5, 0))
    S_RELATEDTRADE_TRADETYPE = Column(String(300))
    S_RELATEDTRADE_SETTLETYPE = Column(String(300))
    CRNCY_CODE = Column(String(10))
    S_RELATEDTRADE_AMOUNT = Column(String(20))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREREGINV(Base):
    __tablename__ = 'ASHAREREGINV'
    __table_args__ = (
        Index('SID_ANNDT_INDEX', 'S_INFO_WINDCODE', 'STR_ANNDATE'),
    )

    OBJECT_ID = Column(String(38), primary_key=True)
    COMP_ID = Column(String(10))
    SEC_ID = Column(String(10))
    S_INFO_WINDCODE = Column(String(40), index=True)
    SUR_INSTITUTE = Column(String(100))
    SUR_REASONS = Column(String(500))
    STR_ANNDATE = Column(String(8), index=True)
    END_ANNDATE = Column(String(8))
    STR_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREREGIONAL(Base):
    __tablename__ = 'ASHAREREGIONAL'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_COMPCODE = Column(String(40), index=True)
    S_INFO_COMPNAME = Column(String(200))
    WIND_SEC_CODE = Column(String(10), index=True)
    ENTRY_DT = Column(String(8))
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARERESTRUCTURINGEVENT(Base):
    __tablename__ = 'ASHARERESTRUCTURINGEVENTS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    PROGRESS = Column(Numeric(9, 0), index=True)
    EVENT = Column(String(200))
    FORM = Column(Numeric(9, 0), index=True)
    PURPOSE = Column(Numeric(9, 0), index=True)
    TYPE = Column(Numeric(9, 0), index=True)
    TRADING_OBJECT = Column(String(200))
    TRADING_OBJECTCODE = Column(Numeric(9, 0), index=True)
    IS_M_A = Column(Numeric(5, 0))
    IS_IMPTRESTRUCTURING = Column(Numeric(5, 0))
    IS_CONTROLCHANGED = Column(Numeric(5, 0))
    SHARES_TRADED = Column(Numeric(20, 4))
    TOTALVALUEDEALS = Column(Numeric(20, 4))
    BUYERTOPAYCASH = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    PAYMENTMETHOD = Column(Numeric(9, 0))
    INJECTEDASSETBOOKNET = Column(Numeric(20, 4))
    ASSETINJECTEDASSESSNET = Column(Numeric(20, 4))
    ULTIMATEASSESSMETHOD = Column(Numeric(9, 0))
    BASEDATE = Column(String(8))
    APPRECIATIONRATE = Column(Numeric(20, 4))
    INDEPFINAADVISER = Column(String(200))
    ACQUIRERADVISER = Column(String(200))
    SUSPENSIONDATE = Column(String(8))
    PRELANDATE = Column(String(8))
    SMTGANNCEDATE = Column(String(8))
    PASSDATE = Column(String(8))
    APPROVEDDATE = Column(String(8))
    SEO_DT = Column(String(8))
    ANN_DT = Column(String(8))
    INSTITUTIONLISTED_DT = Column(String(8))
    ORIENTATIONSEO_DT = Column(String(8))
    ISSUEVOLUME = Column(Numeric(20, 4))
    SEO_PRICE = Column(Numeric(20, 4))
    SEO_DOWNPRICE = Column(Numeric(20, 4))
    BASE_DT_AVGP_20_M = Column(Numeric(20, 4))
    BASE_DT = Column(String(8))
    BASE_DT_TYPE = Column(String(10))
    ESTIMATEDNETPROFIT = Column(Numeric(20, 4))
    FORECASTYEAR = Column(String(10))
    ISSUEDSHARESAGO_SEO_M = Column(Numeric(20, 4))
    ISSUEDSHARESAGO_SEO_A = Column(Numeric(20, 4))
    FIRST_ANN_DATE = Column(String(8))
    EVENT_ID = Column(String(20))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARERIGHTISSUE(Base):
    __tablename__ = 'ASHARERIGHTISSUE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_RIGHTSISSUE_PROGRESS = Column(String(10))
    S_RIGHTSISSUE_PRICE = Column(Numeric(20, 4))
    S_RIGHTSISSUE_RATIO = Column(Numeric(20, 4))
    S_RIGHTSISSUE_AMOUNT = Column(Numeric(20, 4))
    S_RIGHTSISSUE_AMOUNTACT = Column(Numeric(20, 4))
    S_RIGHTSISSUE_NETCOLLECTION = Column(Numeric(20, 4))
    S_RIGHTSISSUE_REGDATESHAREB = Column(String(8))
    S_RIGHTSISSUE_EXDIVIDENDDATE = Column(String(8))
    S_RIGHTSISSUE_LISTEDDATE = Column(String(8))
    S_RIGHTSISSUE_PAYSTARTDATE = Column(String(8))
    S_RIGHTSISSUE_PAYENDDATE = Column(String(8))
    S_RIGHTSISSUE_PREPLANDATE = Column(String(8))
    S_RIGHTSISSUE_SMTGANNCEDATE = Column(String(8))
    S_RIGHTSISSUE_PASSDATE = Column(String(8))
    S_RIGHTSISSUE_APPROVEDDATE = Column(String(8))
    S_RIGHTSISSUE_ANNCEDATE = Column(String(8))
    S_RIGHTSISSUE_RESULTDATE = Column(String(8))
    S_RIGHTSISSUE_LISTANNDATE = Column(String(8))
    S_RIGHTSISSUE_GUARANTOR = Column(String(8))
    S_RIGHTSISSUE_GUARTYPE = Column(Numeric(20, 4))
    S_RIGHTSISSUE_CODE = Column(String(10))
    ANN_DT = Column(String(8), index=True)
    S_RIGHTSISSUE_YEAR = Column(String(8))
    S_RIGHTSISSUE_CONTENT = Column(String(150))
    S_RIGHTSISSUE_NAME = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESALESSEGMENT(Base):
    __tablename__ = 'ASHARESALESSEGMENT'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    REPORT_PERIOD = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_SEGMENT_ITEMCODE = Column(Numeric(9, 0), index=True)
    S_SEGMENT_ITEM = Column(String(100))
    S_SEGMENT_SALES = Column(Numeric(20, 4))
    S_SEGMENT_PROFIT = Column(Numeric(20, 4))
    S_SEGMENT_COST = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESECINDUSTRIESCLAS(Base):
    __tablename__ = 'ASHARESECINDUSTRIESCLASS'
    __table_args__ = (
        Index('SEC_ENTRY_INDEX', 'SEC_IND_CODE', 'ENTRY_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_IND_CODE = Column(String(50), index=True)
    ENTRY_DT = Column(String(8), index=True)
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESECNINDUSTRIESCLAS(Base):
    __tablename__ = 'ASHARESECNINDUSTRIESCLASS'
    __table_args__ = (
        Index('SEC_ENTRY_INDEX', 'SEC_IND_CODE', 'ENTRY_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_IND_CODE = Column(String(50), index=True)
    ENTRY_DT = Column(String(8), index=True)
    REMOVE_DT = Column(String(8))
    CUR_SIGN = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESEO(Base):
    __tablename__ = 'ASHARESEO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_FELLOW_PROGRESS = Column(Numeric(5, 0), index=True)
    S_FELLOW_ISSUETYPE = Column(String(10), index=True)
    CRNCY_CODE = Column(String(10))
    S_FELLOW_PRICE = Column(Numeric(20, 4))
    S_FELLOW_AMOUNT = Column(Numeric(20, 4))
    S_FELLOW_COLLECTION = Column(Numeric(20, 4))
    S_FELLOW_RECORDDATE = Column(String(8))
    S_FELLOW_PAYSTARTDATE = Column(String(8))
    S_FELLOW_PAYENDDATE = Column(String(8))
    S_FELLOW_SUBDATE = Column(String(8))
    S_FELLOW_OTCDATE = Column(String(8))
    S_FELLOW_LISTDATE = Column(String(8))
    S_FELLOW_INSTLISTDATE = Column(String(8))
    S_FELLOW_CHANGEDATE = Column(String(8))
    S_FELLOW_ROADSHOWDATE = Column(String(8))
    S_FELLOW_REFUNDDATE = Column(String(8))
    S_FELLOW_UNFROZEDATE = Column(String(8))
    S_FELLOW_PREPLANDATE = Column(String(8))
    S_FELLOW_SMTGANNCEDATE = Column(String(8))
    S_FELLOW_PASSDATE = Column(String(8))
    S_FELLOW_APPROVEDDATE = Column(String(10))
    S_FELLOW_ANNCEDATE = Column(String(8))
    S_FELLOW_RATIOANNCEDATE = Column(String(8))
    S_FELLOW_OFFERINGDATE = Column(String(8))
    S_FELLOW_LISTANNDATE = Column(String(8))
    S_FELLOW_OFFERINGOBJECT = Column(String(200))
    S_FELLOW_PRICEUPLIMIT = Column(Numeric(20, 4))
    S_FELLOW_PRICEDOWNLIMIT = Column(Numeric(20, 4))
    S_SEO_CODE = Column(String(10))
    S_SEO_NAME = Column(String(20))
    S_SEO_PE = Column(Numeric(20, 4))
    S_SEO_AMTBYPLACING = Column(Numeric(20, 4))
    S_SEO_AMTTOJUR = Column(Numeric(20, 4))
    S_SEO_HOLDERSUBSMODE = Column(String(30))
    S_SEO_HOLDERSUBSRATE = Column(Numeric(20, 4))
    ANN_DT = Column(String(8), index=True)
    PRICINGMODE = Column(Numeric(9, 0))
    S_FELLOW_ORGPRICEMIN = Column(Numeric(20, 4))
    S_FELLOW_DISCNTRATIO = Column(Numeric(20, 4))
    S_FELLOW_DATE = Column(String(8))
    S_FELLOW_SUBINVITATIONDT = Column(String(8))
    S_FELLOW_YEAR = Column(String(8))
    S_FELLOW_OBJECTIVE_CODE = Column(Numeric(9, 0))
    PRICINGDATE = Column(String(8))
    IS_NO_PUBLIC = Column(Numeric(5, 0))
    EXPENSE = Column(Numeric(20, 4))
    S_RIGHTSISSUE_EXDIVIDENDDATE = Column(String(8))
    IS_EXDIVIDEND = Column(Numeric(1, 0))
    SUB_MODE = Column(Numeric(9, 0))
    OLDSPLA_MOLE = Column(Numeric(20, 4))
    OLDSPLA_MODE = Column(String(10))
    EXP_COLLECTION = Column(Numeric(20, 4))
    S_FELLOW_TYPE = Column(String(100))
    S_FELLOW_OBJECTIVE = Column(String(3000))
    S_FELLOW_OFFERINGOBJECT_DES = Column(String(800))
    EVENT_ID = Column(String(40))
    PRICE_CONDITION = Column(String(2000))
    PRICE_DT_TYPE = Column(String(40))
    ORGPRICE_PCT = Column(Numeric(20, 4))
    ACTPRICE_PCT = Column(Numeric(20, 4))
    APPRVBYSASSC_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESEOBT(Base):
    __tablename__ = 'ASHARESEOBT'

    OBJECT_ID = Column(String(100), primary_key=True)
    LATEST_OBJECT_ID = Column(String(100), index=True)
    BACKREASON_CODE = Column(String(2))
    BACKTIME = Column(DateTime, index=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_FELLOW_PROGRESS = Column(Numeric(5, 0))
    S_FELLOW_ISSUETYPE = Column(String(10))
    CRNCY_CODE = Column(String(10))
    S_FELLOW_PRICE = Column(Numeric(20, 4))
    S_FELLOW_AMOUNT = Column(Numeric(20, 4))
    S_FELLOW_COLLECTION = Column(Numeric(20, 4))
    S_FELLOW_RECORDDATE = Column(String(8))
    S_FELLOW_PAYSTARTDATE = Column(String(8))
    S_FELLOW_PAYENDDATE = Column(String(8))
    S_FELLOW_SUBDATE = Column(String(8))
    S_FELLOW_OTCDATE = Column(String(8))
    S_FELLOW_LISTDATE = Column(String(8))
    S_FELLOW_INSTLISTDATE = Column(String(8))
    S_FELLOW_CHANGEDATE = Column(String(8))
    S_FELLOW_ROADSHOWDATE = Column(String(8))
    S_FELLOW_REFUNDDATE = Column(String(8))
    S_FELLOW_UNFROZEDATE = Column(String(8))
    S_FELLOW_PREPLANDATE = Column(String(8))
    S_FELLOW_SMTGANNCEDATE = Column(String(8))
    S_FELLOW_PASSDATE = Column(String(8))
    S_FELLOW_APPROVEDDATE = Column(String(10))
    S_FELLOW_ANNCEDATE = Column(String(8))
    S_FELLOW_RATIOANNCEDATE = Column(String(8))
    S_FELLOW_OFFERINGDATE = Column(String(8))
    S_FELLOW_LISTANNDATE = Column(String(8))
    S_FELLOW_OFFERINGOBJECT = Column(String(200))
    S_FELLOW_PRICEUPLIMIT = Column(Numeric(20, 4))
    S_FELLOW_PRICEDOWNLIMIT = Column(Numeric(20, 4))
    S_SEO_CODE = Column(String(10))
    S_SEO_NAME = Column(String(20))
    S_SEO_PE = Column(Numeric(20, 4))
    S_SEO_AMTBYPLACING = Column(Numeric(20, 4))
    S_SEO_AMTTOJUR = Column(Numeric(20, 4))
    S_SEO_HOLDERSUBSMODE = Column(String(30))
    S_SEO_HOLDERSUBSRATE = Column(Numeric(20, 4))
    ANN_DT = Column(String(8))
    PRICINGMODE = Column(Numeric(9, 0))
    S_FELLOW_ORGPRICEMIN = Column(Numeric(20, 4))
    S_FELLOW_DISCNTRATIO = Column(Numeric(20, 4))
    S_FELLOW_DATE = Column(String(8))
    S_FELLOW_SUBINVITATIONDT = Column(String(8))
    S_FELLOW_YEAR = Column(String(8))
    S_FELLOW_OBJECTIVE_CODE = Column(Numeric(9, 0))
    PRICINGDATE = Column(String(8))
    IS_NO_PUBLIC = Column(Numeric(5, 0))
    EXP_COLLECTION = Column(Numeric(20, 4))
    EXPENSE = Column(Numeric(20, 4))
    S_RIGHTSISSUE_EXDIVIDENDDATE = Column(String(8))
    IS_EXDIVIDEND = Column(Numeric(1, 0))
    SUB_MODE = Column(Numeric(9, 0))
    OLDSPLA_MOLE = Column(Numeric(20, 4))
    OLDSPLA_MODE = Column(String(10))
    S_FELLOW_TYPE = Column(String(100))
    S_FELLOW_OBJECTIVE = Column(String(3000))
    S_FELLOW_OFFERINGOBJECT_DES = Column(String(800))
    EVENT_ID = Column(String(40))
    PRICE_CONDITION = Column(String(2000))
    PRICE_DT_TYPE = Column(String(40))
    ORGPRICE_PCT = Column(Numeric(20, 4))
    ACTPRICE_PCT = Column(Numeric(20, 4))
    APPRVBYSASSC_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREST(Base):
    __tablename__ = 'ASHAREST'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_TYPE_ST = Column(String(8), index=True)
    ENTRY_DT = Column(String(8), index=True)
    REMOVE_DT = Column(String(8), index=True)
    ANN_DT = Column(String(8), index=True)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESTOCKRATING(Base):
    __tablename__ = 'ASHARESTOCKRATING'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_EST_INSTITUTE = Column(String(100))
    S_EST_RATINGANALYST = Column(String(100))
    S_EST_ESTNEWTIME_INST = Column(String(8), index=True)
    S_EST_SCORERATING_INST = Column(Numeric(20, 4))
    S_EST_PRESCORERATING_INST = Column(Numeric(20, 4))
    S_EST_LOWPRICE_INST = Column(Numeric(20, 4))
    S_EST_HIGHPRICE_INST = Column(Numeric(20, 4))
    S_EST_PRELOWPRICE_INST = Column(Numeric(20, 4))
    S_EST_PREHIGHPRICE_INST = Column(Numeric(20, 4))
    ANN_DT = Column(String(8), index=True)
    S_EST_RATING_INST = Column(String(20))
    S_EST_PRERATING_INST = Column(String(20))
    S_EST_REPORT_TITLE = Column(String(400))
    S_EST_REPORT_TYPE = Column(Numeric(9, 0))
    S_EST_RATINGANALYSTID = Column(String(200))
    S_RATING_CHANGE = Column(Numeric(9, 0))
    S_RATING_VALIDENDDT = Column(String(8), index=True)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESTOCKRATINGCONSU(Base):
    __tablename__ = 'ASHARESTOCKRATINGCONSUS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    RATING_DT = Column(String(8), index=True)
    S_WRATING_AVG = Column(Numeric(20, 4))
    S_WRATING_INSTNUM = Column(Numeric(20, 4))
    S_WRATING_UPGRADE = Column(Numeric(20, 4))
    S_WRATING_DOWNGRADE = Column(Numeric(20, 4))
    S_WRATING_MAINTAIN = Column(Numeric(20, 4))
    S_WRATING_NUMOFBUY = Column(Numeric(20, 4))
    S_WRATING_NUMOFOUTPERFORM = Column(Numeric(20, 4))
    S_WRATING_NUMOFHOLD = Column(Numeric(20, 4))
    S_WRATING_NUMOFUNDERPERFORM = Column(Numeric(20, 4))
    S_WRATING_NUMOFSELL = Column(Numeric(20, 4))
    S_WRATING_CYCLE = Column(String(10))
    S_EST_PRICE = Column(Numeric(20, 4))
    S_EST_PRICEINSTNUM = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESTOCKREPO(Base):
    __tablename__ = 'ASHARESTOCKREPO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    EVENT_ID = Column(String(38))
    ANN_DT = Column(String(8), index=True)
    END_DT = Column(String(8), index=True)
    STATUS = Column(Numeric(9, 0))
    EXP_DT = Column(String(8))
    QTY = Column(Numeric(20, 4))
    AMT = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(20))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESTOCKSWAP(Base):
    __tablename__ = 'ASHARESTOCKSWAP'

    OBJECT_ID = Column(String(100), primary_key=True)
    TRANSFERER_WINDCODE = Column(String(40), index=True)
    TRANSFERER_NAME = Column(String(40))
    PROGRESS = Column(String(10))
    TARGETCOMP_WINDCODE = Column(String(40), index=True)
    TARGETCOMP_NAME = Column(String(40))
    TRANSFERER_CONVERSIONPRICE = Column(Numeric(20, 4))
    TARGETCOMP_CONVERSIONPRICE = Column(Numeric(20, 4))
    CONVERSIONRATIO = Column(Numeric(20, 8))
    IS_CASHOPTION = Column(Numeric(5, 0))
    CASHOPTION = Column(Numeric(20, 4))
    CASHOPTION_REPORTSTARTDATE = Column(String(8))
    CASHOPTION_REPORTENDDATE = Column(String(8))
    CASHOPTION_APPLICATIONCODE = Column(String(10))
    CASHOPTION_SHAREPURCHASER = Column(String(80))
    CASHOPTION_APPLICANTNAME = Column(String(40))
    IS_OVERALLLISTING = Column(Numeric(5, 0))
    PRELANDATE = Column(String(8))
    MTSTARTDATE = Column(String(8))
    SMTGRECDATE = Column(String(8))
    SMTGANNCEDATE = Column(String(8))
    IECANNOUNCEMENTDATE = Column(String(8))
    ANNCEDATE = Column(String(8))
    ANNCELSTDATE = Column(String(8))
    ANN_DT = Column(String(8), index=True)
    TRADERESUMPTIONDATE = Column(String(8))
    LASTTRADEDATE = Column(String(8))
    EQUITYREGISTRATIONDATE = Column(String(8))
    LISTDATE = Column(String(8))
    CONSOLIDATIONBASEDATE = Column(String(8))
    FINANCIALADVISOR = Column(String(80))
    PLANDESCRIPTION = Column(String(300))
    CASHARRIVALDATE = Column(String(8))
    EFFECTIVEREPORTEDSHARES = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESTRANGETRADE(Base):
    __tablename__ = 'ASHARESTRANGETRADE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_STRANGE_BGDATE = Column(String(8))
    S_STRANGE_ENDDATE = Column(String(8))
    S_STRANGE_RANGE = Column(Numeric(20, 4))
    S_STRANGE_VOLUME = Column(Numeric(20, 4))
    S_STRANGE_AMOUNT = Column(Numeric(20, 4))
    S_STRANGE_TRADERNAME = Column(String(200))
    S_STRANGE_TRADERAMOUNT = Column(Numeric(20, 4))
    S_STRANGE_BUYAMOUNT = Column(Numeric(20, 4))
    S_STRANGE_SELLAMOUNT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARESTRANGETRADEDETAIL(Base):
    __tablename__ = 'ASHARESTRANGETRADEDETAIL'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TYPE_CODE = Column(String(3))
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    VOLUME = Column(Numeric(20, 4))
    AMOUNT = Column(Numeric(20, 4))
    PCTCHANGE = Column(Numeric(20, 4))
    CHANGE_DEVIATION = Column(Numeric(20, 4))
    SWING = Column(Numeric(20, 4))
    TURN = Column(Numeric(20, 4))
    CHANGE_DEVIATION_3D = Column(Numeric(20, 4))
    TURN_3D = Column(Numeric(20, 4))
    TURN_3DTO5D = Column(Numeric(20, 4))
    MARGIN_PCT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARETRADINGSUSPENSION(Base):
    __tablename__ = 'ASHARETRADINGSUSPENSION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_DQ_SUSPENDDATE = Column(String(8))
    S_DQ_SUSPENDTYPE = Column(Numeric(9, 0), index=True)
    S_DQ_RESUMPDATE = Column(String(8), index=True)
    S_DQ_CHANGEREASON = Column(String(400))
    S_DQ_TIME = Column(String(200))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARETTMANDMRQ(Base):
    __tablename__ = 'ASHARETTMANDMRQ'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPE = Column(String(80))
    S_FA_OR_TTM = Column(Numeric(20, 4))
    S_FA_COST_TTM = Column(Numeric(20, 4))
    S_FA_EXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_GROSSMARGIN_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEINCOME_TTM = Column(Numeric(20, 4))
    S_FA_INVESTINCOME_TTM = Column(Numeric(20, 4))
    S_FA_OP_TTM = Column(Numeric(20, 4))
    S_FA_EBT_TTM = Column(Numeric(20, 4))
    S_FA_PROFIT_TTM = Column(Numeric(20, 4))
    S_FA_NETPROFIT_TTM = Column(Numeric(20, 4))
    S_FA_GR_TTM = Column(Numeric(20, 4))
    S_FA_GC_TTM = Column(Numeric(20, 4))
    S_FA_CASHFLOW_TTM = Column(Numeric(20, 4))
    S_FA_OPERATECASHFLOW_TTM = Column(Numeric(20, 4))
    S_FA_INVESTCASHFLOW_TTM = Column(Numeric(20, 4))
    S_FA_FINANCECASHFLOW_TTM = Column(Numeric(20, 4))
    S_FA_ASSET_MRQ = Column(Numeric(20, 4))
    S_FA_DEBT_MRQ = Column(Numeric(20, 4))
    S_FA_TOTALEQUITY_MRQ = Column(Numeric(20, 4))
    S_FA_EQUITY_MRQ = Column(Numeric(20, 4))
    S_FA_EQUITY_NEW = Column(Numeric(20, 4))
    S_FA_NETPROFITMARGIN_TTM = Column(Numeric(20, 4))
    S_FA_GROSSPROFITMARGIN_TTM = Column(Numeric(20, 4))
    S_FA_EXPENSETOSALES_TTM = Column(Numeric(20, 4))
    S_FA_PROFITTOGR_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEEXPENSETOGR_TTM = Column(Numeric(20, 4))
    S_FA_ADMINEXPENSETOGR_TTM = Column(Numeric(20, 4))
    S_FA_FINAEXPENSETOGR_TTM = Column(Numeric(20, 4))
    S_FA_IMPAIRTOGR_TTM = Column(Numeric(20, 4))
    S_FA_GCTOGR_TTM = Column(Numeric(20, 4))
    S_FA_OPTOGR_TTM = Column(Numeric(20, 4))
    S_FA_ROA_TTM = Column(Numeric(20, 4))
    S_FA_ROA2_TTM = Column(Numeric(20, 4))
    S_FA_ROE_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEINCOMETOEBT_TTM = Column(Numeric(20, 4))
    S_FA_INVESTINCOMETOEBT_TTM = Column(Numeric(20, 4))
    S_FA_NONOPERATEPROFITTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_SALESCASHINTOOR_TTM = Column(Numeric(20, 4))
    S_FA_OCFTOOR_TTM = Column(Numeric(20, 4))
    S_FA_OCFTOOPERATEINCOME_TTM = Column(Numeric(20, 4))
    S_FA_EPS_TTM = Column(Numeric(20, 4))
    S_FA_ORPS_TTM = Column(Numeric(20, 4))
    S_FA_OCFPS_TTM = Column(Numeric(20, 4))
    S_FA_CFPS_TTM = Column(Numeric(20, 4))
    S_FA_BPS_NEW = Column(Numeric(20, 4))
    S_FA_SALESCASHIN_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEEXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_ADMINEXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_FINAEXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_EXPENSE = Column(Numeric(20, 4))
    S_FA_NONOPERATEPROFIT_TTM = Column(Numeric(20, 4))
    S_FA_IMPAIRMENT_TTM = Column(Numeric(20, 4))
    S_FA_EBIT_TTM = Column(Numeric(20, 4))
    S_FA_INVESTCAPITAL_MRQ = Column(Numeric(20, 4))
    FA_ROIC_TTM = Column(Numeric(20, 4))
    S_STM_BSMRQ = Column(Numeric(20, 4))
    S_FA_NONOPPROFIT_TTM = Column(Numeric(20, 4))
    S_FA_PREFINEXP_OP_TTM = Column(Numeric(20, 4))
    S_FA_OPTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_NOPTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_TAXTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_OPTOOR_TTM = Column(Numeric(20, 4))
    S_FA_EBTTOOR_TTM = Column(Numeric(20, 4))
    S_FA_PREFINEXPOPTOOR_TTM = Column(Numeric(20, 4))
    S_FA_NETPROFITTOOR_TTM = Column(Numeric(20, 4))
    S_FA_PREFINEXPOPTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_OCFTOOP_TTM = Column(Numeric(20, 4))
    ROA_EXCLMININTINC_TTM = Column(Numeric(20, 4))
    S_FA_DEBTTOASSETS_MRQ = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHARETTMHI(Base):
    __tablename__ = 'ASHARETTMHIS'
    __table_args__ = (
        Index('SID_PR_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_COMPCODE = Column(String(40))
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPE = Column(String(80))
    TOT_OPER_REV_TTM = Column(Numeric(20, 4))
    OPER_REV_TTM = Column(Numeric(20, 4))
    NET_PROFIT_TTM = Column(Numeric(20, 4))
    NET_PROFIT_PARENT_COMP_TTM = Column(Numeric(20, 4))
    NET_CASH_FLOWS_OPER_ACT_TTM = Column(Numeric(20, 4))
    NET_INCR_CASH_CASH_EQU_TTM = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    S_FA_COST_TTM = Column(Numeric(20, 4))
    S_FA_EXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_GROSSMARGIN_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEINCOME_TTM = Column(Numeric(20, 4))
    S_FA_INVESTINCOME_TTM = Column(Numeric(20, 4))
    S_FA_OP_TTM = Column(Numeric(20, 4))
    S_FA_EBT_TTM = Column(Numeric(20, 4))
    S_FA_GR_TTM = Column(Numeric(20, 4))
    S_FA_GC_TTM = Column(Numeric(20, 4))
    S_FA_INVESTCASHFLOW_TTM = Column(Numeric(20, 4))
    S_FA_FINANCECASHFLOW_TTM = Column(Numeric(20, 4))
    S_FA_ASSET_MRQ = Column(Numeric(20, 4))
    S_FA_DEBT_MRQ = Column(Numeric(20, 4))
    S_FA_TOTALEQUITY_MRQ = Column(Numeric(20, 4))
    S_FA_EQUITY_MRQ = Column(Numeric(20, 4))
    S_FA_NETPROFITMARGIN_TTM = Column(Numeric(20, 4))
    S_FA_GROSSPROFITMARGIN_TTM = Column(Numeric(20, 4))
    S_FA_EXPENSETOSALES_TTM = Column(Numeric(20, 4))
    S_FA_PROFITTOGR_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEEXPENSETOGR_TTM = Column(Numeric(20, 4))
    S_FA_ADMINEXPENSETOGR_TTM = Column(Numeric(20, 4))
    S_FA_FINAEXPENSETOGR_TTM = Column(Numeric(20, 4))
    S_FA_IMPAIRTOGR_TTM = Column(Numeric(20, 4))
    S_FA_GCTOGR_TTM = Column(Numeric(20, 4))
    S_FA_OPTOGR_TTM = Column(Numeric(20, 4))
    S_FA_ROA_TTM = Column(Numeric(20, 4))
    S_FA_ROA2_TTM = Column(Numeric(20, 4))
    S_FA_ROE_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEINCOMETOEBT_TTM = Column(Numeric(20, 4))
    S_FA_INVESTINCOMETOEBT_TTM = Column(Numeric(20, 4))
    S_FA_NONOPERATEPROFITTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_SALESCASHINTOOR_TTM = Column(Numeric(20, 4))
    S_FA_OCFTOOR_TTM = Column(Numeric(20, 4))
    S_FA_OCFTOOPERATEINCOME_TTM = Column(Numeric(20, 4))
    S_FA_DEDUCTEDPROFIT_TTM = Column(Numeric(20, 4))
    S_FA_EBITDA_TTM_INVERSE = Column(Numeric(20, 4))
    S_FA_EBITDA_TTM = Column(Numeric(20, 4))
    S_FA_EBIT_TTM = Column(Numeric(20, 4))
    FA_ROIC_TTM = Column(Numeric(20, 4))
    S_FA_SALESCASHIN_TTM = Column(Numeric(20, 4))
    S_FA_OPERATEEXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_ADMINEXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_FINAEXPENSE_TTM = Column(Numeric(20, 4))
    S_FA_EXPENSE = Column(Numeric(20, 4))
    S_FA_NONOPERATEPROFIT_TTM = Column(Numeric(20, 4))
    S_FA_IMPAIRMENT_TTM = Column(Numeric(20, 4))
    S_FA_EBIT_TTM_INVERSE = Column(Numeric(20, 4))
    S_FA_INVESTCAPITAL_MRQ = Column(Numeric(20, 4))
    FA_ROIC_TTM2 = Column(Numeric(20, 4))
    S_STM_BSMRQ = Column(Numeric(20, 4))
    S_FA_NONOPPROFIT_TTM = Column(Numeric(20, 4))
    S_FA_PREFINEXP_OP_TTM = Column(Numeric(20, 4))
    S_FA_OPTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_NOPTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_TAXTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_OPTOOR_TTM = Column(Numeric(20, 4))
    S_FA_EBTTOOR_TTM = Column(Numeric(20, 4))
    S_FA_PREFINEXPOPTOOR_TTM = Column(Numeric(20, 4))
    S_FA_NETPROFITTOOR_TTM = Column(Numeric(20, 4))
    S_FA_PREFINEXPOPTOEBT_TTM = Column(Numeric(20, 4))
    S_FA_OCFTOOP_TTM = Column(Numeric(20, 4))
    ROA_EXCLMININTINC_TTM = Column(Numeric(20, 4))
    S_FA_DEBTTOASSETS_MRQ = Column(Numeric(20, 4))
    INT_EXP_TTM = Column(Numeric(20, 4))
    INC_TAX_TTM = Column(Numeric(20, 4))
    MINORITY_INT_TTM = Column(Numeric(20, 4))
    CONTINUOUS_NET_OP_TTM = Column(Numeric(20, 4))
    NONCONTINUOUS_NET_OP_TTM = Column(Numeric(20, 4))
    NONNETOPTOTAXPROFIT = Column(Numeric(20, 4))
    NETOPTOTAXPROFIT = Column(Numeric(20, 4))
    S_INFO_WINDCODE = Column(String(40), index=True)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ASHAREWEEKLYYIELD(Base):
    __tablename__ = 'ASHAREWEEKLYYIELD'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_WQ_PCTCHANGE = Column(Numeric(20, 4))
    S_WQ_TURN = Column(Numeric(20, 4))
    S_WQ_AVGTURN = Column(Numeric(20, 4))
    S_WQ_AMOUNT = Column(Numeric(20, 4))
    S_RISK_BETAR100 = Column(Numeric(20, 4))
    S_WQ_VARPCTCHANGE100 = Column(Numeric(20, 4))
    S_WQ_DEVPCTCHANGE100 = Column(Numeric(20, 4))
    S_WQ_AVGPCTCHANGE100 = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CBONDFCTD(Base):
    __tablename__ = 'CBONDFCTD'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CTD_WINDCODE = Column(String(40))
    CTD_IRR = Column(Numeric(20, 4))
    IB_CTD_WINDCODE = Column(String(40))
    IB_CTD_IRR = Column(Numeric(20, 4))
    SH_CTD_WINDCODE = Column(String(40))
    SH_CTD_IRR = Column(Numeric(20, 4))
    SZ_CTD_WINDCODE = Column(String(40))
    SZ_CTD_IRR = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CBONDFSUBJECTCVF(Base):
    __tablename__ = 'CBONDFSUBJECTCVF'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    DLS_WINDCODE = Column(String(40), index=True)
    B_TBF_CVF = Column(Numeric(20, 8))
    ANN_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CBONDFTHIRDPARTYVALUATION(Base):
    __tablename__ = 'CBONDFTHIRDPARTYVALUATION'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    DLS_WINDCODE = Column(String(40))
    PRICETYPE_CODE = Column(Numeric(9, 0))
    TRADE_DT = Column(String(8), index=True)
    DL_INTEREST = Column(Numeric(24, 8))
    INTERVALINTEREST = Column(Numeric(24, 8))
    DL_COST = Column(Numeric(20, 4))
    FS_SPREAD = Column(Numeric(20, 4))
    IRR = Column(Numeric(20, 4))
    RT_SPREAD = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CBONDFUTURESEODPRICE(Base):
    __tablename__ = 'CBONDFUTURESEODPRICES'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    S_DQ_PRESETTLE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_SETTLE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_OI = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CBONDFUTURESPOSITION(Base):
    __tablename__ = 'CBONDFUTURESPOSITIONS'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    FS_INFO_MEMBERNAME = Column(String(40))
    FS_INFO_TYPE = Column(String(40))
    FS_INFO_POSITIONSNUM = Column(Numeric(20, 4))
    FS_INFO_RANK = Column(Numeric(5, 0))
    S_OI_POSITIONSNUMC = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CBONDFVALUATION(Base):
    __tablename__ = 'CBONDFVALUATION'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    DLS_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    DL_INTEREST = Column(Numeric(20, 8))
    INTERVALINTEREST = Column(Numeric(20, 8))
    DL_COST = Column(Numeric(20, 4))
    FS_SPREAD = Column(Numeric(20, 4))
    IRR = Column(Numeric(20, 4))
    RT_SPREAD = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CCOMMODITYFUTURESEODPRICE(Base):
    __tablename__ = 'CCOMMODITYFUTURESEODPRICES'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    S_DQ_PRESETTLE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_SETTLE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_OI = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_OICHANGE = Column(Numeric(20, 4))
    FS_INFO_TYPE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CCOMMODITYFUTURESPOSITION(Base):
    __tablename__ = 'CCOMMODITYFUTURESPOSITIONS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    FS_INFO_MEMBERNAME = Column(String(40))
    FS_INFO_TYPE = Column(String(1))
    FS_INFO_POSITIONSNUM = Column(Numeric(20, 4))
    FS_INFO_RANK = Column(Numeric(5, 0))
    S_OI_POSITIONSNUMC = Column(Numeric(20, 4))
    S_INFO_COMPCODE = Column(String(10))
    SEC_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUNDCHANGEWINDCODE(Base):
    __tablename__ = 'CFUNDCHANGEWINDCODE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_INFO_OLDWINDCODE = Column(String(40))
    S_INFO_NEWWINDCODE = Column(String(40))
    CHANGE_DATE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUNDINDEXTABLE(Base):
    __tablename__ = 'CFUNDINDEXTABLE'

    OBJECT_ID = Column(String(38), primary_key=True)
    F_INFO_WINDCODE = Column(String(10))
    F_CON_WINDCODE = Column(String(10))
    F_TRACKDEV = Column(Numeric(20, 0))
    F_TRACKINGERROR = Column(Numeric(20, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUNDINTRODUCTION(Base):
    __tablename__ = 'CFUNDINTRODUCTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_COMPCODE = Column(String(40))
    S_INFO_PROVINCE = Column(String(20))
    S_INFO_CITY = Column(String(50))
    S_INFO_CHAIRMAN = Column(String(38))
    S_INFO_PRESIDENT = Column(String(38))
    S_INFO_BDSECRETARY = Column(String(500))
    S_INFO_REGCAPITAL = Column(Numeric(20, 4))
    S_INFO_FOUNDDATE = Column(String(8))
    S_INFO_CHINESEINTRODUCTION = Column(String(2000))
    S_INFO_COMPTYPE = Column(String(20))
    S_INFO_WEBSITE = Column(String(80))
    S_INFO_EMAIL = Column(String(80))
    S_INFO_OFFICE = Column(String(80))
    ANN_DT = Column(String(8))
    S_INFO_COUNTRY = Column(String(20))
    S_INFO_BUSINESSSCOPE = Column(String(2000))
    S_INFO_COMPANY_TYPE = Column(String(10))
    S_INFO_TOTALEMPLOYEES = Column(Numeric(20, 0))
    S_INFO_MAIN_BUSINESS = Column(String(1000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUNDRALATEDSECURITIESCODE(Base):
    __tablename__ = 'CFUNDRALATEDSECURITIESCODE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_INFO_RALATEDCODE = Column(String(40))
    S_RELATION_TYPCODE = Column(Numeric(9, 0))
    S_INFO_EFFECTIVE_DT = Column(String(8))
    S_INFO_INVALID_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUNDWINDCUSTOMCODE(Base):
    __tablename__ = 'CFUNDWINDCUSTOMCODE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_INFO_ASHARECODE = Column(String(10))
    S_INFO_COMPCODE = Column(String(10))
    S_INFO_SECURITIESTYPES = Column(String(10))
    S_INFO_SECTYPENAME = Column(String(40))
    S_INFO_COUNTRYNAME = Column(String(100))
    S_INFO_COUNTRYCODE = Column(String(10))
    S_INFO_EXCHMARKETNAME = Column(String(40))
    S_INFO_EXCHMARKET = Column(String(40))
    CRNCY_NAME = Column(String(40))
    CRNCY_CODE = Column(String(10))
    S_INFO_ISINCODE = Column(String(40))
    S_INFO_CODE = Column(String(40))
    S_INFO_NAME = Column(String(50))
    EXCHMARKET = Column(String(40))
    SECURITY_STATUS = Column(Numeric(9, 0))
    S_INFO_ORG_CODE = Column(String(20))
    S_INFO_TYPECODE = Column(Numeric(9, 0))
    S_INFO_MIN_PRICE_CHG_UNIT = Column(Numeric(24, 8))
    S_INFO_LOT_SIZE = Column(Numeric(20, 4))
    S_INFO_ENAME = Column(String(200))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTUREINDEXEODPRICE(Base):
    __tablename__ = 'CFUTUREINDEXEODPRICES'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_PCTCHANGE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESCALENDAR(Base):
    __tablename__ = 'CFUTURESCALENDAR'

    OBJECT_ID = Column(String(100), primary_key=True)
    TRADE_DAYS = Column(String(8), index=True)
    S_INFO_EXCHMARKET = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESCONTPRO(Base):
    __tablename__ = 'CFUTURESCONTPRO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_CODE = Column(String(20), index=True)
    S_INFO_NAME = Column(String(40))
    S_INFO_TUNIT = Column(String(40))
    S_INFO_PUNIT = Column(Numeric(20, 4))
    S_INFO_MFPRICE = Column(String(200))
    S_INFO_FTMARGINS = Column(String(800))
    S_INFO_CDMONTHS = Column(String(200))
    S_INFO_THOURS = Column(String(800))
    S_INFO_LTDATED = Column(String(200))
    S_INFO_DDATE = Column(String(400))
    S_INFO_CEMULTIPLIER = Column(Numeric(20, 4))
    S_INFO_LISTDATE = Column(String(8), index=True)
    S_INFO_DELISTDATE = Column(String(8))
    S_INFO_EXNAME = Column(String(20))
    S_INFO_DMEAN = Column(String(400))
    S_INFO_DSITE = Column(String(400))
    S_INFO_LTDATEHOUR = Column(String(400))
    S_INFO_CEVALUE = Column(String(400))
    S_INFO_MAXPRICEFLUCT = Column(String(800))
    S_INFO_POSLIMIT = Column(String(800))
    S_INFO_UDLSECODE = Column(String(20))
    FS_INFO_PUNIT = Column(String(200))
    S_INFO_RTD = Column(Numeric(20, 4))
    S_SUB_TYPCODE = Column(Numeric(9, 0))
    CONTRACT_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESCONTPROCHANGE(Base):
    __tablename__ = 'CFUTURESCONTPROCHANGE'
    __table_args__ = (
        Index('CONTRACT_DT_INDEX', 'CONTRACT_ID', 'CHANGE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    CONTRACT_ID = Column(String(38), index=True)
    ITEM = Column(String(50))
    CHANGE_DT = Column(String(8), index=True)
    S_INFO_OLD = Column(String(1000))
    S_INFO_NEW = Column(String(1000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESCONTRACTMAPPING(Base):
    __tablename__ = 'CFUTURESCONTRACTMAPPING'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    FS_MAPPING_WINDCODE = Column(String(20))
    STARTDATE = Column(String(8))
    ENDDATE = Column(String(8))
    CONTRACT_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESDESCRIPTION(Base):
    __tablename__ = 'CFUTURESDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_CODE = Column(String(40))
    S_INFO_NAME = Column(String(50))
    S_INFO_ENAME = Column(String(100))
    FS_INFO_SCCODE = Column(String(50))
    FS_INFO_TYPE = Column(Numeric(1, 0))
    FS_INFO_CCTYPE = Column(Numeric(9, 0))
    S_INFO_EXCHMARKET = Column(String(10))
    S_INFO_LISTDATE = Column(String(8))
    S_INFO_DELISTDATE = Column(String(8))
    FS_INFO_DLMONTH = Column(String(8))
    FS_INFO_LPRICE = Column(Numeric(20, 4))
    FS_INFO_LTDLDATE = Column(String(8))
    S_INFO_FULLNAME = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESINTRODUCTION(Base):
    __tablename__ = 'CFUTURESINTRODUCTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_COMPCODE = Column(String(40))
    S_INFO_PROVINCE = Column(String(20))
    S_INFO_CITY = Column(String(50))
    S_INFO_CHAIRMAN = Column(String(38))
    S_INFO_PRESIDENT = Column(String(38))
    S_INFO_BDSECRETARY = Column(String(500))
    S_INFO_REGCAPITAL = Column(Numeric(20, 4))
    S_INFO_FOUNDDATE = Column(String(8))
    S_INFO_CHINESEINTRODUCTION = Column(String(2000))
    S_INFO_COMPTYPE = Column(String(20))
    S_INFO_WEBSITE = Column(String(80))
    S_INFO_EMAIL = Column(String(80))
    S_INFO_OFFICE = Column(String(80))
    ANN_DT = Column(String(8))
    S_INFO_COUNTRY = Column(String(20))
    S_INFO_BUSINESSSCOPE = Column(String(2000))
    S_INFO_COMPANY_TYPE = Column(String(10))
    S_INFO_TOTALEMPLOYEES = Column(Numeric(20, 0))
    S_INFO_MAIN_BUSINESS = Column(String(1000))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESMARGINRATIO(Base):
    __tablename__ = 'CFUTURESMARGINRATIO'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    MARGINRATIO = Column(String(40))
    TRADE_DT = Column(String(8), index=True)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESPOSITIONLIMIT(Base):
    __tablename__ = 'CFUTURESPOSITIONLIMIT'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'CHANGE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    POSITION_LIMIT_TYPE = Column(Numeric(9, 0))
    CHANGE_DT = Column(String(8), index=True)
    POSITION_LIMIT = Column(Numeric(20, 0))
    POSITION_LIMIT_RATIO = Column(Numeric(20, 4))
    CHANGE_REASON = Column(String(100))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESPRICECHANGELIMIT(Base):
    __tablename__ = 'CFUTURESPRICECHANGELIMIT'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'CHANGE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(40))
    PCT_CHG_LIMIT = Column(Numeric(20, 4))
    CHANGE_DT = Column(String(8), index=True)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CFUTURESWAREHOUSESTOCK(Base):
    __tablename__ = 'CFUTURESWAREHOUSESTOCKS'
    __table_args__ = (
        Index('ID_DT_INDEX', 'S_INFO_CODE', 'ANN_DATE'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_INFO_CODE = Column(String(20), index=True)
    ANN_DATE = Column(String(8), index=True)
    WAREHOUSE_REGION = Column(String(40))
    WAREHOUSE_NAME = Column(String(100))
    DELIVERABLE_W = Column(Numeric(20, 4))
    ON_WARRANT_W = Column(Numeric(20, 4))
    AVAILABLE_WAREHOUSE_W = Column(Numeric(20, 4))
    QUANTITY_UNIT = Column(String(40))
    WAREHOUSE_IN = Column(Numeric(20, 4))
    WAREHOUSE_OUT = Column(Numeric(20, 4))
    CANCELLED_WARRANTS = Column(Numeric(20, 4))
    EFFECTIVE_FORECAST = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CGBBENCHMARK(Base):
    __tablename__ = 'CGBBENCHMARK'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CGOLDSPOTDESCRIPTION(Base):
    __tablename__ = 'CGOLDSPOTDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_CODE = Column(String(40))
    S_INFO_NAME = Column(String(50))
    S_INFO_EXCHMARKET = Column(String(40))
    S_INFO_PUNIT = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CGOLDSPOTEODPRICE(Base):
    __tablename__ = 'CGOLDSPOTEODPRICES'
    __table_args__ = (
        Index('ID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_AVGPRICE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_OI = Column(Numeric(20, 4))
    DEL_AMT = Column(Numeric(20, 4))
    S_DQ_SETTLE = Column(Numeric(20, 4))
    DELAY_PAY_TYPECODE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINACLOSEDFUNDEODPRICE(Base):
    __tablename__ = 'CHINACLOSEDFUNDEODPRICE'
    __table_args__ = (
        Index('ID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_PCTCHANGE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_ADJPRECLOSE = Column(Numeric(20, 4))
    S_DQ_ADJOPEN = Column(Numeric(20, 4))
    S_DQ_ADJHIGH = Column(Numeric(20, 4))
    S_DQ_ADJLOW = Column(Numeric(20, 4))
    S_DQ_ADJCLOSE = Column(Numeric(20, 4))
    S_DQ_ADJFACTOR = Column(Numeric(20, 6))
    TRADES_COUNT = Column(Numeric(20, 4))
    DISCOUNT_RATE = Column(Numeric(20, 6))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAETFPCHREDMLIST(Base):
    __tablename__ = 'CHINAETFPCHREDMLIST'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    F_INFO_CASHDIF = Column(Numeric(20, 4))
    F_INFO_MINPRASET = Column(Numeric(20, 4))
    F_INFO_ESTICASH = Column(Numeric(20, 4))
    F_INFO_CASHSUBUPLIMIT = Column(Numeric(20, 4))
    F_INFO_MINPRUNITS = Column(Numeric(20, 4))
    F_INFO_PRPERMIT = Column(Numeric(1, 0))
    F_INFO_CONNUM = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAETFPCHREDMMEMBER(Base):
    __tablename__ = 'CHINAETFPCHREDMMEMBERS'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    S_CON_WINDCODE = Column(String(40))
    S_CON_STOCKNUMBER = Column(Numeric(20, 4))
    F_INFO_CASHSUBSIGN = Column(Numeric(1, 0))
    F_INFO_CASUBPREMRA = Column(Numeric(20, 4))
    F_INFO_CASUBAMOUNT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAFEEDERFUND(Base):
    __tablename__ = 'CHINAFEEDERFUND'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_INFO_FEEDER_WINDCODE = Column(String(40), index=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAFUNDMAJOREVENT(Base):
    __tablename__ = 'CHINAFUNDMAJOREVENT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_EVENT_CATEGORYCODE = Column(Numeric(9, 0))
    S_EVENT_ANNCEDATE = Column(String(8))
    S_EVENT_HAPDATE = Column(String(8))
    S_EVENT_EXPDATE = Column(String(8))
    S_EVENT_CONTENT = Column(String)
    S_EVENT_TEMPLATEID = Column(Numeric(12, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAGRADINGFUND(Base):
    __tablename__ = 'CHINAGRADINGFUND'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_INFO_FEEDER_WINDCODE = Column(String(40), index=True)
    F_INFO_FEEDER_TYPECODE = Column(Numeric(9, 0), index=True)
    F_INFO_FEEDER_SHARERATIO = Column(Numeric(20, 4))
    F_INFO_TERM_TYPECODE = Column(Numeric(9, 0), index=True)
    F_INFO_PERIOD_IFDIV = Column(Numeric(1, 0))
    F_INFO_TERM_IFTRANS = Column(Numeric(1, 0))
    F_INFO_TRANS_BGNDATE = Column(String(8))
    F_INFO_TRANS_ENDDATE = Column(String(8))
    F_INFO_PREFER_IFDIS = Column(Numeric(1, 0))
    F_INFO_PREFER_IFADD = Column(Numeric(1, 0))
    F_INFO_PREFER_FORMULA = Column(String(200))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMFDIVIDEND(Base):
    __tablename__ = 'CHINAMFDIVIDEND'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_E_BCH_DT = Column(String(8))
    F_DIV_PROGRESS = Column(String(10))
    CASH_DVD_PER_SH_TAX = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    EQY_RECORD_DT = Column(String(8))
    EX_DT = Column(String(8))
    F_DIV_EDEXDATE = Column(String(8))
    PAY_DT = Column(String(8))
    F_DIV_PAYDATE = Column(String(8))
    F_DIV_IMPDATE = Column(String(8))
    F_SH_BCH_Y = Column(String(8))
    F_BCH_UNIT = Column(Numeric(20, 4))
    F_E_APR = Column(Numeric(20, 4))
    F_EX_DIV_DT = Column(String(8))
    F_E_APR_AMOUNT = Column(Numeric(20, 4))
    F_REINV_BCH_DT = Column(String(8))
    F_REINV_TOAC_DT = Column(String(8))
    F_REINV_REDEEM_DT = Column(String(8))
    ANN_DATE = Column(String(8), index=True)
    F_DIV_OBJECT = Column(String(100))
    F_DIV_IPAYDT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMFMPERFORMANCE(Base):
    __tablename__ = 'CHINAMFMPERFORMANCE'

    OBJECT_ID = Column(String(38), primary_key=True)
    FUNDMANAGER_ID = Column(String(10), index=True)
    TRADE_DATE = Column(String(8), index=True)
    FMINDEX_TYPE = Column(Numeric(9, 0))
    FMINDEX_POINT = Column(Numeric(20, 8))
    TOTRETURN_YTD = Column(Numeric(20, 8))
    RANKING_YTD = Column(String(20))
    TOTRETURN_1W = Column(Numeric(20, 8))
    RANKING_1W = Column(String(20))
    TOTRETURN_1M = Column(Numeric(20, 8))
    RANKING_1M = Column(String(20))
    TOTRETURN_3M = Column(Numeric(20, 8))
    RANKING_3M = Column(String(20))
    TOTRETURN_6M = Column(Numeric(20, 8))
    RANKING_6M = Column(String(20))
    TOTRETURN_1Y = Column(Numeric(20, 8))
    RANKING_1Y = Column(String(20))
    TOTRETURN_2Y = Column(Numeric(20, 8))
    RANKING_2Y = Column(String(20))
    TOTRETURN_3Y = Column(Numeric(20, 8))
    RANKING_3Y = Column(String(20))
    TOTRETURN_5Y = Column(Numeric(20, 8))
    RANKING_5Y = Column(String(20))
    TOTRETURN_10Y = Column(Numeric(20, 8))
    RANKING_10Y = Column(String(20))
    TOTRETURN_ES = Column(Numeric(20, 8))
    ANNRETURNES = Column(Numeric(20, 8))
    RANKING_ES = Column(String(20))
    WORSTTOTRETURN_6M = Column(Numeric(20, 8))
    BESTTOTRETURN_6M = Column(Numeric(20, 8))
    SUCBASERETURN_YTD = Column(Numeric(20, 8))
    SUCBASERETURN_1W = Column(Numeric(20, 8))
    SUCBASERETURN_1M = Column(Numeric(20, 8))
    SUCBASERETURN_3M = Column(Numeric(20, 8))
    SUCBASERETURN_6M = Column(Numeric(20, 8))
    SUCBASERETURN_1Y = Column(Numeric(20, 8))
    SUCBASERETURN_2Y = Column(Numeric(20, 8))
    SUCBASERETURN_3Y = Column(Numeric(20, 8))
    SUCBASERETURN_5Y = Column(Numeric(20, 8))
    SUCBASERETURN_10Y = Column(Numeric(20, 8))
    SUCBASERETURN_ES = Column(Numeric(20, 8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMFPERFORMANCE(Base):
    __tablename__ = 'CHINAMFPERFORMANCE'
    __table_args__ = (
        Index('SID_ID_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    F_AVGRETURN_THISWEEK = Column(Numeric(20, 6))
    F_AVGRETURN_THISMONTH = Column(Numeric(20, 6))
    F_AVGRETURN_THISQUARTER = Column(Numeric(20, 6))
    F_AVGRETURN_WEEK = Column(Numeric(20, 6))
    F_AVGRETURN_MONTH = Column(Numeric(20, 6))
    F_AVGRETURN_QUARTER = Column(Numeric(20, 6))
    F_AVGRETURN_HALFYEAR = Column(Numeric(20, 6))
    F_AVGRETURN_THISYEAR = Column(Numeric(20, 6))
    F_AVGRETURN_YEAR = Column(Numeric(20, 6))
    F_AVGRETURN_TWOYEA = Column(Numeric(20, 6))
    F_AVGRETURN_THREEYEAR = Column(Numeric(20, 6))
    F_AVGRETURN_FOURYEAR = Column(Numeric(20, 6))
    F_AVGRETURN_FIVEYEAR = Column(Numeric(20, 6))
    F_AVGRETURN_SIXYEAR = Column(Numeric(20, 6))
    F_AVGRETURN_SINCEFOUND = Column(Numeric(20, 6))
    F_ANNUALYEILD = Column(Numeric(20, 6))
    F_TRACKDEV_THISDAY = Column(Numeric(20, 6))
    F_SFRETURN_THISYEAR = Column(Numeric(20, 6))
    F_SFRANK_THISYEAR = Column(Numeric(20, 0))
    F_SFRETURN_RECENTQUARTER = Column(Numeric(20, 6))
    F_SFRANK_RECENTQUARTER = Column(Numeric(20, 0))
    F_SFRETURN_RECENTHALFYEAR = Column(Numeric(20, 6))
    F_SFRANK_RECENTHALFYEAR = Column(Numeric(20, 0))
    F_SFRETURN_RECENTYEAR = Column(Numeric(20, 6))
    F_SFRANK_RECENTYEAR = Column(Numeric(20, 0))
    F_SFRETURN_RECENTTWOYEAR = Column(Numeric(20, 6))
    F_SFRANK_RECENTTWOYEAR = Column(Numeric(20, 0))
    F_SFRETURN_RECENTTHREEYEAR = Column(Numeric(20, 6))
    F_SFRANK_RECENTTHREEYEAR = Column(Numeric(20, 0))
    F_SFRETURN_RECENTFIVEYEAR = Column(Numeric(20, 6))
    F_SFRANK_RECENTFIVEYEAR = Column(Numeric(20, 0))
    F_SFRETURN_SINCEFOUND = Column(Numeric(20, 6))
    F_SFRANK_SINCEFOUND = Column(Numeric(20, 0))
    F_SFANNUALYEILD = Column(Numeric(20, 6))
    F_SFRANK_ANNUALYEILD = Column(Numeric(20, 0))
    F_STDARDDEV_HALFYEAR = Column(Numeric(20, 6))
    F_STDARDDEV_YEAR = Column(Numeric(20, 6))
    F_STDARDDEV_TWOYEAR = Column(Numeric(20, 6))
    F_STDARDDEV_THREEYEAR = Column(Numeric(20, 6))
    F_STDARDDEV_FIVEYEAR = Column(Numeric(20, 6))
    F_STDARDDEV_SINCEFOUND = Column(Numeric(20, 6))
    F_SHARPRATIO_HALFYEAR = Column(Numeric(20, 6))
    F_SHARPRATIO_YEAR = Column(Numeric(20, 6))
    F_SHARPRATIO_TWOYEAR = Column(Numeric(20, 6))
    F_SHARPRATIO_THREEYEAR = Column(Numeric(20, 6))
    F_SFRETURN_RECENTWEEK = Column(Numeric(20, 6))
    F_SFRANK_RECENTWEEK = Column(Numeric(20, 0))
    F_SFRETURN_RECENTMONTH = Column(Numeric(20, 6))
    F_SFRANK_RECENTMONTH = Column(Numeric(20, 0))
    F_FUNDTYPE = Column(String(50))
    F_AVGRETURN_DAY = Column(Numeric(20, 6))
    F_SFRANK_THISYEART = Column(String(50))
    F_SFRANK_RECENTQUARTERT = Column(String(50))
    F_SFRANK_RECENTHALFYEART = Column(String(50))
    F_SFRANK_RECENTYEART = Column(String(50))
    F_SFRANK_RECENTTWOYEART = Column(String(50))
    F_SFRANK_RECENTTHREEYEART = Column(String(50))
    F_SFRANK_RECENTFIVEYEART = Column(String(50))
    F_SFRANK_SINCEFOUNDT = Column(String(50))
    F_SFRANK_ANNUALYEILDT = Column(String(50))
    F_SFRANK_RECENTWEEKT = Column(String(50))
    F_SFRANK_RECENTMONTHT = Column(String(50))
    F_BETA_6M = Column(Numeric(20, 8))
    F_BETA_1Y = Column(Numeric(20, 8))
    F_BETA_2Y = Column(Numeric(20, 8))
    F_BETA_3Y = Column(Numeric(20, 8))
    F_ALPHA_6M = Column(Numeric(20, 8))
    F_ALPHA_1Y = Column(Numeric(20, 8))
    F_ALPHA_2Y = Column(Numeric(20, 8))
    F_ALPHA_3Y = Column(Numeric(20, 8))
    F_SFRETURN_DAY = Column(Numeric(20, 6))
    F_SFRANK_DAY = Column(Numeric(20, 0))
    F_SFRANK_DAYT = Column(String(50))
    F_ANNUALYEILD_SINCEFOUND = Column(Numeric(20, 6))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDASSETPORTFOLIO(Base):
    __tablename__ = 'CHINAMUTUALFUNDASSETPORTFOLIO'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'F_PRT_ENDDATE'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_PRT_ENDDATE = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    F_PRT_TOTALASSET = Column(Numeric(20, 4))
    F_PRT_NETASSET = Column(Numeric(20, 4))
    F_PRT_STOCKVALUE = Column(Numeric(20, 4))
    F_PRT_STOCKTONAV = Column(Numeric(20, 4))
    F_PRT_PASVSTKVALUE = Column(Numeric(15, 2))
    F_PRT_PASVSTKTONAV = Column(Numeric(20, 4))
    F_PRT_POSVSTKVALUE = Column(Numeric(20, 4))
    F_PRT_POSVSTKTONAV = Column(Numeric(20, 4))
    F_PRT_GOVBOND = Column(Numeric(20, 4))
    F_PRT_GOVBONDTONAV = Column(Numeric(20, 4))
    F_PRT_CASH = Column(Numeric(20, 4))
    F_PRT_CASHTONAV = Column(Numeric(20, 4))
    F_PRT_GOVCASHVALUE = Column(Numeric(20, 4))
    F_PRT_GOVCASHTONAV = Column(Numeric(20, 4))
    F_PRT_BDVALUE_NOGOV = Column(Numeric(20, 4))
    F_PRT_BDTONAV_NOGOV = Column(Numeric(20, 4))
    F_PRT_FINANBOND = Column(Numeric(20, 4))
    F_PRT_FINANBONDTONAV = Column(Numeric(20, 4))
    F_PRT_COVERTBOND = Column(Numeric(15, 2))
    F_PRT_COVERTBONDTONAV = Column(Numeric(20, 4))
    F_PRT_CORPBOND = Column(Numeric(20, 4))
    F_PRT_CORPBONDTONAV = Column(Numeric(20, 4))
    F_PRT_BONDVALUE = Column(Numeric(20, 4))
    F_PRT_BONDTONAV = Column(Numeric(20, 4))
    F_PRT_CTRBANKBILL = Column(Numeric(20, 4))
    F_PRT_CTRBANKBILLTONAV = Column(Numeric(20, 4))
    F_PRT_WARRANTVALUE = Column(Numeric(20, 4))
    F_PRT_WARRANTONAV = Column(Numeric(20, 4))
    F_PRT_ABSVALUE = Column(Numeric(20, 4))
    F_PRT_ABSVALUETONAV = Column(Numeric(20, 4))
    F_PRT_POLIFINANBDVALUE = Column(Numeric(20, 4))
    F_PRT_POLIFINANBDTONAV = Column(Numeric(20, 4))
    F_PRT_FUNDVALUE = Column(Numeric(20, 4))
    F_PRT_FUNDTONAV = Column(Numeric(20, 4))
    F_PRT_MMVALUE = Column(Numeric(20, 4))
    F_PRT_MMTONAV = Column(Numeric(20, 4))
    F_PRT_OTHER = Column(Numeric(20, 4))
    F_PRT_OTHERTONAV = Column(Numeric(20, 4))
    F_PRT_DEBCREBALANCE = Column(Numeric(20, 4))
    F_MMF_AVGPTM = Column(Numeric(20, 4))
    F_MMF_REVERSEREPO = Column(Numeric(20, 4))
    F_MMF_REPO = Column(Numeric(20, 4))
    F_PRT_STOCKTOTOT = Column(Numeric(20, 4))
    F_PRT_BONDTOTOT = Column(Numeric(20, 4))
    F_PRT_CASHTOTOT = Column(Numeric(20, 4))
    F_PRT_OTHERTOTOT = Column(Numeric(20, 4))
    F_PRT_WARRANTOTOT = Column(Numeric(20, 4))
    F_PRT_FUNDTOTOT = Column(Numeric(20, 4))
    F_PRT_MMTOTOT = Column(Numeric(20, 4))
    F_PRT_STOCKTONAVCHANGE = Column(Numeric(20, 4))
    F_PRT_BONDTONAVCHANGE = Column(Numeric(20, 4))
    F_PRT_CASHTONAVCHANGE = Column(Numeric(20, 4))
    F_PRT_OTHERTOTOTCHANGE = Column(Numeric(20, 4))
    F_PRT_CPVALUE = Column(Numeric(20, 4))
    F_PRT_MTNVALUE = Column(Numeric(20, 4))
    F_PRT_CDS = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDBENCHMARK(Base):
    __tablename__ = 'CHINAMUTUALFUNDBENCHMARK'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_INDEXWINDCODE = Column(String(40))
    S_INFO_BGNDT = Column(String(8))
    S_INFO_ENDDT = Column(String(8))
    S_INFO_INDEXWEG = Column(Numeric(20, 4))
    S_INFO_OPERATORS = Column(String(20))
    S_INFO_CONSTANT = Column(Numeric(20, 4))
    S_INFO_AFTERTAXORNOT = Column(Numeric(1, 0))
    CUR_SIGN = Column(Numeric(1, 0))
    ANN_DT = Column(String(8))
    S_INFO_FXCODE = Column(String(40))
    S_INC_SEQUENCE = Column(Numeric(2, 0))
    SEC_ID = Column(String(10))
    SEC_ID2 = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDBENCHMARKEOD(Base):
    __tablename__ = 'CHINAMUTUALFUNDBENCHMARKEOD'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    S_DQ_CLOSE = Column(Numeric(20, 8))
    S_DQ_PCTCHANGE = Column(Numeric(20, 8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDBONDPORTFOLIO(Base):
    __tablename__ = 'CHINAMUTUALFUNDBONDPORTFOLIO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_PRT_ENDDATE = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_INFO_BONDWINDCODE = Column(String(40))
    F_PRT_BDVALUE = Column(Numeric(20, 4))
    F_PRT_BDQUANTITY = Column(Numeric(20, 4))
    F_PRT_BDVALUETONAV = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDDESCRIPTION(Base):
    __tablename__ = 'CHINAMUTUALFUNDDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40))
    F_INFO_FRONT_CODE = Column(String(40))
    F_INFO_BACKEND_CODE = Column(String(40))
    F_INFO_FULLNAME = Column(String(100))
    F_INFO_NAME = Column(String(100))
    F_INFO_CORP_FUNDMANAGEMENTCOMP = Column(String(100))
    F_INFO_CUSTODIANBANK = Column(String(100))
    F_INFO_FIRSTINVESTTYPE = Column(String(40))
    F_INFO_SETUPDATE = Column(String(8))
    F_INFO_MATURITYDATE = Column(String(8))
    F_ISSUE_TOTALUNIT = Column(Numeric(20, 4))
    F_INFO_MANAGEMENTFEERATIO = Column(Numeric(20, 4))
    F_INFO_CUSTODIANFEERATIO = Column(Numeric(20, 4))
    CRNY_CODE = Column(String(10))
    F_INFO_PTMYEAR = Column(Numeric(20, 4))
    F_ISSUE_OEF_STARTDATEINST = Column(String(8))
    F_ISSUE_OEF_DNDDATEINST = Column(String(8))
    F_INFO_PARVALUE = Column(Numeric(20, 4))
    F_INFO_TRUSTTYPE = Column(String(40))
    F_INFO_TRUSTEE = Column(String(100))
    F_PCHREDM_PCHSTARTDATE = Column(String(8))
    F_INFO_REDMSTARTDATE = Column(String(8))
    F_INFO_MINBUYAMOUNT = Column(Numeric(20, 4))
    F_INFO_EXPECTEDRATEOFRETURN = Column(Numeric(20, 4))
    F_INFO_ISSUINGPLACE = Column(String(100))
    F_INFO_BENCHMARK = Column(String(500))
    F_INFO_STATUS = Column(Numeric(9, 0))
    F_INFO_RESTRICTEDORNOT = Column(String(20))
    F_INFO_STRUCTUREDORNOT = Column(Numeric(1, 0))
    F_INFO_EXCHMARKET = Column(String(10))
    F_INFO_FIRSTINVESTSTYLE = Column(String(20))
    F_INFO_ISSUEDATE = Column(String(8))
    F_INFO_TYPE = Column(String(20))
    F_INFO_ISINITIAL = Column(Numeric(5, 0))
    F_INFO_PINYIN = Column(String(10))
    F_INFO_INVESTSCOPE = Column(String(2000))
    F_INFO_INVESTOBJECT = Column(String(500))
    F_INFO_INVESTCONCEPTION = Column(String(2000))
    F_INFO_DECISION_BASIS = Column(String(2000))
    IS_INDEXFUND = Column(Numeric(5, 0))
    F_INFO_DELISTDATE = Column(String(8))
    F_INFO_CORP_FUNDMANAGEMENTID = Column(String(10))
    F_INFO_CUSTODIANBANKID = Column(String(40))
    MAX_NUM_HOLDER = Column(Numeric(20, 4))
    MAX_NUM_COLTARGET = Column(Numeric(20, 4))
    INVESTSTRATEGY = Column(String)
    RISK_RETURN = Column(String)
    F_PCHREDM_PCHMINAMT = Column(Numeric(20, 4))
    F_PCHREDM_PCHMINAMT_EX = Column(Numeric(20, 4))
    F_INFO_LISTDATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDFLOATSHARE(Base):
    __tablename__ = 'CHINAMUTUALFUNDFLOATSHARE'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    F_UNIT_FLOATSHARE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDINDPORTFOLIO(Base):
    __tablename__ = 'CHINAMUTUALFUNDINDPORTFOLIO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_PRT_ENDDATE = Column(String(8), index=True)
    S_INFO_CSRCINDUSCODE = Column(String(40))
    F_PRT_INDUSVALUE = Column(Numeric(20, 4))
    F_PRT_INDUSTONAV = Column(Numeric(20, 4))
    F_PRT_INDPOSVALUE = Column(Numeric(20, 4))
    F_PRT_INDPOSPRO = Column(Numeric(20, 4))
    F_PRT_INDPASSIVEVALUE = Column(Numeric(20, 4))
    F_PRT_INDPASSIVEPRO = Column(Numeric(20, 4))
    F_PRT_INDUSTONAVCHANGE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDISSUE(Base):
    __tablename__ = 'CHINAMUTUALFUNDISSUE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    F_INFO_CHINESENAME = Column(String(100))
    F_INFO_ENGLISHNAME = Column(String(100))
    F_INFO_SHORTNAME = Column(String(40))
    F_INFO_MGRCOMP = Column(String(100))
    F_INFO_CUSTODIANBANK = Column(String(100))
    F_INFO_TYPE = Column(Numeric(9, 0))
    F_INFO_INVESTYPE = Column(Numeric(9, 0))
    F_ISSUE_DATE = Column(String(8))
    F_ISSUE_COLPERIOD = Column(String(10))
    F_ISSUE_MAXCOLSHARE = Column(Numeric(20, 4))
    F_ISSUE_NETPCHSHARE = Column(Numeric(20, 4))
    F_ISSUE_NETPCHNUM = Column(Numeric(20, 4))
    F_INFO_PARVALUE = Column(Numeric(20, 4))
    F_INFO_PTMYEAR = Column(Numeric(20, 4))
    F_INFO_MANAFEERATIO = Column(Numeric(20, 4))
    F_INFO_CUSTFEERATIO = Column(Numeric(20, 4))
    F_INFO_SALEFEERATIO = Column(Numeric(20, 4))
    F_INFO_SETUPDATE = Column(String(8))
    F_INFO_MATURITYDATE = Column(String(8))
    F_ISSUE_SHARES = Column(Numeric(20, 4))
    S_FELLOW_DISTOR = Column(Numeric(20, 4))
    F_INFO_LISTDATE = Column(String(8))
    F_PCH_STARTDATE = Column(String(8))
    F_REDM_STARTDATE = Column(String(8))
    F_ISSUE_STARTDATEIND = Column(String(8))
    F_ISSUE_ENDDATEIND = Column(String(8))
    F_ISSUE_MINAMTIND = Column(Numeric(20, 4))
    F_ISSUE_MAXAMTIND = Column(Numeric(20, 4))
    F_PCHREDM_PCHMINAMT = Column(Numeric(20, 4))
    F_PCHREDM_REDMMINAMT = Column(Numeric(20, 4))
    F_PCHREDM_HUGEREDMPRO = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    F_INFO_ISSUEPRICE = Column(Numeric(20, 4))
    F_INFO_OVRSUBRATIO = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDMANAGER(Base):
    __tablename__ = 'CHINAMUTUALFUNDMANAGER'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8), index=True)
    F_INFO_FUNDMANAGER = Column(String(20))
    F_INFO_MANAGER_GENDER = Column(String(10))
    F_INFO_MANAGER_BIRTHYEAR = Column(String(10))
    F_INFO_MANAGER_EDUCATION = Column(String(20))
    F_INFO_MANAGER_NATIONALITY = Column(String(10))
    F_INFO_MANAGER_STARTDATE = Column(String(8))
    F_INFO_MANAGER_LEAVEDATE = Column(String(8))
    F_INFO_MANAGER_RESUME = Column(String)
    F_INFO_FUNDMANAGER_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDNAV(Base):
    __tablename__ = 'CHINAMUTUALFUNDNAV'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8))
    PRICE_DATE = Column(String(8), index=True)
    F_NAV_UNIT = Column(Numeric(20, 4))
    F_NAV_ACCUMULATED = Column(Numeric(20, 4))
    F_NAV_DIVACCUMULATED = Column(Numeric(20, 4))
    F_NAV_ADJFACTOR = Column(Numeric(20, 6))
    CRNCY_CODE = Column(String(10))
    F_PRT_NETASSET = Column(Numeric(20, 4))
    F_ASSET_MERGEDSHARESORNOT = Column(Numeric(1, 0))
    NETASSET_TOTAL = Column(Numeric(20, 4))
    F_NAV_ADJUSTED = Column(Numeric(18, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDPCHREDM(Base):
    __tablename__ = 'CHINAMUTUALFUNDPCHREDM'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_UNIT_RPSTARTDATE = Column(String(8))
    F_UNIT_RPENDDATE = Column(String(8))
    F_UNIT_STARTSHARES = Column(Numeric(20, 4))
    F_UNIT_PURCHASE = Column(Numeric(20, 4))
    F_UNIT_REDEMPTION = Column(Numeric(20, 4))
    F_UNIT_ENDSHARES = Column(Numeric(20, 4))
    TRADE_DT = Column(String(8), index=True)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDPOSESTIMATION(Base):
    __tablename__ = 'CHINAMUTUALFUNDPOSESTIMATION'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'F_EST_DATE'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_EST_DATE = Column(String(8), index=True)
    F_EST_LARGECAPWEG = Column(Numeric(20, 4))
    F_EST_MIDCAPWEG = Column(Numeric(20, 4))
    F_EST_SMALLCAPWEG = Column(Numeric(20, 4))
    F_EST_POSITION = Column(Numeric(20, 4))
    F_EST_NAV = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDREPNAVPER(Base):
    __tablename__ = 'CHINAMUTUALFUNDREPNAVPER'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8))
    F_INFO_REPORTPERIOD = Column(String(8), index=True)
    PERIOD_CODE = Column(String(10))
    F_NAV_RETURN = Column(Numeric(20, 4))
    F_NAV_STDDEVRETURN = Column(Numeric(20, 4))
    F_NAV_BENCHRETURN = Column(Numeric(20, 4))
    F_NAV_BENCHSTDDEV = Column(Numeric(20, 4))
    F_NAV_BENCHDEVRETURN = Column(Numeric(20, 4))
    F_NAV_STDDEVNAVBENCH = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDSEATTRADING(Base):
    __tablename__ = 'CHINAMUTUALFUNDSEATTRADING'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_REPORTPERIOD = Column(String(8), index=True)
    S_INFO_SECURNAME = Column(String(100))
    F_TRADE_STOCKAM = Column(Numeric(20, 4))
    F_TRADE_STOCKPRO = Column(Numeric(20, 4))
    F_TRADE_BONDAM = Column(Numeric(20, 4))
    F_TRADE_BONDPRO = Column(Numeric(20, 4))
    F_TRADE_REPOAM = Column(Numeric(20, 4))
    F_TRADE_REPOPRO = Column(Numeric(20, 4))
    F_TRADE_SBAM = Column(Numeric(20, 4))
    F_TRADE_SBPRO = Column(Numeric(20, 4))
    F_TRADE_WARRANTAM = Column(Numeric(20, 4))
    F_TRADE_WARRANTPRO = Column(Numeric(20, 4))
    F_TRADE_FUNDAM = Column(Numeric(20, 4))
    F_TRADE_FUNDPRO = Column(Numeric(20, 4))
    F_COMMISSIONAM = Column(Numeric(20, 4))
    F_COMMISSIONPRO = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDSECTOR(Base):
    __tablename__ = 'CHINAMUTUALFUNDSECTOR'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40))
    S_INFO_SECTOR = Column(String(40))
    S_INFO_SECTORENTRYDT = Column(String(8))
    S_INFO_SECTOREXITDT = Column(String(8))
    CUR_SIGN = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDSHARE(Base):
    __tablename__ = 'CHINAMUTUALFUNDSHARE'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    CHANGE_DATE = Column(String(8), index=True)
    F_UNIT_TOTAL = Column(Numeric(20, 6))
    F_INFO_SHARE = Column(Numeric(20, 6))
    FUNDSHARE = Column(Numeric(20, 6))
    F_UNIT_MERGEDSHARESORNOT = Column(Numeric(5, 0))
    CHANGEREASON = Column(String(10))
    FUNDSHARE_TOTAL = Column(Numeric(20, 6))
    CUR_SIGN = Column(Numeric(5, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDSTOCKPORTFOLIO(Base):
    __tablename__ = 'CHINAMUTUALFUNDSTOCKPORTFOLIO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_PRT_ENDDATE = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_INFO_STOCKWINDCODE = Column(String(10), index=True)
    F_PRT_STKVALUE = Column(Numeric(20, 4))
    F_PRT_STKQUANTITY = Column(Numeric(20, 4))
    F_PRT_STKVALUETONAV = Column(Numeric(20, 4))
    F_PRT_POSSTKVALUE = Column(Numeric(20, 4))
    F_PRT_POSSTKQUANTITY = Column(Numeric(20, 4))
    F_PRT_POSSTKTONAV = Column(Numeric(20, 4))
    F_PRT_PASSTKEVALUE = Column(Numeric(20, 4))
    F_PRT_PASSTKQUANTITY = Column(Numeric(20, 4))
    F_PRT_PASSTKTONAV = Column(Numeric(20, 4))
    ANN_DATE = Column(String(8), index=True)
    STOCK_PER = Column(Numeric(20, 2))
    FLOAT_SHR_PER = Column(Numeric(20, 2))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDSUSPENDPCHREDM(Base):
    __tablename__ = 'CHINAMUTUALFUNDSUSPENDPCHREDM'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_INFO_SUSPCHSTARTDT = Column(String(8))
    F_INFO_SUSPCHANNDT = Column(String(8))
    F_INFO_REPCHDT = Column(String(8))
    F_INFO_REPCHANNDT = Column(String(8))
    F_INFO_PURCHASEUPLIMIT = Column(Numeric(20, 4))
    F_INFO_SUSPCHREASON = Column(String(800))
    F_INFO_SUSPCHTYPE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDTRACKINGINDEX(Base):
    __tablename__ = 'CHINAMUTUALFUNDTRACKINGINDEX'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_INFO_INDEXWINDCODE = Column(String(40))
    ENTRY_DT = Column(String(8))
    REMOVE_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAMUTUALFUNDTRANSFORMATION(Base):
    __tablename__ = 'CHINAMUTUALFUNDTRANSFORMATION'

    OBJECT_ID = Column(String(100), primary_key=True)
    PREWINDCODE = Column(String(40))
    POSTWINDCODE = Column(String(40))
    F_INFO_PRENAME = Column(String(40))
    F_INFO_PREFULLNAME = Column(String(100))
    F_INFO_PRECODE = Column(String(10))
    F_INFO_POSTNAME = Column(String(40))
    F_INFO_POSTFULLNAME = Column(String(100))
    F_INFO_POSTCODE = Column(String(10))
    F_INFO_POSTISLIST = Column(Numeric(1, 0))
    F_INFO_CTOSUSPENDDATE = Column(String(8))
    F_INFO_MEETINGANNDATE = Column(String(8))
    F_INFO_MEETINGDATE = Column(String(8))
    F_INFO_APPROVALRATE = Column(Numeric(20, 8))
    F_INFO_RESUMPDATE = Column(String(8))
    F_INFO_CFUNDREDEMCODE = Column(String(10))
    F_INFO_CFUNDENDDATE = Column(String(8))
    F_INFO_CFUNDENDANNDATE = Column(String(8))
    F_INFO_TANSTYPE = Column(String(40))
    F_UNIT_PCH = Column(Numeric(20, 4))
    F_UNIT_SPLITSHARE = Column(Numeric(20, 4))
    F_INFOTANSTYPECODE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONCALENDAR(Base):
    __tablename__ = 'CHINAOPTIONCALENDAR'

    OBJECT_ID = Column(String(100), primary_key=True)
    TRADE_DAYS = Column(String(40), index=True)
    S_INFO_EXCHMARKET = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONCONTPRO(Base):
    __tablename__ = 'CHINAOPTIONCONTPRO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    S_INFO_CODE = Column(String(20), index=True)
    S_INFO_NAME = Column(String(40))
    S_INFO_ENAME = Column(String(100))
    S_INFO_EXNAME = Column(String(20))
    S_INFO_TYPE = Column(String(20), index=True)
    S_INFO_EUROAMERICANBERMUDA = Column(String(10))
    S_INFO_SETTLEMENTMETHOD = Column(String(10))
    S_INFO_LISTEDDATE = Column(String(8))
    S_INFO_DELISTDATE = Column(String(8))
    S_INFO_STRIKERATIO = Column(Numeric(20, 4))
    S_INFO_CEVALUE = Column(Numeric(20, 4))
    S_INFO_COVALUE = Column(String(80))
    S_INFO_LSMONTH = Column(String(200))
    S_INFO_MINPRICEFLUCT = Column(String(40))
    S_INFO_THOURS = Column(String(200))
    S_INFO_LASTTRADINGDATE = Column(String(200))
    S_INFO_LSDATE = Column(String(200))
    S_INFO_LASTSETTLE = Column(String(200))
    S_INFO_TRADEFEE = Column(String(200))
    S_INFO_POSLIMIT = Column(String(800))
    S_INFO_MINPOSLIMIT = Column(String(800))
    S_INFO_OPTIONPRICE = Column(String(20))
    S_INFO_EXERCISINGEND = Column(String(40))
    S_INFO_STRIKEPRICE = Column(String(800))
    S_INFO_SIMULATION = Column(String(1))
    S_INFO_QUOTEUNIT = Column(String(40))
    S_INFO_COUNIT = Column(Numeric(20, 4))
    S_INFO_COUNITDIMENSION = Column(String(40))
    S_INFO_ID = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONDAILYSTATISTIC(Base):
    __tablename__ = 'CHINAOPTIONDAILYSTATISTICS'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_CODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_CODE = Column(String(20), index=True)
    S_INFO_NAME = Column(String(40))
    TRADE_DT = Column(String(8), index=True)
    ITEM_CODE = Column(Numeric(9, 0))
    VALUE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONDESCRIPTION(Base):
    __tablename__ = 'CHINAOPTIONDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_CODE = Column(String(40))
    S_INFO_NAME = Column(String(100))
    S_INFO_SCCODE = Column(String(50))
    S_INFO_CALLPUT = Column(Numeric(9, 0))
    S_INFO_STRIKEPRICE = Column(Numeric(20, 4))
    S_INFO_MONTH = Column(String(6))
    S_INFO_MATURITYDATE = Column(String(8))
    S_INFO_FTDATE = Column(String(8))
    S_INFO_LASTTRADINGDATE = Column(String(8))
    S_INFO_EXERCISINGEND = Column(String(8))
    S_INFO_LDDATE = Column(String(8))
    S_INFO_LPRICE = Column(Numeric(20, 4))
    S_INFO_TRADE = Column(String(1))
    S_INFO_EXCODE = Column(String(20))
    S_INFO_EXNAME = Column(String(100))
    S_INFO_COUNIT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONEODPRICE(Base):
    __tablename__ = 'CHINAOPTIONEODPRICES'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_SETTLE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_OI = Column(Numeric(20, 4))
    S_DQ_OICHANGE = Column(Numeric(20, 4))
    S_DQ_PRESETTLE = Column(Numeric(20, 4))
    S_DQ_CHANGE1 = Column(Numeric(20, 4))
    S_DQ_CHANGE2 = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONINDEXEODPRICE(Base):
    __tablename__ = 'CHINAOPTIONINDEXEODPRICES'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_NAME = Column(String(50))
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    S_DQ_PCTCHANGE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    SEC_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONMEMBERSTATISTIC(Base):
    __tablename__ = 'CHINAOPTIONMEMBERSTATISTICS'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_CODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_CODE = Column(String(20), index=True)
    S_INFO_NAME = Column(String(40))
    TRADE_DT = Column(String(8), index=True)
    ITEM_CODE = Column(Numeric(9, 0))
    VALUE = Column(Numeric(20, 4))
    RANK = Column(Numeric(5, 0))
    S_INFO_MEMBERNAME = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CHINAOPTIONVALUATION(Base):
    __tablename__ = 'CHINAOPTIONVALUATION'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    W_ANAL_UNDERLYINGIMPLIEDVOL = Column(Numeric(24, 8))
    W_ANAL_DELTA = Column(Numeric(24, 8))
    W_ANAL_THETA = Column(Numeric(24, 8))
    W_ANAL_GAMMA = Column(Numeric(24, 8))
    W_ANAL_VEGA = Column(Numeric(24, 8))
    W_ANAL_RHO = Column(Numeric(24, 8))
    SURGED_LIMIT = Column(Numeric(24, 8))
    DECLINE_LIMIT = Column(Numeric(24, 8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CINDEXFUTURESEODPRICE(Base):
    __tablename__ = 'CINDEXFUTURESEODPRICES'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    S_DQ_PRESETTLE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_SETTLE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_OI = Column(Numeric(20, 4))
    S_DQ_CHANGE = Column(Numeric(20, 4))
    FS_INFO_TYPE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CINDEXFUTURESPOSITION(Base):
    __tablename__ = 'CINDEXFUTURESPOSITIONS'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    FS_INFO_MEMBERNAME = Column(String(40))
    FS_INFO_TYPE = Column(Numeric(1, 0))
    FS_INFO_POSITIONSNUM = Column(Numeric(20, 4))
    FS_INFO_RANK = Column(Numeric(5, 0))
    S_OI_POSITIONSNUMC = Column(Numeric(20, 4))
    S_INFO_COMPCODE = Column(String(10))
    SEC_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CLOSEDFUNDPCHREDM(Base):
    __tablename__ = 'CLOSEDFUNDPCHREDM'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_EXCHMARKET = Column(String(40))
    SUBSTRIPTION_CODE = Column(String(20))
    SUBSTRIPTION_NAME = Column(String(40))
    SUBSTRIPTION_PRICE = Column(Numeric(20, 4))
    SUBSTRIPTION_START_DT = Column(String(8))
    SUBSTRIPTION_END_DT = Column(String(8))
    PCH_CODE = Column(String(20))
    PCH_NAME = Column(String(40))
    PCH_START_DT = Column(String(8))
    REDM_START_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFAIPINFO(Base):
    __tablename__ = 'CMFAIPINFO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8), index=True)
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    MIN_PURCHASE = Column(Numeric(20, 4))
    MAX_PURCHASE = Column(Numeric(20, 4))
    LEVEL_AMOUNT = Column(Numeric(20, 4))
    TYPE_CODE = Column(Numeric(9, 0))
    COMP_NAME = Column(String(200))
    COMP_ID = Column(String(10))
    MEMO = Column(String(500))
    SEQUENCE = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFBALANCESHEET(Base):
    __tablename__ = 'CMFBALANCESHEET'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(10))
    REPORT_PERIOD = Column(String(8), index=True)
    ANN_DT = Column(String(8))
    IS_LIST = Column(Numeric(5, 0))
    F_STM_BS = Column(Numeric(20, 4))
    SETTLE_RSRV = Column(Numeric(20, 4))
    MRGN_PAID = Column(Numeric(20, 4))
    TRADABLE_FIN_ASSETS = Column(Numeric(20, 4))
    STOCK_VALUE = Column(Numeric(20, 4))
    STOCK_COST = Column(Numeric(20, 4))
    STOCK_ADD_VALUE = Column(Numeric(20, 4))
    FUND_VALUE = Column(Numeric(20, 4))
    BOND_VALUE = Column(Numeric(20, 4))
    ABS_VALUE = Column(Numeric(20, 4))
    BOND_COST = Column(Numeric(20, 4))
    BOND_ADD_VALUE = Column(Numeric(20, 4))
    GOVBOND_COST = Column(Numeric(20, 4))
    GOVBOND_ADD_VALUE = Column(Numeric(15, 2))
    CONVERTBOND_COST = Column(Numeric(20, 4))
    CONVERTBOND_ADD_VALUE = Column(Numeric(20, 4))
    DERIVATIVE_FIN_VALUE = Column(Numeric(20, 4))
    DERIVATIVE_FIN_COST = Column(Numeric(20, 4))
    TRADE_RCV = Column(Numeric(20, 4))
    WAR_RCV = Column(Numeric(20, 4))
    INT_RCV = Column(Numeric(20, 4))
    ACCT_RCV = Column(Numeric(20, 4))
    DVD_RCV = Column(Numeric(20, 4))
    PUR_RCV = Column(Numeric(20, 4))
    DEFERRED_TAX_ASSETS = Column(Numeric(20, 4))
    OTH_RCV = Column(Numeric(20, 4))
    DEFERRED_EXP = Column(Numeric(20, 4))
    REPO_REV = Column(Numeric(20, 4))
    OTH_ASSETS = Column(Numeric(20, 4))
    TOT_ASSETS = Column(Numeric(20, 4))
    ST_BORROW = Column(Numeric(20, 4))
    MANAGE_PAYABLE = Column(Numeric(20, 4))
    TRUSTEE_PAYABLE = Column(Numeric(20, 4))
    SEC_TRADE_PAYABLE = Column(Numeric(20, 4))
    ACCT_PAYABLE = Column(Numeric(20, 4))
    TRADE_PAYABLE = Column(Numeric(20, 4))
    OTH_PAYABLE = Column(Numeric(20, 4))
    RIGHT_PAYABLE = Column(Numeric(20, 4))
    REDE_PAYABLE = Column(Numeric(20, 4))
    REDE_PAYABLE2 = Column(Numeric(20, 4))
    REPO_AMOUNT = Column(Numeric(20, 4))
    INT_PAYABLE = Column(Numeric(20, 4))
    REV_PAYABLE = Column(Numeric(20, 4))
    NOTPAYTAX = Column(Numeric(20, 4))
    ACC_EXP = Column(Numeric(20, 4))
    DEFERRED_TAX_LIAB = Column(Numeric(20, 4))
    OTH_LIAB = Column(Numeric(20, 4))
    SELL_EXP = Column(Numeric(20, 4))
    TRADABLE_FIN_LIAB = Column(Numeric(20, 4))
    DERIVATIVE_FIN_LIAB = Column(Numeric(20, 4))
    TOT_LIAB = Column(Numeric(20, 4))
    PAIDINCAPITAL = Column(Numeric(20, 4))
    UNDISTRIBUTED_ET_INC = Column(Numeric(20, 4))
    UNREALIZED_PROFIT = Column(Numeric(20, 4))
    UNDISTRIBUTED_PROFIT = Column(Numeric(20, 4))
    UNREALIZED_ADD_VALUE = Column(Numeric(20, 4))
    HOLDER_EQUITY = Column(Numeric(20, 4))
    PRT_NETASSET = Column(Numeric(20, 4))
    TOT_LIAB_SHRHLDR_EQY = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFCODEANDSNAME(Base):
    __tablename__ = 'CMFCODEANDSNAME'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TYPE_CODE = Column(Numeric(9, 0))
    S_CODE = Column(String(20))
    S_SNAME = Column(String(100))
    IS_COMMON = Column(Numeric(1, 0))
    MEMO = Column(String(800))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFCONSEPTION(Base):
    __tablename__ = 'CMFCONSEPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_SECTOR = Column(String(10))
    S_INFO_SECTORNAME = Column(String(40))
    S_INFO_SECTORENTRYDT = Column(String(8))
    S_INFO_SECTOREXITDT = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFFINANCIALINDICATOR(Base):
    __tablename__ = 'CMFFINANCIALINDICATOR'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(10))
    REPORT_PERIOD = Column(String(8), index=True)
    ANN_DT = Column(String(8))
    IS_LIST = Column(Numeric(5, 0))
    ANAL_NETINCOME = Column(Numeric(20, 4))
    ANAL_INCOME = Column(Numeric(20, 4))
    ANAL_AVGNETINCOMEPERUNIT = Column(Numeric(20, 4))
    ANAL_AVGNAVRETURN = Column(Numeric(20, 4))
    ANAL_NAV_RETURN = Column(Numeric(20, 4))
    NAV_ACC_RETURN = Column(Numeric(20, 4))
    ANAL_DISTRIBUTABLE = Column(Numeric(20, 4))
    ANAL_DISTRIBUTABLEPERUNIT = Column(Numeric(20, 4))
    PRT_NETASSET = Column(Numeric(20, 4))
    PRT_NETASSETPERUNIT = Column(Numeric(20, 4))
    PRT_TOTALASSET = Column(Numeric(20, 4))
    UNIT_TOTAL = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFHOLDER(Base):
    __tablename__ = 'CMFHOLDER'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    S_FA_LATELYRD = Column(String(8), index=True)
    S_INFO_ENDDATE = Column(String(8))
    B_INFO_HOLDER = Column(String(100))
    B_INFO_HOLDAMOUNT = Column(Numeric(20, 4))
    B_ISSUER_SHARECATEGORY = Column(String(1))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFHOLDERSTRUCTURE(Base):
    __tablename__ = 'CMFHOLDERSTRUCTURE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(40))
    ANN_DT = Column(String(8), index=True)
    END_DT = Column(String(8))
    SCOPE = Column(String(4))
    HOLDER_NUMBER = Column(Numeric(20, 0))
    HOLDER_AVGHOLDING = Column(Numeric(20, 4))
    HOLDER_INSTITUTION_HOLDING = Column(Numeric(20, 4))
    HOLDER_INSTITUTION_HOLDINGPCT = Column(Numeric(20, 4))
    HOLDER_PERSONAL_HOLDING = Column(Numeric(20, 4))
    HOLDER_PERSONAL_HOLDINGPCT = Column(Numeric(20, 4))
    HOLDER_MNGEMP_HOLDING = Column(Numeric(20, 4))
    HOLDER_MNGEMP_HOLDINGPCT = Column(Numeric(20, 8))
    HOLDER_FEEDER_HOLDING = Column(Numeric(20, 4))
    HOLDER_FEEDER_HOLDINGPCT = Column(Numeric(20, 4))
    PUR_COST = Column(Numeric(20, 4))
    SELL_INCOME = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFINCOME(Base):
    __tablename__ = 'CMFINCOME'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(38), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(10))
    REPORT_PERIOD = Column(String(8), index=True)
    ANN_DT = Column(String(8))
    IS_LIST = Column(Numeric(5, 0))
    TOT_INC = Column(Numeric(20, 4))
    INT_INC = Column(Numeric(20, 4))
    CASH_INT_INC = Column(Numeric(20, 4))
    BOND_INT_INC = Column(Numeric(20, 4))
    ABS_INT_INC = Column(Numeric(20, 4))
    REPO_INT_INC = Column(Numeric(20, 4))
    OTHER_INT_INC = Column(Numeric(20, 4))
    INV_INC = Column(Numeric(20, 4))
    STOCK_INV_INC = Column(Numeric(20, 4))
    BOND_INV_INC = Column(Numeric(20, 4))
    FUND_INV_INC = Column(Numeric(20, 4))
    ABS_INV_INC = Column(Numeric(20, 4))
    DERIVATIVE_INV_INC = Column(Numeric(20, 4))
    DVD_INC = Column(Numeric(20, 4))
    CHANGE_FAIR_VALUE = Column(Numeric(22, 4))
    EXCH_INC = Column(Numeric(20, 4))
    OTHER_INC = Column(Numeric(20, 4))
    TOT_EXP = Column(Numeric(20, 4))
    MGMT_EXP = Column(Numeric(20, 4))
    CUST_MAINT_EXP = Column(Numeric(20, 4))
    CUSTODIAN_EXP = Column(Numeric(20, 4))
    SELLING_DIST_EXP = Column(Numeric(20, 4))
    TRADE_EXP = Column(Numeric(20, 4))
    INT_EXP = Column(Numeric(20, 4))
    REPO_EXP = Column(Numeric(20, 4))
    OTHER_EXP = Column(Numeric(20, 4))
    ACCT_EXP = Column(Numeric(20, 4))
    AUDIT_EXP = Column(Numeric(20, 4))
    TOT_PROFIT = Column(Numeric(22, 4))
    INC_TAX = Column(Numeric(20, 4))
    NET_PROFIT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFINDEXDESCRIPTION(Base):
    __tablename__ = 'CMFINDEXDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_CODE = Column(String(40))
    S_INFO_NAME = Column(String(50))
    S_INFO_COMPNAME = Column(String(100))
    S_INFO_EXCHMARKET = Column(String(40))
    S_INFO_PUBLISHER = Column(String(100))
    S_INFO_INDEX_BASEPER = Column(String(8))
    S_INFO_INDEX_BASEPT = Column(Numeric(20, 4))
    S_INFO_LISTDATE = Column(String(8))
    S_INFO_INDEX_WEIGHTSRULE = Column(String(40))
    S_INFO_INDEXSTYLE = Column(String(40))
    INDEX_INTRO = Column(String)
    EXPIRE_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFINDEXEOD(Base):
    __tablename__ = 'CMFINDEXEOD'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_NAME = Column(String(50))
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    S_DQ_PRECLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    SEC_ID = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFISSUINGDATEPREDICT(Base):
    __tablename__ = 'CMFISSUINGDATEPREDICT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(40))
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    S_STM_ACTUAL_ISSUINGDATE = Column(String(8))
    S_STM_CORRECT_NUM = Column(String(20))
    S_STM_CORRECT_ISSUINGDATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFOTHERPORTFOLIO(Base):
    __tablename__ = 'CMFOTHERPORTFOLIO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    END_DT = Column(String(8))
    ANN_DT = Column(String(8))
    CRNCY_CODE = Column(String(10))
    S_INFO_HOLDWINDCODE = Column(String(40))
    VALUE = Column(Numeric(20, 4))
    QUANTITY = Column(Numeric(20, 4))
    VALUETONAV = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFPREFERENTIALFEE(Base):
    __tablename__ = 'CMFPREFERENTIALFEE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(10))
    TYPE_CODE = Column(Numeric(9, 0))
    ANN_DT = Column(String(8), index=True)
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    AMOUNTDOWNLIMIT = Column(Numeric(20, 4))
    AMOUNTUPLIMIT = Column(Numeric(20, 4))
    PREFERENTIAL_RATE = Column(Numeric(20, 4))
    WAY_TYPE = Column(Numeric(9, 0))
    COMP_NAME = Column(String(200))
    COMP_ID = Column(String(10))
    MEMO = Column(String(500))
    SEQUENCE = Column(Numeric(1, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFRISKLEVEL(Base):
    __tablename__ = 'CMFRISKLEVEL'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    RISK_LEVEL = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFSELLINGAGENT(Base):
    __tablename__ = 'CMFSELLINGAGENTS'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40))
    F_ANN_DATE = Column(String(8))
    F_AGENCY_NAMEID = Column(String(20))
    F_AGENCY_NAME = Column(String(200))
    F_RELATION = Column(String(30))
    F_BEGIN_DATE = Column(String(8))
    F_END_DATE = Column(String(8))
    CUR_SIGN = Column(Numeric(5, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFSUBREDFEE(Base):
    __tablename__ = 'CMFSUBREDFEE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8), index=True)
    FEETYPECODE = Column(String(30))
    CHARGEWAY = Column(String(20))
    AMOUNTDOWNLIMIT = Column(Numeric(20, 4))
    AMOUNTUPLIMIT = Column(Numeric(20, 4))
    HOLDINGPERIOD_DOWNLIMIT = Column(Numeric(20, 4))
    HOLDINGPERIOD_UPLIMIT = Column(Numeric(20, 4))
    FEERATIO = Column(Numeric(20, 4))
    ISUPLIMITFEE = Column(String(1))
    CHANGE_DT = Column(String(8))
    SUPPLEMENTARY = Column(String(800))
    TRADINGPLACE = Column(String(40))
    TRADINGPLACECODE = Column(Numeric(9, 0))
    HOLDINGPERIODUNIT = Column(String(20))
    USED = Column(Numeric(1, 0))
    MEMO = Column(String(4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFTHEMECONCEPT(Base):
    __tablename__ = 'CMFTHEMECONCEPT'

    OBJECT_ID = Column(String(38), primary_key=True)
    S_INFO_SECTOR = Column(String(16))
    S_INFO_NAME = Column(String(100))
    S_INFO_WINDCODE = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFTRADINGSUSPENSION(Base):
    __tablename__ = 'CMFTRADINGSUSPENSION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    SEC_ID = Column(String(40))
    S_DQ_SUSPENDDATE = Column(String(8))
    S_DQ_SUSPENDTIME = Column(String(200))
    S_DQ_SUSPENDTYPE = Column(Numeric(9, 0))
    S_DQ_RESUMPDATE = Column(String(8))
    S_DQ_CHANGEREASON = Column(String(400))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFUNDOPERATEPERIOD(Base):
    __tablename__ = 'CMFUNDOPERATEPERIOD'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    OPR_PERIOD = Column(Numeric(10, 0))
    PCH_STARTDT = Column(String(8))
    PCH_ENDDT = Column(String(8))
    OPR_STARTDT = Column(String(8))
    OPR_ENDDT = Column(String(8))
    REDM_STARTDT = Column(String(8))
    REDM_ENDDT = Column(String(8))
    ANTICIPATE_ANNUALYEILD = Column(Numeric(20, 4))
    ANNUALYEILD = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFUNDSPLIT(Base):
    __tablename__ = 'CMFUNDSPLIT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_INFO_SPLITTYPE = Column(String(40))
    F_INFO_SPLITANNDATE = Column(String(8))
    F_INFO_SPLITANNOECDATE = Column(String(8))
    F_INFO_SHARETRANSDATE = Column(String(8))
    F_INFO_SHAREKINDATE = Column(String(8))
    F_INFO_SPLITINC = Column(Numeric(20, 10))
    F_INFO_SPLIT_DT = Column(String(8))
    CONCOSINC = Column(Numeric(20, 10))
    CONRELINC = Column(Numeric(20, 10))
    REL_WINDCODE = Column(String(40))
    F_INFO_SPLITTYPECODE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFUNDTHIRDPARTYRATING(Base):
    __tablename__ = 'CMFUNDTHIRDPARTYRATING'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    RPTDATE = Column(String(10))
    STARLEVEL = Column(Numeric(20, 4))
    F_INFO_TYPE = Column(String(80))
    RATING_INTERVAL = Column(String(40))
    RATING_GAGENCY = Column(String(40))
    RATING_TYPE = Column(String(80))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMFUNDWINDRATING(Base):
    __tablename__ = 'CMFUNDWINDRATING'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'RPTDATE'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    RPTDATE = Column(String(8), index=True)
    STARLEVEL = Column(Numeric(20, 4))
    F_INFO_TYPE = Column(String(80))
    RATING_INTERVAL = Column(String(40))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMMFPORTFOLIOPTM(Base):
    __tablename__ = 'CMMFPORTFOLIOPTM'
    __table_args__ = (
        Index('SID_DT_INDEX', 'F_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    REPORT_PERIOD = Column(String(8), index=True)
    F_PTM_TOP = Column(Numeric(20, 4))
    F_PTM_BOTTOM = Column(Numeric(20, 4))
    RATIO_ASSERT_NAV = Column(Numeric(20, 4))
    RATIO_LIAB_NAV = Column(Numeric(20, 4))
    TYPECODE = Column(String(200))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMONEYMARKETDAILYFINCOME(Base):
    __tablename__ = 'CMONEYMARKETDAILYFINCOME'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    F_INFO_BGNDATE = Column(String(8))
    F_INFO_ENDDATE = Column(String(8))
    F_INFO_UNITYIELD = Column(Numeric(20, 4))
    F_ACCUNITNAV = Column(Numeric(20, 4))
    F_INCOME_PER_MILLION = Column(Numeric(20, 5))
    F_INFO_YEARLYROE = Column(Numeric(20, 4))
    ANN_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMONEYMARKETFINCOME(Base):
    __tablename__ = 'CMONEYMARKETFINCOME'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_INFO_BGNDATE = Column(String(8))
    F_INFO_ENDDATE = Column(String(8))
    F_INFO_UNITYIELD = Column(Numeric(20, 5))
    F_INFO_YEARLYROE = Column(Numeric(20, 4))
    ANN_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CMONEYMARKETFSCARRYOVERM(Base):
    __tablename__ = 'CMONEYMARKETFSCARRYOVERM'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_INFO_INCOMESCARRYOVERM = Column(String(20))
    F_INFO_INCOMESCARRYOVERDTCODE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class COMPANYPREVIOUSNAME(Base):
    __tablename__ = 'COMPANYPREVIOUSNAME'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'CHANGE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(10))
    COMP_NAME = Column(String(200))
    COMP_NAME_ENG = Column(String(200))
    ANN_DT = Column(String(8))
    CHANGE_DT = Column(String(8))
    CHANGE_REASON = Column(String(100))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CPFUNDDESCRIPTION(Base):
    __tablename__ = 'CPFUNDDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40))
    ANN_DT = Column(String(8))
    CP_PERIOD = Column(Numeric(20, 4))
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    GUARANT_FEE = Column(Numeric(20, 4))
    TRIGGER_YIELD = Column(Numeric(20, 4))
    TRIGGER_INFO = Column(String(2000))
    GUARANTOR = Column(String(200))
    GUARANTOR_INFO = Column(String(800))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CWARRANTDESCRIPTION(Base):
    __tablename__ = 'CWARRANTDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    WINDCODE = Column(String(40), index=True)
    UNDERLYINGWINDCODE = Column(String(10), index=True)
    CALLPUT = Column(String(40))
    EQUITYCOVERED = Column(String(40))
    ISSUER = Column(String(200))
    ISSUER_TYPE = Column(String(200))
    ISSUER_IPONUM = Column(Numeric(20, 4))
    ISSUERNUM = Column(Numeric(20, 4))
    INISTRIKEPRICE = Column(Numeric(20, 4))
    INISTRIKERATIO = Column(Numeric(24, 8))
    DURATION_DAYS = Column(Numeric(20, 4))
    DURATION_STARTDATE = Column(String(8))
    DURATION_ENDDATE = Column(String(8))
    ISSUEDATE = Column(String(8))
    ISSUEPRICE = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    LISTEDDATE = Column(String(8))
    ANNISSUEDATE = Column(String(8))
    ANNLISTEDDATE = Column(String(8))
    SETTLEMENTMETHOD = Column(String(40))
    EXERCISINGEND = Column(String(8))
    EXERCODE = Column(String(20))
    EXERNAME = Column(String(40))
    ISRATIOADJUST = Column(Numeric(5, 0))
    ADJUSTRATIO = Column(String(800))
    PROGRESS = Column(String(8))
    PRELAN_DATE = Column(String(8))
    SMTG_DATE = Column(String(8))
    TYPEBYISSUER = Column(String(40))
    OLD_SHRHLDR_RATIO = Column(Numeric(20, 4))
    PLACINGRECORDDATE = Column(String(8))
    OFFERINGOBJECT = Column(String(40))
    GUARTYPE = Column(String(40))
    EXERCISESTARTDATE = Column(String(8))
    REF_PRICE = Column(Numeric(20, 4))
    ISCALLCLAUSE = Column(Numeric(5, 0))
    CALLPRICE = Column(Numeric(20, 4))
    CALLDESCRIPTION = Column(String(500))
    GUARANTOR = Column(String(300))
    GUARDESCRIPTION = Column(String(400))
    WARRANTSOURCE = Column(String(40))
    WARRANT_NUM_PER_BOND = Column(Numeric(20, 4))
    BONDWINDCODE = Column(String(10))
    LASTTRADEDATE = Column(String(8))
    EXPIREDATE = Column(String(8))
    RECORDDATE_PUTWARRANT = Column(String(8))
    S_INFO_LISTBOARD = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class CWARRANTHOLDER(Base):
    __tablename__ = 'CWARRANTHOLDER'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_WINDCODE = Column(String(40), index=True)
    END_DATE = Column(String(8))
    HOLDER = Column(String(200))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class ETFPCHREDM(Base):
    __tablename__ = 'ETFPCHREDM'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_EXCHMARKET = Column(String(20))
    ONLINE_OFFERING_CODE = Column(String(10))
    NETWORKCASHBUYSTARTDT = Column(String(8))
    NETWORKCASHBUYENDDT = Column(String(8))
    NETWORKCASHBUYDOWNLIMIT = Column(Numeric(20, 4))
    NETWORKCASHBUYUPLIMIT = Column(Numeric(20, 4))
    OFFNETWORKBUYSTARTDT = Column(String(8))
    OFFNETWORKBUYENDDT = Column(String(8))
    OFFNETWORKCASHBUYDOWNLIMIT = Column(Numeric(20, 4))
    PCH_START_DT = Column(String(8))
    REDM_START_DT = Column(String(8))
    PCH_CODE = Column(String(10))
    PCH_NAME = Column(String(40))
    LIST_DT = Column(String(8))
    LIST_SHARE = Column(Numeric(20, 4))
    CONVERSION_DT = Column(String(8))
    CONVERSION_RATIO = Column(Numeric(20, 8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class FINDEXPERFORMANCE(Base):
    __tablename__ = 'FINDEXPERFORMANCE'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    PCT_CHG_RECENT1M = Column(Numeric(20, 6))
    PCT_CHG_RECENT3M = Column(Numeric(20, 6))
    PCT_CHG_RECENT6M = Column(Numeric(20, 6))
    PCT_CHG_RECENT1Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT2Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT3Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT4Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT5Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT6Y = Column(Numeric(20, 6))
    PCT_CHG_THISWEEK = Column(Numeric(20, 6))
    PCT_CHG_THISMONTH = Column(Numeric(20, 6))
    PCT_CHG_THISQUARTER = Column(Numeric(20, 6))
    PCT_CHG_THISYEAR = Column(Numeric(20, 6))
    SI_PCT_CHG = Column(Numeric(20, 6))
    ANNUALYEILD = Column(Numeric(20, 6))
    STD_DEV_6M = Column(Numeric(20, 6))
    STD_DEV_1Y = Column(Numeric(20, 6))
    STD_DEV_2Y = Column(Numeric(20, 6))
    STD_DEV_3Y = Column(Numeric(20, 6))
    SHARPRATIO_6M = Column(Numeric(20, 6))
    SHARPRATIO_1Y = Column(Numeric(20, 6))
    SHARPRATIO_2Y = Column(Numeric(20, 6))
    SHARPRATIO_3Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT1W = Column(Numeric(20, 6))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class FINNOTESDETAIL(Base):
    __tablename__ = 'FINNOTESDETAIL'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(38), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    REPORT_PERIOD = Column(String(8), index=True)
    STATEMENT_TYPE = Column(Numeric(9, 0))
    BD_LOSS = Column(Numeric(20, 4))
    INV_LOSS = Column(Numeric(20, 4))
    GI_LOSS = Column(Numeric(20, 4))
    GC_LOSS = Column(Numeric(20, 4))
    AFA_LOSS = Column(Numeric(20, 4))
    HI_LOSS = Column(Numeric(20, 4))
    OTH_LOSS = Column(Numeric(20, 4))
    LTI_INVINC = Column(Numeric(20, 4))
    FAT_INVINC = Column(Numeric(20, 4))
    AFA_INVINC = Column(Numeric(20, 4))
    PAEI_INVINC = Column(Numeric(20, 4))
    FEL_INVINC = Column(Numeric(20, 4))
    CFEO_INVINC = Column(Numeric(20, 4))
    OTHN_INVINC = Column(Numeric(20, 4))
    LTI_COST = Column(Numeric(20, 4))
    LUR_ORIVAL = Column(Numeric(20, 4))
    LUR_ACCA = Column(Numeric(20, 4))
    LUR_IMP = Column(Numeric(20, 4))
    LUR_BOOKVAL = Column(Numeric(20, 4))
    LTI_EQU = Column(Numeric(20, 4))
    MONF_RES = Column(Numeric(20, 4))
    SAL_COS = Column(Numeric(20, 4))
    SAL_GEX = Column(Numeric(20, 4))
    DA_COS = Column(Numeric(20, 4))
    DA_GEX = Column(Numeric(20, 4))
    REN_COS = Column(Numeric(20, 4))
    REN_GEX = Column(Numeric(20, 4))
    STC_COS = Column(Numeric(20, 4))
    APE_COS = Column(Numeric(20, 4))
    LTLOAN_1Y = Column(Numeric(20, 4))
    BONDPAY_1Y = Column(Numeric(20, 4))
    STFINBOND = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class FUNDANNCOLUMN(Base):
    __tablename__ = 'FUNDANNCOLUMN'

    OBJECT_ID = Column(String(100), primary_key=True)
    N_INFO_CODE = Column(String(38))
    N_INFO_PCODE = Column(String(38))
    N_INFO_FCODE = Column(String(38))
    N_INFO_NAME = Column(String(200))
    N_INFO_ISVALID = Column(Numeric(2, 0))
    N_INFO_LEVELNUM = Column(Numeric(2, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class FUNDANNINF(Base):
    __tablename__ = 'FUNDANNINF'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    N_INFO_STOCKID = Column(String(10))
    N_INFO_COMPANYID = Column(String(10))
    ANN_DT = Column(DateTime)
    N_INFO_TITLE = Column(String(1024))
    N_INFO_FCODE = Column(String(200))
    N_INFO_FTXT = Column(String)
    N_INFO_ANNLINK = Column(String(1024))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class FUNDCOMPANYINSIDEHOLDER(Base):
    __tablename__ = 'FUNDCOMPANYINSIDEHOLDER'
    __table_args__ = (
        Index('COMP_DT_INDEX', 'S_INFO_COMPCODE', 'ANN_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_COMPCODE = Column(String(40), index=True)
    COMP_SNAME = Column(String(200))
    ANN_DT = Column(String(8), index=True)
    S_HOLDER_ENDDATE = Column(String(8))
    S_HOLDER_HOLDERCATEGORY = Column(String(1))
    S_HOLDER_NAME = Column(String(200))
    S_HOLDER_QUANTITY = Column(Numeric(20, 4))
    S_HOLDER_PCT = Column(Numeric(20, 4))
    S_HOLDER_SHARECATEGORY = Column(String(40))
    S_HOLDER_RESTRICTEDQUANTITY = Column(Numeric(20, 4))
    S_HOLDER_ANAME = Column(String(100))
    S_HOLDER_SEQUENCE = Column(String(10))
    S_HOLDER_SHARECATEGORYNAME = Column(String(40))
    S_HOLDER_MEMO = Column(String(1500))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class HS300IEODPRICE(Base):
    __tablename__ = 'HS300IEODPRICES'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    CRNCY_CODE = Column(String(10))
    PRE_CLOSE = Column(Numeric(20, 4))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    CHG = Column(Numeric(20, 4))
    PCT_CHG = Column(Numeric(20, 4))
    VOLUME = Column(Numeric(20, 4))
    AMT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class LOFPCHREDM(Base):
    __tablename__ = 'LOFPCHREDM'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_EXCHMARKET = Column(String(20))
    SUBSTRIPTION_CODE = Column(String(20))
    SUBSTRIPTION_NAME = Column(String(40))
    SUBSTRIPTION_PRICE = Column(Numeric(20, 4))
    SUBSTRIPTION_START_DT = Column(String(8))
    SUBSTRIPTION_END_DT = Column(String(8))
    PCH_CODE = Column(String(20))
    PCH_NAME = Column(String(40))
    PCH_START_DT = Column(String(8))
    REDM_START_DT = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class QDIIINDPORTFOLIO(Base):
    __tablename__ = 'QDIIINDPORTFOLIO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DT = Column(String(8))
    END_DT = Column(String(8))
    IND = Column(String(100))
    VALUE = Column(Numeric(20, 4))
    PCT_NAV = Column(Numeric(20, 4))
    GICS_CODE = Column(String(10))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class QDIISCOPEPORTFOLIO(Base):
    __tablename__ = 'QDIISCOPEPORTFOLIO'

    OBJECT_ID = Column(String(38), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    ANN_DATE = Column(String(8))
    ENDDATE = Column(String(8))
    SCOPE = Column(String(10))
    VALUE = Column(Numeric(20, 4))
    POSSTKTONAV = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class QDIISECURITIESPORTFOLIO(Base):
    __tablename__ = 'QDIISECURITIESPORTFOLIO'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    ANN_DATE = Column(String(8))
    ENDDATE = Column(String(8))
    TYPE = Column(String(10))
    INVESTCODE = Column(String(20))
    NAME = Column(String(100))
    COMP_NAME = Column(String(100))
    LISTSCOPE = Column(String(20))
    S_INFO_INVESTWINDCODE = Column(String(40))
    QUANTITY = Column(Numeric(20, 4))
    VALUE = Column(Numeric(20, 4))
    POSSTKTONAV = Column(Numeric(20, 4))
    INVESTFUNDCODE = Column(String(20))
    INVESTFUNDTYPE = Column(String(20))
    FUNDMANAGEMENTCOMP = Column(String(100))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class QFIIDESCRIPTION(Base):
    __tablename__ = 'QFIIDESCRIPTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_COMPNAME = Column(String(100))
    F_INFO_COMPCODE = Column(String(10), index=True)
    TRUSTEE_NAME = Column(String(100))
    TRUSTEE_ID = Column(String(100))
    CUSTODY_SECUR_COMPNAME = Column(String(200))
    CUSTODY_SECUR_COMPID = Column(String(200))
    APPROVAL_DATE = Column(String(8))
    APPROVAL_ENDDATE = Column(String(8))
    APPROVAL_DATE_FIRST = Column(String(8))
    APPROVAL_DATE_ACCOUNT = Column(String(8))
    ANN_DATE = Column(String(8))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class QFIIQUOTACHANGE(Base):
    __tablename__ = 'QFIIQUOTACHANGE'

    OBJECT_ID = Column(String(100), primary_key=True)
    F_INFO_COMPNAME = Column(String(100))
    F_INFO_COMPCODE = Column(String(20))
    INVEST_AMOUNT = Column(Numeric(20, 4))
    CHANGE_DT = Column(String(8))
    CRNCY_CODE = Column(String(10))
    TYPE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class SCFCLAUSE(Base):
    __tablename__ = 'SCFCLAUSE'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TYPE_CODE = Column(Numeric(9, 0))
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    CUR_SIGN = Column(Numeric(1, 0))
    CONTENT = Column(String)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class SCFCONVERT(Base):
    __tablename__ = 'SCFCONVERT'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    CONVERT_REASON = Column(Numeric(9, 0))
    PERIOD = Column(Numeric(20, 4))
    CONVERT_DT = Column(String(8))
    NAV_TYPECODE = Column(Numeric(9, 0))
    NAV_MIN = Column(Numeric(20, 4))
    NAV_MAX = Column(Numeric(20, 4))
    CONVERT_TYPECODE = Column(Numeric(9, 0))
    CONVERT_FUND_TYPECODE = Column(Numeric(9, 0))
    CONVERT_FEEDER_TYPECODE = Column(Numeric(9, 0))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class SCFRETURNDISTRIBUTION(Base):
    __tablename__ = 'SCFRETURNDISTRIBUTION'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    F_INFO_FEEDER_WINDCODE = Column(String(40), index=True)
    RULE_STARETDT = Column(String(8))
    RULE_ENDDT = Column(String(8))
    PRI_BENCHRETURN = Column(Numeric(20, 4))
    TYPE_CODE = Column(Numeric(9, 0))
    RETURNDIST_CODE = Column(Numeric(9, 0))
    EXRETURN_SHARE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class SINDEXPERFORMANCE(Base):
    __tablename__ = 'SINDEXPERFORMANCE'
    __table_args__ = (
        Index('SID_DT_INDEX', 'S_INFO_WINDCODE', 'TRADE_DT'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    TRADE_DT = Column(String(8), index=True)
    PCT_CHG_RECENT1M = Column(Numeric(20, 6))
    PCT_CHG_RECENT3M = Column(Numeric(20, 6))
    PCT_CHG_RECENT6M = Column(Numeric(20, 6))
    PCT_CHG_RECENT1Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT2Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT3Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT4Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT5Y = Column(Numeric(20, 6))
    PCT_CHG_RECENT6Y = Column(Numeric(20, 6))
    PCT_CHG_THISWEEK = Column(Numeric(20, 6))
    PCT_CHG_THISMONTH = Column(Numeric(20, 6))
    PCT_CHG_THISQUARTER = Column(Numeric(20, 6))
    PCT_CHG_THISYEAR = Column(Numeric(20, 6))
    SI_PCT_CHG = Column(Numeric(20, 6))
    ANNUALYEILD = Column(Numeric(20, 6))
    STD_DEV_6M = Column(Numeric(20, 6))
    STD_DEV_1Y = Column(Numeric(20, 6))
    STD_DEV_2Y = Column(Numeric(20, 6))
    STD_DEV_3Y = Column(Numeric(20, 6))
    SHARPRATIO_6M = Column(Numeric(20, 6))
    SHARPRATIO_1Y = Column(Numeric(20, 6))
    SHARPRATIO_2Y = Column(Numeric(20, 6))
    SHARPRATIO_3Y = Column(Numeric(20, 6))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class THIRDPARTYINDEXEOD(Base):
    __tablename__ = 'THIRDPARTYINDEXEOD'

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40))
    TRADE_DT = Column(String(8))
    S_DQ_OPEN = Column(Numeric(20, 4))
    S_DQ_HIGH = Column(Numeric(20, 4))
    S_DQ_LOW = Column(Numeric(20, 4))
    S_DQ_CLOSE = Column(Numeric(20, 4))
    S_DQ_VOLUME = Column(Numeric(20, 4))
    S_DQ_AMOUNT = Column(Numeric(20, 4))
    S_DQ_OI = Column(Numeric(20, 4))
    S_DQ_SETTLE = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class TOP5BYACCOUNTSRECEIVABLE(Base):
    __tablename__ = 'TOP5BYACCOUNTSRECEIVABLE'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(40))
    REPORT_PERIOD = Column(String(8), index=True)
    S_INFO_COMPNAME = Column(String(60))
    AMOUNT = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    PERIOD = Column(String(20))
    REASON = Column(String(30))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class TOP5BYLONGTERMBORROWING(Base):
    __tablename__ = 'TOP5BYLONGTERMBORROWING'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(40))
    REPORT_PERIOD = Column(String(8), index=True)
    ANN_DT = Column(String(8))
    S_INFO_COMPNAME = Column(String(100))
    S_INFO_COMPCODE2 = Column(String(40))
    TYPECODE = Column(Numeric(9, 0))
    CONDITIONCODE = Column(Numeric(9, 0))
    START_DT = Column(String(8))
    END_DT = Column(String(8))
    AMOUNT1 = Column(Numeric(20, 4))
    AMOUNT2 = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    RATE = Column(String(80))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class TOP5BYOPERATINGINCOME(Base):
    __tablename__ = 'TOP5BYOPERATINGINCOME'
    __table_args__ = (
        Index('SID_RP_INDEX', 'S_INFO_WINDCODE', 'REPORT_PERIOD'),
    )

    OBJECT_ID = Column(String(100), primary_key=True)
    S_INFO_WINDCODE = Column(String(40), index=True)
    S_INFO_COMPCODE = Column(String(40))
    REPORT_PERIOD = Column(String(8), index=True)
    ANN_DT = Column(String(8))
    S_INFO_COMPNAME = Column(String(100))
    S_INFO_COMPCODE2 = Column(String(40))
    SALESAMOUNT = Column(Numeric(20, 4))
    CRNCY_CODE = Column(String(10))
    PCT = Column(Numeric(20, 4))
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))


class WINDPDUPDATELOG(Base):
    __tablename__ = 'WIND_PDUPDATE_LOG'

    TABLENAME = Column(String(40), primary_key=True)
    CURFILE = Column(String(512))
    CURFILEDESC = Column(String(1000))
    CURFILETIME = Column(DateTime)
    SERVERFILE = Column(String(512))
    SERVERFILETIME = Column(DateTime)
    OPDATE = Column(DateTime)
    OPMODE = Column(String(1))
