"""
因字库orm 定义
"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Numeric, DateTime, \
    UniqueConstraint, Index, ForeignKey
from config import engine_factors

Base = declarative_base()


class FactorsInfo(Base):
    """因子定义对象
    """
    __tablename__ = 'factors_info'

    fid = Column(Integer, primary_key=True)         # 因子 id
    name = Column(String(20), nullable=False,
                  unique=True)                      # 因子名称
    description = Column(String(2000))              # 因子描述
    group = Column(Integer, nullable=False)         # 因子分组
    status = Column(Integer, nullable=False)        # 因子状态

    gen_time = Column(DateTime)                     # 生成时间
    update_time = Column(DateTime)                  # 更新时间


class FactorsValue(Base):
    """因子值对象
    """
    __tablename__ = 'factors_value'

    id = Column(Integer, primary_key=True)
    fid = Column(Integer,
                 ForeignKey("factors_info.fid",
                            ondelete='CASCADE', onupdate='CASCADE'),
                 nullable=False)                            # 因子 id
    sid = Column(String(20), nullable=False)                # 证券代码
    date = Column(DateTime, nullable=False)                 # 因子对应日期
    value = Column(Numeric(24, 10))                         # 因子的值

    gen_time = Column(DateTime)                             # 生成时间
    update_time = Column(DateTime)                          # 更新时间

    __table_args__ = (
        UniqueConstraint('fid', 'sid', 'date', name='_fid_sid_date_uc'),
        Index('fid_index', "fid"),
        Index('sid_index', "sid"),
        Index('date_index2', "date"),
    )


class FactorsReturn(Base):
    """因子收益对象"""
    __tablename__ = 'factors_return'

    id = Column(Integer, primary_key=True)
    date = Column(DateTime, nullable=False)
    # 对应的因子id（外键，对应factors_info.fid）
    fid = Column(Integer,
                 ForeignKey('factors_info.fid',
                            ondelete='CASCADE', onupdate='CASCADE'),
                 nullable=False)

    #ret_long_short = Column(Numeric(14, 10))
    ret_g1 = Column(Numeric(14, 10))
    ret_g2 = Column(Numeric(14, 10))
    ret_g3 = Column(Numeric(14, 10))
    ret_g4 = Column(Numeric(14, 10))
    ret_g5 = Column(Numeric(14, 10))
    #rank_ic = Column(Numeric(12, 10))
    #ic_ir = Column(Numeric(12, 10))

    gen_time = Column(DateTime)                             # 生成时间
    update_time = Column(DateTime)                          # 更新时间

    __table_args__ = (
        UniqueConstraint('fid', 'date', name='_fid_date_uc'),
        Index('date_index', "date"),
    )


def generate_factors_db():
    """
    生成因子库
    """
    Base.metadata.create_all(engine_factors)

