# 项目说明

## 行情数据
行情数据采用 multi-index 的 DataFrame 存储，双 index 分别是 date 和 sid，包括复权价格和复权因子

## 财务数据
行情数据采用 multi-index 的 DataFrame 存储，双 index 分别是 date 和 sid

## 因子数据

### 因子信息
因子信息采用 sqlite 的形式存储，包括字段

    fid             因子id
    name            因子名称
    description     因子描述

### 因子值
因子值采用 multi-index 的 DataFrame 存储，三 index 分别是 date，sid，fid

### 因子收益
因子收益采用 multi-index 的 DataFrame 存储，双 index 分别是 date 和 fid

## 单因子测试
主要提供五档测试法和回归测试法

## 策略回测
基于排序打分的策略回测

# 安装依赖

